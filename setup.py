import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="swatch",
    version="0.4.5",
    author="Tom Williams",
    author_email="tom.williams@cern.ch",
    description="Python library designed for controlling electronics in the phase-2 upgrade of CMS",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.cern.ch/cms-cactus/phase2/pyswatch",
    project_urls={
        "Bug Tracker": "https://gitlab.cern.ch/cms-cactus/phase2/pyswatch/issues",
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: Apache Software License",
        "Operating System :: OS Independent",
    ],
    entry_points={
        'console_scripts': [
            'compare-board-data = swatch.cli.board_data:main_compare',
            'merge-board-data = swatch.cli.board_data:main_merge',
            'convert-board-data = swatch.cli.board_data:main_convert',
            'generate-board-data = swatch.cli.board_data:main_generate',
            'run-algo-test = swatch.cli.demonstrators:main_single_board_test',
            'run-sim = swatch.cli.firmware:main_run_sim',
            'run-link-test = swatch.cli.demonstrators:main_link_test',
            'run-slice-test = swatch.cli.demonstrators:main_slice_test',
            'run-plugin-test-suite = swatch.test_plugin.__main__:cli',
            'board-manager = swatch.cli.board_manager:cli',
        ]
    },
    package_dir={"": "src"},
    package_data={
      # Include example "*.yml" files
      "": ["config/*/*.yml"],
    },
    packages=setuptools.find_packages(where="src"),
    python_requires=">=3.6",
    install_requires=[
        'click',
        'dataclasses',
        'prompt_toolkit',
        'requests',
        'rich'
    ]
)
