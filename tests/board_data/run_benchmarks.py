
import pyperf
import tempfile

from os.path import join
from swatch.utilities.board_data import BoardData, ChannelData, FileFormat, read, write


NUMBERS_OF_CHANNELS = [32, 64, 128]
DEPTHS = [1024]  # , 2048, 4096, 8192]
FILE_FORMATS = [FileFormat.APx, FileFormat.EMPv2]
TEST_FILE_DIRECTORY = tempfile.mkdtemp(prefix='swatch_board_data_benchmark')


def create_board_data(n_channels, depth, *, sideband=True):
    kwargs = {} if sideband else {'metadata': {}}
    return BoardData('', {i: ChannelData(depth, **kwargs) for i in range(n_channels)})


def get_board_data_file_path(n_channels, depth, file_format):
    return join(TEST_FILE_DIRECTORY, 'board_data_{n_channels}ch_{depth}words_{file_format.name}.txt')


runner = pyperf.Runner()
for n_channels in NUMBERS_OF_CHANNELS:
    for depth in DEPTHS:
        runner.bench_func(f'create_board_data: {n_channels}ch, {depth}words', create_board_data, n_channels, depth)

        for file_format in FILE_FORMATS:
            board_data = create_board_data(n_channels, depth, sideband=(file_format is not FileFormat.APx))
            file_path = get_board_data_file_path(n_channels, depth, file_format)
            write(board_data, file_path, file_format)

            runner.bench_func(f'read: {n_channels}ch, {depth}words, {file_format.name}', read, file_path)
            runner.bench_func(f'write: {n_channels}ch, {depth}words, {file_format.name}',
                              write, board_data, file_path + '_test', file_format)
