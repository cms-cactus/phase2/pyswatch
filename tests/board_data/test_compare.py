
import pytest

from swatch.utilities.board_data import BoardData, ChannelData, ChannelDataMatchResult, compare


def create_test_pattern(depth, packet_length=40, packet_gap=10, offset=0):
    x = ChannelData(depth)
    x._data.strobe = True
    x._data[offset].start_of_orbit = True

    for i in range(depth - offset):
        if (i % (packet_length + packet_gap)) == 0:
            x._data[offset + i].start_of_packet = True
        if (i % (packet_length + packet_gap)) == (packet_length - 1):
            x._data[offset + i].end_of_packet = True

        if (i % (packet_length + packet_gap)) < packet_length:
            x._data[offset + i].valid = True
            x._data[offset + i].payload = i

    return x


def test_single_channel_equal():

    a = create_test_pattern(100)
    b = create_test_pattern(100)

    # Data word changes should be ignored if valid low
    b._data[40].payload = 42
    b._data[49].payload = 0xF0F0F0F0F0F0F0F0
    b._data[90].payload = 0x0123456789ABCDEF
    b._data[95].payload = 0x0F0F0F0F0F0F0F0F

    result = ChannelDataMatchResult(a, b, 0, n_frames=100)
    assert bool(result)
    assert result.latency == 0
    assert result.n_frames_compared == 100
    # assert result.n_frames_equal == 100
    # assert result.n_frames_differ == 0
    assert result.n_header_fields_compared == 500
    assert result.n_header_fields_equal == 500
    assert result.n_header_fields_differ == 0
    assert result.n_data_words_compared == 80
    assert result.n_data_words_equal == 80
    assert result.n_data_words_differ == 0
    assert result.n_fields_compared == 580
    assert result.n_fields_equal == 580
    assert result.n_fields_differ == 0
    assert result.n_bits_compared == 5620

    with pytest.raises(RuntimeError, match=r'Could not compare 101 frames of channel data .*'):
        ChannelDataMatchResult(a, b, 0, n_frames=101)


def test_single_channel_equal_with_latency():

    a = create_test_pattern(100)
    b = create_test_pattern(100, offset=10)

    # Data word changes should be ignored if valid low
    b._data[50].payload = 42
    b._data[52].payload = 0xF0F0F0F0F0F0F0F0
    b._data[56].payload = 0x0123456789ABCDEF
    b._data[59].payload = 0x0F0F0F0F0F0F0F0F

    result = ChannelDataMatchResult(a, b, 10, n_frames=90)
    assert bool(result)
    assert result.latency == 10
    assert result.n_frames_compared == 90
    # assert result.n_frames_equal == 90
    # assert result.n_frames_differ == 0
    assert result.n_header_fields_compared == 450
    assert result.n_header_fields_equal == 450
    assert result.n_header_fields_differ == 0
    assert result.n_data_words_compared == 80
    assert result.n_data_words_equal == 80
    assert result.n_data_words_differ == 0
    assert result.n_fields_compared == 530
    assert result.n_fields_equal == 530
    assert result.n_fields_differ == 0
    assert result.n_bits_compared == 5570

    with pytest.raises(RuntimeError, match=r'Could not compare 91 frames of channel data .*'):
        ChannelDataMatchResult(a, b, 10, n_frames=91)


@pytest.mark.parametrize('latency', [0, 10])
def test_single_channel_differ(latency):

    a = create_test_pattern(100)

    # 1 difference: Strobe in frame 5
    b = create_test_pattern(100, offset=latency)
    b[latency + 5].strobe = False

    result = ChannelDataMatchResult(a, b, latency, n_frames=100 - latency)
    assert not bool(result)
    assert result.latency == latency
    assert result.n_frames_compared == 100 - latency
    assert result.n_header_fields_compared == 5 * (100 - latency)
    assert result.n_header_fields_equal == 5 * (100 - latency) - 1
    assert result.n_header_fields_differ == 1
    assert result.n_data_words_compared == 80
    assert result.n_data_words_equal == 80
    assert result.n_data_words_differ == 0
    assert result.n_fields_compared == 5 * (100 - latency) + 80
    assert result.n_fields_equal == 5 * (100 - latency) + 80 - 1
    assert result.n_fields_differ == 1
    assert result.n_bits_compared == 5 * (100 - latency) + 64 * 80

    # 1 difference: Valid in frame 9
    b = create_test_pattern(100, offset=latency)
    b[latency + 9].valid = False

    result = ChannelDataMatchResult(a, b, latency, n_frames=100 - latency)
    assert not bool(result)
    assert result.latency == latency
    assert result.n_frames_compared == 100 - latency
    assert result.n_header_fields_compared == 5 * (100 - latency)
    assert result.n_header_fields_equal == 5 * (100 - latency) - 1
    assert result.n_header_fields_differ == 1
    assert result.n_data_words_compared == 80
    assert result.n_data_words_equal == 80
    assert result.n_data_words_differ == 0
    assert result.n_fields_compared == 5 * (100 - latency) + 80
    assert result.n_fields_equal == 5 * (100 - latency) + 80 - 1
    assert result.n_fields_differ == 1
    assert result.n_bits_compared == 5 * (100 - latency) + 64 * 80

    # 1 difference: 'Start of orbit' in frame 11
    b = create_test_pattern(100, offset=latency)
    b[latency + 11].start_of_orbit = True

    result = ChannelDataMatchResult(a, b, latency, n_frames=100 - latency)
    assert not bool(result)
    assert result.latency == latency
    assert result.n_frames_compared == 100 - latency
    assert result.n_header_fields_compared == 5 * (100 - latency)
    assert result.n_header_fields_equal == 5 * (100 - latency) - 1
    assert result.n_header_fields_differ == 1
    assert result.n_data_words_compared == 80
    assert result.n_data_words_equal == 80
    assert result.n_data_words_differ == 0
    assert result.n_fields_compared == 5 * (100 - latency) + 80
    assert result.n_fields_equal == 5 * (100 - latency) + 80 - 1
    assert result.n_fields_differ == 1
    assert result.n_bits_compared == 5 * (100 - latency) + 64 * 80

    # 1 difference: 'Start of packet' in frame 12
    b = create_test_pattern(100, offset=latency)
    b[latency + 12].start_of_packet = True

    result = ChannelDataMatchResult(a, b, latency, n_frames=100 - latency)
    assert not bool(result)
    assert result.latency == latency
    assert result.n_frames_compared == 100 - latency
    assert result.n_header_fields_compared == 5 * (100 - latency)
    assert result.n_header_fields_equal == 5 * (100 - latency) - 1
    assert result.n_header_fields_differ == 1
    assert result.n_data_words_compared == 80
    assert result.n_data_words_equal == 80
    assert result.n_data_words_differ == 0
    assert result.n_fields_compared == 5 * (100 - latency) + 80
    assert result.n_fields_equal == 5 * (100 - latency) + 80 - 1
    assert result.n_fields_differ == 1
    assert result.n_bits_compared == 5 * (100 - latency) + 64 * 80

    # 1 difference: 'End of packet' in frame 19
    b = create_test_pattern(100, offset=latency)
    b[latency + 19].end_of_packet = True

    result = ChannelDataMatchResult(a, b, latency, n_frames=100 - latency)
    assert not bool(result)
    assert result.latency == latency
    assert result.n_frames_compared == 100 - latency
    assert result.n_header_fields_compared == 5 * (100 - latency)
    assert result.n_header_fields_equal == 5 * (100 - latency) - 1
    assert result.n_header_fields_differ == 1
    assert result.n_data_words_compared == 80
    assert result.n_data_words_equal == 80
    assert result.n_data_words_differ == 0
    assert result.n_fields_compared == 5 * (100 - latency) + 80
    assert result.n_fields_equal == 5 * (100 - latency) + 80 - 1
    assert result.n_fields_differ == 1
    assert result.n_bits_compared == 5 * (100 - latency) + 64 * 80

    # 1 difference: Data in frame 25
    b = create_test_pattern(100, offset=latency)
    b[latency + 25].payload = 42

    result = ChannelDataMatchResult(a, b, latency, n_frames=100 - latency)
    assert not bool(result)
    assert result.latency == latency
    assert result.n_frames_compared == 100 - latency
    assert result.n_header_fields_compared == 5 * (100 - latency)
    assert result.n_header_fields_equal == 5 * (100 - latency)
    assert result.n_header_fields_differ == 0
    assert result.n_data_words_compared == 80
    assert result.n_data_words_equal == 79
    assert result.n_data_words_differ == 1
    assert result.n_fields_compared == 5 * (100 - latency) + 80
    assert result.n_fields_equal == 5 * (100 - latency) + 80 - 1
    assert result.n_fields_differ == 1
    assert result.n_bits_compared == 5 * (100 - latency) + 64 * 80

    # 14 differences: Mix of fields
    b = create_test_pattern(100, offset=latency)
    b[latency + 1].strobe = False
    b[99].strobe = False
    b[latency].valid = False
    b[latency + 39].valid = False
    b[latency + 10].start_of_orbit = True
    b[99].start_of_orbit = True
    b[latency + 27].start_of_packet = True
    b[latency + 50].start_of_packet = False
    b[latency + 12].end_of_packet = True
    b[latency + 39].end_of_packet = False
    b[latency].payload = 56
    b[latency + 1].payload = 42
    b[latency + 60].payload = 999
    b[latency + 75].payload = 0xFFFFFFFFFFFFFFFF

    result = ChannelDataMatchResult(a, b, latency, n_frames=100 - latency)
    assert not bool(result)
    assert result.latency == latency
    assert result.n_frames_compared == 100 - latency
    assert result.n_header_fields_compared == 5 * (100 - latency)
    assert result.n_header_fields_equal == 5 * (100 - latency) - 10
    assert result.n_header_fields_differ == 10
    assert result.n_data_words_compared == 80
    assert result.n_data_words_equal == 76
    assert result.n_data_words_differ == 4
    assert result.n_fields_compared == 5 * (100 - latency) + 80
    assert result.n_fields_equal == 5 * (100 - latency) + 80 - 14
    assert result.n_fields_differ == 14
    assert result.n_bits_compared == 5 * (100 - latency) + 64 * 80


def test_board_data_equal_exact_latency():

    a = BoardData('my_data', {
                  1: create_test_pattern(200),
                  3: create_test_pattern(200, offset=10),
                  5: create_test_pattern(200, offset=20),
                  })
    b = BoardData('my_data', {
                  1: create_test_pattern(200, offset=25),
                  3: create_test_pattern(200, offset=35),
                  5: create_test_pattern(200, offset=45),
                  })

    result = compare(a, b, latencies=[25])
    assert bool(result)
    assert sorted(result.channels) == [1, 3, 5]
    assert sorted(result.good_channels) == [1, 3, 5]
    assert sorted(result.bad_channels) == []
    assert result.latencies == {25: [1, 3, 5]}

    n_valid_data_words = {1: 145, 3: 135, 5: 125}
    for i in [1, 3, 5]:
        assert bool(result.at(i))
        assert result.at(i).latency == 25
        assert result.at(i).n_frames_compared == 175
        assert result.at(i).n_header_fields_compared == 5 * 175
        assert result.at(i).n_header_fields_equal == 5 * 175
        assert result.at(i).n_header_fields_differ == 0
        assert result.at(i).n_data_words_compared == n_valid_data_words[i]
        assert result.at(i).n_data_words_equal == n_valid_data_words[i]
        assert result.at(i).n_data_words_differ == 0
        assert result.at(i).n_fields_compared == 5 * 175 + n_valid_data_words[i]
        assert result.at(i).n_fields_equal == 5 * 175 + n_valid_data_words[i]
        assert result.at(i).n_fields_differ == 0
        assert result.at(i).n_bits_compared == 5 * 175 + 64 * n_valid_data_words[i]


def test_board_data_differ_exact_latency():

    a = BoardData('my_data', {
                  1: create_test_pattern(200),
                  3: create_test_pattern(200, offset=10),
                  5: create_test_pattern(200, offset=20),
                  })
    b = BoardData('my_data', {
                  1: create_test_pattern(200, offset=25),
                  3: create_test_pattern(200, offset=35),
                  5: create_test_pattern(200, offset=45),
                  })

    # Inject errors on channels 1 & 5
    # (No errors on channel 3 to confirm that 'not bool(BoardDataMatchResult)' doesn't require errors on all channels)
    b.at(1)._data[25].start_of_packet = False
    b.at(1)._data[26].valid = False
    b.at(1)._data[30].payload = 0xFFFFFFFFFFFFFFFF
    b.at(5)._data[50].valid = False
    b.at(5)._data[51].strobe = False
    b.at(5)._data[130].payload = 0xF0F0F0F0F0F0F0F0
    b.at(5)._data[184].payload = 0x0F0F0F0F0F0F0F0F

    result = compare(a, b, latencies=[25])
    assert not bool(result)
    assert sorted(result.channels) == [1, 3, 5]
    assert sorted(result.good_channels) == [3]
    assert sorted(result.bad_channels) == [1, 5]
    assert result.latencies == {25: [1, 3, 5]}

    for i, header_errors, data_errors, n_valid_data_words in [(1, 2, 1, 145), (3, 0, 0, 135), (5, 2, 2, 125)]:
        assert bool(result.at(i)) == ((header_errors + data_errors) == 0)
        assert result.at(i).latency == 25
        assert result.at(i).n_frames_compared == 175
        assert result.at(i).n_header_fields_compared == 5 * 175
        assert result.at(i).n_header_fields_equal == 5 * 175 - header_errors
        assert result.at(i).n_header_fields_differ == header_errors
        assert result.at(i).n_data_words_compared == n_valid_data_words
        assert result.at(i).n_data_words_equal == n_valid_data_words - data_errors
        assert result.at(i).n_data_words_differ == data_errors
        assert result.at(i).n_fields_compared == 5 * 175 + n_valid_data_words
        assert result.at(i).n_fields_equal == 5 * 175 + n_valid_data_words - header_errors - data_errors
        assert result.at(i).n_fields_differ == header_errors + data_errors
        assert result.at(i).n_bits_compared == 5 * 175 + 64 * n_valid_data_words


def test_board_data_equal_latency_range():

    a = BoardData('my_data', {
                  1: create_test_pattern(200, offset=10),
                  3: create_test_pattern(200),
                  5: create_test_pattern(200, offset=20),
                  })
    b = BoardData('my_data', {
                  1: create_test_pattern(200, offset=22),
                  3: create_test_pattern(200, offset=25),
                  5: create_test_pattern(200, offset=55),
                  })

    result = compare(a, b, latencies=[12, 25, 35, 42])
    assert bool(result)
    assert sorted(result.channels) == [1, 3, 5]
    assert sorted(result.good_channels) == [1, 3, 5]
    assert sorted(result.bad_channels) == []
    assert result.latencies == {12: [1], 25: [3], 35: [5]}

    assert bool(result.at(1))
    assert result.at(1).latency == 12
    assert result.at(1).n_frames_compared == 188
    assert result.at(1).n_header_fields_compared == 5 * 188
    assert result.at(1).n_header_fields_equal == 5 * 188
    assert result.at(1).n_header_fields_differ == 0
    assert result.at(1).n_data_words_compared == 148
    assert result.at(1).n_data_words_equal == 148
    assert result.at(1).n_data_words_differ == 0
    assert result.at(1).n_fields_compared == 5 * 188 + 148
    assert result.at(1).n_fields_equal == 5 * 188 + 148
    assert result.at(1).n_fields_differ == 0
    assert result.at(1).n_bits_compared == 5 * 188 + 64 * 148

    assert bool(result.at(3))
    assert result.at(3).latency == 25
    assert result.at(3).n_frames_compared == 175
    assert result.at(3).n_header_fields_compared == 5 * 175
    assert result.at(3).n_header_fields_equal == 5 * 175
    assert result.at(3).n_header_fields_differ == 0
    assert result.at(3).n_data_words_compared == 145
    assert result.at(3).n_data_words_equal == 145
    assert result.at(3).n_data_words_differ == 0
    assert result.at(3).n_fields_compared == 5 * 175 + 145
    assert result.at(3).n_fields_equal == 5 * 175 + 145
    assert result.at(3).n_fields_differ == 0
    assert result.at(3).n_bits_compared == 5 * 175 + 64 * 145

    assert bool(result.at(5))
    assert result.at(5).latency == 35
    assert result.at(5).n_frames_compared == 165
    assert result.at(5).n_header_fields_compared == 5 * 165
    assert result.at(5).n_header_fields_equal == 5 * 165
    assert result.at(5).n_header_fields_differ == 0
    assert result.at(5).n_data_words_compared == 120
    assert result.at(5).n_data_words_equal == 120
    assert result.at(5).n_data_words_differ == 0
    assert result.at(5).n_fields_compared == 5 * 165 + 120
    assert result.at(5).n_fields_equal == 5 * 165 + 120
    assert result.at(5).n_fields_differ == 0
    assert result.at(5).n_bits_compared == 5 * 165 + 64 * 120


def test_board_data_differ_latency_range():

    a = BoardData('my_data', {
                  1: create_test_pattern(200, offset=10),
                  3: create_test_pattern(200),
                  5: create_test_pattern(200, offset=20),
                  })
    b = BoardData('my_data', {
                  1: create_test_pattern(200, offset=22),
                  3: create_test_pattern(200, offset=25),
                  5: create_test_pattern(200, offset=55),
                  })

    # Inject range of errors on all channels
    b.at(1)._data[22].start_of_packet = False
    b.at(1)._data[26].valid = False
    b.at(1)._data[30].payload = 0xFFFFFFFFFFFFFFFF
    b.at(3)._data[34].end_of_packet = True
    b.at(3)._data[174].valid = True
    b.at(3)._data[190].payload = 0
    b.at(3)._data[199].valid = False
    b.at(3)._data[199].end_of_packet = True
    b.at(5)._data[50].valid = True
    b.at(5)._data[51].strobe = False
    b.at(5)._data[130].payload = 0xF0F0F0F0F0F0F0F0
    b.at(5)._data[184].payload = 0x0F0F0F0F0F0F0F0F

    result = compare(a, b, latencies=[12, 25, 35, 42])
    assert not bool(result)
    assert sorted(result.channels) == [1, 3, 5]
    assert sorted(result.good_channels) == []
    assert sorted(result.bad_channels) == [1, 3, 5]
    assert result.latencies == {12: [1], 25: [3], 35: [5]}

    assert not bool(result.at(1))
    assert result.at(1).latency == 12
    assert result.at(1).n_frames_compared == 188
    assert result.at(1).n_header_fields_compared == 5 * 188
    assert result.at(1).n_header_fields_equal == 5 * 188 - 2
    assert result.at(1).n_header_fields_differ == 2
    assert result.at(1).n_data_words_compared == 148
    assert result.at(1).n_data_words_equal == 147
    assert result.at(1).n_data_words_differ == 1
    assert result.at(1).n_fields_compared == 5 * 188 + 148
    assert result.at(1).n_fields_equal == 5 * 188 + 148 - 3
    assert result.at(1).n_fields_differ == 3
    assert result.at(1).n_bits_compared == 5 * 188 + 64 * 148

    assert not bool(result.at(3))
    assert result.at(3).latency == 25
    assert result.at(3).n_frames_compared == 175
    assert result.at(3).n_header_fields_compared == 5 * 175
    assert result.at(3).n_header_fields_equal == 5 * 175 - 4
    assert result.at(3).n_header_fields_differ == 4
    assert result.at(3).n_data_words_compared == 145
    assert result.at(3).n_data_words_equal == 144
    assert result.at(3).n_data_words_differ == 1
    assert result.at(3).n_fields_compared == 5 * 175 + 145
    assert result.at(3).n_fields_equal == 5 * 175 + 145 - 5
    assert result.at(3).n_fields_differ == 5
    assert result.at(3).n_bits_compared == 5 * 175 + 64 * 145

    assert not bool(result.at(5))
    assert result.at(5).latency == 35
    assert result.at(5).n_frames_compared == 165
    assert result.at(5).n_header_fields_compared == 5 * 165
    assert result.at(5).n_header_fields_equal == 5 * 165 - 2
    assert result.at(5).n_header_fields_differ == 2
    assert result.at(5).n_data_words_compared == 120
    assert result.at(5).n_data_words_equal == 118
    assert result.at(5).n_data_words_differ == 2
    assert result.at(5).n_fields_compared == 5 * 165 + 120
    assert result.at(5).n_fields_equal == 5 * 165 + 120 - 4
    assert result.at(5).n_fields_differ == 4
    assert result.at(5).n_bits_compared == 5 * 165 + 64 * 120


def test_board_data_equal_any_latency():

    a = BoardData('my_data', {
                  1: create_test_pattern(200, offset=10),
                  3: create_test_pattern(200),
                  5: create_test_pattern(200, offset=20),
                  })
    b = BoardData('my_data', {
                  1: create_test_pattern(200, offset=22),
                  3: create_test_pattern(200, offset=25),
                  5: create_test_pattern(200, offset=55),
                  })

    result = compare(a, b)
    assert bool(result)
    assert sorted(result.channels) == [1, 3, 5]
    assert sorted(result.good_channels) == [1, 3, 5]
    assert sorted(result.bad_channels) == []
    assert result.latencies == {12: [1], 25: [3], 35: [5]}

    assert bool(result.at(1))
    assert result.at(1).latency == 12
    assert result.at(1).n_frames_compared == 188
    assert result.at(1).n_header_fields_compared == 5 * 188
    assert result.at(1).n_header_fields_equal == 5 * 188
    assert result.at(1).n_header_fields_differ == 0
    assert result.at(1).n_data_words_compared == 148
    assert result.at(1).n_data_words_equal == 148
    assert result.at(1).n_data_words_differ == 0
    assert result.at(1).n_fields_compared == 5 * 188 + 148
    assert result.at(1).n_fields_equal == 5 * 188 + 148
    assert result.at(1).n_fields_differ == 0
    assert result.at(1).n_bits_compared == 5 * 188 + 64 * 148

    assert bool(result.at(3))
    assert result.at(3).latency == 25
    assert result.at(3).n_frames_compared == 175
    assert result.at(3).n_header_fields_compared == 5 * 175
    assert result.at(3).n_header_fields_equal == 5 * 175
    assert result.at(3).n_header_fields_differ == 0
    assert result.at(3).n_data_words_compared == 145
    assert result.at(3).n_data_words_equal == 145
    assert result.at(3).n_data_words_differ == 0
    assert result.at(3).n_fields_compared == 5 * 175 + 145
    assert result.at(3).n_fields_equal == 5 * 175 + 145
    assert result.at(3).n_fields_differ == 0
    assert result.at(3).n_bits_compared == 5 * 175 + 64 * 145

    assert bool(result.at(5))
    assert result.at(5).latency == 35
    assert result.at(5).n_frames_compared == 165
    assert result.at(5).n_header_fields_compared == 5 * 165
    assert result.at(5).n_header_fields_equal == 5 * 165
    assert result.at(5).n_header_fields_differ == 0
    assert result.at(5).n_data_words_compared == 120
    assert result.at(5).n_data_words_equal == 120
    assert result.at(5).n_data_words_differ == 0
    assert result.at(5).n_fields_compared == 5 * 165 + 120
    assert result.at(5).n_fields_equal == 5 * 165 + 120
    assert result.at(5).n_fields_differ == 0
    assert result.at(5).n_bits_compared == 5 * 165 + 64 * 120


def test_board_data_differ_any_latency():

    a = BoardData('my_data', {
                  1: create_test_pattern(200, offset=10),
                  3: create_test_pattern(200),
                  5: create_test_pattern(200, offset=20),
                  })
    b = BoardData('my_data', {
                  1: create_test_pattern(200, offset=22),
                  3: create_test_pattern(200, offset=25),
                  5: create_test_pattern(200, offset=55),
                  })

    # Inject range of errors on all channels
    b.at(1)._data[22].start_of_orbit = False
    b.at(1)._data[26].valid = False
    b.at(1)._data[30].payload = 0xFFFFFFFFFFFFFFFF
    b.at(3)._data[34].end_of_packet = True
    b.at(3)._data[174].valid = True
    b.at(3)._data[190].payload = 0
    b.at(3)._data[199].end_of_packet = True
    b.at(5)._data[35].valid = True
    b.at(5)._data[37].strobe = False
    b.at(5)._data[55].payload = 0xF0F0F0F0F0F0F0F0
    b.at(5)._data[199].payload = 0x0F0F0F0F0F0F0F0F

    result = compare(a, b)
    assert not bool(result)
    assert sorted(result.channels) == [1, 3, 5]
    assert sorted(result.good_channels) == []
    assert sorted(result.bad_channels) == [1, 3, 5]
    assert result.latencies == {12: [1], 25: [3], 35: [5]}

    assert not bool(result.at(1))
    assert result.at(1).latency == 12
    assert result.at(1).n_frames_compared == 188
    assert result.at(1).n_header_fields_compared == 5 * 188
    assert result.at(1).n_header_fields_equal == 5 * 188 - 2
    assert result.at(1).n_header_fields_differ == 2
    assert result.at(1).n_data_words_compared == 148
    assert result.at(1).n_data_words_equal == 147
    assert result.at(1).n_data_words_differ == 1
    assert result.at(1).n_fields_compared == 5 * 188 + 148
    assert result.at(1).n_fields_equal == 5 * 188 + 148 - 3
    assert result.at(1).n_fields_differ == 3
    assert result.at(1).n_bits_compared == 5 * 188 + 64 * 148

    assert not bool(result.at(3))
    assert result.at(3).latency == 25
    assert result.at(3).n_frames_compared == 175
    assert result.at(3).n_header_fields_compared == 5 * 175
    assert result.at(3).n_header_fields_equal == 5 * 175 - 3
    assert result.at(3).n_header_fields_differ == 3
    assert result.at(3).n_data_words_compared == 145
    assert result.at(3).n_data_words_equal == 144
    assert result.at(3).n_data_words_differ == 1
    assert result.at(3).n_fields_compared == 5 * 175 + 145
    assert result.at(3).n_fields_equal == 5 * 175 + 145 - 4
    assert result.at(3).n_fields_differ == 4
    assert result.at(3).n_bits_compared == 5 * 175 + 64 * 145

    assert not bool(result.at(5))
    assert result.at(5).latency == 35
    assert result.at(5).n_frames_compared == 165
    assert result.at(5).n_header_fields_compared == 5 * 165
    assert result.at(5).n_header_fields_equal == 5 * 165 - 2
    assert result.at(5).n_header_fields_differ == 2
    assert result.at(5).n_data_words_compared == 120
    assert result.at(5).n_data_words_equal == 119
    assert result.at(5).n_data_words_differ == 1
    assert result.at(5).n_fields_compared == 5 * 165 + 120
    assert result.at(5).n_fields_equal == 5 * 165 + 120 - 3
    assert result.at(5).n_fields_differ == 3
    assert result.at(5).n_bits_compared == 5 * 165 + 64 * 120
