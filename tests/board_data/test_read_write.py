
import filecmp
import gzip
import os
import random
import shutil
import pytest

from swatch.utilities.board_data import BoardData, Field, FileFormat, ChannelData, read, write


_ALL_FIELDS = [Field.StartOfOrbit, Field.StartOfPacket, Field.EndOfPacket, Field.Valid, Field.Strobe]
_REFERENCE_DATA_FILE_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'references')


@pytest.mark.parametrize('file_suffix,fields',
                         [('APx', _ALL_FIELDS),
                          ('APxNoSideband', []),
                          ('EMPv1', [Field.Valid, Field.Strobe]),
                          ('EMPv2', _ALL_FIELDS)],
                         ids=['APx', 'APxNoSideband', 'EMPv1', 'EMPv2'])
def test_read_simple_file(file_suffix, fields):
    '''Reads reference board data file of specified format, and checks that it was parsed correctly'''

    path = os.path.join(_REFERENCE_DATA_FILE_DIR, f'read_simple_{file_suffix}.txt')
    board_data = read(path)

    assert board_data.channels == [0, 1, 2, 3, 4]
    for field in _ALL_FIELDS:
        assert board_data.has_field(field) == (field in fields)
    assert board_data.has_fields(fields)

    expected = ChannelData(10)
    expected._data.strobe = True
    expected._data.valid = [False, True, True, False, True, True, False, True, True, False]
    expected._data.payload = [0, 1, 2, 0, 4, 5, 0, 7, 8, 0]
    expected[1].start_of_orbit = True
    expected[1:10:3].start_of_packet = True
    expected[2:10:3].end_of_packet = True

    for channel in board_data.channels:
        print(f'Checking channel {channel}')
        chan_data = board_data.at(channel)
        assert len(chan_data) == 10

        for field in _ALL_FIELDS:
            assert chan_data.has_field(field) == (field in fields)
        assert chan_data.has_fields(fields)

        for i in range(len(chan_data)):
            assert chan_data[i].payload == expected[i].payload, f'Channel {channel}, frame {i}: Data incorrect'
            if Field.Valid in fields:
                assert chan_data[i].valid == expected[i].valid, f'Channel {channel}, frame {i}: Valid incorrect'
            if Field.Strobe in fields:
                assert chan_data[i].strobe == expected[i].strobe, f'Channel {channel}, frame {i}: Strobe incorrect'
            if Field.StartOfOrbit in fields:
                assert chan_data[i].start_of_orbit == expected[i].start_of_orbit, (f'Channel {channel}, frame {i}: '
                                                                                   'Start-of-orbit flag incorrect')
            if Field.StartOfPacket in fields:
                assert chan_data[i].start_of_packet == expected[i].start_of_packet, (f'Channel {channel}, frame {i}: '
                                                                                     'Start-of-packet flag incorrect')
            if Field.EndOfPacket in fields:
                assert chan_data[i].end_of_packet == expected[i].end_of_packet, (f'Channel {channel}, frame {i}: '
                                                                                 'End-of-packet flag incorrect')


@pytest.mark.parametrize('format', [FileFormat.EMPv1, FileFormat.EMPv2])
def test_read_strobed_file(format):
    '''Reads reference board data file of specified format, and checks that it was parsed correctly'''

    path = os.path.join(_REFERENCE_DATA_FILE_DIR, f'read_strobed_{format.name}.txt')
    board_data = read(path)

    assert board_data.channels == [0, 1, 2, 3, 4]

    expected = ChannelData(10)
    expected._data.strobe = [False, True, True, False, True, True, False, True, True, False]
    expected._data.valid = [False, True, True, False, True, True, False, True, True, False]
    expected._data.payload = [0, 1, 2, 0, 4, 5, 0, 7, 8, 0]
    expected[1].start_of_orbit = True
    expected[1:10:3].start_of_packet = True
    expected[2:10:3].end_of_packet = True

    for channel in board_data.channels:
        print(f'Checking channel {channel}')
        chan_data = board_data.at(channel)
        assert len(chan_data) == 10

        for i in range(len(chan_data)):
            assert chan_data[i].payload == expected[i].payload, f'Channel {channel}, frame {i}: Data incorrect'
            assert chan_data[i].valid == expected[i].valid, f'Channel {channel}, frame {i}: Valid incorrect'
            assert chan_data[i].strobe == expected[i].strobe, f'Channel {channel}, frame {i}: Strobe incorrect'
            if format != FileFormat.EMPv1:
                assert chan_data[i].start_of_orbit == expected[i].start_of_orbit, (f'Channel {channel}, frame {i}: '
                                                                                   'Start-of-orbit flag incorrect')
                assert chan_data[i].start_of_packet == expected[i].start_of_packet, (f'Channel {channel}, frame {i}: '
                                                                                     'Start-of-packet flag incorrect')
                assert chan_data[i].end_of_packet == expected[i].end_of_packet, (f'Channel {channel}, frame {i}: '
                                                                                 'End-of-packet flag incorrect')


@pytest.mark.parametrize('format', [FileFormat.EMPv1, FileFormat.EMPv2])
def test_read_compressed_file(format, tmp_path):
    '''Reads both uncompressed and compressed reference files, and checks that resulting objects identical'''

    # 1. Compress reference file
    path_plain = os.path.join(_REFERENCE_DATA_FILE_DIR, f'read_simple_{format.name}.txt')
    path_gzipped = os.path.join(tmp_path, f'read_simple_{format.name}.txt.gz')
    print('Creating gzipped file ' + path_gzipped)
    with open(path_plain, 'rb') as f_in:
        with gzip.open(path_gzipped, 'wb') as f_out:
            shutil.copyfileobj(f_in, f_out)

    # 2. Parse both files
    data1 = read(path_plain)
    data2 = read(path_gzipped)

    # 3. Check that both board data objects are identical
    assert data1.name == data2.name
    assert data1.channels == data2.channels

    for channel in data1.channels:
        print(f'Checking channel {channel}')
        chan_data1 = data1.at(channel)
        chan_data2 = data2.at(channel)
        assert len(chan_data1) == len(chan_data2)

        for i, (frame1, frame2) in enumerate(zip(chan_data1, chan_data2)):
            assert frame1.payload == frame2.payload, f'Channel {channel}, frame {i}: Data incorrect'
            assert frame1.valid == frame2.valid, f'Channel {channel}, frame {i}: Valid incorrect'
            assert frame1.strobe == frame2.strobe, f'Channel {channel}, frame {i}: Strobe incorrect'
            if format != FileFormat.EMPv1:
                assert frame1.start_of_orbit == frame2.start_of_orbit, (f'Channel {channel}, frame {i}: '
                                                                        'Start-of-orbit flag incorrect')
                assert frame1.start_of_packet == frame2.start_of_packet, (f'Channel {channel}, frame {i}: '
                                                                          'Start-of-packet flag incorrect')
                assert frame1.end_of_packet == frame2.end_of_packet, (f'Channel {channel}, frame {i}: '
                                                                      'End-of-packet flag incorrect')


@pytest.mark.parametrize('file_format,file_suffix,fields',
                         [(FileFormat.APx, 'APx', _ALL_FIELDS),
                          (FileFormat.APx, 'APxNoSideband', []),
                          (FileFormat.EMPv1, 'EMPv1', [Field.Valid, Field.Strobe]),
                          (FileFormat.EMPv2, 'EMPv2', _ALL_FIELDS)],
                         ids=['APx', 'APxNoSideband', 'EMPv1', 'EMPv2'])
def test_write_simple_file(file_format, file_suffix, fields, tmp_path):

    # 1. Create board data object containing simple pattern
    board_name = 'myBoard'
    num_chans, num_frames = 5, 10
    board_data = BoardData(board_name, {i: ChannelData(num_frames, metadata=fields) for i in range(num_chans)})

    for i in board_data.channels:
        for j, frame in enumerate(board_data.at(i)):
            frame.payload = (i << 32) | (i + j)
            frame.strobe = True
            frame.start_of_orbit = (i + j) == 1
            frame.start_of_packet = ((i + j) % 3) == 1
            frame.end_of_packet = ((i + j) % 3) == 2
            frame.valid = ((i + j) % 3) > 0

    # 2. Call 'write' function, and compare contents of resulting file with reference
    reference_file_path = os.path.join(_REFERENCE_DATA_FILE_DIR, f'write_simple_{file_suffix}.txt')
    target_file_path = os.path.join(tmp_path, f'write_simple_{file_suffix}.txt')
    print('Writing to ' + target_file_path)
    write(board_data, target_file_path, file_format)

    print('Comparing with reference file, ' + reference_file_path)
    assert filecmp.cmp(reference_file_path, target_file_path, shallow=False)


def _expected_missing_field_error_message(file_format):
    if file_format == FileFormat.EMPv1:
        return 'Board data object is missing the following mandatory fields for the EMPv1 file format: Valid, Strobe'
    elif file_format == FileFormat.EMPv2:
        return ('Board data object is missing the following mandatory fields for the EMPv2 file format: '
                'StartOfOrbit, StartOfPacket, EndOfPacket, Valid, Strobe')
    assert False


@pytest.mark.parametrize('file_format', [FileFormat.EMPv1, FileFormat.EMPv2])
def test_write_error_missing_fields(file_format, tmp_path):

    # 1. Create board data object containing simple pattern
    board_name = 'myBoard'
    num_chans, num_frames = 5, 10
    board_data = BoardData(board_name, {i: ChannelData(num_frames, metadata=[]) for i in range(num_chans)})

    for i in board_data.channels:
        for j, frame in enumerate(board_data.at(i)):
            frame.payload = (i << 32) | (i + j)
            frame.strobe = True
            frame.start_of_orbit = (i + j) == 1
            frame.start_of_packet = ((i + j) % 3) == 1
            frame.end_of_packet = ((i + j) % 3) == 2
            frame.valid = ((i + j) % 3) > 0

    # 2. Call 'write' function, and compare contents of resulting file with reference
    target_file_path = os.path.join(tmp_path, 'write_error_missing_fields.txt')
    with pytest.raises(RuntimeError) as e_info:
        write(board_data, target_file_path, file_format)

    assert str(e_info.value) == _expected_missing_field_error_message(file_format)


@pytest.mark.parametrize('format', [FileFormat.EMPv1, FileFormat.EMPv2])
def test_write_strobed_file(format, tmp_path):

    # 1. Create board data object containing strobed pattern
    board_name = 'myBoard2'
    num_chans, num_frames = 5, 10
    board_data = BoardData(board_name, {i: ChannelData(num_frames) for i in range(num_chans)})

    for i in board_data.channels:
        for j, frame in enumerate(board_data.at(i)):
            frame.payload = (i << 32) | (i + j)
            frame.strobe = ((i + j) % 3) > 0
            frame.start_of_orbit = (i + j) == 1
            frame.start_of_packet = ((i + j) % 3) == 1
            frame.end_of_packet = ((i + j) % 3) == 2
            frame.valid = ((i + j) % 3) > 0

    # 2. Call 'write' function, and compare contents of resulting file with reference
    reference_file_path = os.path.join(_REFERENCE_DATA_FILE_DIR, f'write_strobed_{format.name}.txt')
    target_file_path = os.path.join(tmp_path, f'write_strobed_{format.name}.txt')
    print('Writing to ' + target_file_path)
    write(board_data, target_file_path, format)

    print('Comparing with reference file, ' + reference_file_path)
    assert filecmp.cmp(reference_file_path, target_file_path, shallow=False)


@pytest.mark.parametrize('format', [FileFormat.APx, FileFormat.EMPv1, FileFormat.EMPv2])
@pytest.mark.parametrize('num_frames', [1024, 2345])
def test_write_and_readback_large_file(format, num_frames, tmp_path):

    # 1. Create a 1024-frame BoardData object with many channels and random data
    data1 = BoardData('boardX', {i: ChannelData(num_frames) for i in range(128)})
    for i in data1.channels:
        for frame in data1.at(i):
            frame.strobe = True
            frame.payload = random.randint(0, 0xFFFFFFFFFFFFFFFF)
            frame.valid = bool(random.getrandbits(1))
            frame.start_of_orbit = bool(random.getrandbits(1))
            frame.start_of_packet = bool(random.getrandbits(1))
            frame.end_of_packet = bool(random.getrandbits(1))

    # 2. Call 'write' function then 'read', and compare two board data objects
    file_path = os.path.join(tmp_path, f'write_large_{format.name}.txt')
    print('Writing to ' + file_path)
    write(data1, file_path, format)
    print('Reading ' + file_path)
    data2 = read(file_path)

    if format != FileFormat.APx:
        assert data1.name == data2.name
    assert data1.channels == data2.channels

    for channel in data1.channels:
        print(f'Checking channel {channel}')
        chan_data1 = data1.at(channel)
        chan_data2 = data2.at(channel)
        assert len(chan_data1) == len(chan_data2)

        for i, (frame1, frame2) in enumerate(zip(chan_data1, chan_data2)):
            assert frame1.payload == frame2.payload, f'Channel {channel}, frame {i}: Data incorrect'
            assert frame1.valid == frame2.valid, f'Channel {channel}, frame {i}: Valid incorrect'
            assert frame1.strobe == frame2.strobe, f'Channel {channel}, frame {i}: Strobe incorrect'
            if format != FileFormat.EMPv1:
                assert frame1.start_of_orbit == frame2.start_of_orbit, (f'Channel {channel}, frame {i}: '
                                                                        'Start-of-orbit flag incorrect')
                assert frame1.start_of_packet == frame2.start_of_packet, (f'Channel {channel}, frame {i}: '
                                                                          'Start-of-packet flag incorrect')
                assert frame1.end_of_packet == frame2.end_of_packet, (f'Channel {channel}, frame {i}: '
                                                                      'End-of-packet flag incorrect')
