import copy
import os
import pytest
import re

from swatch.config import SettingsPool, LinkSelector, PortSelector
from swatch.config import parse_system_file
from swatch.exceptions import InvalidConfigFormatError, ConfigValidationError
from swatch.io_settings import CSPSettings, IdleMethod, PRBSMode
from swatch.stubs import CrateStub, DAQCategories, DAQBoardStub, BoardStub, ExternalBoardStub
from swatch.stubs import FrontendStub, LinkStub
from testutils import create_temp_test_data, delete_temp_test_data

# The following test script makes use of testdata7 and testdata8 fixtures.
# testdata8 is a simpler fixture and needs no dependencies. The temporary
# data file paths derived from testdata8 makes use of the native pytest
# tmp_path_global fixture. The deletion of the temporary data file path is
# handled by pytest internally. testdata7 is a more complete fixture and
# depends on other included config files. The temporary data file paths
# created from testdata7 need to be in a local directory and need manual
# creation and deletion managed by the create/delete_temp_test_data functions.

file_path = os.path.dirname(os.path.realpath(__file__))
dir_path = os.path.dirname(file_path)


###################################################
# Test block to test the values read on __init__ ##
#                                                ##
def test_parse_system_file_init_values_td7(check):
    systemtuple = parse_system_file(dir_path+'/config/references/td7_parse_system_file_init.yml')

    # The first element in the tuple is a tuple of system stub
    sys_crates = systemtuple[0].crates
    expected_crates = [CrateStub('C1', '11', 'R1', 111, 'Dummy crate definition 1', 101),
                       CrateStub('C2', '22', 'R2', 222, '', 202)]
    check.equal(sys_crates, expected_crates)

    sys_boards = systemtuple[0].boards
    expected_boards = [BoardStub('B1', 'herd1', 'C1', 1, 1001),
                       BoardStub('B2', 'herd2', 'C2', 2, 3000),
                       BoardStub('B3', 'herd-3.cern.ch', 'C2', 3, 3000)]
    check.equal(sys_boards, expected_boards)

    sys_ext_boards = systemtuple[0].external_boards
    expected_ext_boards = [ExternalBoardStub('EB1', 'EC1', 11, {},
                                             {'outproc1_eb1_id': [1, 2, 3, 4, 5, 6, 7, 8, 9, 15],
                                              'outproc2_eb1_id': [26, 39, 38, 37, 36, 35, 34, 33, 32, 31]}),
                           ExternalBoardStub('EB2', 'EC2', 22,
                                             {'inproc1_eb2_id': [101, 102, 103, 104, 105, 106, 107, 108, 109, 115],
                                              'inproc2_eb2_id': [126, 139, 138, 137, 136, 135, 134, 133, 132, 131]},
                                             {}),
                           ExternalBoardStub('EB3', 'EC2', 33,
                                             {'inproc1_eb3_id': [201, 202, 203, 204, 205, 206, 207, 208, 209, 215]},
                                             {'outproc1_eb3_id': [226, 239, 238, 237, 236, 235, 234, 233, 232, 231]})]
    check.equal(sys_ext_boards, expected_ext_boards)

    sys_daq_boards = systemtuple[0].daq_boards
    expected_daq_boards = [DAQBoardStub('DAQB1', DAQCategories.RO,
                                        {'daqproc1': [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
                                         'daqproc2': [21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31]},
                                        {'daqproc11': [101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111],
                                         'daqproc22': [121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131]}),
                           DAQBoardStub('DAQB2', DAQCategories.RO,
                                        {'daqproc1': [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
                                         'daqproc2': [21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31]
                                         }, {})]
    check.equal(sys_daq_boards, expected_daq_boards)

    sys_fe_mods = systemtuple[0].frontends
    expected_fe_mods = [FrontendStub('fe_module_X', 'module_serial_NNN', [101, 11, 12])]
    check.equal(sys_fe_mods, expected_fe_mods)

    sys_links = systemtuple[0].links
    expected_links = [LinkStub('link1_01', 'EB1', 'outproc1_eb1_id', 1, 'B1', 'proc1', 11),
                      LinkStub('link1_02', 'EB1', 'outproc1_eb1_id', 2, 'B1', 'proc1', 12),
                      LinkStub('link1_03', 'EB1', 'outproc1_eb1_id', 3, 'B1', 'proc1', 13),
                      LinkStub('link1_04', 'EB1', 'outproc1_eb1_id', 4, 'B1', 'proc1', 14),
                      LinkStub('link1_05', 'EB1', 'outproc1_eb1_id', 5, 'B1', 'proc1', 15),
                      LinkStub('link2_11', 'B1', 'proc1', 21, 'B2', 'proc2', 31),
                      LinkStub('link2_12', 'B1', 'proc1', 22, 'B2', 'proc2', 32),
                      LinkStub('link2_13', 'B1', 'proc1', 23, 'B2', 'proc2', 33),
                      LinkStub('link2_14', 'B1', 'proc1', 24, 'B2', 'proc2', 34),
                      LinkStub('link2_15', 'B1', 'proc1', 25, 'B2', 'proc2', 35),
                      LinkStub('link2_16', 'B1', 'proc1', 26, 'B2', 'proc2', 37),
                      LinkStub('link2_17', 'B1', 'proc1', 27, 'B2', 'proc2', 39),
                      LinkStub('link3_21', 'B2', 'proc2', 41, 'EB2', 'inproc2_eb2_id', 126),
                      LinkStub('link3_22', 'B2', 'proc2', 42, 'EB2', 'inproc2_eb2_id', 134),
                      LinkStub('link3_23', 'B2', 'proc2', 43, 'EB2', 'inproc2_eb2_id', 133),
                      LinkStub('link3_24', 'B2', 'proc2', 44, 'EB2', 'inproc2_eb2_id', 132),
                      LinkStub('link3_25', 'B2', 'proc2', 45, 'EB2', 'inproc2_eb2_id', 131),
                      LinkStub('link_daq1_01', 'DAQB1', 'daqproc1', 1, 'B1', 'proc11', 101),
                      LinkStub('link_daq1_02', 'DAQB1', 'daqproc1', 2, 'B1', 'proc11', 102),
                      LinkStub('link_daq1_03', 'DAQB1', 'daqproc1', 3, 'B1', 'proc11', 103),
                      LinkStub('link_daq1_04', 'DAQB1', 'daqproc1', 4, 'B1', 'proc11', 104),
                      LinkStub('link_daq1_05', 'DAQB1', 'daqproc1', 5, 'B1', 'proc11', 105),
                      LinkStub('link_feX_101', 'B2', 'proc1', 31, 'fe_module_X', 'lpGBT01', 0),
                      LinkStub('link_feX_102', 'B2', 'proc1', 32, 'fe_module_X', 'lpGBT02', 0)
                      ]
    check.equal(sys_links, expected_links)

    sys_exclusions = systemtuple[0].exclusions
    expected_exclusions = ['B2.proc1', 'B3', 'EB3']
    check.equal(sys_exclusions, expected_exclusions)

    # The second element in the tuple is a LinkModeManager object
    check.equal(systemtuple[1].__class__.__name__, 'LinkModeManager')
    check.equal(systemtuple[1].default_mode, PRBSMode.PRBS7)
    check.equal(systemtuple[1].mask_by_default, False)
    sys_link_settings = systemtuple[1].rules
    expected_link_settings = [(LinkSelector('link1_4', PortSelector('board0', 'proc0', 9999),
                                            PortSelector('board1', 'proc1', None, 'port1')),
                               CSPSettings(100, 200, IdleMethod.Method1), True),
                              (LinkSelector('link1_5'), None, False),
                              (LinkSelector(None, PortSelector('board10')), PRBSMode.PRBS15, None),
                              (LinkSelector(None, PortSelector(), PortSelector(None, 'proc10')), None, True),
                              (LinkSelector(None, PortSelector(), PortSelector(None, None, 8888)),
                               CSPSettings(300, 400, IdleMethod.Method2), None),
                              (LinkSelector(None), None, False),
                              (LinkSelector(None), PRBSMode.PRBS23, None),
                              (LinkSelector(None), PRBSMode.PRBS31, None),
                              (LinkSelector('link_daq1_(1|3|5)'), None, True),]
    check.equal(sys_link_settings, expected_link_settings)

    # The third element in the tuple is a SettingsPool object
    check.equal(systemtuple[2].__class__.__name__, 'SettingsPool')
    check.equal(systemtuple[2].uri, 'file://'+os.path.abspath(
        dir_path+'/config/references/td7_parse_system_file_init.yml'))
    sys_params = systemtuple[2]._parameters
    expected_param_list = {'dx0': {SettingsPool.EntryKey('pid1', None, None): 'val1',
                                   SettingsPool.EntryKey('pid2', 'cmd2', None): 'val2',
                                   SettingsPool.EntryKey('pid3', 'cmd31', 'nsp3'): 'val3',
                                   SettingsPool.EntryKey('pid3', 'cmd32', 'nsp3'): 'val3'},
                           'dx1': {SettingsPool.EntryKey('pid1', None, None): 'val4',
                                   SettingsPool.EntryKey('pid2', 'cmd2', None): 'val5',
                                   SettingsPool.EntryKey('pid3', 'cmd31', 'nsp3'): 'val6',
                                   SettingsPool.EntryKey('pid3', 'cmd32', 'nsp3'): 'val6'},
                           'dx2': {SettingsPool.EntryKey('pid1', 'cmd1', None): 'val7',
                                   SettingsPool.EntryKey('pid2', 'cmd21', 'nsp2'): 'val8',
                                   SettingsPool.EntryKey('pid2', 'cmd22', 'nsp2'): 'val8'},
                           'dx3': {SettingsPool.EntryKey('pid1', 'cmd1', None): 'val9',
                                   SettingsPool.EntryKey('pid2', 'cmd21', 'nsp2'): 'val10',
                                   SettingsPool.EntryKey('pid2', 'cmd22', 'nsp2'): 'val10'}}
    check.equal(sys_params, expected_param_list)


def test_parse_system_file_init_values_td8(check):
    systemtuple = parse_system_file(dir_path+'/config/references/td8_parse_system_file_skeleton_init.yml')

    # The first element in the tuple is a tuple of system stub
    sys_crates = systemtuple[0].crates
    expected_crates = [CrateStub('C1', '11', 'R1', 111, 'Dummy crate definition 1', 1),
                       CrateStub('C2', '22', 'R2', 222, '', 2)]
    check.equal(sys_crates, expected_crates)

    sys_boards = systemtuple[0].boards
    expected_boards = [BoardStub('B1', 'herd1', 'C1', 1, 1001),
                       BoardStub('B-2', 'herd2', 'C2', 2, 3000)]
    check.equal(sys_boards, expected_boards)

    check.equal(systemtuple[0].external_boards, [])

    sys_links = systemtuple[0].links
    expected_links = [LinkStub('link1_01', 'B1', 'proc1', 11, 'B-2', 'proc1', 21),
                      LinkStub('link1_02', 'B1', 'proc1', 12, 'B-2', 'proc1', 22),
                      LinkStub('link1_03', 'B1', 'proc1', 13, 'B-2', 'proc1', 23),
                      LinkStub('link1_04', 'B1', 'proc1', 14, 'B-2', 'proc1', 24),
                      LinkStub('link1_05', 'B1', 'proc1', 15, 'B-2', 'proc1', 25)]
    check.equal(sys_links, expected_links)

    check.equal(systemtuple[0].exclusions, [])

    # The second element in the tuple is a LinkModeManager object
    check.equal(systemtuple[1].__class__.__name__, 'LinkModeManager')
    check.equal(systemtuple[1].default_mode, None)
    check.equal(systemtuple[1].mask_by_default, None)
    sys_link_settings = systemtuple[1].rules
    expected_link_settings = [(LinkSelector('link1_4', PortSelector('B1', 'proc1', 14),
                                            PortSelector('B-2', 'proc1', 24)), None, True)]
    check.equal(sys_link_settings, expected_link_settings)

    # The third element in the tuple is a SettingsPool object
    check.equal(systemtuple[2].__class__.__name__, 'SettingsPool')
    check.equal(systemtuple[2].uri, 'file://'+os.path.abspath(
        dir_path+'/config/references/td8_parse_system_file_skeleton_init.yml'))
    sys_params = systemtuple[2]._parameters
    expected_param_list = {'dx0': {SettingsPool.EntryKey('pid1', None, None): 'val1'}}
    check.equal(sys_params, expected_param_list)

#                                                ##
# End of test block for values ready on __init__ ##
###################################################


def test_parse_system_file_err():
    with pytest.raises(FileNotFoundError):
        parse_system_file(dir_path+'/config/references/td7_parse_system_file_ABSENT_init.yml')


@pytest.mark.parametrize('testkey', [
    'null',
    'system',
    'link_settings',
    'system.boards',
    'system.crates',
    'system.links'
])
def test_parse_system_file_err_badcfgdefs_missingfields(tmp_path_global, testdata8, testkey):

    testdata8copy = copy.deepcopy(testdata8)
    m = re.match(r'(\w+)\.(\w+)', testkey)
    if m:
        del testdata8copy[m.group(1)][m.group(2)]
    elif testkey != 'null':
        del testdata8copy[testkey]
    else:
        pass

    create_temp_test_data(tmp_path_global, testdata8copy)

    if testkey != 'null':
        with pytest.raises(ConfigValidationError):
            parse_system_file(tmp_path_global)
    else:
        assert parse_system_file(tmp_path_global)


# TODO def test_parse_system_file_err_badcfgdefs_badtypes():
#     assert False


@pytest.mark.parametrize('testkey', [
    'null',
    'crates.building',
    'crates.rack',
    'crates.height',
    'crates.csp_id',
    'boards.crate',
    'boards.slot',
    'boards.hostname',
])
def test_parse_system_file_err_badcfgdefs_crates_boards_missingfields(check, tmp_path_global, testdata8, testkey):

    if testkey != 'null':
        testkey0, testkey1 = testkey.split('.')
        for subsystem in testdata8['system'][testkey0].keys():
            testdata8copy = copy.deepcopy(testdata8)
            del testdata8copy['system'][testkey0][subsystem][testkey1]
            create_temp_test_data(tmp_path_global, testdata8copy)

            with check.raises(ConfigValidationError):
                parse_system_file(tmp_path_global)

    else:
        create_temp_test_data(tmp_path_global, testdata8)
        assert parse_system_file(tmp_path_global)


def test_parse_system_file_err_badcfgdefs_crates_badtypes(check, tmp_path_global, testdata8):

    for testkey in ['building', 'rack', 'description']:
        for crate in testdata8['system']['crates'].keys():
            testdata8copy = copy.deepcopy(testdata8)

            if testkey in testdata8copy['system']['crates'][crate].keys():
                testdata8copy['system']['crates'][crate][testkey] = 123
                create_temp_test_data(tmp_path_global, testdata8copy)

                with check.raises(ConfigValidationError):
                    parse_system_file(tmp_path_global)


def test_parse_system_file_err_bacfgdefs_crates_badvalues(check, tmp_path_global, testdata8):

    for testkey in ['height', 'csp_id']:
        for testvalue in ['string', -1]:
            for crate in testdata8['system']['crates'].keys():
                testdata8copy = copy.deepcopy(testdata8)
                testdata8copy['system']['crates'][crate][testkey] = testvalue
                create_temp_test_data(tmp_path_global, testdata8copy)

                with check.raises(ConfigValidationError):
                    parse_system_file(tmp_path_global)


def test_parse_system_file_err_badcfgdefs_boards_badtypes(tmp_path_global, testdata8):
    testkeys = ['hostname', 'crate']
    testvalues = [1, ['str'], {'a': 'b'}]
    for testkey in testkeys:
        for testvalue in testvalues:
            testdata8copy = copy.deepcopy(testdata8)
            testdata8copy['system']['boards']['B1'][testkey] = testvalue
            create_temp_test_data(tmp_path_global, testdata8copy)

            with pytest.raises(ConfigValidationError):
                parse_system_file(tmp_path_global)

    testkeys = ['slot', 'port']
    testvalues = ['str', 1.1]
    for testkey in testkeys:
        for testvalue in testvalues:
            testdata8copy = copy.deepcopy(testdata8)
            testdata8copy['system']['boards']['B1'][testkey] = testvalue
            create_temp_test_data(tmp_path_global, testdata8copy)

            with pytest.raises(ConfigValidationError):
                parse_system_file(tmp_path_global)


def test_parse_system_file_err_badcfgdefs_boards_badvalues(tmp_path_global, testdata8):
    testdata8copy = copy.deepcopy(testdata8)
    testdata8copy['system']['boards']['badBoard.B1'] = {'hostname': 'herd1', 'port': 1001, 'crate': 'C1', 'slot': 1}
    create_temp_test_data(tmp_path_global, testdata8copy)

    with pytest.raises(ConfigValidationError):
        parse_system_file(tmp_path_global)

    testkeys = ['hostname', 'crate', 'slot', 'port']
    testvalueslist = [['herd/1'], ['C-1', 'C.1'], [21, -1], [-1]]
    for testkey, testvalues in zip(testkeys, testvalueslist):
        for testvalue in testvalues:
            testdata8copy = copy.deepcopy(testdata8)
            testdata8copy['system']['boards']['B1'][testkey] = testvalue
            create_temp_test_data(tmp_path_global, testdata8copy)

            with pytest.raises(ConfigValidationError):
                parse_system_file(tmp_path_global)


@pytest.fixture
def tmpbadsysfile():
    return dir_path+'/config/references/tmp_bad_sys.yml'


def test_parse_system_file_err_badcfgdefs_extboards_missingfields(check, testdata7, tmpbadsysfile):

    for testkey in ['crate', 'slot']:
        for extboard in testdata7['system'].get('external_boards', {}).keys():
            testdata7copy = copy.deepcopy(testdata7)
            del testdata7copy['system']['external_boards'][extboard][testkey]
            create_temp_test_data(tmpbadsysfile, testdata7copy)

            with check.raises(ConfigValidationError):
                parse_system_file(tmpbadsysfile)
            delete_temp_test_data(tmpbadsysfile)


@pytest.mark.parametrize('testboardkey, res', [
    ('EB1', ConfigValidationError), ('EB2', ConfigValidationError), ('EB3', None)
])
def test_parse_system_file_err_badcfgdefs_extboards_moremissingfields(testdata7, tmpbadsysfile, testboardkey, res):

    for testkey in ['inputs', 'outputs']:
        testkeydeleted = False
        testdata7copy = copy.deepcopy(testdata7)
        if testkey in testdata7copy['system']['external_boards'][testboardkey].keys():
            testkeydeleted = True
            del testdata7copy['system']['external_boards'][testboardkey][testkey]
        create_temp_test_data(tmpbadsysfile, testdata7copy)

        if testkeydeleted and res is not None:
            with pytest.raises(res):
                parse_system_file(tmpbadsysfile)
        else:
            assert parse_system_file(tmpbadsysfile)
        delete_temp_test_data(tmpbadsysfile)


def test_parse_system_file_err_badcfgdefs_extboards_badtypes(check, testdata7, tmpbadsysfile):

    badtypes = {'crate': 999, 'slot': '1', 'inputs': ['a', 'b'], 'outputs': ['a', 'b']}
    for testkey, testvalue in badtypes.items():
        testdata7copy = copy.deepcopy(testdata7)
        for ext_brd in testdata7['system']['external_boards'].keys():
            if testkey in testdata7copy['system']['external_boards'][ext_brd].keys():
                testdata7copy['system']['external_boards'][ext_brd][testkey] = testvalue
        create_temp_test_data(tmpbadsysfile, testdata7copy)

        with check.raises(ConfigValidationError):
            parse_system_file(tmpbadsysfile)
        delete_temp_test_data(tmpbadsysfile)


def test_parse_system_file_err_badcfgdefs_extboards_badvalues(check, testdata7, tmpbadsysfile):

    badvalues = {'slot': -99, 'inputs': {'a': 1}, 'outputs': {'a': 1}}
    for testkey, testvalue in badvalues.items():
        testdata7copy = copy.deepcopy(testdata7)
        for ext_brd in testdata7['system']['external_boards'].keys():
            if testkey in testdata7copy['system']['external_boards'][ext_brd].keys():
                testdata7copy['system']['external_boards'][ext_brd][testkey] = testvalue
        create_temp_test_data(tmpbadsysfile, testdata7copy)

        with check.raises(ConfigValidationError):
            parse_system_file(tmpbadsysfile)
        delete_temp_test_data(tmpbadsysfile)


def test_parse_system_file_err_badcfgdefs_daqboards_missingfields(testdata7, tmpbadsysfile):
    testdata7copy = copy.deepcopy(testdata7)
    del testdata7copy['system']['daq_boards']
    del testdata7copy['system']['frontends']
    testdata7copy['system']['daq_boards'] = {'DAQ1': {}}
    create_temp_test_data(tmpbadsysfile, testdata7copy)

    with pytest.raises(ConfigValidationError):
        parse_system_file(tmpbadsysfile)

    delete_temp_test_data(tmpbadsysfile)


def test_parse_system_file_err_badcfgdefs_daqboards_badtypes(testdata7, tmpbadsysfile):
    testdata7copy = copy.deepcopy(testdata7)
    del testdata7copy['system']['daq_boards']
    del testdata7copy['system']['frontends']
    for badvalue in [[{'DB1': {'inputs': {'p1': '1-11'}}}, {'DB2': {'outputs': {'p2', '21-31'}}}],
                     {'DB1': [{'inputs': {'p1': '1-11'}}, {'outputs': {'p2': '21-31'}}]},
                     {'DB1': {'inputs': [{'p1': '1-11'}, {'p2': '21-31'}]}},
                     {'DB1': {'inputs': {'p1': ['1-11']}}}
                     ]:
        testdata7copy['system']['daq_boards'] = badvalue
        create_temp_test_data(tmpbadsysfile, testdata7copy)

        with pytest.raises(ConfigValidationError):
            parse_system_file(tmpbadsysfile)

        delete_temp_test_data(tmpbadsysfile)


def test_parse_system_file_err_badcfgdefs_daqboards_badvalues(testdata7, tmpbadsysfile):
    testdata7copy = copy.deepcopy(testdata7)
    del testdata7copy['system']['daq_boards']
    del testdata7copy['system']['frontends']
    for badvalue in [{'DB1': {'inputs': {'pI1': '1,-11'}, 'outputs': {'pO': '21-31'}}},
                     {'DB1': {'inputs': {'pI1': '1-11'}, 'outputs': {'pO': '21,-31'}}},
                     {'DB1': {'inputs': {'p-I1': '1-11'}, 'outputs': {'pO': '21-31'}}},
                     {'DB1': {'inputs': {'pI1': '1-11'}, 'outputs': {'p-O': '21-31'}}},
                     {'DB1': {'inputs': {'pI1': '1-11'}, 'outputx': {'pO': '21-31'}}},
                     {'DB1': {'inputx': {'pI1': '1-11'}, 'outputs': {'pO': '21-31'}}},
                     {'dummy.DBX': {'inputs': {'pI1': '1-11'}, 'outputs': {'pO': '21-31'}}}]:
        testdata7copy['system']['daq_boards'] = badvalue
        create_temp_test_data(tmpbadsysfile, testdata7copy)

        with pytest.raises(ConfigValidationError):
            parse_system_file(tmpbadsysfile)

        delete_temp_test_data(tmpbadsysfile)


def test_parse_system_file_err_badcfgdefs_frontends_missingfields(testdata7, tmpbadsysfile):
    testdata7copy = copy.deepcopy(testdata7)
    del testdata7copy['system']['frontends']
    for badvalue in [{'fe1': {'serial': 's1'}},
                     {'fe1': {'lpgbt_fuseids': '101-103'}},
                     {'fe1': {'serial': 's1', 'lpgbt_fuseids': '101-103'}, 'xtra': 'x'}]:
        testdata7copy['system']['frontends'] = badvalue
        create_temp_test_data(tmpbadsysfile, testdata7copy)

        with pytest.raises(ConfigValidationError):
            parse_system_file(tmpbadsysfile)

        delete_temp_test_data(tmpbadsysfile)


def test_parse_system_file_err_badcfgdefs_frontends_badtypes(testdata7, tmpbadsysfile):
    testdata7copy = copy.deepcopy(testdata7)
    del testdata7copy['system']['frontends']
    for badvalue in [{'fe1': {'serial': ['s1'], 'lpgbt_fuseids': '101-103'}},
                     {'fe1': {'serial': {'a': 'b'}, 'lpgbt_fuseids': '101-103'}},
                     {'fe1': [{'serial': 's1', 'lpgbt_fuseids': '101-103'}], },
                     [{'fe1': {'serial': 's1', 'lpgbt_fuseids': '101-103'}}, ],
                     {'fe1': {'serial': 's1', 'lpgbt_fuseids': [101, 102, 103]}},
                     {'fe1': {'serial': 's1', 'lpgbt_fuseids': {'a1': 'a1', 'a2': 'a2'}}}]:
        testdata7copy['system']['frontends'] = badvalue
        create_temp_test_data(tmpbadsysfile, testdata7copy)

        with pytest.raises(ConfigValidationError):
            parse_system_file(tmpbadsysfile)

        delete_temp_test_data(tmpbadsysfile)


def test_parse_system_file_err_badcfgdefs_frontends_badvalues(testdata7, tmpbadsysfile):
    testdata7copy = copy.deepcopy(testdata7)
    del testdata7copy['system']['frontends']
    for badvalue in [{'fe1': [{'serial': 's1'}, {'lpgbt_fuseids': '101-103'}]},
                     {'fe1': {'serial': 101, 'lpgbt_fuseids': '101-103'}},
                     {'fe1': {'serial': True, 'lpgbt_fuseids': '101-103'}},
                     {'fe1': {'serial': 's1', 'lpgbt_fuseids': 'x101-x103'}},
                     {'fe1': {'serial': 's1', 'lpgbt_fuseids': '101,-103'}}]:
        testdata7copy['system']['frontends'] = badvalue
        create_temp_test_data(tmpbadsysfile, testdata7copy)

        with pytest.raises(ConfigValidationError):
            parse_system_file(tmpbadsysfile)

        delete_temp_test_data(tmpbadsysfile)


def test_parse_system_file_err_badcfgdefs_links_missingfields(check, testdata8, tmp_path_global):
    for testkey in ('from', 'to'):
        testdata8copy = copy.deepcopy(testdata8)
        del testdata8copy['system']['links']['link1_[1-5]'][testkey]
        create_temp_test_data(tmp_path_global, testdata8copy)

        with check.raises(ConfigValidationError):
            parse_system_file(tmp_path_global)


def test_parse_system_file_err_badcfgdefs_links_badtypes(check, testdata8, tmp_path_global):
    badtypes = {'from': ('B1', 'proc1', '11-15'), 'to': ('B2', 'proc1', '21-25')}
    for testkey, testvalue in badtypes.items():
        testdata8copy = copy.deepcopy(testdata8)
        testdata8copy['system']['links']['link1_[1-5]'][testkey] = testvalue
        create_temp_test_data(tmp_path_global, testdata8copy)

        with check.raises(ConfigValidationError):
            parse_system_file(tmp_path_global)


def test_parse_system_file_err_badcfgdefs_links_badvalues(check, testdata8, tmp_path_global):
    for testkey in ('from', 'to'):
        for testvalue in (['B1.proc1', '11-15'], ['B2', 'proc1', '21-25', '31-35']):
            testdata8copy = copy.deepcopy(testdata8)
            testdata8copy['system']['links']['link1_[1-5]'][testkey] = testvalue
            create_temp_test_data(tmp_path_global, testdata8copy)

            with check.raises(ConfigValidationError):
                parse_system_file(tmp_path_global)


def test_parse_system_file_err_badcfgdefs_links_difflengthvalues(check, testdata8, tmp_path_global):
    # Any two similar
    difflengthvaldict = {'links:link1_[1-5]:from': ['B1', 'proc1', '11-14'],
                         'links:link1_[1-5]:to': ['B2', 'proc1', '21-26']}
    for testkey, testvalue in difflengthvaldict.items():
        tk1, tk2, tk3 = testkey.split(':')
        testdata8copy = copy.deepcopy(testdata8)
        testdata8copy['system'][tk1][tk2][tk3] = testvalue
        create_temp_test_data(tmp_path_global, testdata8copy)

        with check.raises(InvalidConfigFormatError):
            parse_system_file(tmp_path_global)

    # Any two similar continued
    testdata8copy = copy.deepcopy(testdata8)
    testdata8copy['system']['links'] = {'link1_[1-10]': {'from': ['B1', 'proc1', '11-15'],
                                                         'to': ['B2', 'proc1', '21-25']}}
    create_temp_test_data(tmp_path_global, testdata8copy)
    with check.raises(InvalidConfigFormatError):
        parse_system_file(tmp_path_global)

    # All three different
    testdata8copy = copy.deepcopy(testdata8)
    testdata8copy['system']['links'] = {'link1_[1-10]': {'from': ['B1', 'proc1', '11-16'],
                                                         'to': ['B2', 'proc1', '21-24']}}
    create_temp_test_data(tmp_path_global, testdata8copy)
    with check.raises(InvalidConfigFormatError):
        parse_system_file(tmp_path_global)


def test_parse_system_file_badcfgdefs_links_allowedfromtocfgs(testdata7, tmpbadsysfile):
    testdata7copy = copy.deepcopy(testdata7)
    del testdata7copy['system']['links']['link_feX_[101-102]']
    trialvalue_dict = [{'value': {'from': ['B2', 'proc1', '31-32'], 'to': ['fe_module_X']},
                        'exp_out': [LinkStub('link_feX_101', 'B2', 'proc1', 31, 'fe_module_X', 'lpGBT00', 0),
                                    LinkStub('link_feX_102', 'B2', 'proc1', 32, 'fe_module_X', 'lpGBT01', 0)]},
                       {'value': {'from': ['B2', 'proc1', '31-32'], 'to': ['fe_module_X', '1-2']},
                        'exp_out': [LinkStub('link_feX_101', 'B2', 'proc1', 31, 'fe_module_X', 'lpGBT01', 0),
                                    LinkStub('link_feX_102', 'B2', 'proc1', 32, 'fe_module_X', 'lpGBT02', 0)]},]
    for trialvalue in trialvalue_dict:
        testdata7copy['system']['links']['link_feX_[101-102]'] = trialvalue.get('value', {})
        create_temp_test_data(tmpbadsysfile, testdata7copy)
        sys_tuple = parse_system_file(tmpbadsysfile)
        assert sys_tuple[0].links[22] == trialvalue.get('exp_out', {})[0]
        assert sys_tuple[0].links[23] == trialvalue.get('exp_out', {})[1]
        delete_temp_test_data(tmpbadsysfile)


def test_parse_system_file_err_badcfgdefs_links_allowedfromtocfgs(testdata7, tmpbadsysfile):
    testdata7copy = copy.deepcopy(testdata7)
    del testdata7copy['system']['links']['link_feX_[101-102]']
    trialvalue_dict = [{'value': {'from': ['B2', 'proc1', '31-32'], 'to': ['fe_module_X', '1-3']}},
                       {'value': {'from': ['B2', 'proc1', '31'], 'to': ['fe_module_X']}}]
    for trialvalue in trialvalue_dict:
        testdata7copy['system']['links']['link_feX_[101-102]'] = trialvalue.get('value', {})
        create_temp_test_data(tmpbadsysfile, testdata7copy)
        with pytest.raises(InvalidConfigFormatError):
            parse_system_file(tmpbadsysfile)
        delete_temp_test_data(tmpbadsysfile)

    testdata7copy = copy.deepcopy(testdata7)
    del testdata7copy['system']['links']['link_feX_[101-102]']
    trialvalue_dict = [{'value': {'from': ['B2', 'proc1', '31-32'], 'to': ['fe_module_X', 'lpGBT', '1-2']}},
                       {'value': {'from': ['B2', 'proc1', '31-32'], 'to': ['fe_module_X', 'lpGBT']}},
                       {'value': {'from': ['B2', 'proc1', '31-32'], 'to': ['B3', 'proc1']}},
                       {'value': {'from': ['B2', '31-32'], 'to': ['fe_module_X', '1-2']}},
                       {'value': {'from': ['B2', '31-32'], 'to': ['fe_module_X']}},
                       {'value': {'from': ['B2', 'proc1', '31-32'], 'to': ['fe_module_X', '-2,1']}}]
    for trialvalue in trialvalue_dict:
        testdata7copy['system']['links']['link_feX_[101-102]'] = trialvalue.get('value', {})
        create_temp_test_data(tmpbadsysfile, testdata7copy)
        with pytest.raises(ConfigValidationError):
            parse_system_file(tmpbadsysfile)
        delete_temp_test_data(tmpbadsysfile)


def test_parse_system_file_err_bacfgdefs_exclude_badvalues(check, testdata7, tmpbadsysfile):
    # Invalid entry format
    testdata7copy = copy.deepcopy(testdata7)
    testdata7copy['exclude'] = ['B2.proc2.sys']
    create_temp_test_data(tmpbadsysfile, testdata7copy)
    with check.raises(ConfigValidationError):
        parse_system_file(tmpbadsysfile)
    delete_temp_test_data(tmpbadsysfile)

    # Board not present
    testdata7copy = copy.deepcopy(testdata7)
    testdata7copy['exclude'] = ['badBoard']
    create_temp_test_data(tmpbadsysfile, testdata7copy)
    with check.raises(InvalidConfigFormatError):
        parse_system_file(tmpbadsysfile)
    delete_temp_test_data(tmpbadsysfile)
