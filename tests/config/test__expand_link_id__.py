import pytest

from swatch.config import _expand_link_id
from swatch.exceptions import InvalidConfigFormatError


def test__expand_link_id(check):

    lidr_string = '0a1_b2[0]'
    check.equal(_expand_link_id(lidr_string), ['0a1_b200'])

    lidr_string = '0a1_b2[9]'
    check.equal(_expand_link_id(lidr_string), ['0a1_b209'])

    lidr_string = '0a1_b2[99]'
    check.equal(_expand_link_id(lidr_string), ['0a1_b299'])

    lidr_string = '0a1_b2[999]'
    check.equal(_expand_link_id(lidr_string), ['0a1_b2999'])

    lidr_string = '0a1_b2[99]c6_7d'
    check.equal(_expand_link_id(lidr_string), ['0a1_b299c6_7d'])

    lidr_string = '0a1_b2[19-21]'
    check.equal(_expand_link_id(lidr_string), ['0a1_b219',
                                               '0a1_b220',
                                               '0a1_b221'])

    lidr_string = '0a1_b2[10-8]'
    check.equal(_expand_link_id(lidr_string), ['0a1_b210',
                                               '0a1_b209',
                                               '0a1_b208'])

    lidr_string = '0a1_b2[19-21]c6_7d'
    check.equal(_expand_link_id(lidr_string), ['0a1_b219c6_7d',
                                               '0a1_b220c6_7d',
                                               '0a1_b221c6_7d'])

    lidr_string = '0a1_b2[19-21,25,30,39-42,45]'
    check.equal(_expand_link_id(lidr_string), ['0a1_b219',
                                               '0a1_b220',
                                               '0a1_b221',
                                               '0a1_b225',
                                               '0a1_b230',
                                               '0a1_b239',
                                               '0a1_b240',
                                               '0a1_b241',
                                               '0a1_b242',
                                               '0a1_b245'])

    lidr_string = '0a1_b2[19-21,25,30,39-42,45]c6_7d'
    check.equal(_expand_link_id(lidr_string), ['0a1_b219c6_7d',
                                               '0a1_b220c6_7d',
                                               '0a1_b221c6_7d',
                                               '0a1_b225c6_7d',
                                               '0a1_b230c6_7d',
                                               '0a1_b239c6_7d',
                                               '0a1_b240c6_7d',
                                               '0a1_b241c6_7d',
                                               '0a1_b242c6_7d',
                                               '0a1_b245c6_7d'])


# Parametrizing reduces the number of code lines
@pytest.mark.parametrize('lidr_string', [
    '0a1_b2[]',
    '0a1_b2[-19]',
    '0a1_b2[,19]',
    '0a1_b2[19-]',
    '0a1_b2[19,]',
    '0a1_b2[19-,21]',
    '0a1_b2[19,-21]',
    '0a1_b2[19, 21]',
    '0a1_b2[19-21,-25]'
])
def test__expand_link_id_err(lidr_string):

    with pytest.raises(InvalidConfigFormatError):
        assert _expand_link_id(lidr_string)
