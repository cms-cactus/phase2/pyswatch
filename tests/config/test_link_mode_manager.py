import pytest
from unittest.mock import Mock

from swatch.board import Board, Processor, Link
from swatch.config import LinkModeManager, PortSelector
from swatch.exceptions import InvalidConfigFormatError
from swatch.io_settings import CSPSettings, IdleMethod, PRBSMode


@pytest.fixture
def def_mode():
    return CSPSettings(packet_length=100, packet_periodicity=10, idle_method=IdleMethod.Method2)


@pytest.fixture
def lmm_obj(def_mode):
    return LinkModeManager(default_mode=def_mode, mask_by_default=False)


def test_linkmodemanager_init(lmm_obj, def_mode):
    assert str(lmm_obj.default_mode) == str(def_mode)
    assert lmm_obj.rules == []
    assert lmm_obj.mask_by_default is False

    # Test default init
    lmm_obj_null = LinkModeManager()
    assert lmm_obj_null.default_mode is None
    assert lmm_obj_null.rules == []
    assert lmm_obj_null.mask_by_default is None


def create_link_terminus(params, valid=True):
    terminus = Mock(spec=Link.Terminus)
    terminus.board = Mock(spec=Board, id=f"board_{params[1]}_link{params[0]}")
    terminus.processor = Mock(spec=Processor, id=f"proc_{params[1]}_link{params[0]}")
    terminus.processor.is_included = valid
    terminus.port = Mock(spec=Processor.AbstractPort, index=params[2], id=f"port_{params[1]}_link{params[0]}")
    return terminus


@pytest.fixture
def ps_src():
    return PortSelector(board_id_regex='board_src_link0', proc_id_regex='proc_src_link0',
                        port_index=99, port_id_regex='port_src_link0')


@pytest.fixture
def ps_dst():
    return PortSelector(board_id_regex='board_dst_link0', proc_id_regex='proc_dst_link0',
                        port_index=88, port_id_regex='port_dst_link0')


@pytest.fixture
def link_obj():
    link_obj = Mock(spec=Link)
    link_obj.id = 'lmm_rule0'
    link_obj.source = create_link_terminus([0, 'src', 99])
    link_obj.destination = create_link_terminus([0, 'dst', 88])
    return link_obj


def test_linkmodemanager_add_rule(lmm_obj, ps_src, ps_dst, link_obj):
    # Test add rule with mode
    opts = [{'mode': PRBSMode.PRBS7},
            {'mode': PRBSMode.PRBS9, 'mask': True},
            {'mode': CSPSettings(packet_length=999, packet_periodicity=99, idle_method=IdleMethod.Method1)},
            {'mode': CSPSettings(packet_length=999, packet_periodicity=99, idle_method=IdleMethod.Method1),
             'mask': False}]
    for i, opt in enumerate(opts):
        if 'mask' in opt:
            lmm_obj.add_rule(id_regex='lmm_rule0', src_selector=ps_src,
                             dst_selector=ps_dst, mode=opt['mode'], mask=opt['mask'])
            assert lmm_obj.rules[i][2] is opt['mask']
        else:
            lmm_obj.add_rule(id_regex='lmm_rule0', src_selector=ps_src, dst_selector=ps_dst, mode=opt['mode'])
            assert lmm_obj.rules[i][2] is None
        assert lmm_obj.rules[i][0](link_obj)
        assert str(lmm_obj.rules[i][1]) == str(opt['mode'])

    lmm_obj.add_rule(id_regex='lmm_rule0', src_selector=ps_src, dst_selector=ps_dst, mask=True)
    assert lmm_obj.rules[len(opts)][0](link_obj)
    assert lmm_obj.rules[len(opts)][1] is None
    assert lmm_obj.rules[len(opts)][2] is True


def test_linkmodemanager_add_rule_err(lmm_obj, ps_src, ps_dst):
    with pytest.raises(InvalidConfigFormatError):
        lmm_obj.add_rule(id_regex='lmm_rule0', src_selector=ps_src, dst_selector=ps_dst)


def test_linkmodemanager_get_mode_lastinfirstout(lmm_obj, ps_src, ps_dst, link_obj):
    lmm_obj.add_rule(id_regex='lmm_rule0', src_selector=ps_src, dst_selector=ps_dst, mode=PRBSMode.PRBS15)
    assert str(lmm_obj.get_mode(link_obj)) == 'PRBS15'

    # Test Last In First Out
    lmm_obj.add_rule(id_regex='lmm_rule0', src_selector=ps_src, dst_selector=ps_dst, mode=PRBSMode.PRBS23)
    assert str(lmm_obj.get_mode(link_obj)) == 'PRBS23'

    # Test Last In First Out which is not None
    lmm_obj.add_rule(id_regex='lmm_rule0', src_selector=ps_src, dst_selector=ps_dst, mode=None, mask=PRBSMode.PRBS31)
    assert str(lmm_obj.get_mode(link_obj)) == 'PRBS23'


def test_linkmodemanager_get_mode_default(check, lmm_obj, ps_src, ps_dst, link_obj, def_mode):

    # Test return default mode when a matching link is not found
    check.equal(str(lmm_obj.get_mode(link_obj)), str(def_mode))

    # Test return default mode when a matching link is found, but is masked and mode is not defined
    lmm_obj.add_rule(id_regex='lmm_rule0', src_selector=ps_src, dst_selector=ps_dst, mask=True)
    check.equal(str(lmm_obj.get_mode(link_obj)), str(def_mode))

    # Test return when default mode is not set when a matching link is found, but is masked and mode is not defined
    lmm_obj_null = LinkModeManager()
    lmm_obj_null.add_rule(id_regex='lmm_rule0', src_selector=ps_src, dst_selector=ps_dst, mask=True)
    check.equal(lmm_obj_null.get_mode(link_obj), None)


def test_linkmodemanager_get_mode_err(link_obj):
    lmm_obj_def = LinkModeManager()
    with pytest.raises(InvalidConfigFormatError):
        assert lmm_obj_def.get_mode(link_obj)

# # TODO Test the 'get_input_modes(self, system)' functionality after unit tests for systems
# # TODO Test the 'get_output_modes(self, system)' functionality after unit tests for systems


def test_linkmodemanager_get_mask_badlink(check, lmm_obj):

    # Test get mask when link source or destination is not included
    link_invalidsrc = Mock(spec=Link)
    link_invalidsrc.source = create_link_terminus([0, 'invalid_src', 99], False)
    link_invalidsrc.destination = create_link_terminus([0, 'dst', 88])
    check.is_true(lmm_obj.get_mask(link_invalidsrc))

    link_invaliddst = Mock(spec=Link)
    link_invaliddst.source = create_link_terminus([0, 'src', 99])
    link_invaliddst.destination = create_link_terminus([0, 'invalid_dst', 88], False)
    check.is_true(lmm_obj.get_mask(link_invaliddst))


def test_linkmodemanager_get_mask_lastinfirstout(lmm_obj, ps_src, ps_dst, link_obj):

    lmm_obj.add_rule(id_regex='lmm_rule0', src_selector=ps_src, dst_selector=ps_dst, mask=False)
    assert lmm_obj.get_mask(link_obj) is False

    lmm_obj.add_rule(id_regex='lmm_rule0', src_selector=ps_src, dst_selector=ps_dst, mask=True)
    assert lmm_obj.get_mask(link_obj) is True


def test_linkmodemanager_get_mask_default(lmm_obj, link_obj):

    # Test return default mask when a matching link is not found
    assert lmm_obj.get_mask(link_obj) is False

    # Test return default mask when a matching link is found but mask is None
    lmm_obj_null = LinkModeManager()
    assert lmm_obj_null.get_mask(link_obj) is None


# # TODO Test the 'get_masked_inputs(self, system)' functionality after unit tests for systems
# # TODO Test the 'get_masked_outputs(self, system)' functionality after unit tests for systems
