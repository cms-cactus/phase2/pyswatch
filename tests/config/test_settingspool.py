import pytest

from swatch import exceptions
from datetime import datetime, timezone

from swatch.config import SettingsPool


@pytest.fixture
def sp_obj():
    return SettingsPool("testString")


def test_settingspool_init(sp_obj):
    assert sp_obj.uri == "testString"
    assert (sp_obj.timestamp - datetime.now(timezone.utc)).total_seconds() < 1
    assert sp_obj._parameters == {}


def test_settingspool_addparams_getparams(check, sp_obj):
    sp_obj.add_parameter('c0', 'p0', 'v0')
    sp_obj.add_parameter('c0', 'p1', 'v1', 'cmd0')
    sp_obj.add_parameter('c0', 'p2', 'v2', 'cmd1', 'n0')
    sp_obj.add_parameter('c1', 'p0', 'v3')
    sp_obj.add_parameter('c1', 'p1', 'v4', 'cmd0')
    sp_obj.add_parameter('c1', 'p2', 'v5', 'cmd1', 'n0')
    check.equal(sp_obj.get_parameter('n0', 'cmd1', 'p2', ['c0', 'c1']), 'v2')
    check.equal(sp_obj.get_parameter('nx', 'cmd0', 'p1', ['c0', 'c1']), 'v1')
    check.equal(sp_obj.get_parameter('nx', 'cmdx', 'p0', ['c0', 'c1']), 'v0')
    check.equal(sp_obj.get_parameter('n0', 'cmd1', 'p2', ['c1', 'c0']), 'v5')
    check.equal(sp_obj.get_parameter('nx', 'cmd0', 'p1', ['c1', 'c0']), 'v4')
    check.equal(sp_obj.get_parameter('nx', 'cmdx', 'p0', ['c1', 'c0']), 'v3')


def test_settingspool_addparams_badinits(sp_obj):
    with pytest.raises(exceptions.InvalidArgumentError):
        sp_obj.add_parameter('c0', 'p0', 'v0', None, 'n0')  # no cmd


def test_settingspool_addparams_duplicates(check, sp_obj):
    sp_obj.add_parameter('c0', 'p0', 'v0', 'cmd0', 'n0')
    check.equal(sp_obj.get_parameter('n0', 'cmd0', 'p0', ['c0']), 'v0')
    with pytest.raises(exceptions.DuplicateEntryError):
        sp_obj.add_parameter('c0', 'p0', 'vx', 'cmd0', 'n0')  # duplicate

    sp_obj.add_parameter('c1', 'p0', 'v1', 'cmd0')
    check.equal(sp_obj.get_parameter('nx', 'cmd0', 'p0', ['c1']), 'v1')
    with pytest.raises(exceptions.DuplicateEntryError):
        sp_obj.add_parameter('c1', 'p0', 'vx', 'cmd0')  # duplicate

    sp_obj.add_parameter('c2', 'p0', 'v2')
    check.equal(sp_obj.get_parameter('nx', 'cmdx', 'p0', ['c2']), 'v2')
    with pytest.raises(exceptions.DuplicateEntryError):
        sp_obj.add_parameter('c2', 'p0', 'vx')  # duplicate


def test_settingspool_getparams_badcalls(check, sp_obj):
    sp_obj.add_parameter('c0', 'p0', 'v0')
    check.equal(sp_obj.get_parameter('n0', 'cmd0', 'p0', ['c0']), 'v0')
    check.equal(sp_obj.get_parameter('nx', 'cmd0', 'p0', ['c0']), 'v0')
    check.equal(sp_obj.get_parameter('nx', 'cmdx', 'p0', ['c0']), 'v0')
    with check.raises(exceptions.InvalidArgumentError):
        check.equal(sp_obj.get_parameter(None, 'cmd0', 'p0', ['c0']), 'v0')
        check.equal(sp_obj.get_parameter('n0', None, 'p0', ['c0']), 'v0')
        check.equal(sp_obj.get_parameter(None, None, 'p0', ['c0']), 'v0')
        check.equal(sp_obj.get_parameter('', 'cmd0', 'p0', ['c0']), 'v0')
        check.equal(sp_obj.get_parameter('n0', '', 'p0', ['c0']), 'v0')
        check.equal(sp_obj.get_parameter('', '', 'p0', ['c0']), 'v0')


def test_settingspool_getparams_notfound(check, sp_obj):
    sp_obj.add_parameter('c0', 'p0', 'v0', 'cmd0', 'n0')
    check.equal(sp_obj.get_parameter('n0', 'cmd0', 'p0', ['c0']), 'v0')
    check.equal(sp_obj.get_parameter('n0', 'cmd0', 'p0', ['cx']), None)
    check.equal(sp_obj.get_parameter('n0', 'cmd0', 'px', ['c0']), None)


def test_settingspool_getparams_matchorder(check, sp_obj):
    sp_obj.add_parameter('c0', 'p0', 'v0', 'cmd0', 'n0')
    sp_obj.add_parameter('c0', 'p0', 'v1', 'cmd0',)
    sp_obj.add_parameter('c0', 'p0', 'v2')
    check.equal(sp_obj.get_parameter('n0', 'cmd0', 'p0', ['c0']), 'v0')
    check.equal(sp_obj.get_parameter('nx', 'cmd0', 'p0', ['c0']), 'v1')
    check.equal(sp_obj.get_parameter('nx', 'cmdx', 'p0', ['c0']), 'v2')


def test_settingspool_getparams_cntxtordr(check, sp_obj):
    sp_obj.add_parameter('c0', 'p0', 'v0', 'cmd0', 'n0')
    sp_obj.add_parameter('c1', 'p0', 'v1')
    check.equal(sp_obj.get_parameter('n0', 'cmd0', 'p0', ['c1', 'c0']), 'v1')
    check.equal(sp_obj.get_parameter('n0', 'cmd0', 'p0', ['c0', 'c1']), 'v0')
