import os
import pytest
import yaml


file_path = os.path.dirname(os.path.realpath(__file__))
dir_path = os.path.dirname(file_path)


@pytest.fixture
def testdata7():
    with open(dir_path+'/config/references/td7_parse_system_file_init.yml', 'r') as f:
        data = yaml.load(f, Loader=yaml.FullLoader)
    return data


@pytest.fixture
def testdata8():
    with open(dir_path+'/config/references/td8_parse_system_file_skeleton_init.yml', 'r') as f:
        data = yaml.load(f, Loader=yaml.FullLoader)
    return data


@pytest.fixture
def td7ls():
    with open(dir_path+'/config/references/td7_parse_system_file_init_link_settings.yml', 'r') as f:
        data = yaml.load(f, Loader=yaml.FullLoader)
    return data['link_settings']
