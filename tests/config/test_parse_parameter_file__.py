import pytest
import yaml
import os

from testutils import create_temp_test_data
from swatch.config import parse_parameter_file
from swatch.exceptions import InvalidConfigFormatError
from swatch.parameters import File

file_path = os.path.dirname(os.path.realpath(__file__))
dir_path = os.path.dirname(file_path)


def test_file_path():
    assert parse_parameter_file(dir_path+'/config/references/td2_parse_parameter_file_include.yml')
    with pytest.raises(FileNotFoundError):
        assert parse_parameter_file(dir_path+'/config/references/nonexistentfile.txt')


@pytest.fixture
def testdata1():
    pool = parse_parameter_file(dir_path+'/config/references/td2_parse_parameter_file_include.yml')
    return pool


def test_parseparameterfile_readsimple(check, testdata1):
    check.equal(testdata1.get_parameter('nx', 'cmdX', 'pid1', ['dx0', 'dx1']), 'val1')
    check.equal(testdata1.get_parameter('nx', 'cmd2', 'pid2', ['dx0', 'dx1']), 'val2')
    check.equal(testdata1.get_parameter('nsp3', 'cmd31', 'pid3', ['dx0', 'dx1']), 'val3')
    check.equal(testdata1.get_parameter('nsp3', 'cmd32', 'pid3', ['dx0', 'dx1']), 'val3')
    check.equal(testdata1.get_parameter('nx', 'cmdX', 'pid1', ['dx1', 'dx0']), 'val4')
    check.equal(testdata1.get_parameter('nx', 'cmd2', 'pid2', ['dx1', 'dx0']), 'val5')
    check.equal(testdata1.get_parameter('nsp3', 'cmd31', 'pid3', ['dx1', 'dx0']), 'val6')
    check.equal(testdata1.get_parameter('nsp3', 'cmd32', 'pid3', ['dx1', 'dx0']), 'val6')

# This test function is to read data with key starting with §


def test_parseparameterfile_readsubkeys(check, testdata1):
    check.equal(testdata1.get_parameter('nX', 'cmd1', 'pid1', ['dx2', 'dx3']), 'val7')
    check.equal(testdata1.get_parameter('nsp2', 'cmd21', 'pid2', ['dx2', 'dx3']), 'val8')
    check.equal(testdata1.get_parameter('nsp2', 'cmd22', 'pid2', ['dx2', 'dx3']), 'val8')
    check.equal(testdata1.get_parameter('nX', 'cmd1', 'pid1', ['dx3', 'dx2']), 'val9')
    check.equal(testdata1.get_parameter('nsp2', 'cmd21', 'pid2', ['dx3', 'dx2']), 'val10')
    check.equal(testdata1.get_parameter('nsp2', 'cmd22', 'pid2', ['dx3', 'dx2']), 'val10')


def test_parseparameterfile_badinclude(tmp_path_global):
    with open(dir_path+'/config/references/td1_parse_parameter_file_context_pars.yml', 'r') as f:
        data = yaml.load(f, Loader=yaml.FullLoader)
    abspath = os.path.abspath(dir_path+'/config/references/td1_parse_parameter_file_context_pars.yml')
    dirname = os.path.dirname(abspath)
    data['include'] = [dirname+'/wrongincludefile.yml']
    create_temp_test_data(tmp_path_global, data)
    # bad file path
    with pytest.raises(FileNotFoundError):
        assert parse_parameter_file(tmp_path_global)

    # bad include format
    with pytest.raises(InvalidConfigFormatError):
        assert parse_parameter_file(dir_path+'/config/references/td3_parse_parameter_file_badincludeinit.yml')


def test_parseparameterfile_nocontextinfile():
    with pytest.raises(InvalidConfigFormatError):
        assert parse_parameter_file(dir_path+'/config/references/td4_parse_parameter_file_variables.yml')


def test_parseparameterfile_badcontext_init(tmp_path_global):
    with open(dir_path+'/config/references/td1_parse_parameter_file_context_pars.yml', 'r') as f:
        data = yaml.load(f, Loader=yaml.FullLoader)
    data['contexts']['dx0']['badcontext'] = {'badkey': 'badvalue'}
    create_temp_test_data(tmp_path_global, data)
    with pytest.raises(InvalidConfigFormatError):
        assert parse_parameter_file(tmp_path_global)


@pytest.mark.parametrize('badkey, badvalue, excpt', [
    ('bk1:bk2:bk3', 'badvalue1', None),
    ('bk1:bk2:bk3:bk4', 'badvalue1', InvalidConfigFormatError),
    ('§bk1:bk2', {'bk4': 'badvalue2'}, None),
    ('§bk1:bk2:bk3', {'bk4': 'badvalue2'}, InvalidConfigFormatError),
    ('§bk1:bk2', 'badvalue3', AttributeError),
])
def test_parseparameterfile_badkey(tmp_path_global, badkey, badvalue, excpt):
    with open(dir_path+'/config/references/td1_parse_parameter_file_context_pars.yml', 'r') as f:
        data = yaml.load(f, Loader=yaml.FullLoader)
    data['contexts']['dx0']['parameters'][badkey] = badvalue
    create_temp_test_data(tmp_path_global, data)
    if excpt is not None:
        with pytest.raises(excpt):
            assert parse_parameter_file(tmp_path_global)
    else:
        assert parse_parameter_file(tmp_path_global)


@pytest.fixture
def replacement_testdata5():
    return {'replaceval_1111': 'val51', 'replaceval_2111': 'val52', 'replaceval_1122': 'val53',
            'replaceval_2122': 'val54', 'replaceval_1231': 'val55/file.ext',
            'replaceval_2231': 'https://val56/file.ext'}


@pytest.fixture
def testdata5(replacement_testdata5):
    pool = parse_parameter_file(dir_path+'/config/references/td5_parse_parameter_file_context_par_replace.yml',
                                replacement_testdata5, True)
    return pool


def test_parseparameterfile_contextdictinit(check, testdata5):
    check.equal(testdata5.get_parameter('nsp1', 'cmd1', 'pid1', ['dx1', 'dx2']), 'val51')
    check.equal(testdata5.get_parameter('nsp1', 'cmd1', 'pid1', ['dx2', 'dx1']), 'val52')
    check.equal(testdata5.get_parameter('nsp1', 'cmd2', 'pid2', ['dx1', 'dx2']), ['val53', 'val500'])
    check.equal(testdata5.get_parameter('nsp1', 'cmd2', 'pid2', ['dx2', 'dx1']), ['val54'])
    check.equal(testdata5.get_parameter('nsp2', 'cmd3', 'pid1', ['dx1', 'dx2']),
                File("val55/file.ext", content_type="", content_format="", onboard=True))
    check.equal(testdata5.get_parameter('nsp2', 'cmd3', 'pid1', ['dx2', 'dx1']),
                File("https://val56/file.ext", content_type="", content_format="", onboard=False))
    check.equal(testdata5.get_parameter('nsp2', 'cmd3', 'pid1', ['dx3']),
                File("https://val510.txt", content_type="", content_format=""))
    check.equal(testdata5.get_parameter('nsp2', 'cmd3', 'pid2', ['dx1', 'dx2']),
                File("", content_type="", content_format=""))
    check.equal(testdata5.get_parameter('nsp2', 'cmd3', 'pid2', ['dx2', 'dx1']),
                [File("", content_type="", content_format="")])
    check.equal(testdata5.get_parameter('nsp3', 'cmd3', 'pid2', ['dx3']),
                [File("val520/file.txt", content_type="", content_format="", onboard=True), 'val530'])


def test_parseparameterfile_useallreplacement(replacement_testdata5):
    replacement_testdata5['extra'] = 'unused'
    with pytest.raises(InvalidConfigFormatError):
        # Don't allow extra keys in replacement dict
        assert parse_parameter_file(dir_path+'/config/references/td5_parse_parameter_file_context_par_replace.yml',
                                    replacement_testdata5, True)

    # Allow extra keys in replacement dict
    assert parse_parameter_file(dir_path+'/config/references/td5_parse_parameter_file_context_par_replace.yml',
                                replacement_testdata5, False)


@pytest.mark.parametrize('del_key', ['replaceval_1111', 'replaceval_2111', 'replaceval_1122',
                                     'replaceval_2122', 'replaceval_1231', 'replaceval_2231'])
def test_parseparameterfile_readreplacement(replacement_testdata5, del_key):
    del replacement_testdata5[del_key]
    with pytest.raises(InvalidConfigFormatError):
        assert parse_parameter_file(dir_path+'/config/references/td5_parse_parameter_file_context_par_replace.yml',
                                    replacement_testdata5, False)


def test_parseparameterfile_badparams_init():
    with pytest.raises(InvalidConfigFormatError):
        assert parse_parameter_file(dir_path+'/config/references/td6_parse_parameter_file_context_badparams.yml')
