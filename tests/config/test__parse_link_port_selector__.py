import copy
import os
import pytest
from unittest.mock import Mock


from swatch.board import Board, Processor
from swatch.config import _parse_link_port_selector
from swatch.exceptions import InvalidConfigFormatError


file_path = os.path.dirname(os.path.realpath(__file__))
dir_path = os.path.dirname(file_path)


@pytest.fixture
def mockboard(request):
    bobj = Mock(spec=Board)
    bobj.id = "board"+str(request.param)
    return bobj


@pytest.fixture
def mockproc(request):
    pobj = Mock(spec=Processor)
    pobj.id = "proc"+str(request.param)
    return pobj


@pytest.fixture
def mockport(request):
    pobj = Mock(spec=Processor.AbstractPort)
    pobj.id = "port"+str(request.param)
    pobj.index = request.param
    return pobj


@pytest.mark.parametrize(
    'cfgidx, cfgstr, mockboard, mockproc, mockport', [
        (1, 'from', 0, 0, 9999),
        (1, 'to', 1, 1, 1),
        (3, 'from', 10, 111, 111),
        (4, 'to', 222, 10, 222),
        (5, 'to', 1, 1, 8888),
    ], indirect=['mockboard', 'mockproc', 'mockport'])
def test__parse_link_port_selector__init(check, td7ls, cfgidx, cfgstr, mockboard, mockproc, mockport):
    ps = _parse_link_port_selector(td7ls[cfgidx][cfgstr])
    assert ps(mockboard, mockproc, mockport)


def test__parse_link_port_selector__err_badtype(check, td7ls):
    for key in ('from', 'to'):
        bad_ps = copy.deepcopy(td7ls)
        bad_ps[1][key] = ['board0', 'proc0', 'port0']
        with check.raises(InvalidConfigFormatError):
            _parse_link_port_selector(bad_ps[1][key])

        bad_ps = copy.deepcopy(td7ls)
        bad_ps[1][key] = ('board0', 'proc0', 'port0')
        with check.raises(InvalidConfigFormatError):
            _parse_link_port_selector(bad_ps[1][key])

        bad_ps = copy.deepcopy(td7ls)
        bad_ps[1][key] = 'board0.proc0.port0'
        with check.raises(InvalidConfigFormatError):
            _parse_link_port_selector(bad_ps[1][key])


def test__parse_link_port_selector__err_badvalue(check, td7ls):
    bad_ps = copy.deepcopy(td7ls)
    bad_ps[3]['from'] = {'board': 'board0'}
    _parse_link_port_selector(bad_ps[3]['from'])

    bad_ps = copy.deepcopy(td7ls)
    bad_ps[3]['from'] = {'board': 'board0', 'INVALID': 'INVALID'}
    with check.raises(InvalidConfigFormatError):
        _parse_link_port_selector(bad_ps[3]['from'])

    bad_ps = copy.deepcopy(td7ls)
    bad_ps[3]['from'] = {'board': 123}
    with check.raises(InvalidConfigFormatError):
        _parse_link_port_selector(bad_ps[3]['from'])

    bad_ps = copy.deepcopy(td7ls)
    bad_ps[3]['from'] = {'proc': 123}
    with check.raises(InvalidConfigFormatError):
        _parse_link_port_selector(bad_ps[3]['from'])

    bad_ps = copy.deepcopy(td7ls)
    bad_ps[3]['from'] = {'port': -123}
    with check.raises(InvalidConfigFormatError):
        _parse_link_port_selector(bad_ps[3]['from'])

    bad_ps = copy.deepcopy(td7ls)
    bad_ps[3]['from'] = {'port': False}
    with check.raises(InvalidConfigFormatError):
        _parse_link_port_selector(bad_ps[3]['from'])

    bad_ps = copy.deepcopy(td7ls)
    bad_ps[3]['from'] = {'port': {'index': 123}}
    with check.raises(InvalidConfigFormatError):
        _parse_link_port_selector(bad_ps[3]['from'])
