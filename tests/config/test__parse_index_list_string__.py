import pytest

from swatch.config import _parse_index_list_string
from swatch.exceptions import InvalidConfigFormatError


def test__parse_index_list_string(check):

    ils_string = '0'
    check.equal(_parse_index_list_string(ils_string), [0])

    ils_string = '0,1,2,3,4'
    check.equal(_parse_index_list_string(ils_string), [0, 1, 2, 3, 4])

    ils_string = '0-4'
    check.equal(_parse_index_list_string(ils_string), [0, 1, 2, 3, 4])

    ils_string = '0-4,6'
    check.equal(_parse_index_list_string(ils_string), [0, 1, 2, 3, 4, 6])

    ils_string = '0-4,6,8-10'
    check.equal(_parse_index_list_string(ils_string), [0, 1, 2, 3, 4, 6, 8, 9, 10])

    ils_string = '0-4,6,10-8'
    check.equal(_parse_index_list_string(ils_string), [0, 1, 2, 3, 4, 6, 10, 9, 8])

    ils_string = '0-4,6,8-10,12'
    check.equal(_parse_index_list_string(ils_string), [0, 1, 2, 3, 4, 6, 8, 9, 10, 12])

    ils_string = '12,10-8,6,4-0'
    check.equal(_parse_index_list_string(ils_string), [12, 10, 9, 8, 6, 4, 3, 2, 1, 0])


@pytest.mark.parametrize('ils_string', [
    '1,', ',1',
    '1-', '-1',
    '1-4,', '1-,4', '1,-4',
    '1-4, 5', '4--1',
    '1-4,-5'
])
def test__parse_index_list_string_err(ils_string):

    with pytest.raises(InvalidConfigFormatError):
        _parse_index_list_string(ils_string)
