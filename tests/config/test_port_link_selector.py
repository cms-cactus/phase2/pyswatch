import pytest
from unittest.mock import Mock

from swatch.board import Board, Processor, Link
from swatch.config import PortSelector, LinkSelector


@pytest.fixture
def board():
    bobj = Mock(spec=Board)
    bobj.id = "board0"
    return bobj


@pytest.fixture
def proc():
    pcobj = Mock(spec=Processor)
    pcobj.id = "proc0"
    return pcobj


@pytest.fixture
def port():
    prtobj = Mock(spec=Processor.AbstractPort)
    prtobj.index = 99
    prtobj.id = "port0"
    return prtobj


@pytest.fixture
def ps_obj(request):
    params = request.param
    psobj = PortSelector(board_id_regex=params[0],
                         proc_id_regex=params[1],
                         port_index=params[2],
                         port_id_regex=params[3])
    return psobj


@pytest.mark.parametrize("ps_obj", [
    ([None, None, None, None]),
    (["board0", "proc0", 99, "port0"]),
    (["board0", None, None, None]),
    ([None, "proc0", None, None]),
    ([None, None, 99, None]),
    ([None, None, None, "port0"])
], indirect=True)
def test_portselector_call(board, proc, port, ps_obj):
    assert ps_obj(board, proc, port)


@pytest.mark.parametrize("ps_obj", [
    (["board1", "proc0", 99, "port0"]),
    (["board0", "proc1", 99, "port0"]),
    (["board0", "proc0", 98, "port0"]),
    (["board0", "proc0", 99, "port1"]),
    (["board1", None, None, None]),
    ([None, "proc1", None, None]),
    ([None, None, 98, None]),
    ([None, None, None, "port1"])
], indirect=True)
def test_portselector_err(board, proc, port, ps_obj):
    assert not (ps_obj(board, proc, port))

# TODO the PortSelector class does not throw error when wrong type is passed to its constructor
# TODO test wrong object type input and wrong sequence of input e.g proc, port, board


def create_link_terminus(params):
    terminus = Mock(spec=Link.Terminus)
    terminus.board = Mock(spec=Board, id=f"board_{params[1]}_link{params[0]}")
    terminus.processor = Mock(spec=Processor, id=f"proc_{params[1]}_link{params[0]}")
    terminus.port = Mock(spec=Processor.AbstractPort, index=params[2], id=f"port_{params[1]}_link{params[0]}")
    return terminus


@pytest.fixture
def mock_link(request):
    link = Mock(spec=Link)
    link.id = 'link0'
    link.source = create_link_terminus([0, 'src', 99])
    link.destination = create_link_terminus([0, 'dst', 88])
    return link


def test_linkselector_call(check, mock_link):
    port_src = PortSelector(board_id_regex="board_src_link0", proc_id_regex="proc_src_link0",
                            port_index=99, port_id_regex="port_src_link0")
    port_dst = PortSelector(board_id_regex="board_dst_link0", proc_id_regex="proc_dst_link0",
                            port_index=88, port_id_regex="port_dst_link0")
    lsobj = LinkSelector(id_regex='link0', src=port_src, dst=port_dst)
    # Regular call to LinkSelector
    check.is_true(lsobj(mock_link))

    # Call LinkSelector without match to Link ID
    lsobj = LinkSelector(id_regex=None, src=port_src, dst=port_dst)
    check.is_true(lsobj(mock_link))


@pytest.mark.parametrize("lsidreg, src_brdidreg, src_prcidreg, src_prtidreg, src_prtidx,\
                         dst_brdidreg, dst_prcidreg, dst_prtidreg, dst_prtidx", [
    ("link1", "board_src_link0", "proc_src_link0", "port_src_link0",
     99, "board_dst_link0", "proc_dst_link0", "port_dst_link0", 88),
    ("link0", "board_src_linkX", "proc_src_link0", "port_src_link0",
     99, "board_dst_link0", "proc_dst_link0", "port_dst_link0", 88),
    ("link0", "board_src_link0", "proc_src_linkX", "port_src_link0",
     99, "board_dst_link0", "proc_dst_link0", "port_dst_link0", 88),
    ("link0", "board_src_link0", "proc_src_link0", "port_src_linkX",
     99, "board_dst_link0", "proc_dst_link0", "port_dst_link0", 88),
    ("link0", "board_src_link0", "proc_src_link0", "port_src_link0",
     98, "board_dst_link0", "proc_dst_link0", "port_dst_link0", 88),
    ("link0", "board_src_link0", "proc_src_link0", "port_src_link0",
     99, "board_dst_linkX", "proc_dst_link0", "port_dst_link0", 88),
    ("link0", "board_src_link0", "proc_src_link0", "port_src_link0",
     99, "board_dst_link0", "proc_dst_linkX", "port_dst_link0", 88),
    ("link0", "board_src_link0", "proc_src_link0", "port_src_link0",
     99, "board_dst_link0", "proc_dst_link0", "port_dst_linkX", 88),
    ("link0", "board_src_link0", "proc_src_link0", "port_src_link0",
     99, "board_dst_link0", "proc_dst_link0", "port_dst_link0", 89)
])
def test_linkselector_err(mock_link, lsidreg, src_brdidreg, src_prcidreg, src_prtidreg,
                          src_prtidx, dst_brdidreg, dst_prcidreg, dst_prtidreg, dst_prtidx):
    port_src = PortSelector(board_id_regex=src_brdidreg, proc_id_regex=src_prcidreg,
                            port_index=src_prtidx, port_id_regex=src_prtidreg)
    port_dst = PortSelector(board_id_regex=dst_brdidreg, proc_id_regex=dst_prcidreg,
                            port_index=dst_prtidx, port_id_regex=dst_prtidreg)
    lsobj = LinkSelector(id_regex=lsidreg, src=port_src, dst=port_dst)

    assert not lsobj(mock_link)
