import copy
import os
import pytest

from swatch.config import parse_system_file, _parse_multiple_system_files
from swatch.exceptions import DuplicateEntryError, InvalidArgumentError, InvalidConfigFormatError, ConfigValidationError
from testutils import create_temp_test_data

# The following test script makes use of testdata7 and testdata8 fixtures.
# testdata8 is a simpler fixture and needs no dependencies. The temporary
# data file paths derived from testdata8 makes use of the native pytest
# tmp_path_global fixture. The deletion of the temporary data file path is
# handled by pytest internally. testdata7 is a more complete fixture and
# depends on other included config files. The temporary data file paths
# created from testdata7 need to be in a local directory and need manual
# creation and deletion managed by the create/delete_temp_test_data functions.

file_path = os.path.dirname(os.path.realpath(__file__))
dir_path = os.path.dirname(file_path)


def test__parse_multiple_system_files__err_noinclude(testdata8):
    with pytest.raises(InvalidArgumentError):
        _parse_multiple_system_files(testdata8)


def test__parse_multiple_system_files__err_badinclude():
    with pytest.raises(InvalidConfigFormatError):
        parse_system_file(dir_path+'/config/references/td9_parse_system_file_err_badinclude.yml')


def test__parse_multiple_system_files__err_extrafield(testdata8, tmp_path_global):
    testdata8copy = copy.deepcopy(testdata8)
    testdata8copy['extrafield'] = 'extra'
    create_temp_test_data(tmp_path_global, testdata8copy)
    with pytest.raises(ConfigValidationError):
        parse_system_file(tmp_path_global)


def test__parse_multiple_system_files__err_duplicateentry(check, tmp_path_global, testdata7):

    for testkey in ['null', 'include', 'system', 'link_settings', 'contexts', 'variables', 'exclude']:

        testdata7copy = copy.deepcopy(testdata7)
        testdata7copy['include'].append(str(tmp_path_global))

        data_in_tmptestfile = {testkey: {'key': 'value'}}
        create_temp_test_data(tmp_path_global, data_in_tmptestfile)

        if testkey != 'null':
            with check.raises(DuplicateEntryError):
                _parse_multiple_system_files(testdata7copy, dir_path +
                                             '/config/references/td7_parse_system_file_init.yml')
        else:
            _parse_multiple_system_files(testdata7copy, dir_path+'/config/references/td7_parse_system_file_init.yml')


def test__parse_multiple_system_files__err_filenotfound(testdata7):
    testdata7['include'].append('nonexistent_file.yml')
    with pytest.raises(FileNotFoundError):
        _parse_multiple_system_files(testdata7, dir_path+'/config/references/td7_parse_system_file_init.yml')
