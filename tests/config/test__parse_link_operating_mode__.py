import copy
import os
import pytest


from swatch.config import _parse_link_operating_mode
from swatch.exceptions import InvalidConfigFormatError


file_path = os.path.dirname(os.path.realpath(__file__))
dir_path = os.path.dirname(file_path)


def test__parse_link_operating_mode__init(check, td7ls):
    check.equal(str(_parse_link_operating_mode(td7ls[0]['mode'])), 'PRBS7')
    check.equal(str(_parse_link_operating_mode(td7ls[3]['mode'])), 'PRBS15')
    check.equal(str(_parse_link_operating_mode(td7ls[7]['mode'])), 'PRBS23')
    check.equal(str(_parse_link_operating_mode(td7ls[8]['mode'])), 'PRBS31')

    check.equal(repr(_parse_link_operating_mode(td7ls[1]['mode'])), 'CSP(100, 200, IdleMethod1)')
    check.equal(repr(_parse_link_operating_mode(td7ls[5]['mode'])), 'CSP(300, 400, IdleMethod2)')


def test__parse_link_operating_mode__err_badmode(td7ls):
    bad_mode = copy.deepcopy(td7ls)
    bad_mode[0]['mode'] = {'PRBS7': ''}
    with pytest.raises(InvalidConfigFormatError):
        _parse_link_operating_mode(bad_mode[0]['mode'])

    bad_mode = copy.deepcopy(td7ls)
    bad_mode[0]['mode'] = 123
    with pytest.raises(InvalidConfigFormatError):
        _parse_link_operating_mode(bad_mode[0]['mode'])

    bad_mode = copy.deepcopy(td7ls)
    bad_mode[0]['mode'] = ('PRBS7',)
    with pytest.raises(InvalidConfigFormatError):
        _parse_link_operating_mode(bad_mode[0]['mode'])


# TODO Think of a better way to test than a random  check when you have a handful of entries
# Above situation is called fuzz testing - Learn for a later iteration.
def test__parse_link_operating_mode__err_prbsmode(td7ls):
    bad_mode = copy.deepcopy(td7ls)
    bad_mode[0]['mode'] = 'PRBS99'
    with pytest.raises(InvalidConfigFormatError):
        _parse_link_operating_mode(bad_mode[0]['mode'])


def test__parse_link_operating_mode__err_cspmode(check, td7ls):
    bad_mode = copy.deepcopy(td7ls)
    bad_mode[1]['mode'] = ['CSP', 100, 200, 'idle1', 'idle2']
    with check.raises(InvalidConfigFormatError):
        _parse_link_operating_mode(bad_mode[1]['mode'])

    bad_mode = copy.deepcopy(td7ls)
    bad_mode[1]['mode'] = ['CSP', 100]
    with check.raises(InvalidConfigFormatError):
        _parse_link_operating_mode(bad_mode[1]['mode'])

    bad_mode = copy.deepcopy(td7ls)
    bad_mode[1]['mode'] = ['CSP', 100, 200, 'idle1', 300]
    with check.raises(InvalidConfigFormatError):
        _parse_link_operating_mode(bad_mode[1]['mode'])

    bad_mode = copy.deepcopy(td7ls)
    bad_mode[1]['mode'] = ['Cxp', 100, 200, 'idle1']
    with check.raises(InvalidConfigFormatError):
        _parse_link_operating_mode(bad_mode[1]['mode'])


def test__parse_link_operating_mode__err_cspmode_badvalue(check, td7ls):
    bad_mode = copy.deepcopy(td7ls)
    bad_mode[1]['mode'] = ['CSP', 100, 200, 300]
    with check.raises(InvalidConfigFormatError):
        _parse_link_operating_mode(bad_mode[1]['mode'])

    bad_mode = copy.deepcopy(td7ls)
    bad_mode[1]['mode'] = ['CSP', -100, 200]
    with check.raises(InvalidConfigFormatError):
        _parse_link_operating_mode(bad_mode[1]['mode'])

    bad_mode = copy.deepcopy(td7ls)
    bad_mode[1]['mode'] = ['CSP', 100, -200]
    with check.raises(InvalidConfigFormatError):
        _parse_link_operating_mode(bad_mode[1]['mode'])

    bad_mode = copy.deepcopy(td7ls)
    bad_mode[1]['mode'] = ['CSP', 100, 200, 'idle3']
    with check.raises(InvalidConfigFormatError):
        _parse_link_operating_mode(bad_mode[1]['mode'])
