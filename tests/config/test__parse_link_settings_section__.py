import copy
import os
import pytest

from swatch.config import _parse_link_settings_section
from swatch.exceptions import InvalidConfigFormatError


file_path = os.path.dirname(os.path.realpath(__file__))
dir_path = os.path.dirname(file_path)


def test__parse_link_settings_section__init(td7ls):
    assert _parse_link_settings_section(td7ls)

    good_lsdict1 = [{'default': '', 'mode': 'PRBS7'}]
    assert _parse_link_settings_section(good_lsdict1)

    good_lsdict2 = [{'default': '', 'mode': 'PRBS7'}, {
        'id': 'link1_4',
        'from': {'board': 'B0'},
        'to': {'board': 'B1'},
        'mode': 'PRBS31'}]
    assert _parse_link_settings_section(good_lsdict2)


def test__parse_link_settings_section__err_badinit():
    badlsdict1 = {'default': '', 'mode': 'PRBS7'}
    with pytest.raises(InvalidConfigFormatError):
        _parse_link_settings_section(badlsdict1)

    badlsdict2 = [{'default': '', 'mode': 'PRBS7'}, ['dl']]
    with pytest.raises(InvalidConfigFormatError):
        _parse_link_settings_section(badlsdict2)


def test__parse_link_settings_section__err_badkey(td7ls):
    bad_lsdict = copy.deepcopy(td7ls)
    bad_lsdict.append({'id': 'link11_14', 'badkey': 'badvalue'})
    with pytest.raises(InvalidConfigFormatError):
        _parse_link_settings_section(bad_lsdict)


def test__parse_link_settings_section__err_baddefault(check, td7ls):
    # default should be declared at the beginning
    bad_lsdict = copy.deepcopy(td7ls)
    default = bad_lsdict.pop(0)
    bad_lsdict.append(default)
    with check.raises(InvalidConfigFormatError):
        _parse_link_settings_section(bad_lsdict)

    # default cannot have 'id', 'from' or 'to' keys
    for testkey in ['id', 'from', 'to']:
        bad_lsdict = copy.deepcopy(td7ls)
        bad_lsdict[0] = {'default': '', testkey: 'badvalue'}
        with check.raises(InvalidConfigFormatError):
            _parse_link_settings_section(bad_lsdict)

    # default needs at least 'mode' or 'mask' behaviour defined
    bad_lsdict = copy.deepcopy(td7ls)
    bad_lsdict[0] = {'default': ''}
    with check.raises(InvalidConfigFormatError):
        _parse_link_settings_section(bad_lsdict)

    for testdict in [
        {'default': '', 'mode': 'prbs7'},
        {'default': '', 'mask': 'False'},
        {'default': '', 'mode': 'prbs7', 'mask': 'False'}
    ]:
        good_lsdict = copy.deepcopy(td7ls)
        good_lsdict[0] = testdict
        assert _parse_link_settings_section(good_lsdict)


def test__parse_link_settings_section__err_badidtype(check, td7ls):
    bad_lsdict = copy.deepcopy(td7ls)
    bad_lsdict[1]['id'] = 123
    with check.raises(InvalidConfigFormatError):
        _parse_link_settings_section(bad_lsdict)


def test__parse_link_settings_section__err_needsmodeormask(check, td7ls):
    bad_lsdict = copy.deepcopy(td7ls)
    bad_lsdict[1].pop('mode')
    _parse_link_settings_section(bad_lsdict)

    bad_lsdict = copy.deepcopy(td7ls)
    bad_lsdict[1].pop('mask')
    _parse_link_settings_section(bad_lsdict)

    bad_lsdict = copy.deepcopy(td7ls)
    bad_lsdict[1].pop('mode')
    bad_lsdict[1].pop('mask')
    with check.raises(InvalidConfigFormatError):
        _parse_link_settings_section(bad_lsdict)
