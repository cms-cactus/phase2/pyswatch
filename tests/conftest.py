import os
import pytest


@pytest.fixture
def tmp_path_global(tmp_path):
    # When using this fixture delete_tmp_test_data() is not needed.
    # pytest takes care to delete the files that use the fixture tmp_path.
    return os.path.join(tmp_path, 'test_data.yml')
