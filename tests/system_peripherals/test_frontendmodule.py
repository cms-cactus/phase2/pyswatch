import pytest

from swatch.system_peripherals import FrontendModule
from swatch.stubs import FrontendStub


@pytest.fixture
def fem():
    return FrontendModule(FrontendStub(id='abc', serial='def', lpgbt_fuseids=[5, 42, 13]))


def test_frontendmodule_init(fem):

    assert fem.serial == 'def'
    assert len(fem.lpgbts) == 3
    assert [x.path for x in fem.lpgbts] == ['abc.lpGBT00', 'abc.lpGBT01', 'abc.lpGBT02']
    assert [x.fuseid for x in fem.lpgbts] == [5, 42, 13]

    assert fem.lpgbt('lpGBT00').path == 'abc.lpGBT00'
    assert fem.lpgbt('lpGBT01').path == 'abc.lpGBT01'
    assert fem.lpgbt('lpGBT02').path == 'abc.lpGBT02'

    assert fem.id == 'abc'
    assert fem.alias == 'abc'
    assert fem.path == 'abc'
    assert [p.path for p in fem.processors] == ['abc.lpGBT00', 'abc.lpGBT01', 'abc.lpGBT02']

    assert [x.path for x in fem.processor('lpGBT00').inputs] == ['abc.lpGBT00.Rx00']
    assert [x.path for x in fem.processor('lpGBT00').outputs] == ['abc.lpGBT00.Tx00']
    assert [x.path for x in fem.processor('lpGBT01').inputs] == ['abc.lpGBT01.Rx00']
    assert [x.path for x in fem.processor('lpGBT01').outputs] == ['abc.lpGBT01.Tx00']
    assert [x.path for x in fem.processor('lpGBT02').inputs] == ['abc.lpGBT02.Rx00']
    assert [x.path for x in fem.processor('lpGBT02').outputs] == ['abc.lpGBT02.Tx00']


def test_frontendmodule_err_procnotfound(fem):
    with pytest.raises(RuntimeError):
        fem.processor(3)


def test_frontendmodule_err_procnotfound2(fem):
    with pytest.raises(RuntimeError):
        fem.lpgbt(3)
