import pytest

from swatch.system_peripherals import DAQBoard
from swatch.stubs import DAQBoardStub, DAQCategories


@pytest.fixture
def board():
    return DAQBoard(DAQBoardStub('idX', DAQCategories.RO, {'inpX': [101, 104, 106]}, {'outX': [202, 205, 208, 210]}))


def test_daqboard_init(board):

    assert board.category == DAQCategories.RO

    assert board.id == 'idX'
    assert board.alias == 'idX'
    assert board.path == 'idX'

    assert set(p.path for p in board.processors) == {'idX.inpX', 'idX.outX'}
    assert set(p.path for p in board.processor('inpX').inputs) == {'idX.inpX.Rx101', 'idX.inpX.Rx104',
                                                                   'idX.inpX.Rx106'}
    assert len(list(p for p in board.processor('inpX').outputs)) == 0
    assert len(list(p for p in board.processor('outX').inputs)) == 0
    assert set(p.path for p in board.processor('outX').outputs) == {'idX.outX.Tx202', 'idX.outX.Tx205',
                                                                    'idX.outX.Tx208', 'idX.outX.Tx210'}


def test_daqboard_err_procnotfound(board):
    with pytest.raises(RuntimeError):
        board.processor('not_found')
