import pytest

from swatch.system_peripherals import PeripheralBaseModule


class Stub:
    def __init__(self, id):
        self.id = id


@pytest.fixture
def peribm():
    stub = Stub(id="test_id")
    return PeripheralBaseModule(stub)


def test_peripheralbasemodule_init(peribm):

    assert peribm.id == "test_id"
    assert peribm.alias == "test_id"
    assert peribm.path == "test_id"
    assert list(peribm.processors) == []


def test_peripheralbasemodule_err_procnotfound(peribm):
    with pytest.raises(KeyError):
        peribm.processor("non_existent_processor")
