from swatch.system_peripherals import PeripheralOutputPort, PeripheralInputPort


def test_peripheraloutputport_init():

    outport = PeripheralOutputPort(port_id='abc', parent_path='def', index=1)

    assert outport.id == 'abc'
    assert outport.alias == 'abc'
    assert outport.path == 'def.abc'
    assert outport.index == 1
    assert outport.is_masked is False


def test_peripheralinputport_init():

    inport = PeripheralInputPort(port_id='abc', parent_path='def', index=1)

    assert inport.id == 'abc'
    assert inport.alias == 'abc'
    assert inport.path == 'def.abc'
    assert inport.index == 1
    assert inport.is_masked is False
