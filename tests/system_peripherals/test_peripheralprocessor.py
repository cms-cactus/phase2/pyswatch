import pytest

from swatch.system_peripherals import PeripheralProcessor


@pytest.fixture
def proc():
    return PeripheralProcessor(obj_id='abc', parent_path='def', inputs=[0, 1, 11], outputs=[0, 11, 101, 1001])


def test_peripheralprocessor_init(proc):

    assert proc.id == 'abc'
    assert proc.alias == 'abc'
    assert proc.path == 'def.abc'
    assert proc.is_included is True

    assert [port.id for port in proc.inputs] == ['Rx00', 'Rx01', 'Rx11']
    assert [port.path for port in proc.inputs] == ['def.abc.Rx00', 'def.abc.Rx01', 'def.abc.Rx11']
    assert [port.index for port in proc.inputs] == [0, 1, 11]

    assert [port.id for port in proc.outputs] == ['Tx00', 'Tx11', 'Tx101', 'Tx1001']
    assert [port.path for port in proc.outputs] == ['def.abc.Tx00', 'def.abc.Tx11', 'def.abc.Tx101', 'def.abc.Tx1001']
    assert [port.index for port in proc.outputs] == [0, 11, 101, 1001]


def test_peripheralprocessor_set_included(proc):

    assert proc.is_included is True

    proc.exclude()
    assert proc.is_included is False

    proc.include()
    assert proc.is_included is True


def test_peripheralprocessor_err_get_input(proc):

    assert proc.input(1).id == 'Rx01'
    assert proc.input(1).path == 'def.abc.Rx01'
    assert proc.input(1).index == 1

    with pytest.raises(RuntimeError):
        assert proc.input(101).id == 'Rx101'


def test_peripheralprocessor_err_get_output(proc):

    assert proc.output(101).id == 'Tx101'
    assert proc.output(101).path == 'def.abc.Tx101'
    assert proc.output(101).index == 101

    with pytest.raises(RuntimeError):
        assert proc.output(1).id == 'Tx01'
