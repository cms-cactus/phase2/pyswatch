import pytest

from swatch.system_peripherals import LpGBT


@pytest.fixture
def lpgbt():
    return LpGBT(obj_id='abc', parent_path='def', fuseid=123)


def test_lpgbt_init(lpgbt):

    assert lpgbt.fuseid == 123

    assert lpgbt.id == 'abc'
    assert lpgbt.alias == 'abc'
    assert lpgbt.path == 'def.abc'
    assert lpgbt.is_included is True

    assert [port.id for port in lpgbt.inputs] == ['Rx00']
    assert [port.path for port in lpgbt.inputs] == ['def.abc.Rx00']
    assert [port.index for port in lpgbt.inputs] == [0]

    assert [port.id for port in lpgbt.outputs] == ['Tx00']
    assert [port.path for port in lpgbt.outputs] == ['def.abc.Tx00']
    assert [port.index for port in lpgbt.outputs] == [0]


def test_lpgbt_set_included(lpgbt):

    assert lpgbt.is_included is True

    lpgbt.exclude()
    assert lpgbt.is_included is False

    lpgbt.include()
    assert lpgbt.is_included is True


def test_lpgbt_err_get_input(lpgbt):

    assert lpgbt.input(0).id == 'Rx00'

    with pytest.raises(RuntimeError):
        assert lpgbt.input(1).id == 'Rx01'


def test_lpgbt_err_get_output(lpgbt):

    assert lpgbt.output(0).id == 'Tx00'

    with pytest.raises(RuntimeError):
        assert lpgbt.output(1).id == 'Tx01'
