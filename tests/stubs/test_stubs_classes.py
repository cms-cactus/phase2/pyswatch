import pytest
from dataclasses import FrozenInstanceError

from swatch.stubs import DAQCategories, DAQBoardStub, FrontendStub


@pytest.fixture
def daqboard():
    return DAQBoardStub('daq1', DAQCategories.RO, {'inp0': [11, 12, 13], 'inp1': [21, 22, 23]},
                        {'out0': [31, 32, 33], 'out1': [41, 42, 43]})


def test_daqboardstub_init(daqboard):
    assert daqboard.id == 'daq1'
    assert daqboard.category == DAQCategories.RO
    assert daqboard.inputs == {'inp0': [11, 12, 13], 'inp1': [21, 22, 23]}
    assert daqboard.outputs == {'out0': [31, 32, 33], 'out1': [41, 42, 43]}


def test_daqboardstub_err_immutable(daqboard):
    with pytest.raises(FrozenInstanceError):
        daqboard.id = 'daq2'
    with pytest.raises(FrozenInstanceError):
        daqboard.category = DAQCategories.RO
    with pytest.raises(FrozenInstanceError):
        daqboard.inputs = {'inp0': [111, 112, 113]}
    with pytest.raises(FrozenInstanceError):
        daqboard.outputs = {'out0': [331, 332, 333]}


def test_daqboardstub_err_badinit():
    with pytest.raises(TypeError):
        DAQBoardStub('daq1')
    with pytest.raises(TypeError):
        DAQBoardStub('daq1', DAQCategories.RO)
    with pytest.raises(TypeError):
        DAQBoardStub('daq1', DAQCategories.RO, {'inp0': [11, 12, 13]})
    with pytest.raises(TypeError):
        DAQBoardStub(id='daq1', category=DAQCategories.RO, outputs={'inp0': [11, 12, 13]})
    with pytest.raises(TypeError):
        DAQBoardStub(id='daq1', inputs={}, outputs={'out0': [11, 12, 13]})
    with pytest.raises(TypeError):
        DAQBoardStub(category=DAQCategories.RO, inputs='daq1', outputs={'inp0': [11, 12, 13]})


def test_daqcategories():
    assert DAQCategories.RO.value == 'RO'


def test_daqcategories_attributes():
    expected_attributes = {'RO'}
    actual_attributes = {attrb.name for attrb in DAQCategories}

    assert actual_attributes == expected_attributes


@pytest.fixture
def frontendmod():
    return FrontendStub('fe_mod_X', 'mfg_dummy_serialX', [101, 102, 103])


def test_frontendstub_init(frontendmod):
    assert frontendmod.id == 'fe_mod_X'
    assert frontendmod.serial == 'mfg_dummy_serialX'
    assert frontendmod.lpgbt_fuseids == [101, 102, 103]


def test_frontendstub_err_immutable(frontendmod):
    with pytest.raises(FrozenInstanceError):
        frontendmod.id = 'fe_mod_Y'
    with pytest.raises(FrozenInstanceError):
        frontendmod.serial = 'serial_Y'
    with pytest.raises(FrozenInstanceError):
        frontendmod.lpgbt_fusefields = [110]


def test_frontendstub_err_badinit():
    with pytest.raises(TypeError):
        FrontendStub(serial='srlX', lpgbt_fuseids=[101])
    with pytest.raises(TypeError):
        FrontendStub(id='dummyX', lpgbt_fuseids=[101])
    with pytest.raises(TypeError):
        FrontendStub(id='dummyX', serial='srlX')
    with pytest.raises(TypeError):
        FrontendStub(id='dummyX', serial='srlX', lpgbt_fuseids=[101], extra='xtra')
