import os
import yaml


def create_temp_test_data(tmpdatapath, testdatacopy):
    with open(tmpdatapath, 'w') as f:
        yaml.dump(testdatacopy, f)
    return


def delete_temp_test_data(tmpdatapath):
    os.remove(tmpdatapath)
    return
