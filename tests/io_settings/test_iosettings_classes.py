import pytest
from dataclasses import FrozenInstanceError

from swatch.io_settings import PRBSMode, IdleMethod, CSPSettings


def test_prbsmode(check):

    check.equal(str(PRBSMode.PRBS7), 'PRBS7')
    check.equal(str(PRBSMode.PRBS9), 'PRBS9')
    check.equal(str(PRBSMode.PRBS15), 'PRBS15')
    check.equal(str(PRBSMode.PRBS23), 'PRBS23')
    check.equal(str(PRBSMode.PRBS31), 'PRBS31')


def test_prbsmode_attributes():
    expected_attributes = {'PRBS7', 'PRBS9', 'PRBS15', 'PRBS23', 'PRBS31'}
    actual_attributes = {attrb.name for attrb in PRBSMode}

    assert actual_attributes == expected_attributes


def test_idlemethod(check):

    check.equal(str(IdleMethod.Method1), 'IdleMethod1')
    check.equal(str(IdleMethod.Method2), 'IdleMethod2')


def test_idlemethod_attributes():
    expected_attributes = {'Method1', 'Method2'}
    actual_attributes = {attrb.name for attrb in IdleMethod}

    assert actual_attributes == expected_attributes


def test_cspsettings_init():
    csp = CSPSettings(100, 200, IdleMethod.Method1)
    assert csp.packet_length == 100
    assert csp.packet_periodicity == 200
    assert csp.idle_method == IdleMethod.Method1


def test_cspsettings_repr():
    csp = CSPSettings(100, 200, IdleMethod.Method1)
    assert repr(csp) == 'CSP(100, 200, IdleMethod1)'


def test_cspsettings_immutable():
    csp = CSPSettings(100, 200, IdleMethod.Method1)
    with pytest.raises(FrozenInstanceError):
        csp.packet_length = 200
    with pytest.raises(FrozenInstanceError):
        csp.packet_periodicity = 300
    with pytest.raises(FrozenInstanceError):
        csp.idle_method = IdleMethod.Method2

# Ensure unit test is updated when CSPSettings defintion is changed


def test_cspsettings_attrbs():
    expected_fields = {'packet_length', 'packet_periodicity', 'idle_method'}
    actual_fields = {f for f in CSPSettings.__dataclass_fields__}

    assert actual_fields == expected_fields

    expected_types = {
        'packet_length': int,
        'packet_periodicity': int,
        'idle_method': IdleMethod,
    }
    actual_types = {f: CSPSettings.__dataclass_fields__[f].type for f in CSPSettings.__dataclass_fields__}

    assert actual_types == expected_types
