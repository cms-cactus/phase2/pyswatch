import click
import os
import pytest

from rich.console import Console
from swatch.cli.demonstrators import _makemultifile_pool_list
from swatch.parameters import File
from swatch.config import parse_parameter_file

file_path = os.path.dirname(os.path.realpath(__file__))
dir_path = os.path.dirname(file_path)


@pytest.fixture
def console():
    return Console()


@pytest.fixture
def config():
    return dir_path+'/demonstrators/references/testdata1_config_pars.yml'


@pytest.fixture
def replacements():
    return {'var1': 'value1', 'var2': 'value2'}


@pytest.fixture
def infilepath():
    return 'd1/d2/d3/in_data{file_index:03}.format'


@pytest.fixture
def outfilepath():
    return 'd1/d2/d3/out_data{file_index:04}.format'


def test_makemultifile_pool_list_init_v1(check, console, config, replacements, infilepath, outfilepath):

    fileindices = ''
    result = _makemultifile_pool_list(console, config, replacements, 'd1/d2/d3/in_data.format',
                                      'd1/d2/d3/out_data.format', fileindices)

    replacements['input_data_file'] = File('d1/d2/d3/in_data.format', content_type='', content_format='')
    pool = parse_parameter_file(config, replacements=replacements, use_all_replacements=True)
    check.equal(result[0]['pool']._parameters, pool._parameters)
    check.equal(result[0]['outfile_path'], 'd1/d2/d3/out_data.format')

    fileindices = '11-11'
    result = _makemultifile_pool_list(console, config, replacements, infilepath, outfilepath, fileindices)

    replacements['input_data_file'] = File('d1/d2/d3/in_data011.format', content_type='', content_format='')
    pool = parse_parameter_file(config, replacements=replacements, use_all_replacements=True)
    check.equal(result[0]['pool']._parameters, pool._parameters)
    check.equal(result[0]['outfile_path'], 'd1/d2/d3/out_data0011.format')


@pytest.fixture
def repl_list1(config, replacements):
    repl_list1 = []
    for idx in range(98, 102):
        replacements['input_data_file'] = File('d1/d2/d3/in_data{:03}.format'.format(idx),
                                               content_type='', content_format='')
        pool = parse_parameter_file(config, replacements=replacements, use_all_replacements=True)
        repl_list1.append({'pool': pool, 'outfile_path': 'd1/d2/d3/out_data{:04}.format'.format(idx)})
    return repl_list1


def test_makemultifile_pool_list_init_v2(check, config, console, replacements, infilepath, outfilepath, repl_list1):
    fileindices = '98-101'
    result = _makemultifile_pool_list(console, config, replacements, infilepath, outfilepath, fileindices)
    for res, repl in zip(result, repl_list1):
        check.equal(res['pool']._parameters, repl['pool']._parameters)
        check.equal(res['outfile_path'], repl['outfile_path'])

    fileindices = '098-101'
    result = _makemultifile_pool_list(console, config, replacements, infilepath, outfilepath, fileindices)
    for res, repl in zip(result, repl_list1):
        check.equal(res['pool']._parameters, repl['pool']._parameters)
        check.equal(res['outfile_path'], repl['outfile_path'])


@pytest.fixture
def repl_list2(config, replacements):
    repl_list2 = []
    for idx in range(101, 97, -1):
        replacements['input_data_file'] = File('d1/d2/d3/in_data{:03}.format'.format(idx),
                                               content_type='', content_format='')
        pool = parse_parameter_file(config, replacements=replacements, use_all_replacements=True)
        repl_list2.append({'pool': pool, 'outfile_path': 'd1/d2/d3/out_data{:04}.format'.format(idx)})
    return repl_list2


def test_makemultifile_pool_list_init_v3(check, console, config, replacements, infilepath, outfilepath, repl_list2):
    fileindices = '101-98'
    result = _makemultifile_pool_list(console, config, replacements, infilepath, outfilepath, fileindices)
    for res, repl in zip(result, repl_list2):
        check.equal(res['pool']._parameters, repl['pool']._parameters)
        check.equal(res['outfile_path'], repl['outfile_path'])

    fileindices = '101-098'
    result = _makemultifile_pool_list(console, config, replacements, infilepath, outfilepath, fileindices)
    for res, repl in zip(result, repl_list2):
        check.equal(res['pool']._parameters, repl['pool']._parameters)
        check.equal(res['outfile_path'], repl['outfile_path'])


def test_makemultifile_pool_list_err_badfilepath(check, console, config, replacements, infilepath, outfilepath):

    fileindices = '98-101'

    badinfilepath = 'd1/d2/d3/in_data_format.txt'
    with check.raises(click.BadParameter):
        _makemultifile_pool_list(console, config, replacements, badinfilepath, outfilepath, fileindices)

    badinfilepath = 'd1/d2/d3/in_data_format{file_idx}.txt'
    with check.raises(click.BadParameter):
        _makemultifile_pool_list(console, config, replacements, badinfilepath, outfilepath, fileindices)

    badinfilepath = 'd1/d2/d3/in_data_format{file_idx:}.txt'
    with check.raises(click.BadParameter):
        _makemultifile_pool_list(console, config, replacements, badinfilepath, outfilepath, fileindices)

    badoutfilepath = 'd1/d2/d3/out_data_format.txt'
    with check.raises(click.BadParameter):
        _makemultifile_pool_list(console, config, replacements, infilepath, badoutfilepath, fileindices)

    badoutfilepath = 'd1/d2/d3/out_data_format{file_idx}.txt'
    with check.raises(click.BadParameter):
        _makemultifile_pool_list(console, config, replacements, infilepath, badoutfilepath, fileindices)

    badinfilepath = 'd1/d2/d3/in_data_format{file_idx:}.txt'
    with check.raises(click.BadParameter):
        _makemultifile_pool_list(console, config, replacements, badinfilepath, outfilepath, fileindices)


def test_makemultifile_pool_list_err_badfileindices(check, config, console, replacements, infilepath, outfilepath):

    for fileindices in ['_96-101', '96-_101', '96', '96to101', '96-', '-101']:
        with check.raises(click.BadParameter):
            _makemultifile_pool_list(console, config, replacements, infilepath, outfilepath, fileindices)
