# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.4.5] - 2023-02-02

### Added

* Add log messages to all key functions, and corresponding ``--log-level`` flag for all scripts/tools (!92)
* Support for tracing key high-level procedures, both local and those on HERD (!92)
* Support for links to externally-controlled boards (!99, !101)

### Changed

* CI: Migrate to k8s executors (!95)
* Update to HERD v0.4.8

### Fixed

* Board data: Resolve bug in parsing of EMPv2 files that have more spaces than references (!97)
* Devcontainer: Resolve GPG keys issue (!98)
* Devcontainer: Resolve psutils build error affecting arm64 machines (!98)


## [0.4.4] - 2023-11-30

### Added

* `board-manager show actions`: Lists registered commands and FSMs for a specific processor or service module (!82)
* `board-manager show fsm`: Lists a specific FSM's transitions, the commands that make them up, and their parameters (!84)
* Board data: Support for APx files without sideband
* `convert-board-data`: Converts board data files between different formats - including support for adding/removing packet structure as is required to convert between APx files without sideband information and other formats.

### Changed

* Board data: Speed up EMPv2 write function (!91)
* Resolve issues highlighted by pylint warnings (!86, !87)

### Fixed

* Board data: Resolve bug in parsing of single-channel APx data files (!81)
* Checks for missing parameters: Fix misleading message arising from accidental use of loop variable outside of for loop (!83)
* Resolve offsets in timestamps due to incorrect handling of timezones (!89)


## [0.4.3] - 2023-10-16

### Added

* `board-manager console`: An interactive console for running `board-manager` subcommands (#28, !67)
* `board-manager show links`: Displays link table formerly shown by `show system`, with ability to filter based on link ID, board, processor, port and/or optical module (#26, !68)
* Support for excluding boards/processors, via new `exclude` section in config file (#23,!70)
* Support for routing all traffic via a SOCKS proxy, by setting `SWATCH_SOCKS_PROXY` environment variable (!69)
* Support for saving output of command-line tools (four formats: plain text, text with ANSI style codes, SVG, HTML) (!65)
* Board manager: Print suggestions if subcommands are misspelt (!80)

### Changed

* `board-manager`: Gracefully handle exceptions from callbacks of subcommands (#32, !77)
* `board-manager show board-inputs/outputs`: Add link ID to table, to simplify debugging of link issues (#25, !66)
* `board-manager run-command`: Refresh monitoring data after executing the command (!73)
* Link test runner script: Finish in `RxConfigured` state rather than `Synchronized`, so that links are left in configured state at end of script (!76)
* Remove disabled metrics from metric summary tables (e.g. in `board-manager show metrics`), since their data will be stale (!74)
* Update source code to follow PEP8 naming conventions (!64)

### Fixed

* Link and slice test runner scripts: Resolve issues in handling of board-internal links (#20, #21, !72)
* Board data files: Remove hardcoded 1024 frame limit from read functions (!78)


## [0.4.2] - 2023-08-08

### Added

- Support for masking links in configuration file (#22)
- `merge-board-data` script, which combines channels from multiple board data files into one file (!61)

### Changed

- Update core classes to use `httpx` for HTTP communication, rather than `requests` (!59)
- Improve speed of link and slice tests by parallesing transitions using asyncio (!59)


## [0.4.1] - 2023-07-20

### Added

- Board data files: Support for reading gzip-compressed files (!55)
- Board manager CLI: Various new action-, monitoring and system-related subcommands (!53, !57)
- Plugin test suite: Tests for inter-FPGA copper links (!58)

### Changed

- Link test runner script: Check status of optical modules (#18)
- Link test runner script: Add option to mask all exception certain links (#19)
- Board data: Add unit tests for file parsing & writing; split off core classes and functions into dedicated module (!55)
- Board data: Significant performance improvements in core classes (numpy-based implementation) (!58)
