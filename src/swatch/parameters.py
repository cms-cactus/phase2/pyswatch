
import base64
import httpx
import logging
import math
import os

from .utilities.tracing import EventTimer
from urllib.parse import urlparse

_LOGGER = logging.getLogger('shep')


class Parameter:
    def __init__(self, param_id, param_type, description, rule=None):
        self.id = param_id
        self.type = param_type
        self.description = description
        self.rule = rule

    def check_type(self, value):
        if self.type == 'bool':
            return type(value) is bool
        elif self.type in {'uint16', 'uint32', 'uint64', 'int32', 'int64'}:
            return type(value) is int
        elif self.type in {'float', 'double'}:
            return type(value) is float
        elif self.type == 'string':
            return type(value) is str
        elif self.type.startswith('vector:'):
            if type(value) is not list:
                return False
            for x in value:
                if self.type == 'vector:bool':
                    if type(x) is not bool:
                        return False
                elif self.type in {'vector:uint16', 'vector:uint32', 'vector:uint64', 'vector:int32', 'vector:int64'}:
                    if type(x) is not int:
                        return False
                elif self.type == 'vector:float':
                    if type(x) is not float:
                        return False
                elif self.type == 'vector:string':
                    if type(x) is not str:
                        return False
                elif self.type == 'vector:file':
                    if type(x) is not File:
                        return False
                else:
                    raise RuntimeError(f'Parameter "{self.id}" has unknown type "{self.type}"')
            return True
        elif self.type == 'file':
            return type(value) is File
        else:
            raise RuntimeError(f'Parameter "{self.id}" has unknown type "{self.type}"')

    def check_value(self, value):
        '''Checks value against rule; returns true if passes rule'''
        if self.rule is None:
            return True
        if not self.check_type(value):
            return False
        return self.rule(value)


class File:
    # TODO: path -> URL
    def __init__(self, url, *, content_type, content_format, onboard=False):
        self.url = url
        self._type = content_type
        self._format = content_format
        self.onboard = onboard
        self._content = None
        self._fetch_failed = False

    def __eq__(self, other):
        isequals = True
        isequals = isequals and (self.url.endswith(other.url) or
                                 other.url.endswith(self.url))
        isequals = isequals and self._type == other._type
        isequals = isequals and self._format == other._format
        isequals = isequals and self.onboard == other.onboard
        if other.url == '' and self.url == '':  # Equality for null declaration
            isequals = True
        return isequals

    @property
    def type(self):
        return self._type

    @property
    def format(self):
        return self._format

    def exists(self):
        if self._fetch_failed:
            return False
        try:
            self.fetch()
            return True
        except Exception:  # pylint: disable=broad-exception-caught
            return False

    def fetch(self):
        '''Downloads any files '''
        if self.onboard or (self._content is not None):
            return

        url = urlparse(self.url)
        if url.scheme in ['', 'file']:
            with open(url.path, mode='rb') as file:
                self._content = file.read()
        elif url.scheme in ['http', 'https']:
            _LOGGER.info('Downloading %s', self.url)
            with EventTimer('File download'):
                r = httpx.get(self.url, follow_redirects=True, headers={'Accept-Encoding': 'gzip'})
                if r.status_code != 200:
                    self._fetch_failed = True
                    r.raise_for_status()
                self._content = r.content
        else:
            raise RuntimeError(f'Unsupported URL scheme for file {self.url}')

    def __repr__(self):
        if self.onboard and len(self.url) == 0:
            return 'File()'
        elif self.onboard:
            return f'File("{self.url}", onboard, type="{self.type}", format="{self.format})'
        else:
            return f'File("{self.url}", type="{self.type}", format="{self.format}")'


def _serialize_file(x):
    if x.onboard:
        return {'path': x.url, 'type': x.type, 'format': x.format}
    else:
        result = {'name': os.path.basename(x.url), 'type': x.type, 'format': x.format}
        x.fetch()
        result['contents'] = base64.b64encode(x._content).decode('utf-8')
        return result


def serialize_parameter(x):
    if type(x) is File:
        return _serialize_file(x)
    elif type(x) is list:
        return [_serialize_file(y) if type(y) is File else y for y in x]
    else:
        return x


def serialize_parameter_set(param_set):
    return {x: serialize_parameter(param_set[x]) for x in param_set}


def serialize_parameter_set_list(param_set_list):
    return [{x: serialize_parameter(pset[x]) for x in pset} for pset in param_set_list]


def create_rule(rule_spec):
    if rule_spec is None:
        return None

    if type(rule_spec) is not list:
        raise RuntimeError('Invalid object type for rule specification')

    if len(rule_spec) == 0:
        raise RuntimeError('Invalid object type for rule specification (empty list)')

    if len(rule_spec) == 1:
        return UnknownRule(rule_spec[0])

    rule_types = {
        'FiniteNumber': FiniteNumber,
        'FiniteVector': FiniteVector,
        'GreaterThan': GreaterThan,
        'InRange': InRange,
        'IsAmong': IsAmong,
        'LesserThan': LesserThan,
        'NonEmptyString': NonEmptyString,
        'OfSize': OfSize,
        'OutOfRange': OutOfRange,
        'All': All
    }
    if rule_spec[1] in rule_types:
        return rule_types[rule_spec[1]](rule_spec[0], *rule_spec[2:])
    else:
        raise RuntimeError(f'create_rule: Encountered unknown rule type, {rule_spec[1]}')


class UnknownRule:
    def __init__(self, description):
        self.description = description

    def __call__(self, x):
        return True


class FiniteNumber:
    def __init__(self, description):
        self.description = description

    def __call__(self, x):
        return math.isfinite(x)


class FiniteVector:
    def __init__(self, description):
        self.description = description

    def __call__(self, xx):
        return all([math.isfinite(x) for x in xx])


class GreaterThan:
    def __init__(self, description, lower_bound):
        self.description = description
        self.lower_bound = lower_bound

    def __call__(self, x):
        return x > self.lower_bound


class InRange:
    def __init__(self, description, lower_bound, upper_bound):
        self.description = description
        self.lower_bound = lower_bound
        self.upper_bound = upper_bound

    def __call__(self, x):
        return (x > self.lower_bound) and (x < self.upper_bound)


class IsAmong:
    def __init__(self, description, *allowed_values):
        self.description = description
        self.allowed_values = allowed_values

    def __call__(self, x):
        return x in self.allowed_values


class LesserThan:
    def __init__(self, description, upper_bound):
        self.description = description
        self.upper_bound = upper_bound

    def __call__(self, x):
        return x < self.upper_bound


class NonEmptyString:
    def __init__(self, description):
        self.description = description

    def __call__(self, x):
        return len(x) > 0


class OfSize:
    def __init__(self, description, expected_size):
        self.description = description
        self.expected_size = expected_size

    def __call__(self, x):
        return len(x) == self.expected_size


class OutOfRange:
    def __init__(self, description, lower_bound, upper_bound):
        self.description = description
        self.lower_bound = lower_bound
        self.upper_bound = upper_bound

    def __call__(self, x):
        return (x < self.lower_bound) or (self.upper_bound < x)


class All:
    def __init__(self, description, sub_rule_settings):
        self.description = description
        self.sub_rule = create_rule(sub_rule_settings)

    def __call__(self, xx):
        return all([self.sub_rule(x) for x in xx])


class Constraint:
    def __init__(self, name, description, parameters):
        self.name = name
        self.description = description
        self.parameters = parameters
