
import httpx
import json
import logging
import os
import time
from sys import is_finalizing as sys_is_finalizing

from .action import _RUNNER, ActionableObject, Command, Transition
from .io_settings import CSPSettings, IdleMethod, PRBSMode
from .monitoring import PropertiedMonitorableObject, Status
from .utilities.misc import format_index_list
from .utilities.tracing import EventTimer, perf_time, register_external_process, time_function
from dataclasses import dataclass
from datetime import datetime, timezone
from enum import Enum, unique
from typing import Mapping, Optional, Sequence, Union
from swatch import history

_HEADERS = {'Content-Type': "application/json", 'Accept': "application/json", 'Accept-Encoding': 'gzip'}
_LOGGER = logging.getLogger('shep')
_TIMEOUT = httpx.Timeout(1.0, connect=2.0, read=30.0, write=30.0)


def _log_http_request(request):
    _LOGGER.debug('Sending HTTP %s request %s (%s bytes)', request.method, request.url, len(request.content))
    request.hook_precisetime = perf_time()


def _log_http_response(response):
    request = response.request
    response.hook_precisetime = perf_time()
    _LOGGER.debug('Received HTTP %s response %s', request.method, request.url)  # ({len(response.content)} bytes)')


async def _alog_http_request(request):
    _log_http_request(request)


async def _alog_http_response(response):
    _log_http_response(response)


@unique
class PackageBuildType(Enum):
    MANUAL = 'Manual'
    GITLABCI = 'GitLabCI'


@unique
class PackageVCSType(Enum):
    GIT = 'git'


@unique
class PackageGitRefType(Enum):
    COMMIT = 'commit'
    BRANCH = 'branch'
    TAG = 'tag'


@dataclass(frozen=True)  # TODO From Python 3.10: kw_only=True
class PackageGitInfo:
    type: PackageVCSType
    sha: str
    ref_type: PackageGitRefType
    ref_name: str
    clean: bool


@dataclass(frozen=True)  # TODO From Python 3.10: kw_only=True
class PackageManualBuildInfo:
    type: PackageBuildType
    time: datetime


@dataclass(frozen=True)  # TODO From Python 3.10: kw_only=True
class PackageGitLabCIBuildInfo:
    type: PackageBuildType
    time: datetime
    server_url: str
    project_path: str
    project_id: int
    pipeline_id: int
    job_id: int


@dataclass(frozen=True)  # TODO From Python 3.10: kw_only=True
class PackageVersion:
    major: int
    minor: int
    patch: int


@dataclass(frozen=True)  # TODO From Python 3.10: kw_only=True
class PackageInfo:
    name: str
    version: PackageVersion
    build: Union[PackageManualBuildInfo, PackageGitLabCIBuildInfo]
    vcs: PackageGitInfo


_PRBS_DECODER = {
    'PRBS7': PRBSMode.PRBS7,
    'PRBS9': PRBSMode.PRBS9,
    'PRBS15': PRBSMode.PRBS15,
    'PRBS23': PRBSMode.PRBS23,
    'PRBS31': PRBSMode.PRBS31
}

_PRBS_ENCODER = {
    PRBSMode.PRBS7: 'PRBS7',
    PRBSMode.PRBS9: 'PRBS9',
    PRBSMode.PRBS15: 'PRBS15',
    PRBSMode.PRBS23: 'PRBS23',
    PRBSMode.PRBS31: 'PRBS31'
}


def _encode_mode(x: Union[PRBSMode, CSPSettings]):
    if type(x) is CSPSettings:
        idle_method = 'idle1' if x.idle_method == IdleMethod.Method1 else 'idle2'
        return ['CSP', x.packet_length, x.packet_periodicity, idle_method]
    else:
        return [_PRBS_ENCODER[x]]


class Lease:
    def __init__(self, board, duration):
        self._token = None
        self.board = board
        self._obtain(duration)

    def __del__(self):
        self.retire()

    def _obtain(self, duration):
        _LOGGER.info('Obtaining %s-second lease from %s (%s)',
                     duration, self.board.id, self.board._sync_client.base_url)

        req_data = {'supervisor': 'Python library ({})'.format(os.uname()[1]), 'duration': duration}
        # print(f"Obtaining lease @ {board._baseURL}/lease/obtain")
        self.start = time.time()
        try:
            r = self.board._sync_client.put('/lease/obtain', json=req_data)
            r.raise_for_status()
        except httpx.RequestError as exc:
            raise RuntimeError(f"An error occurred while requesting {exc.request.url!r}.")
        except httpx.HTTPStatusError as exc:
            _LOGGER.error('Failed to obtain lease for %s', self.board.id)
            raise RuntimeError(f'Error response {exc.response.status_code} while obtaining lease from '
                               f'{self.board._baseURL} : {exc.response.text}.')

        result = json.loads(r.text)
        # print("Response: " + r.text)

        self._token = result['lease']
        self.end = self.start + result['duration']

    def renew(self, duration):
        _LOGGER.info('Renewing lease of %s (%s) for %s seconds',
                     self.board.id, self.board._sync_client.base_url, duration)
        if self._token is None:
            raise RuntimeError('Lease has already been retired.')

        if self.end < time.time():
            self._obtain(duration)
            return

        req_time = time.time()
        req_data = {'lease': self._token, 'duration': duration}
        r = self.board._sync_client.put('/lease/renew', json=req_data)
        if r.status_code != 200:
            raise RuntimeError("Control app reported an error when renewing a lease: " + r.text)

        result = json.loads(r.text)
        # print("Response: " + r.text)
        self.end = req_time + result['duration']

    def retire(self):
        if not sys_is_finalizing():
            _LOGGER.info('Retiring lease of %s (%s)', self.board.id, self.board._sync_client.base_url)
        if (self._token is not None) and (self.end >= time.time()):
            self.end = time.time()
            r = self.board._sync_client.put('/lease/retire', json={'lease': self._token})
            if r.status_code != 200:
                raise RuntimeError("Control app reported an error when retiring a lease: " + r.text)
            # print("Response: " + r.text)

        self._token = None


class Device(ActionableObject):

    def __init__(self, obj_id, parent, *,
                 raw_info, raw_action_info, contexts, raw_mon_data, custom_monitorables,
                 refresh_callback, async_refresh_callback):
        super().__init__(obj_id,
                         parent,
                         raw_info=raw_info,
                         raw_action_info=raw_action_info,
                         contexts=contexts,
                         raw_mon_data=raw_mon_data,
                         custom_monitorables=custom_monitorables)
        self.role = raw_info['role']
        self.type = raw_info['type']
        self.uri = raw_info['uri']
        self.addressTable = raw_info['addressTable']
        self.refresh = refresh_callback
        self.async_refresh = async_refresh_callback


class ServiceModule(Device):

    def __init__(self, obj_id, board, *,
                 raw_info, raw_action_info, contexts, raw_mon_data, refresh_callback, async_refresh_callback):
        self._client = board._client
        self._sync_client = board._sync_client
        super().__init__(obj_id,
                         board,
                         raw_info=raw_info,
                         raw_action_info=raw_action_info,
                         contexts=contexts,
                         raw_mon_data=raw_mon_data,
                         custom_monitorables={},
                         refresh_callback=refresh_callback,
                         async_refresh_callback=async_refresh_callback)
        self._update(raw_info, raw_mon_data)

    def _update(self, raw_info, raw_mon_data):
        super()._update(raw_info, raw_mon_data, custom_monitorables={})


class Processor(Device):

    @dataclass(frozen=True)  # TODO From Python 3.10: kw_only=True
    class LinkSourceInfo:
        '''Represents sender info for a CSP link'''
        crate: int
        slot: int
        channel: int

    class AbstractPort(PropertiedMonitorableObject):
        def __init__(self, *, obj_id: str, parent, raw_info: dict, raw_mon_data: dict, custom_monitorables: dict):
            super().__init__(obj_id=obj_id, parent=parent, raw_info=raw_info, raw_mon_data=raw_mon_data)
            self._update(raw_info, raw_mon_data, custom_monitorables={})

        def _update(self, raw_info, raw_mon_data, custom_monitorables):

            self._index = raw_info['index']
            self._csp_index = self._parent._parent._csp_index_offset + self._index
            self._is_masked = raw_info['isMasked']
            self._is_in_loopback = raw_info['isInLoopback']

            if raw_info['operatingMode'][0] == 'CSP':
                idle_method = IdleMethod.Method1 if raw_info['operatingMode'][3] == 'idle1' else IdleMethod.Method2
                self._operating_mode = CSPSettings(packet_length=raw_info['operatingMode'][1],
                                                   packet_periodicity=raw_info['operatingMode'][2],
                                                   idle_method=idle_method)
            else:
                self._operating_mode = _PRBS_DECODER[raw_info['operatingMode'][0]]

            self._connected_optics = None
            self._connected_port = None
            if raw_info['connectedObject'] is not None:
                if raw_info['connectedObject'][0] == 'proc':
                    self._connected_port = (raw_info['connectedObject'][1], raw_info['connectedObject'][2])
                elif raw_info['connectedObject'][0] == 'opto':
                    self._connected_optics = (raw_info['connectedObject'][1], raw_info['connectedObject'][2])
                else:
                    raise RuntimeError(f'Info for port "{self.path}": Unexpected connected object type '
                                       f'value, "{raw_info["connectedObject"][0]}"')

            super()._update(raw_info, raw_mon_data, custom_monitorables=custom_monitorables)

        @property
        def is_masked(self):
            return self._is_masked

        @property
        def is_in_loopback(self):
            return self._is_in_loopback

        @property
        def operating_mode(self):
            return self._operating_mode

        @property
        def index(self):
            return self._index

        @property
        def csp_index(self):
            return self._csp_index

        @property
        def connected_optics(self):
            return self._connected_optics

        @property
        def connected_port(self):
            return self._connected_port

    class InputPort(AbstractPort):
        # def __init__(self, *, obj_id: str, parent, raw_info: dict, raw_mon_data: dict, custom_monitorables: dict):
        #     super().__init__(obj_id=obj_id, parent=parent, raw_info=raw_info, raw_mon_data=raw_mon_data,
        #                      custom_monitorables={})

        @property
        def source_info(self):
            source_crate_metric = self.metric('sourceCrate')
            source_slot_metric = self.metric('sourceSlot')
            source_channel_metric = self.metric('sourceChannel')
            if ((source_crate_metric.status == Status.UNKNOWN) or
                    (source_slot_metric.status == Status.UNKNOWN) or
                    (source_channel_metric.status == Status.UNKNOWN)):
                return None
            else:
                return Processor.LinkSourceInfo(source_crate_metric.value,
                                                source_slot_metric.value,
                                                source_channel_metric.value)

    class OutputPort(AbstractPort):
        pass
        # def __init__(self, *, obj_id: str, parent, raw_info: dict, raw_mon_data: dict, custom_monitorables: dict):
        #     super().__init__(obj_id=obj_id, parent=parent, raw_info=raw_info, raw_mon_data=raw_mon_data,
        #                      custom_monitorables={})

    @staticmethod
    def _custom_creators(obj_id, raw_info):
        creator_map = {}
        for k, v in raw_info['inputs'].items():
            creator_map[obj_id + '.inputPorts.' + k] = (Processor.InputPort, v)
        for k, v in raw_info['outputs'].items():
            creator_map[obj_id + '.outputPorts.' + k] = (Processor.OutputPort, v)
        return creator_map

    def __init__(self, obj_id, board, *,
                 raw_info, raw_action_info, contexts, raw_mon_data, refresh_callback, async_refresh_callback):
        self._client = board._client
        self._sync_client = board._sync_client
        super().__init__(obj_id,
                         board,
                         raw_info=raw_info,
                         raw_action_info=raw_action_info,
                         contexts=contexts,
                         raw_mon_data=raw_mon_data,
                         custom_monitorables=Processor._custom_creators(board.path + '.' + obj_id, raw_info),
                         refresh_callback=refresh_callback,
                         async_refresh_callback=async_refresh_callback)
        self._update(raw_info, raw_mon_data)
        self.refresh = refresh_callback

    @property
    def csp_index_offset(self):
        return self._csp_index_offset

    @property
    def tcds2_source(self):
        return self._tcds2_source

    @property
    def inputs(self):
        return self._inputs.values()

    def input(self, i):
        if i not in self._inputs:
            raise RuntimeError(f'Processor "{self.id}" does not contain an input port with index {i}')
        return self._inputs[i]

    @property
    def outputs(self):
        return self._outputs.values()

    def output(self, i):
        if i not in self._outputs:
            raise RuntimeError(f'Processor "{self.id}" does not contain an output port with index {i}')
        return self._outputs[i]

    def mask_input_ports(self, *, port_indices, lease):
        mask_set = set(port_indices)
        clear_indices = set()
        for port in self.inputs:
            if port.index not in mask_set:
                clear_indices.add(port.index)

        port_indices = sorted(port_indices)
        clear_indices = sorted(clear_indices)

        log_message = f'{self.path}: Setting input port masks.'
        if len(clear_indices) > 0:
            log_message += f' {len(clear_indices)} will be active (channels {format_index_list(clear_indices)}).'
        if len(port_indices) > 0:
            log_message += f' {len(port_indices)} will be masked (channels {format_index_list(port_indices)}).'
        if len(clear_indices) == 0:
            log_message += ' None will be active.'
        if len(port_indices) == 0:
            log_message += ' None will be masked.'
        _LOGGER.info(log_message)

        req_data = {'lease': lease._token, 'mask': port_indices, 'clear': clear_indices}
        r = self._sync_client.put(f'/input-masks/{self.id}', json=req_data)
        if r.status_code != 200:
            raise RuntimeError("Control app reported an error when masking input ports on "
                               f"{self.path}: {r.text}")

    def mask_output_ports(self, *, port_indices, lease):
        mask_set = set(port_indices)
        clear_indices = set()
        for port in self.outputs:
            if port.index not in mask_set:
                clear_indices.add(port.index)

        port_indices = sorted(port_indices)
        clear_indices = sorted(clear_indices)

        log_message = f'{self.path}: Setting output port masks.'
        if len(clear_indices) > 0:
            log_message += f' {len(clear_indices)} will be active (channels {format_index_list(clear_indices)}).'
        if len(port_indices) > 0:
            log_message += f' {len(port_indices)} will be masked (channels {format_index_list(port_indices)}).'
        if len(clear_indices) == 0:
            log_message += ' None will be active.'
        if len(port_indices) == 0:
            log_message += ' None will be masked.'
        _LOGGER.info(log_message)

        req_data = {'lease': lease._token, 'mask': port_indices, 'clear': clear_indices}
        r = self._sync_client.put(f'/output-masks/{self.id}', json=req_data)
        if r.status_code != 200:
            raise RuntimeError("Control app reported an error when masking output ports on "
                               f"{self.path}: {r.text}")

    def set_io_operating_modes(self, *,
                               input_modes: Mapping[int, Union[PRBSMode, CSPSettings]],
                               output_modes: Mapping[int, Union[PRBSMode, CSPSettings]],
                               loopback: Sequence[int],
                               lease: Lease):
        _LOGGER.info('%s: Setting I/O operating modes', self.path)

        req_data = {'lease': lease._token, 'inputs': [], 'outputs': []}
        for i, mode in input_modes.items():
            req_data['inputs'].append([[i]] + _encode_mode(mode))
        for i, mode in output_modes.items():
            req_data['outputs'].append([[i]] + _encode_mode(mode))

        loopback_index_set = {i for i in loopback}
        full_rx_index_set = set()
        for x in self.inputs:
            full_rx_index_set.add(x.index)
        full_tx_index_set = set()
        for x in self.outputs:
            full_tx_index_set.add(x.index)
        req_data['loopback'] = [i for i in loopback_index_set]
        req_data['standard'] = [i for i in (full_rx_index_set.intersection(full_tx_index_set) - loopback_index_set)]
        r = self._sync_client.put(f'/io-operating-modes/{self.id}', json=req_data)
        if r.status_code != 200:
            raise RuntimeError("Control app reported an error when setting I/O operating modes for "
                               f"{self.path}: {r.text}")

    def _update(self, raw_info, raw_mon_data):
        self._csp_index_offset = raw_info['cspIndexOffset']
        self._tcds2_source = raw_info['tcds2Source']

        custom_creators = Processor._custom_creators(self.path, raw_info)
        super()._update(raw_info, raw_mon_data, custom_monitorables=custom_creators)

        self._inputs = {v['index']: self.monitorable('inputPorts').monitorable(k)
                        for k, v in raw_info['inputs'].items()}
        self._outputs = {v['index']: self.monitorable('outputPorts').monitorable(k)
                         for k, v in raw_info['outputs'].items()}


class OpticalModule(PropertiedMonitorableObject):

    class InputChannel(PropertiedMonitorableObject):
        def __init__(self, *, obj_id: str, parent, raw_info: dict, raw_mon_data: dict, custom_monitorables: dict):
            super().__init__(obj_id=obj_id, parent=parent, raw_info=raw_info, raw_mon_data=raw_mon_data,
                             custom_monitorables={})
            self._index = raw_info['index']
            self._connected_port = None
            if raw_info['connectedPort'] is not None:
                self._connected_port = (raw_info['connectedPort'][0], raw_info['connectedPort'][1])
            self._connected_fibre = None
            if raw_info['connectedFibre'] is not None:
                self._connected_fibre = (raw_info['connectedFibre'][0], raw_info['connectedFibre'][1])
            self._update(raw_info, raw_mon_data, custom_monitorables={})

        @property
        def index(self):
            return self._index

        @property
        def connected_port(self):
            return self._connected_port

        @property
        def connected_fibre(self):
            '''ID and channel number of corresponding frontpanel MTP connector'''
            return self._connected_fibre

    class OutputChannel(PropertiedMonitorableObject):
        def __init__(self, *, obj_id: str, parent, raw_info: dict, raw_mon_data: dict, custom_monitorables: dict):
            super().__init__(obj_id=obj_id, parent=parent, raw_info=raw_info, raw_mon_data=raw_mon_data,
                             custom_monitorables={})
            self._index = raw_info['index']
            self._connected_port = None
            if raw_info['connectedPort'] is not None:
                self._connected_port = (raw_info['connectedPort'][0], raw_info['connectedPort'][1])
            self._connected_fibre = None
            if raw_info['connectedFibre'] is not None:
                self._connected_fibre = (raw_info['connectedFibre'][0], raw_info['connectedFibre'][1])
            self._update(raw_info, raw_mon_data, custom_monitorables={})

        @property
        def index(self):
            return self._index

        @property
        def connected_port(self):
            return self._connected_port

        @property
        def connected_fibre(self):
            '''ID and channel number of corresponding frontpanel MTP connector'''
            return self._connected_fibre

    @staticmethod
    def _custom_creators(obj_id, raw_info):
        creator_map = {}
        for x in raw_info['inputs']:
            creator_map[obj_id + '.' + x['id']] = (OpticalModule.InputChannel, x)
        for x in raw_info['outputs']:
            creator_map[obj_id + '.' + x['id']] = (OpticalModule.OutputChannel, x)
        return creator_map

    def __init__(self, *, obj_id: str, board, raw_info: dict, raw_mon_data: dict):
        custom_creators = OpticalModule._custom_creators(obj_id, raw_info)
        super().__init__(obj_id=obj_id,
                         parent=board,
                         raw_info=raw_info,
                         raw_mon_data=raw_mon_data,
                         custom_monitorables=custom_creators)
        self._inputs = {}
        self._outputs = {}
        self._update(raw_info, raw_mon_data)

    @property
    def inputs(self):
        return self._inputs.values()

    def input(self, i):
        if i not in self._inputs:
            raise RuntimeError(f'Optical module "{self.id}" does not contain an input channel with index {i}')
        return self._inputs[i]

    @property
    def outputs(self):
        return self._outputs.values()

    def output(self, i):
        if i not in self._outputs:
            raise RuntimeError(f'Optical module "{self.id}" does not contain an output channel with index {i}')
        return self._outputs[i]

    def _update(self, raw_info, raw_mon_data):
        super()._update(raw_info, raw_mon_data, custom_monitorables=OpticalModule._custom_creators(self.path, raw_info))

        self._inputs = {x['index']: self.monitorable(x['id']) for x in raw_info['inputs']}
        self._outputs = {x['index']: self.monitorable(x['id']) for x in raw_info['outputs']}


class Board:
    @time_function(lambda _self, stub: f'{stub.id} construction')
    def __init__(self, stub):
        _LOGGER.info('Constructing Board object "%s" = %s:%s', stub.id, stub.hostname, stub.port)

        self.__stub = stub
        self._baseURL = f'http://{stub.hostname}:{stub.port}'
        client_kwargs = {
            'base_url': f'http://{stub.hostname}:{stub.port}',
            'headers': _HEADERS,
            'timeout': _TIMEOUT,
        }
        if 'SWATCH_SOCKS_PROXY' in os.environ:
            client_kwargs['proxies'] = 'socks5://' + os.environ['SWATCH_SOCKS_PROXY']
        self._client = httpx.AsyncClient(**client_kwargs,
                                         event_hooks={
                                             'request': [_alog_http_request],
                                             'response': [_alog_http_response]})
        self._sync_client = httpx.Client(**client_kwargs,
                                         event_hooks={
                                             'request': [_log_http_request],
                                             'response': [_log_http_response]})

        try:
            r = self._sync_client.get('/devices')
        except httpx.RequestError:
            raise RuntimeError(f'Could not connect to {self._baseURL} (endpoint: /devices)')
        if r.status_code != 200:
            print("Other error")
        raw_device_info = json.loads(r.text)

        r = self._sync_client.get('/commands')
        if r.status_code != 200:
            raise RuntimeError("ERROR in getting command info: " + r.text)
        raw_command_info = json.loads(r.text)

        r = self._sync_client.get('/fsms')
        if r.status_code != 200:
            raise RuntimeError("ERROR in getting FSM info: " + r.text)
        raw_fsm_info = json.loads(r.text)

        raw_app_info = self._fetch_app_info()
        self._update_app_info(raw_app_info)
        self._hw_type = raw_app_info['hardwareType']

        raw_mon_data = self._fetch_monitoring_data()

        self._devices = {}
        self._processors = {}
        for k, v in raw_device_info['processors'].items():
            contexts = [f'{self.id}.{k}',
                        f'{self._hw_type.lower()}.{k}',
                        f'{self._hw_type.lower()}.processors',
                        'processors']
            self._processors[k] = Processor(k,
                                            self,
                                            raw_info=v,
                                            raw_action_info=(raw_command_info, raw_fsm_info),
                                            contexts=contexts,
                                            raw_mon_data=raw_mon_data[k],
                                            refresh_callback=self.refresh,
                                            async_refresh_callback=self.async_refresh)
            self._devices[k] = self._processors[k]
        service = raw_device_info['serviceModule']
        service_id = service['id']
        del service['id']
        service_contexts = [f'{self.id}.{service_id}',
                            f'{self._hw_type.lower()}.{service_id}',
                            f'{self._hw_type.lower()}.serviceModules',
                            'serviceModules']
        self._serviceModule = ServiceModule(service_id,
                                            self,
                                            raw_info=service,
                                            raw_action_info=(raw_command_info, raw_fsm_info),
                                            contexts=service_contexts,
                                            raw_mon_data=raw_mon_data[service_id],
                                            refresh_callback=self.refresh,
                                            async_refresh_callback=self.async_refresh)
        self._devices[service_id] = self._serviceModule

        self._optical_modules = {}
        for k, v in raw_device_info['optics'].items():
            self._optical_modules[k] = OpticalModule(obj_id=k, board=self, raw_info=v, raw_mon_data=raw_mon_data[k])

        register_external_process(self.id, sorted(x.path for x in self.devices))

    @property
    def id(self):
        return self.__stub.id

    @property
    def path(self):
        return self.id

    @property
    def hostname(self):
        return self.__stub.hostname

    @property
    def port(self):
        return self.__stub.port

    @property
    def host_and_port(self):
        return f'{self.__stub.hostname}:{self.__stub.port}'

    @property
    def crate_id(self):
        return self.__stub.crate

    @property
    def logical_slot(self):
        return self.__stub.slot

    @property
    def hardware_type(self):
        return self._hw_type

    @property
    def app_start_time(self):
        return self._app_start_time

    def app_uptime(self):
        return datetime.now(timezone.utc) - self._app_start_time

    @property
    def app_packages(self):
        return self._app_packages.values()

    def app_package(self, name):
        if name not in self._app_packages:
            raise RuntimeError(f'HERD app on board "{self.id}" does not have any source package named "{name}"')
        return self._app_packages[name]

    def get_lease_info(self):
        _LOGGER.info('%s: Retrieving lease info', self.id)
        r = self._sync_client.get('/lease')
        if r.status_code != 200:
            raise RuntimeError("ERROR in getting lease info: " + r.text)

        result = json.loads(r.text)
        return result['supervisor'], result['duration']

    def get_history(self):
        r = self._sync_client.get('/history')
        if r.status_code != 200:
            raise RuntimeError("ERROR in getting history info: " + r.text)

        data = json.loads(r.text)
        result = []
        for info in data['data']:
            timestamp = datetime.fromtimestamp(info['time'], timezone.utc)
            if (info['event_type'] == 'fsm:engage'):
                event = history.FSMEngage(device=info['device'], fsm=info['fsm'], time=timestamp)

            elif (info['eventType'] == 'fsm:reset'):
                event = history.FSMReset(device=info['device'], fsm=info['fsm'], time=timestamp)

            elif (info['eventType'] == 'fsm:disengage'):
                event = history.FSMDisengage(device=info['device'], fsm=info['fsm'], time=timestamp)

            elif (info['eventType'] == 'fsm:transition'):
                actionable_path = self.path + '.' + info['device']
                event = history.Transition(device=info['device'], fsm=info['fsm'], time=timestamp,
                                           transition=info['transition'],
                                           snapshot=Transition.parse_snapshot(info['status'],
                                                                              actionable_path=actionable_path,
                                                                              parameters=info['parameters']))

            elif (info['eventType'] == 'command'):
                event = history.Command(device=info['device'], command=info['command'], time=timestamp,
                                        snapshot=Command.parse_snapshot(info['status'], info['parameters']))

            elif (info['eventType'] == 'lease:obtain'):
                event = history.LeaseRequest(supervisor=info['lease']['supervisor'], time=timestamp,
                                             duration=info['lease']['duration'])

            elif (info['eventType'] == 'lease:renew'):
                event = history.LeaseRenewal(supervisor=info['lease']['supervisor'], time=timestamp,
                                             duration=info['lease']['duration'])

            elif (info['eventType'] == 'lease:retire'):
                event = history.LeaseRetire(supervisor=info['lease']['supervisor'], time=timestamp,
                                            duration=info['lease']['duration'])

            else:
                raise RuntimeError(f'History for board "{self.id}": Unexpected event type '
                                   f'value, "{info["eventType"]}"')

            result.append(event)
        return result

    @property
    def devices(self):
        return self._devices.values()

    def device(self, device_id):
        if device_id not in self._devices:
            raise RuntimeError(f'Board "{self.id}" does not contain any device with ID "{device_id}"')
        return self._devices[device_id]

    @property
    def processors(self):
        return self._processors.values()

    def processor(self, proc_id):
        if proc_id not in self._processors:
            raise RuntimeError(f'Board "{self.id}" does not contain any processor with ID "{proc_id}"')
        return self._processors[proc_id]

    @property
    def optical_modules(self):
        return self._optical_modules.values()

    def optical_module(self, module_id):
        if module_id not in self._optical_modules:
            raise RuntimeError(f'Board "{self.id}" does not contain any optical module with ID "{module_id}"')
        return self._optical_modules[module_id]

    @property
    def processors_in_tcds_configuration_order(self):
        result = [[]]
        done = set()
        remaining_procs = set()

        for proc in self.processors:
            if proc.tcds2_source is None:
                result[0].append(proc)
                done.add(proc.id)
            else:
                remaining_procs.add(proc)

        while len(remaining_procs) > 0:
            result.append([])

            for proc in remaining_procs:
                if proc.tcds2_source in done:
                    result[-1].append(proc)

            if len(result[-1]) == 0:
                raise RuntimeError('Could not determine processor TCDS2 configuration order (there may be loops in the'
                                   ' graph). Remaining processors: ' + ', '.join([p.path for p in remaining_procs]))

            for x in result[-1]:
                remaining_procs.pop(x)
                done.add(x.path)

        return result

    @property
    def interfpga_links(self):
        links = []
        for tx_proc in self.processors:
            for tx_port in tx_proc.outputs:
                if tx_port.connected_port is not None:
                    rx_proc = self.processor(tx_port.connected_port[0])
                    rx_port = rx_proc.input(tx_port.connected_port[1])
                    links.append(Link(f'{self.id}:{tx_proc.id}_{tx_port.id}:{rx_proc.id}_{rx_port.id}',
                                      self, tx_proc.id, tx_port.index,
                                      self, rx_proc.id, rx_port.index,
                                      is_optical=False))
        return links

    async def _async_refresh_monitoring_data(self, event_timer):
        with event_timer.pause():
            r = await self._client.put('/monitoring', json={})
        if r.status_code != 200:
            raise RuntimeError("ERROR in refreshing monitoring data: " + r.text)

    def _fetch_monitoring_data(self):
        r = self._sync_client.get('/monitoring')
        data = json.loads(r.text)
        return data

    async def _async_fetch_monitoring_data(self, event_timer):
        with event_timer.pause():
            r = await self._client.get('/monitoring')
        data = json.loads(r.text)
        return data

    def _fetch_app_info(self):
        '''Fetches application info from base endpoint (e.g. packages, startTime)'''
        try:
            r = self._sync_client.get('/')
        except httpx.RequestError:
            raise RuntimeError(f'Could not connect to {self._baseURL} (endpoint: /)')
        return json.loads(r.text)

    async def _async_fetch_app_info(self, event_timer):
        '''Fetches application info from base endpoint (e.g. packages, startTime)'''
        try:
            with event_timer.pause():
                r = await self._client.get('/')
        except httpx.RequestError:
            raise RuntimeError(f'Could not connect to {self._baseURL} (endpoint: /)')
        return json.loads(r.text)

    def _update_app_info(self, raw_data):
        '''Updates application info gathered from base endpoint (e.g. packages, startTime)'''
        self._app_start_time = datetime.fromtimestamp(raw_data['startTime'], timezone.utc)

        package_data = raw_data['packages']
        self._app_packages = {}
        for k, v in package_data.items():
            vsn = PackageVersion(major=v['version'][0],
                                 minor=v['version'][1],
                                 patch=v['version'][2])

            vcs_data = v.get('versionControlSystem', None)
            vcs = None
            if (vcs_data is not None) and (vcs_data['type'] == 'git'):
                if 'tag' in vcs_data:
                    ref_type = PackageGitRefType.TAG
                    ref_name = vcs_data['tag']
                elif 'branch' in vcs_data:
                    ref_type = PackageGitRefType.BRANCH
                    ref_name = vcs_data['branch']
                else:
                    ref_type = PackageGitRefType.COMMIT
                    ref_name = ''

                vcs = PackageGitInfo(type=PackageVCSType.GIT,
                                     sha=vcs_data['sha'],
                                     ref_type=ref_type,
                                     ref_name=ref_name,
                                     clean=vcs_data['clean'])

            if v['build']['type'] == 'manual':
                build_info = PackageManualBuildInfo(type=PackageBuildType.MANUAL,
                                                    time=datetime.fromtimestamp(v['build']['time'], timezone.utc))
            elif v['build']['type'] == 'gitlab':
                build_info = PackageGitLabCIBuildInfo(type=PackageBuildType.GITLABCI,
                                                      time=datetime.fromtimestamp(v['build']['time'], timezone.utc),
                                                      server_url=v['build']['serverURL'],
                                                      project_path=v['build']['projectPath'],
                                                      project_id=v['build']['projectID'],
                                                      pipeline_id=v['build']['pipelineID'],
                                                      job_id=v['build']['jobID'])
            else:
                raise RuntimeError(f'Package "{k}" for board "{self.id}": Unexpected build type '
                                   f'value, "{v["build"]["type"]}"')
            self._app_packages[k] = PackageInfo(name=k,
                                                version=vsn,
                                                build=build_info,
                                                vcs=vcs)

    async def async_refresh(self):
        with EventTimer(f'{self.id} refresh') as timer:
            _LOGGER.info('%s: Refreshing app info & monitoring data', self.id)

            raw_app_info = await self._async_fetch_app_info(event_timer=timer)
            self._update_app_info(raw_app_info)

            await self._async_refresh_monitoring_data(event_timer=timer)

            try:
                with timer.pause():
                    r = await self._client.get('/devices')
            except httpx.RequestError:
                raise RuntimeError(f'Could not connect to {self._baseURL} (endpoint: /devices)')
            if r.status_code != 200:
                print("Other error")
            raw_info = json.loads(r.text)

            data = await self._async_fetch_monitoring_data(event_timer=timer)
            total_metric_update_duration = 0.0
            for k in self._processors:
                self._processors[k]._update(raw_info['processors'][k], data[k])
                total_metric_update_duration += self._processors[k].total_metric_update_duration
            self._serviceModule._update(raw_info['serviceModule'], data[self._serviceModule.id])
            for k in self._optical_modules:
                self._optical_modules[k]._update(raw_info['optics'][k], data[k])
                total_metric_update_duration += self._optical_modules[k].total_metric_update_duration

            _LOGGER.info('%s: Finished refreshing app info & monitoring data', self.id)
            return total_metric_update_duration

    def refresh(self):
        return _RUNNER.run(self.async_refresh())


class Link(object):

    @dataclass(frozen=True)
    class Terminus(object):
        board: Board
        optical_module: Optional[OpticalModule]
        optical_channel: Union[OpticalModule.InputChannel, OpticalModule.OutputChannel, None]
        processor: Processor
        port: Union[Processor.InputPort, Processor.OutputPort]

    def __init__(self, link_id: str,
                 src_board: Board, src_device: str, src_port: int,
                 dst_board: Board, dst_device: str, dst_port: int,
                 *, is_optical: bool):
        self.__id = link_id

        # 1) Sender FPGA & optics - grab objects from ID strings & indices
        #   Option A: src_device is ID string of processor FPGA
        if src_device in [x.id for x in src_board.processors]:
            proc = src_board.processor(src_device)
            try:
                port = proc.output(src_port)
                if (type(src_board) is Board) and is_optical:
                    if port.connected_port is not None:
                        raise RuntimeError(f'Link {link_id}: Output port {src_port} on processor {src_device} '
                                           'terminates a board-internal link, so cannot be part of an optical link')
                    if port.connected_optics is None:
                        raise RuntimeError(f'Link {link_id}: No optical module is connected to output port {src_port}'
                                           f' of processor {src_device}')
                    optical_module = src_board.optical_module(port.connected_optics[0])
                    optical_channel = optical_module.output(port.connected_optics[1])
                else:  # For board-internal copper links or those from external boards
                    optical_module = None
                    optical_channel = None
            except RuntimeError as e:
                raise RuntimeError(f'Link {link_id}: {e}')
        #   Option B: src_device is ID string of optical module
        elif (type(src_board) is Board) and src_device in [x.id for x in src_board.optical_modules]:
            optical_module = src_board.optical_module(src_device)
            try:
                optical_channel = optical_module.output(src_port)
                if optical_channel.connected_port is None:
                    raise RuntimeError(f'No FPGA port is connected to output channel {src_port}'
                                       f' of optical module {src_device}')
                proc = src_board.processor(optical_channel.connected_port[0])
                port = proc.output(optical_channel.connected_port[1])
            except RuntimeError as e:
                raise RuntimeError(f'Link {link_id}: {e}')
        else:
            raise RuntimeError(f'Link {link_id}: Board "{src_board.id}" does not contain any processor FPGAs'
                               f' or optical modules with ID {src_device}')

        self.__source = Link.Terminus(src_board, optical_module, optical_channel, proc, port)

        # 2) Receiver FPGA & optics - grab objects from ID strings & indices
        #   Option A: dst_device is ID string of processor FPGA
        if dst_device in [x.id for x in dst_board.processors]:
            proc = dst_board.processor(dst_device)
            try:
                port = proc.input(dst_port)
                if (type(dst_board) is Board) and is_optical:
                    if port.connected_port is not None:
                        raise RuntimeError(f'Link {link_id}: Input port {dst_port} on processor {dst_device} '
                                           'terminates a board-internal link, so cannot be part of an optical link')
                    if port.connected_optics is None:
                        raise RuntimeError(f'Link {link_id}: No optical module is connected to input port {dst_port}'
                                           f' of processor {dst_device}')
                    optical_module = dst_board.optical_module(port.connected_optics[0])
                    optical_channel = optical_module.input(port.connected_optics[1])
                else:  # For board-internal copper links
                    optical_module = None
                    optical_channel = None
            except RuntimeError as e:
                raise RuntimeError(f'Link {link_id}: {e}')
        #   Option B: dst_device is ID string of optical module
        elif (type(dst_board) is Board) and dst_device in [x.id for x in dst_board.optical_modules]:
            optical_module = dst_board.optical_module(dst_device)
            try:
                optical_channel = optical_module.input(dst_port)
                if optical_channel.connected_port is None:
                    raise RuntimeError(f'Link {link_id}: No FPGA port is connected to input channel {dst_port}'
                                       f' of optical module {dst_device}')
                proc = dst_board.processor(optical_channel.connected_port[0])
                port = proc.input(optical_channel.connected_port[1])
            except RuntimeError as e:
                raise RuntimeError(f'Link {link_id}: {e}')
        else:
            raise RuntimeError(f'Link {link_id}: Board "{dst_board.id}" does not contain any processor FPGAs'
                               f' or optical modules with ID {dst_device}')

        self.__destination = Link.Terminus(dst_board, optical_module, optical_channel, proc, port)

    @property
    def id(self):
        return self.__id

    @property
    def source(self):
        return self.__source

    @property
    def tx(self):
        return self.__source

    @property
    def destination(self):
        return self.__destination

    @property
    def rx(self):
        return self.__destination
