
import os
import re
import yaml

from dataclasses import dataclass
from datetime import datetime, timezone
from pydantic import ValidationError
from typing import Any, List, Mapping, Optional, Tuple, Union

from .exceptions import InvalidArgumentError, DuplicateEntryError, InvalidConfigFormatError, ConfigValidationError
from .parameters import File
from .io_settings import CSPSettings, IdleMethod, PRBSMode
from .stubs import BoardStub, CrateStub, DAQCategories, DAQBoardStub, ExternalBoardStub
from .stubs import FrontendStub, LinkStub, SystemStub
from .stubs import ConfigModel, IndexListStringModel, LinkIdModel, LinkSettingsListModel
from .system_peripherals import PeripheralProcessor


class SettingsPool:
    '''Represents pool of parameters and monitoring settings held in map of 'contexts',
       each of which can apply to one or more boards, such that values of common
       parameters or settings do not have to be specified multiple times)'''

    @dataclass(frozen=True)
    class EntryKey:
        '''Represents key for a parameter value in the settings pool'''
        param_id: str
        command: str
        namespace: str

    def __init__(self, uri):
        self.uri = uri
        self.timestamp = datetime.now(timezone.utc)
        self._parameters = {}

    def add_parameter(self, context: str, param_id: str, value: Any, command: str = None,
                      namespace: str = None) -> None:
        if (namespace is not None) and (command is None):
            raise InvalidArgumentError('Invalid call to add_parameter(): Namespace '
                                       f'"{namespace}" specified, but command not specified')
        if context not in self._parameters:
            self._parameters[context] = {}

        if self.EntryKey(param_id, command, namespace) in self._parameters[context]:
            raise DuplicateEntryError(
                f'Invalid call to add_parameter(): Duplicate entry for PARAMETER ID: \
                    {param_id}, COMMAND: {command} and NAMESPACE: {namespace} in the \
                        CONTEXT" {context}.')
        self._parameters[context][self.EntryKey(param_id, command, namespace)] = value

    def get_parameter(self, namespace: str, command: str, param_id: str, contexts: List[str]):

        if namespace is None or namespace == '':
            raise InvalidArgumentError(
                'Invalid call to get_parameter(): namespace cannot have None or empty value.')

        if command is None or command == '':
            raise InvalidArgumentError(
                'Invalid call to get_parameter(): command cannot have None or empty value.')

        for c in contexts:
            if c in self._parameters:
                # Look-up order within context: Most specific match -> Generic match to just param_id
                for k in [self.EntryKey(param_id, command, namespace),
                          self.EntryKey(param_id, command, None),
                          self.EntryKey(param_id, None, None)]:
                    if k in self._parameters[c]:
                        return self._parameters[c][k]

        # Return None if cannot find any entries matching specified parameter
        return None


def _extract_file_parameter(x, replacements, used_replacements, dir_path):
    if len(x) == 1:  # Default to on-board file with empty path
        return File('', onboard=True, content_type='', content_format='')
    else:
        onboard = x.get('onboard', False)
        if isinstance(x['path'], dict) and list(x['path'].keys()) == ['replace_with']:
            if x['path']['replace_with'] not in replacements:
                raise InvalidConfigFormatError(f"Cannot find replacement '{x['path']['replace_with']}'")
            path = replacements[x['path']['replace_with']]
            used_replacements.add(x['path']['replace_with'])
        else:
            path = x['path']
        # Treat non-absolute paths as relative to the config file directory
        if not path.startswith(('http://', 'https://')):
            path = os.path.join(dir_path, path)
        return File(path, onboard=onboard, content_type='', content_format='')


def _extract_parameter_value(x, replacements, used_replacements, dir_path):
    if isinstance(x, dict):
        if list(x.keys()) == ['replace_with']:
            if x['replace_with'] not in replacements:
                raise InvalidConfigFormatError(f"Cannot find replacement '{x['replace_with']}'")
            used_replacements.add(x['replace_with'])
            return replacements[x['replace_with']]

        elif ('type' in x) and x['type'] == 'file':
            return _extract_file_parameter(x, replacements, used_replacements, dir_path)
        else:
            raise InvalidConfigFormatError(f'Invalid parameter value: {x}')

    elif isinstance(x, list):
        result = [y for y in x]
        for i in range(len(x)):
            if isinstance(x[i], dict):
                if list(x[i].keys()) == ['replace_with']:
                    if x[i]['replace_with'] not in replacements:
                        raise InvalidConfigFormatError(f"Cannot find replacement '{x[i]['replace_with']}'")
                    used_replacements.add(x[i]['replace_with'])
                    result[i] = replacements[x[i]['replace_with']]
                elif ('type' in x[i]) and x[i]['type'] == 'file':
                    result[i] = _extract_file_parameter(x[i], replacements, used_replacements, dir_path)
        return result

    else:
        return x


@dataclass(frozen=True)  # TODO From Python 3.10: kw_only=True
class PortSelector:
    board_id_regex: Optional[str] = None
    proc_id_regex: Optional[str] = None
    port_index: Optional[int] = None
    port_id_regex: Optional[str] = None

    def __call__(self, board, processor, port) -> bool:
        result = True
        if self.board_id_regex is not None:
            result = result and re.fullmatch(self.board_id_regex, board.id)
        if self.proc_id_regex is not None:
            result = result and re.fullmatch(self.proc_id_regex, processor.id)
        if self.port_index is not None:
            result = result and (port.index == self.port_index)
        if self.port_id_regex is not None:
            result = result and re.fullmatch(self.port_id_regex, port.id)
        return result


@dataclass(frozen=True)  # TODO From Python 3.10: kw_only=True
class LinkSelector:
    id_regex: Optional[str]
    src: PortSelector = PortSelector()
    dst: PortSelector = PortSelector()

    def __call__(self, link) -> bool:
        if (self.id_regex is not None) and not re.fullmatch(self.id_regex, link.id):
            return False
        src = link.source
        if not self.src(src.board, src.processor, src.port):
            return False
        dst = link.destination
        if not self.dst(dst.board, dst.processor, dst.port):
            return False
        return True


class LinkModeManager:

    def __init__(self, *, default_mode=None, mask_by_default=None):
        self._default_mode = default_mode
        self._rules = []
        self._mask_by_default = mask_by_default

    @property
    def default_mode(self):
        return self._default_mode

    @property
    def mask_by_default(self):
        return self._mask_by_default

    @property
    def rules(self):
        return self._rules

    def add_rule(self, *, id_regex, src_selector: PortSelector, dst_selector: PortSelector,
                 mode: Optional[Union[PRBSMode, CSPSettings]] = None, mask: Optional[bool] = None):
        if mode is None and mask is None:
            raise InvalidConfigFormatError(f'In LinkModeManager:add_rule() - Neither mode nor mask '
                                           f'specified in a link settings rule from '
                                           f'source (board: {src_selector.board_id_regex}, '
                                           f'processor: {src_selector.proc_id_regex}, '
                                           f'port: {src_selector.port_index} - {src_selector.port_id_regex}) -->'
                                           f'destination (board: {dst_selector.board_id_regex}, '
                                           f'processor: {dst_selector.proc_id_regex}, '
                                           f'port: {dst_selector.port_index} - {dst_selector.port_id_regex}).')
        self._rules.append((LinkSelector(id_regex, src_selector, dst_selector), mode, mask))

    def get_mode(self, link) -> Optional[Union[PRBSMode, CSPSettings]]:
        # First try the individual selectors (last one has highest precedence)
        for link_selector, mode, *_ in reversed(self._rules):
            if link_selector(link) and mode is not None:
                return mode
        if self._default_mode is not None:
            return self._default_mode
        if self.get_mask(link):
            return None
        raise InvalidConfigFormatError(f'No link selector matched "{link.id}"'
                                       f' ({link.source.port.path} -> {link.destination.port.path}).'
                                       f'  Please specify a mode in the config file.')

    def get_input_modes(self, system) -> Mapping[Any, Mapping[int, Union[PRBSMode, CSPSettings]]]:  # all procs in sys
        mode_map = {}
        for board in system.boards:
            for proc in board.processors:
                mode_map[proc] = {}

        for link in system.links:
            if (not isinstance(link.destination.processor, PeripheralProcessor) and
                    type(link.destination.processor).__name__ != 'ExternalProcessor'):
                mode = self.get_mode(link)
                if mode is not None:
                    mode_map[link.destination.processor][link.destination.port.index] = mode
        return mode_map

    def get_output_modes(self, system) -> Mapping[Any, Mapping[int, Union[PRBSMode, CSPSettings]]]:  # all procs in sys
        mode_map = {}
        for board in system.boards:
            for proc in board.processors:
                mode_map[proc] = {}

        for link in system.links:
            if (not isinstance(link.source.processor, PeripheralProcessor) and
                    type(link.source.processor).__name__ != 'ExternalProcessor'):
                mode = self.get_mode(link)
                if mode is not None:
                    mode_map[link.source.processor][link.source.port.index] = mode
        return mode_map

    def get_mask(self, link) -> Optional[bool]:
        # If either processor is excluded link should be masked
        if not (link.source.processor.is_included and link.destination.processor.is_included):
            return True

        # Otherwise try the individual selectors (last one has highest precedence)
        for link_selector, _mode, mask in reversed(self._rules):
            if link_selector(link) and mask is not None:
                return mask

        # Finally, fall back to default
        return self._mask_by_default

    def get_masked_inputs(self, system) -> Tuple[Mapping[Any, int], List[Any], List[Any]]:  # all procs in sys
        mask_map = {}
        masked_inputs = []
        unmasked_inputs = []

        for board in system.boards:
            for proc in board.processors:
                mask_map[proc] = []

        for link in system.links:
            if (not isinstance(link.destination.processor, PeripheralProcessor) and
                    type(link.destination.processor).__name__ != 'ExternalProcessor'):
                if self.get_mask(link):
                    mask_map[link.destination.processor].append(link.destination.port.index)
                    masked_inputs.append(link)
                else:
                    unmasked_inputs.append(link)

        return mask_map, masked_inputs, unmasked_inputs

    def get_masked_outputs(self, system) -> Tuple[Mapping[Any, int], List[Any], List[Any]]:  # all procs in sys
        mask_map = {}
        masked_outputs = []
        unmasked_outputs = []

        for board in system.boards:
            for proc in board.processors:
                mask_map[proc] = []

        for link in system.links:
            if (not isinstance(link.source.processor, PeripheralProcessor) and
                    type(link.source.processor).__name__ != 'ExternalProcessor'):
                if self.get_mask(link):
                    mask_map[link.source.processor].append(link.source.port.index)
                    masked_outputs.append(link)
                else:
                    unmasked_outputs.append(link)

        return mask_map, masked_outputs, unmasked_outputs


def parse_parameter_file(file_path, replacements=None, use_all_replacements=False) -> SettingsPool:
    if replacements is None:
        replacements = {}

    if not os.path.isfile(file_path):
        raise FileNotFoundError(f"File '{file_path}' does not exist (working dir: {os.getcwd()})")

    abs_path = os.path.abspath(file_path)
    dir_path = os.path.dirname(abs_path)
    pool = SettingsPool('file://' + abs_path)

    with open(abs_path, 'r', encoding='utf-8') as f:
        data = yaml.load(f, Loader=yaml.Loader)

        if 'include' in data:
            _parse_multiple_system_files(data, file_path)

        used_replacements = set()

        if 'contexts' not in data:
            raise InvalidConfigFormatError('Invalid config file format: Could not find top-level "contexts" entry')

        for c in data['contexts']:
            invalid_keys = set(data['contexts'][c]) - set(['parameters'])
            if len(invalid_keys) > 0:
                raise InvalidConfigFormatError(f'Invalid entries found in context "{c}": ' + ', '.join(invalid_keys))

            if 'parameters' in data['contexts'][c]:
                param_map = data['contexts'][c]['parameters']

                for k, value in param_map.items():
                    # If key starts with § this indicates a subkey with parameter IDs for a specific command
                    # (i.e. '§COMMAND_ID')
                    if k.startswith('§'):
                        k_tokens = k[1:].split(':')
                        if len(k_tokens) == 1:
                            namespace = None
                            commands = k_tokens[0].split(',')
                        elif len(k_tokens) == 2:
                            namespace = k_tokens[0]
                            commands = k_tokens[1].split(',')
                        else:
                            raise InvalidConfigFormatError(f'Too many colons found in entry "{k}" in context "{c}"')

                        for k2, v2 in value.items():
                            param_value = _extract_parameter_value(v2, replacements, used_replacements, dir_path)
                            for command in commands:
                                pool.add_parameter(c, k2, param_value, command, namespace)

                    else:
                        k_tokens = k.split(':')
                        if len(k_tokens) == 1:
                            namespace = None
                            commands = [None]
                            param_id = k
                        elif len(k_tokens) == 2:
                            namespace = None
                            commands = k_tokens[0].split(',')
                            param_id = k_tokens[1]
                        elif len(k_tokens) == 3:
                            namespace = k_tokens[0]
                            commands = k_tokens[1].split(',')
                            param_id = k_tokens[2]
                        else:
                            raise InvalidConfigFormatError(f'Too many colons found in entry "{k}" in context "{c}"')

                        param_value = _extract_parameter_value(value, replacements, used_replacements, dir_path)
                        for command in commands:
                            pool.add_parameter(c, param_id, param_value, command, namespace)

    # Check that all replacements used (if requested)
    if use_all_replacements:
        unused = {k for k in replacements} - used_replacements
        if len(unused) > 0:
            raise InvalidConfigFormatError(
                'The following variables are not used in the config file: ' + ','.join(unused))

    return pool


def _expand_link_id(id_string):
    try:
        linkidmodel = LinkIdModel(link_id=id_string)
        m = linkidmodel._LINK_ID_REGEX.search(linkidmodel.link_id)
    except ValidationError as e:
        raise InvalidConfigFormatError(f'Invalid config file format: {e}')

    prefix = m.group(1)
    indices = _parse_index_list_string(m.group(2))
    suffix = '' if m.group(3) is None else m.group(3)

    result = []
    for x in indices:
        result.append(f'{prefix}{x:02d}{suffix}')

    return result


def _parse_index_list_string(index_list_str):
    try:
        IndexListStringModel(index_list_string=index_list_str)
    except ValidationError as e:
        raise InvalidConfigFormatError(f'Invalid config file format: {e}')

    tokens = index_list_str.split(',')
    indices = []
    for token in tokens:
        if '-' in token:
            start, end = token.split('-')
            start = int(start)
            end = int(end)
            step = (end - start) // abs(end - start)
            for i in range(start, end + step, step):
                indices.append(i)
        else:
            indices.append(int(token))

    return indices


_PRBS_DECODER = {
    'PRBS7': PRBSMode.PRBS7,
    'PRBS9': PRBSMode.PRBS9,
    'PRBS15': PRBSMode.PRBS15,
    'PRBS23': PRBSMode.PRBS23,
    'PRBS31': PRBSMode.PRBS31
}


def _parse_link_operating_mode(x: Union[str, List]) -> Union[PRBSMode, CSPSettings]:
    if type(x) is str:
        if x.upper() not in _PRBS_DECODER:
            raise InvalidConfigFormatError(f'Invalid config file format: Invalid link mode string "{x}". '
                                           'Should be one of: ' + ', '.join(_PRBS_DECODER.keys()))
        return _PRBS_DECODER[x.upper()]
    elif type(x) is list:
        if (len(x) < 3) or (len(x) > 4):
            raise InvalidConfigFormatError(f'Invalid config file format: Link mode list "{str(x)}" has wrong length.'
                                           ' Should be array of 3 or 4 items for CSP (or a string for PRBS).')

        if not x[0].upper() == 'CSP':
            raise InvalidConfigFormatError(f'Invalid config file format: First entry, "{str(x[0])}" , in mode spec '
                                           'is invalid. Should be either "CSP" or "csp"')
        if ((type(x[1]) is not int) or (x[1] < 0) or (type(x[2]) is not int) or (x[2] < 0) or
           ((len(x) == 4) and (type(x[3]) is not str))):
            raise InvalidConfigFormatError('Invalid config file format: Mode spec contains entry of incorrect type. '
                                           'Should either be string (for PRBS) or ["CSP", uint, uint] or '
                                           '["CSP", uint, uint, str] (for CSP).')
        idle = IdleMethod.Method1
        if len(x) == 4:
            if x[3] not in {'idle1', 'idle2'}:
                raise InvalidConfigFormatError(f'Invalid config file format: Invalid last entry in CSP mode spec, '
                                               f'"{x[3]}". Should either be "idle1" or "idle2"')
            idle = IdleMethod.Method1 if x[3] == 'idle1' else IdleMethod.Method2

        return CSPSettings(x[1], x[2], idle)
    else:
        raise InvalidConfigFormatError(f'Invalid config file format: Incorrect type ({type(x)}) used for link mode.'
                                       ' Should be string (for PRBS) or array (for CSP))')


_VALID_PORT_SELECTOR_FIELDS = {'board', 'processor', 'port'}


def _parse_link_port_selector(x) -> PortSelector:
    if type(x) is not dict:
        raise InvalidConfigFormatError('Invalid config file format: Port selector has incorrect format - '
                                       'must be a map')

    # Check for invalid fields (could be typos)
    invalid_fields = set(x.keys()) - _VALID_PORT_SELECTOR_FIELDS
    if len(invalid_fields) > 0:
        raise InvalidConfigFormatError(f'Invalid config file format: Port selector contains {len(invalid_fields)} '
                                       f'invalid fields: ' + ', '.join(invalid_fields))

    board_regex = None
    if 'board' in x:
        if type(x['board']) is not str:
            raise InvalidConfigFormatError(f'Invalid config file format: Incorrect type ({type(x["board"])}) used '
                                           'for "board" field in port selector. Should be string.')
        board_regex = x['board']

    proc_regex = None
    if 'processor' in x:
        if type(x['processor']) is not str:
            raise InvalidConfigFormatError(f'Invalid config file format: Incorrect type ({type(x["processor"])}) '
                                           'used for "processor" field in port selector. Should be string.')
        proc_regex = x['processor']

    port_index = None
    port_regex = None
    if 'port' in x:
        if type(x['port']) is int and x['port'] >= 0:
            port_index = x['port']
        elif type(x['port']) is str:
            port_regex = x['port']
        else:
            raise InvalidConfigFormatError(f'Invalid config file format: Incorrect type ({type(x["port"])}) used for'
                                           ' "port" field in port selector. Should be string or uint.')

    return PortSelector(board_id_regex=board_regex,
                        proc_id_regex=proc_regex,
                        port_index=port_index,
                        port_id_regex=port_regex)


def _parse_link_settings_section(data) -> LinkModeManager:

    try:
        LinkSettingsListModel(link_settings=data)
    except ValidationError as e:
        raise InvalidConfigFormatError(f'Invalid config file format: {e}')

    link_mode_mgr = LinkModeManager()

    for x in data:
        if ('default' in x):
            default_mask = x.get('mask', False)  # All links unmasked by default.
            default_mode = None  # Optional: default mode
            if 'mode' in x:
                default_mode = _parse_link_operating_mode(x['mode'])

            link_mode_mgr = LinkModeManager(default_mode=default_mode, mask_by_default=default_mask)

        else:
            id_regex = None
            if ('id' in x):
                id_regex = x['id']

            src_selector = PortSelector()
            if 'from' in x:
                src_selector = _parse_link_port_selector(x['from'])

            dst_selector = PortSelector()
            if 'to' in x:
                dst_selector = _parse_link_port_selector(x['to'])

            mask = x.get('mask', None)
            mode = None
            if 'mode' in x:
                mode = _parse_link_operating_mode(x['mode'])

            link_mode_mgr.add_rule(id_regex=id_regex,
                                   src_selector=src_selector,
                                   dst_selector=dst_selector,
                                   mode=mode,
                                   mask=mask)

    return link_mode_mgr


def _parse_multiple_system_files(data, file_path=None):

    # To raise error in testing so that the function is not called from where an 'include' key cannot be found
    if 'include' not in data:
        raise InvalidArgumentError(
            f'Invalid call to _parse_multiple_system_files(): Could not find "include" entry in {file_path}')

    if not isinstance(data['include'], list):
        raise InvalidConfigFormatError(
            'Invalid config file format: "include" entry has incorrect format - must be a list')

    for i, x in enumerate(data['include']):

        try:
            file_path_x = os.path.join(os.path.dirname(file_path), x)
            with open(os.path.abspath(file_path_x), 'r', encoding='utf-8') as f:
                included_data = yaml.load(f, Loader=yaml.Loader)

                for k in included_data:
                    if k in data:
                        raise DuplicateEntryError(
                            f'Invalid config file format: '
                            f'Duplicate entry "{k}" found in included file "{x}" (entry {i})')
                    else:
                        data[k] = included_data[k]

        except FileNotFoundError:
            raise FileNotFoundError(f"File '{x}' does not exist (working dir: {os.path.dirname(file_path)})")

    del data['include']

    return


def parse_system_file(file_path, replacements=None, use_all_replacements=False):
    if replacements is None:
        replacements = {}

    if not os.path.isfile(file_path):
        raise FileNotFoundError(f"File '{file_path}' does not exist (working dir: {os.getcwd()})")

    with open(os.path.abspath(file_path), 'r', encoding='utf-8') as f:
        data = yaml.load(f, Loader=yaml.Loader)

        # Gather all data before model validating
        if 'include' in data:
            _parse_multiple_system_files(data, file_path)

        try:
            system_stub_model = ConfigModel.model_validate(data)
        except ValidationError as e:
            raise ConfigValidationError(e) from None

        crate_stubs = []
        for k, v in system_stub_model.system.crates.items():
            crate_stubs.append(CrateStub(id=k, **v.model_dump(by_alias=True)))

        board_stubs = []
        for k, v in system_stub_model.system.boards.items():
            board_stubs.append(BoardStub(id=k, **v.model_dump(by_alias=True)))

        external_board_stubs = []
        for k, v in system_stub_model.system.external_boards.items():
            processor_output_port_map = {}
            for proc_id, index_list_string in v.outputs.items():
                processor_output_port_map[proc_id] = _parse_index_list_string(index_list_string)
            processor_input_port_map = {}
            for proc_id, index_list_string in v.inputs.items():
                processor_input_port_map[proc_id] = _parse_index_list_string(index_list_string)

            external_board_stubs.append(ExternalBoardStub(id=k, crate=v.crate, slot=v.slot,
                                                          inputs=processor_input_port_map,
                                                          outputs=processor_output_port_map))

        link_stubs = []
        for k, v in system_stub_model.system.links.items():
            # TODO: Check that source & destination boards exist
            link_ids = _expand_link_id(k)

            if len(v.from_) == 3:
                srcs = [(v.from_[0], v.from_[1], x) for x in _parse_index_list_string(v.from_[2])]
            elif len(v.from_) == 2:
                srcs = [(v.from_[0], f'lpGBT{x:02d}', 0) for x in _parse_index_list_string(v.from_[1])]
            else:
                srcs = [(v.from_[0], f'lpGBT{x:02d}', 0) for x in range(len(link_ids))]

            if len(v.to) == 3:
                dsts = [(v.to[0], v.to[1], x) for x in _parse_index_list_string(v.to[2])]
            elif len(v.to) == 2:
                dsts = [(v.to[0], f'lpGBT{x:02d}', 0) for x in _parse_index_list_string(v.to[1])]
            else:
                dsts = [(v.to[0], f'lpGBT{x:02d}', 0) for x in range(len(link_ids))]

            if not (len(link_ids) == len(srcs) and len(srcs) == len(dsts)):
                raise InvalidConfigFormatError(f'Link entry "{k}": Numbers of IDs, sources and destinations after'
                                               f' expansion differ ( {len(link_ids)} IDs, {len(srcs)} sources,'
                                               f' {len(dsts)} destinations)')
            for link_id, src, dst in zip(link_ids, srcs, dsts):
                link_stubs.append(LinkStub(link_id, src[0], src[1], src[2], dst[0], dst[1], dst[2]))

        # Parse 'excluded boards' list (may eventually become rules, like the link settings)
        exclusions = []
        for i, object_id in enumerate(system_stub_model.exclude):
            tokens = object_id.split('.')
            board_id = tokens[0]
            found = False
            for x in board_stubs:
                if x.id == board_id:
                    exclusions.append(object_id)
                    found = True
                    break

            for x in external_board_stubs:
                if x.id == board_id:
                    exclusions.append(object_id)
                    found = True
                    break

            if not found:
                raise InvalidConfigFormatError(f'Invalid entry in exclude list: No board matches "{object_id}"'
                                               f' (entry {i})')

        daq_board_stubs = []
        for k, v in system_stub_model.system.daq_boards.items():
            daq_proc_output_port_map = {}
            for proc_id, index_list_string in v.outputs.items():
                daq_proc_output_port_map[proc_id] = _parse_index_list_string(index_list_string)
            daq_proc_input_port_map = {}
            for proc_id, index_list_string in v.inputs.items():
                daq_proc_input_port_map[proc_id] = _parse_index_list_string(index_list_string)

            daq_board_stubs.append(DAQBoardStub(id=k, category=DAQCategories.RO,
                                                inputs=daq_proc_input_port_map, outputs=daq_proc_output_port_map))

        frontend_stubs = []
        for k, v in system_stub_model.system.frontends.items():
            frontend_stubs.append(FrontendStub(id=k, serial=v.serial,
                                               lpgbt_fuseids=_parse_index_list_string(v.lpgbt_fuseids)))

        link_mode_mgr = _parse_link_settings_section(system_stub_model.link_settings)

        for k, v in system_stub_model.variables.items():
            if k not in replacements.keys():
                replacements[k] = v

    system_stub = SystemStub(crates=crate_stubs,
                             boards=board_stubs,
                             external_boards=external_board_stubs,
                             links=link_stubs,
                             exclusions=exclusions,
                             daq_boards=daq_board_stubs,
                             frontends=frontend_stubs)

    return (system_stub, link_mode_mgr, parse_parameter_file(file_path, replacements, use_all_replacements))
