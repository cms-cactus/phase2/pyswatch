import click
import os
import re
import time

from ..action import FileResult
from ..board import Board, Lease
from ..cli.board_data import BoardData, ChannelData, FileFormat, print_mismatches
from ..config import parse_parameter_file, parse_system_file, PortSelector
from ..console.utilities import check_connected_optics
from ..console.utilities import check_link_map, check_parameters_for_fsm, create_metric_data_table
from ..console.utilities import create_link_comparison_table, get_system_map
from ..console.utilities import empty_line, ensure_devices_in_fsm_initial_state
from ..console.utilities import require_monitoring_status, run_fsm_transition
from ..monitoring import Status
from ..parameters import File
from ..stubs import BoardStub
from ..system import System
from ..utilities.board_data import BoardDataMatchResult, compare, read, write
from ..utilities.misc import format_index_list
from .board_data import parse_channels
from .utilities import decorate_rich_click_root_command, parse_var_flag
from datetime import datetime, timezone
from rich.padding import Padding

_CONTEXT_SETTINGS = dict(help_option_names=['-h', '--help'])

_LEASE_DURATION = 300


def get_capture_data_files(transition_snapshot, *, rx_required=False, tx_required=False):
    '''Returns RX & TX captured data files - the command results of FileResult type, containing rx or tx'''
    rx_data_files = [x.result for x in transition_snapshot.commands
                     if type(x.result) is FileResult and x.result.type == 'processor-rx-buffer-data']
    tx_data_files = [x.result for x in transition_snapshot.commands
                     if type(x.result) is FileResult and x.result.type == 'processor-tx-buffer-data']

    if rx_required and (len(rx_data_files) == 0):
        raise RuntimeError(f"Could not find RX capture data file in results of transition '{transition_snapshot.path}'")

    if tx_required and (len(tx_data_files) == 0):
        raise RuntimeError(f"Could not find TX capture data file in results of transition '{transition_snapshot.path}'")

    if len(rx_data_files) > 1:
        filenames = ', '.join([x.name for x in rx_data_files])
        raise RuntimeError(f"Multiple RX capture data files ({filenames}) found in "
                           f"results of transition '{transition_snapshot.path}'")

    if len(tx_data_files) > 1:
        filenames = ', '.join([x.name for x in tx_data_files])
        raise RuntimeError(f"Multiple TX capture data files ({filenames}) found in "
                           f"results of transition '{transition_snapshot.path}'")

    return (None if len(rx_data_files) == 0 else rx_data_files[0],
            None if len(tx_data_files) == 0 else tx_data_files[0])


def save_capture_data(console, transition_snapshot, rx_path, tx_path, *, description):
    rx_data, tx_data = get_capture_data_files(transition_snapshot,
                                              rx_required=rx_path is not None,
                                              tx_required=tx_path is not None)
    if rx_path is not None:
        if len(os.path.dirname(rx_path)) > 0:
            os.makedirs(os.path.dirname(rx_path), exist_ok=True)
        console.print(f'Saving RX capture data for {description} to {rx_path}')
        rx_data.save(rx_path)

    if tx_path is not None:
        if len(os.path.dirname(tx_path)) > 0:
            os.makedirs(os.path.dirname(tx_path), exist_ok=True)
        console.print(f'Saving TX capture data for {description} to {tx_path}')
        tx_data.save(tx_path)


def parse_save_flag(_ctx, _param, value):
    # Split out parameter variable entries into map
    save_paths = {}
    for x in value:
        equals_index = x.find('=')
        if equals_index < 0:
            raise click.BadParameter(f'Incorrect format in save flag value "{x}": Must contain an equals sign')
        save_paths[x[0:equals_index]] = x[equals_index+1:]

    return save_paths


_DEFAULT_ALGO_TEST_CONFIG_PATHS = {
    'serenity': os.path.join(os.path.dirname(os.path.dirname(__file__)), 'config', 'algo-test', 'serenity.yml'),
    'dummy': os.path.join(os.path.dirname(os.path.dirname(__file__)), 'config', 'algo-test', 'dummy.yml')
}


def get_config_arg_parser(defaults):
    def parse_config_arg(_ctx, _param, value):
        if value.startswith('default:'):
            name = value[8:]
            if name not in defaults:
                raise click.BadParameter(f'Default config file for "{name}" not found. Current options: ' +
                                         ', '.join(defaults))
            return defaults[name]
        else:
            return value

    return parse_config_arg


def compare_link_data(console, system, snapshot_map, tmp_path, latency, max_frames):

    tx_files = []
    rx_files = []
    tx_data = {proc.path: BoardData(f'{proc.path}', {}) for proc in system.connected_processors}
    rx_data = {proc.path: BoardData(f'{proc.path}', {}) for proc in system.connected_processors}

    # Save captured data
    system_map = get_system_map(system)
    for proc in system.connected_processors:
        rx_data_file, tx_data_file = get_capture_data_files(snapshot_map[proc], rx_required=True, tx_required=True)
        proc_path = proc.path.replace(".", "_")
        rx_capture_path = tmp_path + 'capture_rx_' + proc_path + '.txt'
        tx_capture_path = tmp_path + 'capture_tx_' + proc_path + '.txt'
        rx_data_file.save(rx_capture_path)
        rx_files.append(rx_capture_path)
        tx_data_file.save(tx_capture_path)
        tx_files.append(tx_capture_path)

        # BoardData objects
        tx_data[proc.path] = read(tx_capture_path)
        rx_data[proc.path] = read(rx_capture_path)

    console.print('Board data files ...')
    console.print('    Tx:')
    for tx_path in tx_files:
        console.print(f'    {tx_path}')
    console.print('    Rx:')
    for rx_path in rx_files:
        console.print(f'    {rx_path}')

    # Create expected data based on link mapping (excluding masked and external board links)
    expected_data = {proc.path: BoardData(f'{proc.path}', {}) for proc in system.connected_processors}
    match_results = {proc.path: BoardDataMatchResult({}) for proc in system.connected_processors}
    n_compared_channels = 0
    for proc_path, data in rx_data.items():
        # Skip processors with all links masked or to/from external boards
        if len(system_map[proc_path].keys()) == 0:
            continue
        for c in data.channels:
            if c in system_map[proc_path].keys():
                expected_tx_proc = system_map[proc_path][c][0]
                expected_tx_chan = system_map[proc_path][c][1]
                expected_channel_data = ChannelData(tx_data[expected_tx_proc].at(expected_tx_chan)._data, copy=False)
                expected_data[proc_path].add(c, expected_channel_data)
        expected_proc_path = proc_path.replace(".", "_")
        expected_data_path = tmp_path + 'expected_rx_' + expected_proc_path + '.txt'
        write(expected_data[proc_path], expected_data_path, FileFormat.EMPv2)

        # Compare the expected and observed data
        match_results[proc_path] = compare(expected_data[proc_path], rx_data[proc_path], latencies=latency,
                                           max_frames=max_frames, require_equal_channels=False)
        n_compared_channels += len(expected_data[proc_path].channels)

    bad_channels = {proc.path: [] for proc in system.connected_processors}
    n_bad_channels = 0
    for proc_path in match_results.keys():
        result = match_results[proc_path]
        if len(result.channels) > 0:
            console.print(f'\nComparing expected and observed data: {proc_path}', style='bold')
            if bool(result):
                console.print(f'    Perfect match found: {proc_path}'
                              f' ch {format_index_list(result.channels)}\n')
            else:
                print_mismatches(result, expected_data[proc_path], rx_data[proc_path], False)
                bad_channels[proc_path] = [c for c in result.channels if not bool(result.at(c))]
                n_bad_channels += len(bad_channels[proc_path])
                console.print(f'    Could not find a perfect match: {proc_path} '
                              f'ch {format_index_list(bad_channels[proc_path])}!\n')
    console.print(f'\nCompared {n_compared_channels} ' + ('channel' if n_compared_channels == 1 else 'channels.'))

    create_link_comparison_table(console, system, match_results, bad_channels)

    if n_bad_channels > 0:
        raise click.ClickException('Mismatched data found!')
    else:
        console.print(':sparkles: Perfect match found! :sparkles:\n')


def _makemultifile_pool_list(console, config, replacements, in_datafile_path, out_datafile_path, file_indices):
    pool_list = []

    if file_indices != '':
        reg_pat = r'\{file_index(:\d+)?\}'
        if not re.search(reg_pat, in_datafile_path):
            raise click.BadParameter('Input data file path must contain "{file_index}" when --file-indices specified')
        if not re.search(reg_pat, out_datafile_path):
            raise click.BadParameter('Output data file path must contain "{file_index}" when --file-indices specified')

        console.print('InputFiles: ' + in_datafile_path.format(file_index=file_indices))
        if '-' not in file_indices:
            raise click.BadParameter('Expected format "<start>-<end>". Ex: "96-101". Must contain a hyphen')
        start, end = file_indices.split('-')
        if not start.isdigit() or not end.isdigit():
            raise click.BadParameter('Expected format "<start>-<end>". Ex: "96-101". Start and end must be integers')
        start = int(start)
        end = int(end)
        filenumrange = list(range(start, end + 1)) if end >= start else list(range(start, end - 1, -1))

        for filenum in filenumrange:
            infilepath = in_datafile_path.format(file_index=filenum)
            outfilepath = out_datafile_path.format(file_index=filenum)
            replacements['input_data_file'] = File(infilepath, content_type='', content_format='')
            pool = parse_parameter_file(config, replacements=replacements, use_all_replacements=True)
            pool_list.append({'pool': pool, 'outfile_path': outfilepath})
    else:
        console.print('InputFiles: ' + in_datafile_path)

        replacements['input_data_file'] = File(in_datafile_path, content_type='', content_format='')
        pool = parse_parameter_file(config, replacements=replacements, use_all_replacements=True)
        pool_list.append({'pool': pool, 'outfile_path': out_datafile_path})

    empty_line(console)

    return pool_list


@click.command()
@click.help_option('-h', '--help')
@click.option('--input-channels', type=str, default=None, help="List of input channels")
@click.option('--output-channels', type=str, default=None, help="List of output channels")
@click.option('--continue', 'continue_', is_flag=True, help='Continue slice test - i.e. inject new data and capture, '
                                                            'without repeating setup')
@click.option('--var', 'param_replacements', multiple=True, callback=parse_var_flag,
              help="Specify variable for replacing parameter values in config file")
@click.option('--show-metrics', 'metric_regex', type=str, default='^$',
              help='Regex for metrics to displays after each transition')
@click.option('--strict', is_flag=True,
              help="Return non-zero exit code if warnings issued or if any monitoring data unknown")
@click.option('--dry-run', is_flag=True,
              help="Verify configuration file and parameter values, but exit before running any transitions")
@click.option('--rx-capture', 'rx_capture', type=str, default=None, help='Path to save captured input data')
@click.option('--file-indices', type=str, default='',
              help=' Format: "<start>-<end>". Example: "96-101". Continuous range of input data files to use.'
              ' For file_indices: "96-101" and input-data-file: "inp_file{file_index}.txt", the algo will try'
              ' all files from "inp_file96.txt" to "inp_file101.txt". If not specified, a single file will be'
              ' used. Allows padding with zeros with format "inp_file{file_index:0<padding>}.txt".')
@click.argument('hostname')
@click.argument('device')
@click.argument('config', callback=get_config_arg_parser(_DEFAULT_ALGO_TEST_CONFIG_PATHS))
@click.argument('fw-package')
@click.argument('input-data-file')
@click.argument('output-data-file')
@decorate_rich_click_root_command
def main_single_board_test(console, input_channels, output_channels, continue_, param_replacements,
                           metric_regex, strict, rx_capture, hostname, device, config, fw_package,
                           input_data_file, output_data_file, dry_run, file_indices):

    port = 3000
    if ':' in hostname:
        port = int(hostname.split(':')[1])
        hostname = hostname.split(':')[0]

    replacements = {'firmware_package': File(fw_package, content_type='', content_format='')}
    if input_channels is not None:
        replacements['input_channels'] = input_channels
    if output_channels is not None:
        replacements['output_channels'] = output_channels
    replacements.update(param_replacements)

    console.print(f'Performing algo test with processor FPGA {device} on {hostname.split(":")[0]}')
    empty_line(console)
    console.print(f'Config file: {config}')
    console.print('Variables:')
    max_key_length = max([len(k) for k in replacements])
    for k in sorted(replacements.keys()):
        console.print(f'   {k:{max_key_length}} = ' + repr(replacements[k]))

    board = Board(BoardStub('board', hostname=hostname, port=port))
    proc = board.device(device)
    console.print(f'Connected to board @ {hostname}')
    console.print()

    pool_list = _makemultifile_pool_list(console, config, replacements, input_data_file,
                                         output_data_file, file_indices)

    check_parameters_for_fsm(console, [proc], 'payloadTest', pool_list[0]['pool'], verbose=dry_run)

    # If '--dry-run' flag used, exit after parameter check (and before claiming the boards)
    if dry_run:
        console.print('Parameter values validated - no errors found.')
        return

    lease = Lease(board, _LEASE_DURATION)
    if not continue_:
        ensure_devices_in_fsm_initial_state(console, [proc], 'payloadTest', {proc: lease})
        empty_line(console)

    snapshots = []
    if not continue_:
        lease.renew(_LEASE_DURATION)
        snapshots += [run_fsm_transition(console,
                                         [proc],
                                         'payloadTest', 'Halted', 'setup',
                                         pool_list[0]['pool'],
                                         {proc: lease},
                                         refresh=True,
                                         metric_regex=metric_regex,
                                         strict=strict)[proc]]

    for pool_dict in pool_list:
        pool = pool_dict['pool']
        outfilepath = pool_dict['outfile_path']
        console.print(f'Payload to be saved in: {outfilepath}')

        lease.renew(_LEASE_DURATION)
        snapshots += [run_fsm_transition(console,
                                         [proc],
                                         'payloadTest', 'Ready', 'playback',
                                         pool,
                                         {proc: lease},
                                         refresh=True,
                                         metric_regex=metric_regex,
                                         strict=strict)[proc]]
        save_capture_data(console, snapshots[-1], rx_capture, outfilepath, description=device)


@click.command()
@click.help_option('-h', '--help')
@click.argument('config', required=True)
@click.option('--repeat', type=int, default=1, help='Number of iterations for full FSM', show_default=True)
@click.option('--repeat-io-config', type=int, default=1, show_default=True,
              help='Number of times that I/O configuration transitions are run per iteration of full FSM')
@click.option('--config-interval', type=int, default=60,
              help='Duration of gap between transitions being run', show_default=True)
@click.option('--check-interval', type=int, default=15,
              help='Duration of gap between monitoring data updates', show_default=True)
@click.option('--var', 'param_replacements', multiple=True, callback=parse_var_flag,
              help="Specify variable for replacing parameter values in config file")
@click.option('--mask', 'masks', type=str, multiple=True, help="Links to exclude (ID regex)")
@click.option('--mask-all-except', 'mask_all_except', is_flag=True, default=False,
              help="Use with --mask to mask all links except (ID regex)")
@click.option('--show-metrics', 'metric_regex', type=str, default='^$',
              help='Regex for metrics to displays after each transition')
@click.option('--strict', is_flag=True,
              help="Return non-zero exit code if warnings issued or if any monitoring data unknown")
@click.option('--dry-run', is_flag=True,
              help="Verify configuration file and parameter values, but exit before running any transitions")
@click.option('--no-pause-for-externals', 'pause_for_externals', is_flag=True,
              callback=lambda _ctx, _param, value: not value,
              help="Do not pause for externally-controlled boards to be configured before configuring rx")
@click.option('--quiet', is_flag=True)
@click.option('--check-io-data', 'check_io_data', is_flag=True, default=False,
              help="Compare expected and observed Rx data", show_default=True)
@click.option('--capture-path', 'tmp_path', type=str, default='./',
              help='Path for saving captured data', show_default=True)
@click.option('--latency', 'latency', type=str, callback=parse_channels,
              help="Latency value or range of values (positive integers) for expected/observed data comparison",
              show_default=True)
@click.option('--max-frames', type=click.INT, default=None,
              help='Maximum number of clock cycles to compare', show_default=True)
@decorate_rich_click_root_command
def main_link_test(console, config, repeat, repeat_io_config, config_interval, check_interval, param_replacements,
                   masks, mask_all_except, metric_regex, strict, dry_run, pause_for_externals, quiet, check_io_data,
                   tmp_path, latency, max_frames):

    if latency:
        if not check_io_data:
            raise click.ClickException("You have selected '--latency' but have not selected '--check-io-data'.")

        for x in latency:
            if x < 0:
                raise click.ClickException('Latency values must be non-negative integers')

    if max_frames:
        if not check_io_data:
            raise click.ClickException("You have selected '--max-frames' but have not selected '--check-io-data'.")

    system_stub, link_mode_mgr, pool = parse_system_file(config, param_replacements, use_all_replacements=True)

    # Handle command-line masking (overrides config file rules)
    if mask_all_except:
        link_mode_mgr.add_rule(id_regex='.*', src_selector=PortSelector(), dst_selector=PortSelector(), mask=True)
        if len(masks) == 0:
            console.print('You have selected the --mask-all-except flag but have not specified any links to unmask.'
                          ' This will mask all links.  Are you sure about this?')
    if len(masks) > 0:
        for m in masks:
            link_mode_mgr.add_rule(id_regex=m,
                                   src_selector=PortSelector(),
                                   dst_selector=PortSelector(),
                                   mask=not mask_all_except)

    system = System(system_stub)
    input_port_modes = link_mode_mgr.get_input_modes(system)
    output_port_modes = link_mode_mgr.get_output_modes(system)

    excluded_procs = sorted([proc.path for proc in system.excluded_processors])
    if len(excluded_procs) > 0:
        console.print(f'Excluding {len(excluded_procs)} processors: {", ".join(excluded_procs)}\n')

    check_parameters_for_fsm(console, system.connected_processors, 'linkTest', pool, verbose=dry_run)
    # If '--dry-run' flag used, exit after parameter check (and before claiming the boards)
    if dry_run:
        console.print('Parameter values validated - no errors found.')
        check_connected_optics(console, system, True)
        return

    leases = {}
    for b in system.boards:
        lease = Lease(b, _LEASE_DURATION)
        for d in b.devices:
            leases[d] = lease

    for i in range(1, repeat + 1):
        ensure_devices_in_fsm_initial_state(console, system.connected_processors, 'linkTest', leases)
        for proc in system.connected_processors:
            proc.set_io_operating_modes(input_modes=input_port_modes[proc],
                                        output_modes=output_port_modes[proc],
                                        loopback=[], lease=leases[proc])

        masked_links, unmasked_links = system.mask_links(link_mode_mgr, leases=leases)
        if len(masked_links) > 0:
            console.print(f'\nMasking {len(masked_links)} links: ' + ', '.join([x.id for x in masked_links]) + '\n')
            if mask_all_except:
                console.print(f'\nUnmasking {len(unmasked_links)} links: ' + ', '.join([x.id for x in unmasked_links])
                              + '\n')

        for procs in system.connected_processors_in_tcds_configuration_order:
            run_fsm_transition(console, procs, 'linkTest', 'Halted', 'setup', pool, leases,
                               refresh=True, metric_regex=metric_regex, strict=strict, verbose=not quiet)

        for j in range(1, repeat_io_config + 1):
            if j > 1:
                run_fsm_transition(console, system.connected_processors, 'linkTest', 'RxConfigured', 'continue',
                                   pool, leases, refresh=True, metric_regex=metric_regex, strict=strict,
                                   verbose=not quiet)

            steps = [('Synchronized', 'configureTx', False), ('TxConfigured', 'configureRx', True)]
            for state, transition, refresh in steps:
                # Pause for any externally-controlled boards to be configured before configuring rx
                if pause_for_externals and state == 'TxConfigured' and len(system.connected_external_boards) > 0:
                    empty_line(console)
                    input('Pausing for configuration of externally-controlled boards ('
                          f'{", ".join(x.id for x in system.connected_external_boards)}). Press enter to continue ...')
                    empty_line(console)

                for lease in leases.values():
                    lease.renew(_LEASE_DURATION)
                run_fsm_transition(console, system.connected_processors, 'linkTest', state, transition, pool, leases,
                                   refresh=refresh, metric_regex=metric_regex, strict=strict, verbose=not quiet)

            check_link_map(console, system.links, {x.id: x for x in system.crates})

            # Compare the expected and observed data
            if check_io_data:
                snapshot_map = (run_fsm_transition(console, system.connected_processors, 'linkTest', 'RxConfigured',
                                                   'capture', pool, leases, refresh=True, metric_regex=metric_regex,
                                                   strict=strict))

                compare_link_data(console, system, snapshot_map, tmp_path, latency, max_frames)

            # TODO: Update monitoring data every check_interval seconds, for total of config_interval seconds
            status_check_start = time.time()
            k = 1
            while (time.time() - status_check_start) < config_interval:
                for lease in leases.values():
                    lease.renew(_LEASE_DURATION + check_interval)

                time.sleep(max(0, status_check_start + k * check_interval - time.time()))

                # Update monitoring data, and inform users of errors
                console.print(f'Status after {time.time() - status_check_start:.0f} seconds [dim](at '
                              f'{datetime.now(timezone.utc).strftime("%H:%M:%S %Z")})[/dim]')
                for proc in system.connected_processors:
                    proc.refresh()
                    console.print(f'    {proc.path}: [bold]{proc.status.__rich__()}[/bold]')
                check_connected_optics(console, system, False)

                status_summary = create_metric_data_table(system.connected_processors + system.connected_optics,
                                                          metric_regex=metric_regex)
                if status_summary is not None:
                    console.print(Padding.indent(status_summary, 7))
                    empty_line(console)

                allowed_status_values = [Status.NO_LIMIT, Status.GOOD]
                if not strict:
                    allowed_status_values += [Status.WARNING, Status.UNKNOWN]
                console.print()

                require_monitoring_status(system.connected_processors + system.connected_optics,
                                          allowed_status_values)
                k = k + 1

            # TODO: Analyse captured data
            if j == repeat_io_config:
                console.rule(f'[bold]Completed iteration {i} of {repeat}[/bold]')
            else:
                console.rule(f'[bold]Iteration {i} of {repeat}: Completed checks after I/O '
                             f'configuration {j} of {repeat_io_config}[/bold]', characters='- ')
            empty_line(console)


@click.command()
@click.help_option('-h', '--help')
@click.argument('config', required=True)
@click.option('--save-rx', 'rx_capture_paths', type=str, multiple=True, callback=parse_save_flag,
              help='Save RX capture data from specified FPGA (format "<board>.<device>=<filepath>"')
@click.option('--save-tx', 'tx_capture_paths', type=str, multiple=True, callback=parse_save_flag,
              help='Save TX capture data from specified FPGA (format "<board>.<device>=<filepath>"')
@click.option('--var', 'param_replacements', multiple=True, callback=parse_var_flag,
              help="Specify variable for replacing parameter values in config file")
@click.option('--mask', 'masks', type=str, multiple=True, help="Links to exclude (ID regex)")
@click.option('--mask-all-except', 'mask_all_except', is_flag=True, default=False,
              help="Use with --mask to mask all links except (ID regex)")
@click.option('--continue', 'continue_', is_flag=True, help='Continue slice test - i.e. inject new data, reconfigure'
                                                            ' MGTs and capture, without repeating setup')
@click.option('--show-metrics', 'metric_regex', type=str, default='^$',
              help='Regex for metrics to displays after each transition')
@click.option('--strict', is_flag=True,
              help="Return non-zero exit code if warnings issued or if any monitoring data unknown")
@click.option('--dry-run', is_flag=True,
              help="Verify configuration file and parameter values, but exit before running any transitions")
@decorate_rich_click_root_command
def main_slice_test(console, config, rx_capture_paths, tx_capture_paths, param_replacements, masks, mask_all_except,
                    continue_, metric_regex, strict, dry_run):

    system_stub, link_mode_mgr, pool = parse_system_file(config, param_replacements, use_all_replacements=True)

    # Handle command-line masking (overrides config file rules)
    if mask_all_except:
        link_mode_mgr.add_rule(id_regex='.*', src_selector=PortSelector(), dst_selector=PortSelector(), mask=True)
        if len(masks) == 0:
            console.print('You have selected the --mask-all-except flag but have not specified any links to unmask.'
                          '  This will mask all links.  Are you sure about this?')
    if len(masks) > 0:
        for m in masks:
            link_mode_mgr.add_rule(id_regex=m,
                                   src_selector=PortSelector(),
                                   dst_selector=PortSelector(),
                                   mask=not mask_all_except)

    system = System(system_stub)
    input_port_modes = link_mode_mgr.get_input_modes(system)
    output_port_modes = link_mode_mgr.get_output_modes(system)

    for proc_idpath in rx_capture_paths:
        if proc_idpath not in [x.path for x in system.connected_processors]:
            raise click.ClickException(f'Invalid "--save-rx" value: Device "{proc_idpath}" does not exist')
    for proc_idpath in tx_capture_paths:
        if proc_idpath not in [x.path for x in system.connected_processors]:
            raise click.ClickException(f'Invalid "--save-tx" value: Device "{proc_idpath}" does not exist')

    excluded_procs = sorted([proc.path for proc in system.excluded_processors])
    if len(excluded_procs) > 0:
        console.print(f'Excluding {len(excluded_procs)} processors: {", ".join(excluded_procs)}\n')

    check_parameters_for_fsm(console, system.connected_processors, 'sliceTest', pool, verbose=dry_run)
    # If '--dry-run' flag used, exit after parameter check (and before claiming the boards)
    if dry_run:
        console.print('Parameter values validated - no errors found.')
        return

    leases = {}
    for b in system.boards:
        lease = Lease(b, _LEASE_DURATION)
        for d in b.devices:
            leases[d] = lease

    if not continue_:
        ensure_devices_in_fsm_initial_state(console, system.connected_processors, 'sliceTest', leases)
        for proc in system.connected_processors:
            proc.set_io_operating_modes(input_modes=input_port_modes[proc],
                                        output_modes=output_port_modes[proc],
                                        loopback=[], lease=leases[proc])

        masked_links, unmasked_links = system.mask_links(link_mode_mgr, leases=leases)
        if len(masked_links) > 0:
            console.print(f'\nMasking {len(masked_links)} links: ' + ', '.join([x.id for x in masked_links]) + '\n')
            if mask_all_except:
                console.print(f'\nUnmasking {len(unmasked_links)} links: ' + ', '.join([x.id for x in unmasked_links])
                              + '\n')

        for procs in system.connected_processors_in_tcds_configuration_order:
            run_fsm_transition(console, procs, 'sliceTest', 'Halted', 'setup',
                               pool, leases, refresh=True, metric_regex=metric_regex, strict=strict)
        for lease in leases.values():
            lease.renew(_LEASE_DURATION)
    else:
        run_fsm_transition(console, system.connected_processors, 'sliceTest', 'Configured', 'continue',
                           pool, leases, refresh=False, metric_regex=metric_regex, strict=strict)

    for procs in system.connected_processors_in_backend_link_order:
        run_fsm_transition(console, procs, 'sliceTest', 'Synchronized', 'configure', pool, leases,
                           refresh=True, metric_regex=metric_regex, strict=strict)

    check_link_map(console, system.links, {x.id: x for x in system.crates})

    snapshot_map = {}
    for procs in system.connected_processors_in_backend_link_order:
        snapshot_map.update(run_fsm_transition(console, procs, 'sliceTest', 'Configured', 'capture', pool, leases,
                                               refresh=True, metric_regex=metric_regex, strict=strict))

    # Save captured data
    for proc, snapshot in snapshot_map.items():
        save_capture_data(console,
                          snapshot,
                          rx_capture_paths.get(proc.path, None),
                          tx_capture_paths.get(proc.path, None),
                          description=proc.path)
