
import click
import functools
import logging
import re
import os

from typing import Union

from ..console.utilities import create_console
from ..io_settings import CSPSettings, IdleMethod, PRBSMode
from ..utilities import tracing


__LOG_LEVELS = {
    'DEBUG': logging.DEBUG,
    'INFO': logging.INFO,
    'WARNING': logging.WARNING,
    'ERROR': logging.ERROR,
    'CRITICAL': logging.CRITICAL
}
_SUPPORTED_RECORDING_EXTENSIONS = ['.txt', '.asc', '.svg', '.html']
_SHOW_TRACKEBACK_KEY = f'{__name__}.show_traceback'


def validate_recording_path(_ctx, _param, value):
    for path in value:
        ext = os.path.splitext(path)[1]
        if ext == '':
            raise click.BadParameter('Extension missing in recording path - must be one of: ' +
                                     ', '.join(_SUPPORTED_RECORDING_EXTENSIONS))

        if ext not in _SUPPORTED_RECORDING_EXTENSIONS:
            raise click.BadParameter(f'Invalid recording extension "{ext}" - must be one of: ' +
                                     ', '.join(_SUPPORTED_RECORDING_EXTENSIONS))

        dir_path = os.path.dirname(path)

        if not os.path.isdir(dir_path):
            raise click.BadParameter(f'Will not be able to save recording to "{path}" - directory does not exist')

        if not os.access(dir_path, os.W_OK):
            raise click.BadParameter(f'Will not be able to save recording to "{path}" - no write access to directory')

    return value


def _handle_exception(func, args, kwargs, *, console, show_traceback):

    try:
        return func(*args, **kwargs)

    except SystemExit:
        raise

    except BaseException as e:  # pylint: disable=broad-exception-caught
        if show_traceback:
            console.print(f'ERROR occurred! {type(e).__name__} exception thrown:')
            console.print_exception(extra_lines=2)
            raise click.ClickException(f'{e}')
        else:
            raise click.ClickException(f'[{type(e).__name__}] {e}')


def __log_level_callback(_ctx, _param, value):
    if value is None:
        return None
    return __LOG_LEVELS[value]


def decorate_rich_click_root_command(func):
    '''Handles exceptions from root function of CLI, creates rich console, and adds option to record output'''

    @functools.wraps(func)
    @click.option('--log-level', required=False, type=click.Choice(__LOG_LEVELS.keys(), case_sensitive=False),
                  callback=__log_level_callback,
                  help='Threshold used for log file (by default, no log file is created)')
    @click.option('--show-traceback', is_flag=True, help="Display full exception stack if error occurred")
    @click.option('--record', 'recording_paths', type=click.Path(exists=False), callback=validate_recording_path,
                  multiple=True, help='Record command-line output; supported formats: TEXT (.txt/.asc), HTML, SVG')
    @click.pass_context
    def wrapper(ctx, *args, log_level, show_traceback, recording_paths, **kwargs):
        console = create_console(record=(len(recording_paths) > 0))

        # Start top-level tracing event
        tracing.begin_duration_event('main')

        # Configure logging - if log threshold set then write all of our messages above that threshold to file, along
        # with all WARNING/ERROR/CRITICAL messages from other libraries
        if log_level is None:
            logging.getLogger().addHandler(logging.NullHandler())
        else:
            handler = logging.FileHandler('/tmp/shep.log', mode='w', encoding='utf-8')
            handler.setLevel(log_level)
            handler.setFormatter(logging.Formatter('%(asctime)s.%(msecs)03d %(levelname)-8s %(name)-10s > %(message)s',
                                                   '%Y-%m-%d %H:%M:%S'))

            def log_filter(record: logging.LogRecord):
                return (record.name == 'shep') or (record.levelno >= logging.WARNING)
            handler.addFilter(log_filter)

            logging.getLogger().setLevel(logging.DEBUG)
            logging.getLogger().addHandler(handler)

        # Save recordings & trace only when the context is torn down, after all subcommands have finished
        @ctx.call_on_close
        def save_recordings():
            for path in recording_paths:
                ext = os.path.splitext(path)[1]
                if ext == '.txt':
                    console.save_text(path, clear=False)
                elif ext == '.asc':
                    console.save_text(path, clear=False, styles=True)
                elif ext == '.svg':
                    console.save_svg(path, clear=False, title=ctx.info_name)
                elif ext == '.html':
                    console.save_html(path, clear=False)
            if len(recording_paths) > 0:
                console.print('\nSaved console output to ' + ', '.join(recording_paths), style='grey42')

            tracing.end_current_duration_event()
            if 'TRACE_EVENTS_FILE' in os.environ:
                tracing.save(os.environ['TRACE_EVENTS_FILE'])

        # Store 'show_traceback' for later use in subcommand wrapper from @handle_rich_click_subcommand_exceptions
        ctx.meta[_SHOW_TRACKEBACK_KEY] = show_traceback

        kwargs['console'] = console
        return _handle_exception(func, args, kwargs, console=console, show_traceback=show_traceback)

    return wrapper


def handle_rich_click_subcommand_exceptions(func):
    '''
    Catches exceptions and prints info to rich console, including traceback if --show-traceback flag used.

    Note: root command must have been wrapped by decorate_rich_click_root_command; ctx.obj.console must exist,
          and be a rich console
    '''

    @functools.wraps(func)
    @click.pass_context
    def wrapper(ctx, *args, **kwargs):
        return _handle_exception(func, args, kwargs,
                                 console=ctx.obj.console,
                                 show_traceback=ctx.meta[_SHOW_TRACKEBACK_KEY])

    return wrapper


_PRBS_VALUE_MAP = {
    "PRBS7": PRBSMode.PRBS7,
    "PRBS9": PRBSMode.PRBS9,
    "PRBS15": PRBSMode.PRBS15,
    "PRBS23": PRBSMode.PRBS23,
    "PRBS31": PRBSMode.PRBS31
}

_IOPORT_MODE_CSP_REGEX = r'CSP:(\d+)/(\d+):IDLE([12])'


def parse_index_list_string(value, exception_type=click.ClickException):

    if value is None:
        return value

    if not (value[0].isdigit() and value[-1].isdigit()):
        raise exception_type(f'Malformed channel list, "{value}" (comma separated list expected)')

    _sep = ','
    _dash = '-'

    numbers = []
    items = value.split(_sep)
    for item in items:
        if len(item) == 0:
            raise exception_type(f'Malformed channel list, "{value}" (comma separated list expected, '
                                 'found consecutive commas)')

        nums = item.split(_dash)
        if len(nums) == 1:
            numbers.append(int(item))
        elif len(nums) == 2:

            try:
                i = int(nums[0])
                j = int(nums[1])
            except Exception:
                raise exception_type(f'Invalid interval "{item}" encountered in channel list "{value}"')

            if i > j:
                raise exception_type(f'Invalid interval "{item}" encountered in channel list "{value}"')
            numbers.extend(range(i, j+1))
        else:
            raise exception_type('Malformed channel list, "{value}" (comma separated list expected)')

    return numbers


def parse_ioport_mode_arg(value: str) -> Union[CSPSettings, PRBSMode]:
    uppercase_value = value.upper()
    if uppercase_value in _PRBS_VALUE_MAP:
        return _PRBS_VALUE_MAP[uppercase_value]

    m = re.fullmatch(_IOPORT_MODE_CSP_REGEX, uppercase_value)
    if m is None:
        raise ValueError(f'I/O port mode argument has invalid value, "{value}"')

    result = CSPSettings(packet_length=int(m.group(1)),
                         packet_periodicity=int(m.group(2)),
                         idle_method=IdleMethod.Method1 if m.group(3) == '1' else IdleMethod.Method2)
    return result


def parse_var_flag(_ctx, _param, value):
    # Split out parameter variable entries into map
    parameter_replacements = {}
    for x in value:
        equals_index = x.find('=')
        if equals_index < 0:
            raise click.BadParameter(f'Incorrect format for --var "{x}": Must contain an equals sign')
        parameter_replacements[x[0:equals_index]] = x[equals_index+1:]

    return parameter_replacements
