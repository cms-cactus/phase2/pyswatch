#!/usr/bin/env python3

import sys
import subprocess
import time


_LOG_FILE = 'sim.log'


def main_run_sim():
    print("Running simulation ...")

    with open(_LOG_FILE, 'w', encoding='utf-8') as f:
        try:
            exe = "./run_sim"
            args = ["", "-c", "work.top", "work.glbl", "-do", "run 50us", "-do", "quit"] + sys.argv[1:]
            print(f"  Command: {exe} " + " ".join([(f'"{x}"' if " " in x else x) for x in args]))
            start = time.time()
            subprocess.run(executable=exe, args=args, check=True, stdout=f, stderr=f, universal_newlines=True)
        except subprocess.CalledProcessError as e:
            print(f'ERROR: Simulation returned with non-zero exit code {e.returncode}')
            print(f'   Command: {e.cmd}')
            sys.exit(1)

    end = time.time()
    print(f"Simulation finished!   (Duration: {end - start:.3} seconds)")

    errors = []
    warnings = []
    with open(_LOG_FILE, 'r', encoding='utf-8') as f:
        for line in f:
            if line.startswith('# ** Fatal:') or line.startswith('# ** Error:') or line.startswith('# ** Failure:'):
                errors.append(line)
            if line.startswith('# ** Warning:'):
                warnings.append(line)

    if len(warnings) > 0:
        print()
        print(f"Found {len(warnings)} warnings: ")
        for w in warnings:
            print(w)

    if len(errors) > 0:
        print()
        print(f"Found {len(errors)} errors: ")
        for e in errors:
            print(e)
        sys.exit('Errors encountered while running simulation. Please check the messages above or the log file at '
                 f'{_LOG_FILE} or the transcript_*.log')
