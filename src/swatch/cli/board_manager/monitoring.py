
import click
import re
import math

from .core import BOARDS_PARAM_TYPE
from ..utilities import handle_rich_click_subcommand_exceptions
from ...console.utilities import DurationFmt, require_monitoring_status
from ...monitoring import Setting, Status

from rich.tree import Tree
from rich.table import Table
from rich.text import Text
from rich.progress import track

# update-metrics BOARD_REGEX/_HOSTNAME DEVICE_REGEX


@click.command('update-metrics')
@click.argument('boards', metavar='board_id_regex_or_hostname', type=BOARDS_PARAM_TYPE,
                required=False, default='.*')
@click.argument('device_regex', type=str, required=False, default='.*')
@click.pass_obj
@handle_rich_click_subcommand_exceptions
def update_metrics(obj, boards, device_regex):
    device_pattern = re.compile(device_regex)
    device_pattern  # pylint: disable=pointless-statement
    for board in boards:
        # Directly calling refresh method on board for now, since each device's
        # refresh method just redirects to the board-level one right now
        obj.console.print(f'Updating monitoring data for {board.path}')
        board.refresh()

        # for x in [y for y in board.devices] + [y for y in board.optical_modules]:
        #     if device_pattern.match(x.id):
        #         obj.console.print(f'Updating monitoring data for {x.path}')
        #         x.refresh()


# check-status BOARD_REGEX/_HOSTNAME DEVICE_REGEX
@click.command('check-status')
@click.argument('boards', metavar='board_id_regex_or_hostname', type=BOARDS_PARAM_TYPE,
                required=False, default='.*')
@click.argument('device_regex', type=str, required=False, default='.*')
@click.option('--strict', is_flag=True,
              help='Return non-zero exit code if warnings issued or if any monitoring data unknown')
@click.option('--update-metrics', 'refresh_metrics', is_flag=True,
              help='Return non-zero exit code if warnings issued or if any monitoring data unknown')
@click.pass_obj
@handle_rich_click_subcommand_exceptions
def check_status(obj, boards, device_regex, strict, refresh_metrics):
    device_pattern = re.compile(device_regex)
    mon_objs = []
    for board in boards:
        for x in [y for y in board.devices] + [y for y in board.optical_modules]:
            if device_pattern.match(x.id):
                mon_objs.append(x)

    if refresh_metrics:
        for x in mon_objs:
            obj.console.print(f'Updating monitoring data for {x.path}')
            x.refresh()

    allowed_status_values = [Status.NO_LIMIT, Status.GOOD]
    if not strict:
        allowed_status_values += [Status.WARNING, Status.UNKNOWN]
    require_monitoring_status(mon_objs, allowed_status_values)


# review-metric-update-duration BOARD_REGEX/_HOSTNAME [DEVICE_REGEX]
@click.command('review-metric-update-duration')
@click.argument('boards', metavar='board_id_regex_or_hostname', type=BOARDS_PARAM_TYPE,
                required=False, default='.*')
@click.argument('device_regex', type=str, required=False, default='.*')
@click.option('--max-depth', type=click.IntRange(min=0), default=0xFFFFFFFF)
@click.pass_obj
@handle_rich_click_subcommand_exceptions
def review_metric_update_duration(obj, boards, device_regex, max_depth):
    device_pattern = re.compile(device_regex)

    def populate_tree(t, mon_obj, depth):
        if (depth > 0) and (mon_obj.setting != Setting.DISABLED):
            self_fraction = mon_obj.metric_update_duration / \
                mon_obj.total_metric_update_duration
            branch = t.add(f'[b]{mon_obj.id}[/b]: '
                           f'[bright_blue]Total {DurationFmt(mon_obj.total_metric_update_duration)}, '
                           f'self {DurationFmt(mon_obj.metric_update_duration)} '
                           f'({100 * self_fraction:.1f}%)[/bright_blue]')
            for child in mon_obj.monitorables:
                populate_tree(branch, child, depth - 1)

    for board in boards:
        tree = Tree(board.id)
        for x in [y for y in board.devices] + [y for y in board.optical_modules]:
            if device_pattern.match(x.id):
                populate_tree(tree, x, max_depth)

        obj.console.print(tree)


# review-metric-update-multirun BOARD_REGEX/_HOSTNAME [DEVICE_REGEX]


@click.command('review-metric-update-multirun')
@click.argument('boards', metavar='board_id_regex_or_hostname', type=BOARDS_PARAM_TYPE,
                required=False, default='.*')
@click.argument('device_regex', type=str, required=False, default='.*')
@click.option('--max-depth', type=click.IntRange(min=0), default=0xFFFFFFFF)
@click.option('--num-loops', type=click.IntRange(min=1, max=100), required=True, default=5)
@click.pass_obj
def review_metric_update_multirun(obj, boards, device_regex, max_depth, num_loops):
    device_pattern = re.compile(device_regex)
    multirun_out = []

    def populate_table_loop0(out_list, mon_obj, depth):
        if (depth > 0) and (mon_obj.setting != Setting.DISABLED):
            tot_duration = mon_obj.total_metric_update_duration
            slf_duration = mon_obj.metric_update_duration
            child_list = [tot_duration / num_loops, (tot_duration * tot_duration) / num_loops,
                          slf_duration / num_loops, (slf_duration * slf_duration) / num_loops]
            out_list.append(child_list)
            for child in mon_obj.monitorables:
                populate_table_loop0(child_list, child, depth - 1)

    def populate_table_loopn(out_list, mon_obj, depth):
        if (depth > 0) and (mon_obj.setting != Setting.DISABLED):
            tot_duration = mon_obj.total_metric_update_duration
            slf_duration = mon_obj.metric_update_duration
            out_list[0] = out_list[0] + (tot_duration / num_loops)
            out_list[1] = out_list[1] + \
                ((tot_duration * tot_duration) / num_loops)
            out_list[2] = out_list[2] + (slf_duration / num_loops)
            out_list[3] = out_list[3] + \
                ((slf_duration * slf_duration) / num_loops)
            child_list = iter(out_list[4:])
            for child in mon_obj.monitorables:
                if (depth > 1) and (child.setting != Setting.DISABLED):
                    populate_table_loopn(next(child_list), child, depth - 1)

    def print_metric_update_duration(t, out_list, mon_obj, depth, parentduration):
        if (depth > 0) and (mon_obj.setting != Setting.DISABLED):
            tot_sigma = math.sqrt(abs(out_list[1] - (out_list[0] * out_list[0])))
            slf_sigma = math.sqrt(abs(out_list[3] - (out_list[2] * out_list[2])))
            parent_fraction = out_list[0] / parentduration
            rtext1 = Text()
            rtext1.append(f'{"-> "*(max_depth-depth)}', style="bold")
            rtext1.append(f'{mon_obj.id} ', style="bold")
            rtext2 = Text(
                f'{DurationFmt(out_list[0]):.2f} +- {DurationFmt(tot_sigma):.2f}')
            rtext3 = Text(
                f'{DurationFmt(out_list[2]):.2f} +- {DurationFmt(slf_sigma):.2f}')
            rbkg_color = int((1 - parent_fraction) * 255.0)
            rtext4 = Text(f'{100 * parent_fraction:.1f}',
                          style=f"rgb({255-rbkg_color},{255-rbkg_color},255) on rgb({rbkg_color},{rbkg_color},255)")
            t.add_row(rtext1, rtext2, rtext3, rtext4)
            sorted_out_list = sorted(
                out_list[4:], key=lambda m: m[0], reverse=True)
            valid_monitorable_list = [
                x for x in mon_obj.monitorables if x.setting != Setting.DISABLED]
            sorted_monitorables = [x for _, x in sorted(zip(out_list[4:], valid_monitorable_list),
                                                        key=lambda m: m[0],
                                                        reverse=True)]
            child_list = iter(sorted_out_list)
            for child in sorted_monitorables:
                if (depth > 1) and (child.setting != Setting.DISABLED):
                    print_metric_update_duration(
                        t, next(child_list), child, depth - 1, out_list[0])

    for board in boards:
        for x in [y for y in board.devices] + [y for y in board.optical_modules]:
            if device_pattern.match(x.id):
                populate_table_loop0(multirun_out, x, max_depth)
        board.refresh()

    for _ in track(range(num_loops - 1), description="Executing Iteration: "):
        multirun_itr = iter(multirun_out)
        for board in boards:
            for x in [y for y in board.devices] + [y for y in board.optical_modules]:
                if device_pattern.match(x.id):
                    populate_table_loopn(next(multirun_itr), x, max_depth)
        board.refresh()

    total_device_count = 0
    for board in boards:

        sum_metric_nonopts = sum(list(
            list(zip(*multirun_out[total_device_count:total_device_count + len(board.devices)]))[0]))
        sum_metric_optics = sum(list(
            list(zip(*multirun_out[total_device_count + len(board.devices):total_device_count +
                                   len(board.devices) + len(board.optical_modules)]))[0]))
        sum_metric_devices = sum_metric_nonopts + sum_metric_optics

        richout = Table(title=Text(board.id) + Text("       ----       " + f'{DurationFmt(sum_metric_devices):.2f}'))
        richout.add_column("ID", justify="left")
        richout.add_column("Total", justify="right", style="bright_blue")
        richout.add_column("Self", justify="right", style="bright_blue")
        richout.add_column("parent %", justify="right")

        devices = [y for y in board.devices]
        sorted_devices = [d for _, d in sorted(zip(multirun_out[total_device_count:total_device_count + len(devices)],
                                                   devices),
                                               key=lambda m: m[0],
                                               reverse=True)]
        sorted_output_list = sorted(multirun_out[total_device_count:total_device_count + len(devices)],
                                    key=lambda m: m[0], reverse=True)
        sorted_output_list_itr = iter(sorted_output_list)
        for x in sorted_devices:
            if device_pattern.match(x.id):
                print_metric_update_duration(richout, next(sorted_output_list_itr), x, max_depth,
                                             sum_metric_devices)
        total_device_count = total_device_count + len(devices)

        rtext1 = Text(f'{board.id}', style='rgb(255,255,255) on rgb(0,0,0)')
        rtext1.append(" OPTICS", style='rgb(255,255,255) on rgb(0,0,0)')
        rtext2 = Text(f'{DurationFmt(sum_metric_optics):.2f}', style='rgb(255,255,255) on rgb(0,0,255)')
        rtext3 = Text("--------------", style='rgb(255,255,255) on rgb(0,0,255)')
        rbkg_color = int((1 - sum_metric_optics / sum_metric_devices) * 255.0)
        rtext4 = Text(f'{100 * sum_metric_optics/sum_metric_devices:.1f}',
                      style=f'rgb({255-rbkg_color},{255-rbkg_color},255) on rgb({rbkg_color},{rbkg_color},255)')
        richout.add_row(rtext1, rtext2, rtext3, rtext4)
        devices = [y for y in board.optical_modules]
        sorted_devices = [d for _, d in sorted(zip(multirun_out[total_device_count:total_device_count + len(devices)],
                                                   devices),
                                               key=lambda m: m[0],
                                               reverse=True)]
        sorted_output_list = sorted(multirun_out[total_device_count:total_device_count + len(devices)],
                                    key=lambda m: m[0], reverse=True)
        sorted_output_list_itr = iter(sorted_output_list)
        for x in sorted_devices:
            if device_pattern.match(x.id):
                print_metric_update_duration(richout, next(sorted_output_list_itr), x, max_depth,
                                             sum_metric_devices)
        total_device_count = total_device_count + len(devices)

        obj.console.print(richout)


COMMANDS = [update_metrics, check_status,
            review_metric_update_duration, review_metric_update_multirun]
