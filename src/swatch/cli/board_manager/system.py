
import click
import re

from .core import BOARDS_PARAM_TYPE, obtain_leases
from ..utilities import handle_rich_click_subcommand_exceptions
from ...board import Lease
from ...config import PortSelector
from ...console.utilities import check_link_map


@click.command('check-link-map')
@click.pass_obj
@handle_rich_click_subcommand_exceptions
def _check_link_map(obj):
    if obj.system is None:
        raise click.UsageError('System must be defined if running this subcommand!')

    check_link_map(obj.console, obj.system.links, {x.id: x for x in obj.system.crates})


@click.command('set-io-operating-modes')
@click.argument('boards', metavar='board_id_regex_or_hostname', type=BOARDS_PARAM_TYPE)
@click.argument('processor_regex', type=str, )
@click.pass_obj
@handle_rich_click_subcommand_exceptions
def set_io_operating_modes(obj, boards, processor_regex):
    if obj.system is None:
        raise click.UsageError('System must be defined if running this subcommand!')

    input_port_modes = obj.link_mode_mgr.get_input_modes(obj.system)
    output_port_modes = obj.link_mode_mgr.get_output_modes(obj.system)

    proc_pattern = re.compile(processor_regex)
    for board in boards:
        lease = Lease(board, 2)
        for proc in board.processors:
            if proc_pattern.match(proc.id):
                obj.console.print(f'Setting I/O operating modes for {proc.path}')
                proc.set_io_operating_modes(input_modes=input_port_modes[proc],
                                            output_modes=output_port_modes[proc],
                                            loopback=[], lease=lease)


@click.command('mask-links')
@click.argument('link_regexes', metavar='link_regex', type=str, nargs=-1)
@click.option('--invert-selection', is_flag=True, help='Mask all links excluding those listed')
@click.pass_obj
@handle_rich_click_subcommand_exceptions
def mask_links(obj, link_regexes, invert_selection):
    if obj.system is None:
        raise click.UsageError('System must be defined if running this subcommand!')

    if invert_selection:
        obj.link_mode_mgr.add_rule(id_regex='.*', src_selector=PortSelector(), dst_selector=PortSelector(), mask=True)
        if len(link_regexes) == 0:
            obj.console.print('You have specified --invert-selection but have not specified any links to unmask.  '
                              'This will mask all links.  Are you sure about this?')
    if len(link_regexes) > 0:
        for r in link_regexes:
            obj.link_mode_mgr.add_rule(id_regex=r,
                                       src_selector=PortSelector(),
                                       dst_selector=PortSelector(),
                                       mask=not invert_selection)

    masked_links, unmasked_links = obj.system.mask_links(obj.link_mode_mgr, leases=obtain_leases(obj.system.boards, 2))
    if len(masked_links) > 0:
        obj.console.print(f'Masking {len(masked_links)} links: ' + ', '.join([x.id for x in masked_links]) + '\n')
        if invert_selection:
            obj.console.print(f'Unmasking {len(unmasked_links)} links: {", ".join([x.id for x in unmasked_links])}\n')


COMMANDS = [set_io_operating_modes, mask_links, _check_link_map]
