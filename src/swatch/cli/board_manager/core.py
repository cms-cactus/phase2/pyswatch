
import click
import re

from ...board import Board, Lease
from ...stubs import BoardStub


class CLIObject:
    def __init__(self, console, system=None, link_mode_mgr=None, pool=None):
        self.console = console
        self.system = system
        self.link_mode_mgr = link_mode_mgr
        self.settings_pool = pool


def obtain_leases(boards, duration):
    leases = {}
    for b in boards:
        lease = Lease(b, duration)
        for d in b.devices:
            leases[d] = lease
    return leases


class BoardsParamType(click.ParamType):
    name = "boards"

    def convert(self, value, param, ctx):
        if ctx.obj.system is None:
            if ':' in value:
                hostname = value.split(':')[0]
                port = int(value.split(':')[1])
            else:
                hostname, port = value, 3000

            return [Board(BoardStub('board', hostname, port))]
        else:
            board_id_pattern = re.compile(value)
            boards = []
            for board in ctx.obj.system.boards:
                if board_id_pattern.match(board.id):
                    boards.append(board)

            if len(boards) == 0:
                self.fail(f'No boards match regex "{value}"', param, ctx)

            return boards


BOARDS_PARAM_TYPE = BoardsParamType()
