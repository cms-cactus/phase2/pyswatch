
import click
import re

from ..utilities import handle_rich_click_subcommand_exceptions
from ...board import Board, PackageBuildType, PackageGitRefType
from ...console.utilities import create_metric_data_table, create_property_table, empty_line
from ...history import EventType
from ...monitoring import Setting, Status
from ...stubs import BoardStub
from ...system import ExternalBoard
from click_didyoumean import DYMGroup
from datetime import timezone
from rich import box
from rich.table import Table
from rich.padding import Padding
from swatch.console.utilities import create_command_snapshot_table, create_transition_snapshot_table


class BoardParamType(click.ParamType):
    name = "board"

    def convert(self, value, param, ctx):
        if ctx.obj.system is None:
            if ':' in value:
                hostname = value.split(':')[0]
                port = int(value.split(':')[1])
            else:
                hostname, port = value, 3000

            return Board(BoardStub('board', hostname, port))
        else:
            if value in {x.id for x in ctx.obj.system.boards}:
                return ctx.obj.system.board(value)
            else:
                self.fail(f'Board "{value}" not found in system', param, ctx)


BOARD_PARAM_TYPE = BoardParamType()


@click.group(cls=DYMGroup)
@click.pass_obj
@handle_rich_click_subcommand_exceptions
def show(_obj):
    pass


@show.command('system', help='Shows system summary')
@click.pass_obj
@handle_rich_click_subcommand_exceptions
def show_system(obj):
    # 1. Crates
    t = Table()
    t.add_column('Crate')
    t.add_column('Building')
    t.add_column('Rack')
    t.add_column('Height')
    t.add_column('Description')
    t.add_column('CSP ID')

    for crate in obj.system.crates:
        t.add_row(crate.id,
                  crate.building,
                  crate.rack,
                  str(crate.height),
                  crate.description,
                  str(crate.csp_id))
    obj.console.print(Padding(t, (1, 0, 1, 3)))

    # 2. Boards
    t = Table()
    t.add_column('Board')
    t.add_column('Location')
    t.add_column('Host:Port')
    t.add_column('Status')
    t.add_column('Hardware type', justify='center')
    t.add_column('Uptime')

    for board in sorted([x for x in obj.system.boards] + [x for x in obj.system.external_boards],
                        key=lambda x: (x.crate_id, x.logical_slot)):
        if type(board) is ExternalBoard:
            t.add_row(board.id,
                      f'{board.crate_id}, log. slot {board.logical_slot}',
                      '',
                      ':exclamation_mark: [i]Externally controlled[/i]',
                      '',
                      '')
        else:
            status = ':white_check_mark: Online'
            if any(not proc.is_included for proc in board.processors):
                if all(not proc.is_included for proc in board.processors):
                    status += ' but excluded'
                else:
                    status += ' but partially excluded'
            t.add_row(board.id,
                      f'{board.crate_id}, log. slot {board.logical_slot}',
                      board.host_and_port,
                      status,
                      board.hardware_type,
                      f'{int(board.app_uptime().total_seconds())} seconds')
    obj.console.print(Padding(t, (0, 0, 1, 3)))


@show.command('links', help='Show board-to-board link summary')
@click.argument('direction', type=click.Choice(['tx', 'rx', 'any']), default='any')
@click.option('--board', 'board_regex', type=str, default='.*', help='Board ID regex')
@click.option('--link', 'link_regex', type=str, default='.*', help='Link ID regex')
@click.option('--proc', 'processor_regex', type=str, default='.*', help='Processor ID regex')
@click.option('--port', 'port_regex', type=str, default='.*', help='Port index')
@click.option('--optics', 'optics_regex', type=str, default='.*', help='Optical module ID regex')
@click.pass_obj
@handle_rich_click_subcommand_exceptions
def show_links(obj, direction, board_regex, link_regex, processor_regex, port_regex, optics_regex):

    t = Table()
    t.add_column('Link')
    t.add_column('Source board')
    t.add_column('FPGA port & status')
    t.add_column('Optics & stats')
    t.add_column('Destination board')
    t.add_column('FPGA port & status')
    t.add_column('Optics & status')
    t.add_column('Comments')

    def get_link_sorting_key(link):
        return (link.source.board.id, link.source.processor.id, link.source.port.index)

    board_pattern = re.compile(board_regex)
    link_pattern = re.compile(link_regex)
    proc_pattern = re.compile(processor_regex)
    port_pattern = re.compile(port_regex)
    optics_pattern = re.compile(optics_regex)

    previous_proc_path = None
    for link in sorted(obj.system.optical_links, key=get_link_sorting_key):

        src, dst = link.source, link.destination
        match_link = link_pattern.match(link.id)

        if direction == 'tx':
            match_board = board_pattern.match(src.board.id)
            match_proc = proc_pattern.match(src.processor.id)
            match_port = port_pattern.match(str(src.port.index))
            match_optics = (optics_pattern.match(src.optical_module.id) if src.optical_module is not None
                            else False)
        elif direction == 'rx':
            match_board = board_pattern.match(dst.board.id)
            match_proc = proc_pattern.match(dst.processor.id)
            match_port = port_pattern.match(str(dst.port.index))
            match_optics = (optics_pattern.match(dst.optical_module.id) if dst.optical_module is not None
                            else False)
        else:
            match_board = board_pattern.match(src.board.id) or board_pattern.match(dst.board.id)
            match_proc = proc_pattern.match(src.processor.id) or proc_pattern.match(dst.processor.id)
            match_port = port_pattern.match(str(src.port.index)) or port_pattern.match(str(dst.port.index))
            match_optics = ((src.optical_module is not None and optics_pattern.match(src.optical_module.id))
                            or (dst.optical_module is not None and optics_pattern.match(dst.optical_module.id)))

        if not (match_board and match_link and match_proc and match_port and match_optics):
            continue

        src_port_status = src.port.status.__rich__() if src.board in obj.system.boards else ''
        dst_port_status = dst.port.status.__rich__() if dst.board in obj.system.boards else ''
        src_optics_status = src.optical_module.status.__rich__() if src.board in obj.system.boards else ''
        dst_optics_status = dst.optical_module.status.__rich__() if dst.board in obj.system.boards else ''

        # Separate section for each TX processor
        if (previous_proc_path is not None) and (previous_proc_path != src.processor.path):
            t.add_section()
        previous_proc_path = src.processor.path

        comment = ''
        if src.port.is_masked and dst.port.is_masked:
            comment = '[i]Masked[/i]'
        elif src.port.is_masked:
            comment = '[i]Tx masked[/i]'
        elif dst.port.is_masked:
            comment = '[i]Rx masked[/i]'
        t.add_row(link.id,
                  src.board.id,
                  f'{src.processor.id}, ch {src.port.index:3}  {src_port_status}',
                  '' if src.optical_module is None
                  else f'{src.optical_module.id}, ch {src.optical_channel.index:2}  {src_optics_status}',
                  dst.board.id,
                  f'{dst.processor.id}, ch {dst.port.index:3}  {dst_port_status}',
                  '' if dst.optical_module is None
                  else f'{dst.optical_module.id}, ch {dst.optical_channel.index:2}  {dst_optics_status}',
                  comment)

    if t.row_count > 0:
        obj.console.print(Padding(t, (0, 0, 1, 3)))
    else:
        obj.console.print('\nNo links matched your selection.')


@show.command('link-rules', help='Shows link settings from config file')
@click.pass_obj
@handle_rich_click_subcommand_exceptions
def show_link_rules(obj):

    t = Table()

    t.add_column('Rule')
    t.add_column('Link ID regex')
    t.add_column('Tx port selector')
    t.add_column('Rx port selector')
    t.add_column('Mode')
    t.add_column('Mask')

    match_all_str = '[grey42]Match all[/grey42]'
    not_applic_str = '[grey42]N/A[/grey42]'

    t.add_row(
        '0 (default)',
        None,
        match_all_str,
        match_all_str,
        not_applic_str if obj.link_mode_mgr._default_mode is None else str(obj.link_mode_mgr._default_mode),
        str(obj.link_mode_mgr._mask_by_default),
        end_section=True
    )

    def get_port_selector_description(x):
        fields = []
        if x.board_id_regex is not None:
            fields.append(f'board matches "{x.board_id_regex}"')
        if x.proc_id_regex is not None:
            fields.append(f'FPGA matches "{x.proc_id_regex}"')
        if x.port_id_regex is not None:
            fields.append(f'port matches "{x.port_id_regex}"')
        if x.port_index is not None:
            fields.append(f'port index == {x.port_index}')
        if len(fields) > 0:
            return ' & '.join(fields)
        else:
            return match_all_str

    for i, rule in enumerate(obj.link_mode_mgr._rules, start=1):
        t.add_row(
            str(i),
            rule[0].id_regex,
            get_port_selector_description(rule[0].src),
            get_port_selector_description(rule[0].dst),
            not_applic_str if rule[1] is None else str(rule[1]),
            not_applic_str if rule[2] is None else str(rule[2]),
            end_section=True
        )

    empty_line(obj.console)
    obj.console.print(Padding.indent(t, 2))
    empty_line(obj.console)


@show.command('app-info', help='Shows HERD uptime, plus build and version info')
@click.argument('boards', metavar='board', type=BOARD_PARAM_TYPE, nargs=-1)
@click.pass_obj
@handle_rich_click_subcommand_exceptions
def show_app_info(obj, boards):
    empty_line(obj.console)

    if len(boards) == 1:
        start_time = boards[0].app_start_time.astimezone(timezone.utc)
        obj.console.print(f'HERD app started at {start_time:%H:%M:%S} UTC (up for '
                          f'{int(boards[0].app_uptime().total_seconds())} seconds)')

        t = Table()

        t.add_column("")
        t.add_column("Branch/Tag")
        t.add_column("Commit")
        t.add_column("Local changes?")
        t.add_column("Build time (UTC)")
        t.add_column("Build type")

        for p in boards[0].app_packages:
            if p.build.type == PackageBuildType.MANUAL:
                t.add_row(p.name,
                          p.vcs.ref_name,
                          p.vcs.sha[:8],
                          "No" if p.vcs.clean else "Yes",
                          p.build.time.strftime("%H:%M:%S"),
                          "Manual")
            else:
                t.add_row(p.name,
                          p.vcs.ref_name,
                          p.vcs.sha[:8],
                          "No" if p.vcs.clean else "Yes",
                          p.build.time.strftime("%H:%M:%S"),
                          f"GitLab CI (job {p.build.job_id})")

        obj.console.print(Padding.indent(t, 2))

    else:
        t = Table(show_lines=True)
        t.add_column("")
        package_column_indices = {}
        i = 1
        for board in boards:
            for p in board.app_packages:
                if p.name not in package_column_indices:
                    t.add_column(p.name)
                    package_column_indices[p.name] = i
                    i += 1

        for board in boards:
            start_time = board.app_start_time.astimezone(timezone.utc)
            uptime = int(board.app_uptime().total_seconds())
            row_contents = [f"{board.hostname}:{board.port}\n[i](Up for {uptime}s)[/i]"]
            row_contents += [""] * len(package_column_indices)
            for p in board.app_packages:
                if p.vcs.ref_type == PackageGitRefType.TAG:
                    summary = f"{p.vcs.ref_name} (commit {p.vcs.sha[0:8]})\n"
                    if not p.vcs.clean:
                        summary += "with changes.\n"
                elif p.vcs.ref_type != PackageGitRefType.BRANCH:
                    summary = f"Branch {p.vcs.ref_name}, {p.vcs.sha[0:8]},\n"
                    if not p.vcs.clean:
                        summary += "with changes.\n"
                else:
                    summary = f"Commit {p.vcs.sha[0:8]}"
                    if not p.vcs.clean:
                        summary += " with changes"
                    summary += "\n"

                summary += f"Built at {p.build.time}\n"
                if p.build.type == PackageBuildType.GITLABCI:
                    summary += f"in CI job {p.build.job_id}"

                row_contents[package_column_indices[p.name]] = summary
            t.add_row(*row_contents)

        obj.console.print(Padding.indent(t, 2))

    empty_line(obj.console)


@show.command('actions', help='Lists registered commands and FSMs for a specific processor or service module')
@click.argument('board', type=BOARD_PARAM_TYPE)
@click.argument('device_id', type=str)
@click.pass_obj
@handle_rich_click_subcommand_exceptions
def show_actions(obj, board, device_id):

    if device_id not in [x.id for x in board.devices]:
        raise click.ClickException(f'No processor or service module has ID "{device_id}". Device IDs are: '
                                   f'{", ".join([x.id for x in board.devices])}')
    device = board.device(device_id)

    # Part 1: Commands
    t = Table(show_lines=True)
    t.add_column("Command")
    t.add_column("Parameters")

    for cmd in device.commands:
        cmd_description = f'[b]{cmd.id}[/b]'
        if len(cmd.description) > 0:
            cmd_description += f'\n[i][grey42]{cmd.description}[/grey42][/i]'

        if len(cmd.parameters) == 0:
            param_info = '[i][grey42]None[/grey42][/i]'
        else:
            param_info = []
            max_id_length = max(len(x.id) for x in cmd.parameters)
            max_type_length = max(len(x.type) for x in cmd.parameters)

            for param in cmd.parameters:
                param_info.append((rf'{param.id:{max_id_length}} [grey42]\[{param.type}][/grey42]' +
                                   ' ' * (max_type_length - len(param.type))))
                if len(param.description) > 0:
                    param_info[-1] += f'   [i]{param.description}[/i]'
            param_info = '\n'.join(param_info)

        t.add_row(cmd_description, param_info)

    empty_line(obj.console)
    obj.console.print(Padding.indent(t, 2))
    empty_line(obj.console)

    # Part 2: FSMs
    t = Table(show_lines=True)
    t.add_column("FSM")
    t.add_column("States")
    t.add_column("Transitions")

    max_tr_id_length = max([len(x.id) for fsm in device.fsms for s in fsm.states for x in s.transitions])
    for fsm in device.fsms:
        states = []
        transitions = []
        for state in fsm.states:
            if state.id == fsm.initial_state:
                states.append(f'{state.id} [grey42](initial state)[/grey42]')
            elif (state.id == fsm.error_state) and (state.id != 'Error'):
                states.append(f'{state.id} [grey42](error state)[/grey42]')
            else:
                states.append(state.id)

            for transition in state.transitions:
                transitions.append(f'{transition.id:{max_tr_id_length}}   [grey42][i]{transition.start_state}[/i]'
                                   f' ➜ [i]{transition.end_state}[/i][/grey42]')

        t.add_row(fsm.id, '\n'.join(states), '\n'.join(transitions))

    obj.console.print(Padding.indent(t, 2))
    empty_line(obj.console)


@show.command('fsm', help='Lists a specific FSM\'s transitions, the commands make them up, and their parameters')
@click.argument('board', type=BOARD_PARAM_TYPE)
@click.argument('device_id', type=str)
@click.argument('fsm_id', type=str)
@click.pass_obj
@handle_rich_click_subcommand_exceptions
def show_fsm(obj, board, device_id, fsm_id):

    if device_id not in [x.id for x in board.devices]:
        raise click.ClickException(f'No processor or service module has ID "{device_id}". Device IDs are: '
                                   f'{", ".join([x.id for x in board.devices])}')
    device = board.device(device_id)

    if fsm_id not in [x.id for x in device.fsms]:
        raise click.ClickException(f'FSM "{fsm_id}" not found. Registered FSMs are: '
                                   f'{", ".join([x.id for x in device.fsms])}')
    fsm = device.fsm(fsm_id)

    t = Table(show_lines=True)
    t.add_column("Transition")
    t.add_column("Commands")

    for state in fsm.states:
        for transition in state.transitions:
            commands = []
            for i, step in enumerate(transition.steps, start=1):
                commands.append(f'{i}. {step.command.id}\n   [grey42]')
                if len(step.command.parameters) == 0:
                    commands[-1] += 'No parameters'
                else:
                    commands[-1] += 'Parameters: ' + ', '.join(p.id for p in step.command.parameters)
                commands[-1] += '[/grey42]'

            t.add_row(f'{transition.id}\n[grey42][i]{state.id}[/i] ➜ [i]{transition.end_state}[/grey42]',
                      '\n'.join(commands) if len(commands) > 0 else '[i]None[/i]')

    empty_line(obj.console)
    obj.console.print(Padding.indent(t, 2))
    empty_line(obj.console)


@show.command('board-input-map')
@click.argument('board', type=BOARD_PARAM_TYPE)
@click.argument('processor_regex', required=False, default='.*')
@click.argument('optics_regex', required=False, default='.*')
@click.option('--sort-by', type=click.Choice(['port', 'optics', 'frontpanel']), default=None)
@click.option('--include-absent', is_flag=True)
@click.pass_obj
@handle_rich_click_subcommand_exceptions
def show_board_input_map(obj, board, processor_regex, optics_regex, sort_by, include_absent):
    proc_pattern = re.compile(processor_regex)
    optics_pattern = re.compile(optics_regex)

    for proc in board.processors:
        if not proc_pattern.match(proc.id):
            continue

        table_data = []
        for port in proc.inputs:
            if port.connected_optics is not None:
                if optics_pattern.match(port.connected_optics[0]):
                    if (include_absent or
                       (port.is_present and board.optical_module(port.connected_optics[0]).is_present)):
                        optics_channel = board.optical_module(port.connected_optics[0]).input(port.connected_optics[1])
                        table_data.append((port.index,
                                           port.id,
                                           port.connected_optics[0],
                                           port.connected_optics[1],
                                           optics_channel.connected_fibre
                                           ))

        if sort_by == 'port':
            table_data = sorted(table_data, key=lambda x: x[0])
        elif sort_by == 'optics':
            table_data = sorted(table_data, key=lambda x: (x[2], x[3]))
        elif sort_by == 'frontpanel':
            table_data = sorted(table_data, key=lambda x: x[4])

        table = Table('FPGA port', 'Connected optics', 'Frontpanel')
        for _, port_id, optomodule_id, optomodule_channel, frontpanel in table_data:
            table.add_row(port_id, f'{optomodule_id}, ch {optomodule_channel:2}',
                          '[i]Unknown[/i]' if frontpanel is None else f'{frontpanel[0]}, ch {frontpanel[1]:2}')

        obj.console.print(f'Processor: {proc.id}')
        obj.console.print(Padding(table, (1, 0, 1, 3)))


@show.command('board-output-map')
@click.argument('board', type=BOARD_PARAM_TYPE)
@click.argument('processor_regex', required=False, default='.*')
@click.argument('optics_regex', required=False, default='.*')
@click.option('--sort-by', type=click.Choice(['port', 'optics']), default=None)
@click.option('--include-absent', is_flag=True)
@click.pass_obj
@handle_rich_click_subcommand_exceptions
def show_board_output_map(obj, board, processor_regex, optics_regex, sort_by, include_absent):
    proc_pattern = re.compile(processor_regex)
    optics_pattern = re.compile(optics_regex)

    for proc in board.processors:
        if not proc_pattern.match(proc.id):
            continue

        table = Table('FPGA port', 'Connected optics', 'Frontpanel')

        table_data = []
        for port in proc.outputs:
            if port.connected_optics is not None:
                if optics_pattern.match(port.connected_optics[0]):
                    if (include_absent or
                       (port.is_present and board.optical_module(port.connected_optics[0]).is_present)):
                        optics_channel = board.optical_module(port.connected_optics[0]).output(port.connected_optics[1])
                        table_data.append((port.index,
                                           port.id,
                                           port.connected_optics[0],
                                           port.connected_optics[1],
                                           optics_channel.connected_fibre
                                           ))

        if sort_by == 'port':
            table_data = sorted(table_data, key=lambda x: x[0])
        elif sort_by == 'optics':
            table_data = sorted(table_data, key=lambda x: (x[2], x[3]))
        elif sort_by == 'frontpanel':
            table_data = sorted(table_data, key=lambda x: x[4])

        for _, port_id, optomodule_id, optomodule_channel, frontpanel in table_data:
            table.add_row(port_id, f'{optomodule_id}, ch {optomodule_channel:2}',
                          '[i]Unknown[/i]' if frontpanel is None else f'{frontpanel[0]}, ch {frontpanel[1]:2}')

        obj.console.print(f'Processor: {proc.id}')
        obj.console.print(Padding(table, (1, 0, 1, 3)))


def get_port_status_string(port):
    if port.is_masked:
        status_str = 'Masked'
        if port.setting == Setting.ENABLED:
            status_str = 'Masked, but enabled (' + port.status.__rich__() + ')'
        elif port.setting == Setting.NON_CRITICAL:
            status_str = 'Masked, but not disabled (' + port.status.__rich__() + ')'
    else:
        status_str = ''
        if port.setting == Setting.DISABLED:
            status_str = 'Disabled'
        else:
            if port.setting == Setting.NON_CRITICAL:
                status_str = 'Non-critical, '
            status_str += port.status.__rich__()

    return status_str


@show.command('io-operating-modes')
@click.argument('board', type=BOARD_PARAM_TYPE)
@click.argument('processor_id_regex', required=False, default='.*')
@click.option('--include-absent', is_flag=True)
@click.pass_obj
@handle_rich_click_subcommand_exceptions
def show_io_operating_modes(obj, board, processor_id_regex, include_absent):
    proc_pattern = re.compile(processor_id_regex)

    for proc in board.processors:
        if not proc_pattern.match(proc.id):
            continue

        obj.console.print(f'Processor: {proc.id}')
        table = Table('Index', 'Input port', 'Status', 'Mode', 'Output port', 'Status', 'Mode', 'Loopback')

        ports = {}
        for port in proc.inputs:
            ports[port.index] = (port, None)
        for port in proc.outputs:
            ports[port.index] = (ports[port.index][0], port) if port.index in ports else (None, port)

        ports = [(k, v[0], v[1]) for k, v in ports.items()]
        ports = sorted(ports)

        for (i, input_port, output_port) in ports:

            if (include_absent or
               (input_port is not None and input_port.is_present) or
               (output_port is not None and output_port.is_present)):
                row_data = [str(i)]

                if input_port is None:
                    row_data += ['-', '', '']
                else:

                    row_data += [input_port.id, get_port_status_string(input_port), str(input_port.operating_mode)]

                if output_port is None:
                    row_data += ['-', '', '']
                else:
                    row_data += [output_port.id, get_port_status_string(output_port), str(output_port.operating_mode)]

                row_data.append('Yes' if input_port.is_in_loopback else 'No')
                table.add_row(*row_data)

        obj.console.print(table)


def _summarize_port_mode(port):
    summary = str(port.operating_mode)
    if port.is_masked:
        summary += ', masked'
    if port.is_in_loopback:
        summary += ', in loopback'
    return summary


def _summarize_mon_obj_status(mon_obj):
    if mon_obj.setting == Setting.DISABLED:
        return '[i]Disabled[/i]'

    prefix = '[i]Non-critical[/i], ' if mon_obj.setting == Setting.NON_CRITICAL else ''
    if mon_obj.status == Status.GOOD:
        return prefix + '[on green]GOOD[/on green]'
    elif mon_obj.status == Status.WARNING:
        return prefix + '[on dark_orange]WARNING[/on dark_orange]'
    elif mon_obj.status == Status.ERROR:
        return prefix + '[on red]ERROR[/on red]'
    elif mon_obj.status == Status.UNKNOWN:
        return prefix + '[orange_red1]Unknown[/orange_red1]'
    else:
        return prefix + 'Neutral'


@show.command('board-inputs')
@click.argument('board', type=BOARD_PARAM_TYPE)
@click.argument('processor_regex', required=False, default='.*')
@click.argument('optics_regex', required=False, default='.*')
@click.option('--sort-by', type=click.Choice(['port', 'optics', 'frontpanel', 'link']), default=None)
@click.option('--include-masked', is_flag=True)
@click.option('--include-absent', is_flag=True)
@click.pass_obj
@handle_rich_click_subcommand_exceptions
def show_board_inputs(obj, board, processor_regex, optics_regex, sort_by, include_masked, include_absent):
    proc_pattern = re.compile(processor_regex)
    optics_pattern = re.compile(optics_regex)

    table_data = []
    for proc in board.processors:
        if not proc_pattern.match(proc.id):
            continue

        for port in proc.inputs:
            if (not include_masked) and port.is_masked:
                continue

            if port.connected_optics is not None:
                if not optics_pattern.match(port.connected_optics[0]):
                    continue

                optical_module = board.optical_module(port.connected_optics[0])
                if (not include_absent) and (not port.is_present) and (not optical_module.is_present):
                    continue

                optical_channel = optical_module.input(port.connected_optics[1])

                link_id = 'None'
                for link in obj.system.optical_links:
                    if ((link.destination.board.id is board.id)
                       and (link.destination.processor.id is proc.id)
                       and (link.destination.port.index is port.index)):
                        link_id = link.id
                        break

                table_data.append((proc,
                                   port,
                                   optical_module,
                                   optical_channel,
                                   link_id))

    if sort_by == 'port':
        table_data = sorted(table_data, key=lambda x: (x[0].id, x[1].index))
    elif sort_by == 'optics':
        table_data = sorted(table_data, key=lambda x: (x[2].id, x[3].index))
    elif sort_by == 'frontpanel':
        table_data = sorted(table_data, key=lambda x: x[3].connected_fibre)
    elif sort_by == 'link':
        table_data = sorted(table_data, key=lambda x: x[4])

    t = Table('Link ID', 'Front panel', 'Optical channel', 'Optics status', '', 'FPGA port', 'Port mode', 'Port status',
              box=box.SIMPLE)
    for proc, port, optical_module, optical_channel, link_id in table_data:
        frontpanel = optical_channel.connected_fibre
        t.add_row(f'{link_id}',
                  '[i]Unknown[/i]' if frontpanel is None else f'{frontpanel[0]}, ch {frontpanel[1]:2}',
                  f'{optical_module.id}, ch {optical_channel.index:2}',
                  _summarize_mon_obj_status(optical_channel),
                  ':right_arrow:',
                  f'{proc.id}, ch {port.index:3}',
                  _summarize_port_mode(port),
                  _summarize_mon_obj_status(port))

    obj.console.print(Padding(t, (1, 0, 1, 3)))


@show.command('board-outputs')
@click.argument('board', type=BOARD_PARAM_TYPE)
@click.argument('processor_regex', required=False, default='.*')
@click.argument('optics_regex', required=False, default='.*')
@click.option('--sort-by', type=click.Choice(['port', 'optics', 'frontpanel', 'link']), default=None)
@click.option('--include-masked', is_flag=True)
@click.option('--include-absent', is_flag=True)
@click.pass_obj
@handle_rich_click_subcommand_exceptions
def show_board_outputs(obj, board, processor_regex, optics_regex, sort_by, include_masked, include_absent):
    proc_pattern = re.compile(processor_regex)
    optics_pattern = re.compile(optics_regex)

    table_data = []
    for proc in board.processors:
        if not proc_pattern.match(proc.id):
            continue

        for port in proc.outputs:
            if (not include_masked) and port.is_masked:
                continue

            if port.connected_optics is not None:
                if not optics_pattern.match(port.connected_optics[0]):
                    continue

                optical_module = board.optical_module(port.connected_optics[0])
                if (not include_absent) and (not port.is_present) and (not optical_module.is_present):
                    continue

                optical_channel = optical_module.output(port.connected_optics[1])

                link_id = 'None'
                for link in obj.system.optical_links:
                    if ((link.source.board.id is board.id)
                       and (link.source.processor.id is proc.id)
                       and (link.source.port.index is port.index)):
                        link_id = link.id
                        break

                table_data.append((proc,
                                   port,
                                   optical_module,
                                   optical_channel,
                                   link_id))

    if sort_by == 'port':
        table_data = sorted(table_data, key=lambda x: (x[0].id, x[1].index))
    elif sort_by == 'optics':
        table_data = sorted(table_data, key=lambda x: (x[2].id, x[3].index))
    elif sort_by == 'frontpanel':
        table_data = sorted(table_data, key=lambda x: x[3].connected_fibre)
    elif sort_by == 'link':
        table_data = sorted(table_data, key=lambda x: x[4])

    t = Table('FPGA port', 'Port mode', 'Port status', '', 'Optical channel', 'Optics status', 'Front panel', 'Link',
              box=box.SIMPLE)
    for proc, port, optical_module, optical_channel, link_id in table_data:
        frontpanel = optical_channel.connected_fibre
        t.add_row(f'{proc.id}, ch {port.index:3}',
                  _summarize_port_mode(port),
                  _summarize_mon_obj_status(port),
                  ':right_arrow:',
                  f'{optical_module.id}, ch {optical_channel.index:2}',
                  _summarize_mon_obj_status(optical_channel),
                  '[i]Unknown[/i]' if frontpanel is None else f'{frontpanel[0]}, ch {frontpanel[1]:2}',
                  f'{link_id}')

    obj.console.print(Padding(t, (1, 0, 1, 3)))


@show.command('board-internal-links')
@click.argument('board', type=BOARD_PARAM_TYPE)
@click.option('--include-masked', is_flag=True)
@click.option('--include-absent', is_flag=True)
@click.pass_obj
@handle_rich_click_subcommand_exceptions
def show_board_internal_links(obj, board, include_masked, include_absent):

    t = Table('Output port', 'Status', '', 'Input port', 'Status', 'Operating mode', box=box.SIMPLE)
    for tx_proc in board.processors:
        for tx_port in tx_proc.outputs:
            if tx_port.connected_port is not None:
                rx_proc = board.processor(tx_port.connected_port[0])
                rx_port = rx_proc.input(tx_port.connected_port[1])

                if (not include_masked) and tx_port.is_masked and rx_port.is_masked:
                    continue

                if (not include_absent) and (not tx_port.is_present) and (not rx_port.is_present):
                    continue

                if ((tx_port.operating_mode != rx_port.operating_mode) or
                   (tx_port.is_masked != rx_port.is_masked) or
                   (tx_port.is_in_loopback != rx_port.is_in_loopback)):
                    mode_summary = f'TX: {_summarize_port_mode(tx_port)}\nRX: {_summarize_port_mode(rx_port)}'
                else:
                    mode_summary = _summarize_port_mode(tx_port)

                t.add_row(f'{tx_proc.id}, ch {tx_port.index:3}',
                          _summarize_mon_obj_status(tx_port),
                          ':right_arrow:',
                          f'{rx_proc.id}, ch {rx_port.index:3}',
                          _summarize_mon_obj_status(rx_port),
                          mode_summary
                          )

    obj.console.print(Padding(t, (1, 0, 1, 3)))


@show.command('properties')
@click.argument('board', type=BOARD_PARAM_TYPE)
@click.argument('device_regex', required=True)
@click.argument('property_path_regex', required=False, default=None)
# TODO: Add ability to filter on obj status
@click.pass_obj
@handle_rich_click_subcommand_exceptions
def show_properties(obj, board, device_regex, property_path_regex):
    devices = []
    for p in board.processors:
        if re.fullmatch(device_regex, p.id, re.IGNORECASE):
            devices.append(p)
    for m in board.optical_modules:
        if re.fullmatch(device_regex, m.id, re.IGNORECASE):
            devices.append(m)

    if len(devices) == 0:
        raise click.ClickException(f'No FPGA or optical module matches "{device_regex}"')

    def obj_status_filter(_x, _y):
        return False
    if property_path_regex is None:
        status_summary = create_property_table(devices, relative_path_regex='.*', obj_status_filter=obj_status_filter)
    else:
        status_summary = create_property_table(devices,
                                               relative_path_regex=f'{board.id}\\.{device_regex}\\.'
                                                                   f'{property_path_regex}',
                                               obj_status_filter=obj_status_filter)

    if status_summary is None:
        obj.console.print('No properties match the supplied criteria')
    else:
        obj.console.print(Padding.indent(status_summary, 7))


@show.command('metrics')
@click.argument('board', type=BOARD_PARAM_TYPE)
@click.argument('device_regex', required=True)
@click.argument('metric_path_regex', required=False, default=None)
# TODO: Add ability to filter on obj status
@click.pass_obj
@handle_rich_click_subcommand_exceptions
def show_metrics(obj, board, device_regex, metric_path_regex):
    devices = []
    for p in board.processors:
        if re.fullmatch(device_regex, p.id, re.IGNORECASE):
            devices.append(p)
    for m in board.optical_modules:
        if re.fullmatch(device_regex, m.id, re.IGNORECASE):
            devices.append(m)

    if len(devices) == 0:
        raise click.ClickException(f'No FPGA or optical module matches "{device_regex}"')

    def obj_status_filter(_x, _y):
        return False
    if metric_path_regex is None:
        status_summary = create_metric_data_table(devices, metric_regex='.*', obj_status_filter=obj_status_filter)
    else:
        obj.console.print(f'METRIC REGEX: {board.id}\\.{device_regex}\\.{metric_path_regex}')
        status_summary = create_metric_data_table(devices,
                                                  metric_regex=f'{board.id}\\.{device_regex}\\.{metric_path_regex}',
                                                  obj_status_filter=obj_status_filter)

    if status_summary is None:
        obj.console.print('No metrics match the supplied criteria')
    else:
        obj.console.print(Padding.indent(status_summary, 7))


@show.command('history')
@click.argument('board', type=BOARD_PARAM_TYPE)
@click.pass_obj
@handle_rich_click_subcommand_exceptions
def show_history(obj, board):
    history = board.get_history()
    if len(history) == 0:
        obj.console.print('No events since application started')
        return

    obj.console.print('Chronological history ...')
    for i, item in enumerate(history):
        empty_line(obj.console)
        time_str = f'{item.time.astimezone(timezone.utc):%H:%M:%S} UTC'
        if item.event_type == EventType.LeaseRequest:
            obj.console.print(f'[bold]{i}. Lease requested[/bold] at {time_str}')
            obj.console.print(f'   Supervisor: {item.supervisor}', style='bright_blue')
            obj.console.print(f'   Duration: {item.duration:.1f} seconds', style='bright_blue')
        elif item.event_type == EventType.LeaseRenewal:
            obj.console.print(f'[bold]{i}. Lease renewed[/bold] at {time_str}')
            obj.console.print(f'   Supervisor: {item.supervisor}', style='bright_blue')
        elif item.event_type == EventType.LeaseRetire:
            obj.console.print(f'[bold]{i}. Lease retired[/bold] at {time_str}')
            obj.console.print(f'   Supervisor: {item.supervisor}', style='bright_blue')
        elif item.event_type == EventType.Command:
            obj.console.print(f'[bold]{i}. {item.device}: Ran {item.command} command[/bold] at {time_str}')
            obj.console.print(Padding(create_command_snapshot_table(item.snapshot), (0, 0, 0, 1)))
        elif item.event_type == EventType.FSMEngage:
            obj.console.print(f'[bold]{i}. {item.device}: Engaged {item.fsm} FSM[/bold] at {time_str}')
        elif item.event_type == EventType.FSMReset:
            obj.console.print(f'[bold]{i}. {item.device}: Reset {item.fsm} FSM[/bold] at {time_str}')
        elif item.event_type == EventType.FSMDisengage:
            obj.console.print(f'[bold]{i}. {item.device}: Disengaged {item.fsm} FSM[/bold] at {time_str}')
        elif item.event_type == EventType.FSMTransition:
            obj.console.print(f'[bold]{i}. {item.device}: Ran {item.transition} transition[/bold] at {time_str}')
            obj.console.print(Padding(create_transition_snapshot_table([item.snapshot], [None], [None]), (0, 0, 0, 1)))


COMMANDS = [show]
