
import click

from click_didyoumean import DYMGroup
from click_repl import repl
from .core import CLIObject
from . import actions, monitoring, show, system
from ..utilities import decorate_rich_click_root_command, handle_rich_click_subcommand_exceptions, parse_var_flag
from ...config import parse_system_file
from ...system import System


COMMANDS = actions.COMMANDS + monitoring.COMMANDS + show.COMMANDS + system.COMMANDS


@click.group(cls=DYMGroup)
@click.help_option('-h', '--help')
@click.option('-c', '--config', envvar='SWATCH_CONFIG', type=click.Path(exists=True), default=None,
              help='Configuration file (incl. system description and parameter values)')
@click.option('--var', 'param_replacements', multiple=True, callback=parse_var_flag,
              help="Specify variable for replacing parameter values in config file")
@click.pass_context
@decorate_rich_click_root_command
def cli(ctx, console, config, param_replacements):
    if config is None:
        ctx.obj = CLIObject(console)
    else:
        system_stub, link_mode_mgr, pool = parse_system_file(config, param_replacements, use_all_replacements=True)
        ctx.obj = CLIObject(console, System(system_stub), link_mode_mgr, pool)


for cmd in COMMANDS:
    cli.add_command(cmd)


@cli.command('console')
@click.pass_obj
@handle_rich_click_subcommand_exceptions
def _console(obj):
    obj.console.print('All of the board-manager subcommands can be run from this console; to list them, type "--help".')
    obj.console.print('To list console-specific commands, type ":help".')
    obj.console.print('To quit, type ":q", ":quit" or ":exit".\n')

    repl(click.get_current_context())
