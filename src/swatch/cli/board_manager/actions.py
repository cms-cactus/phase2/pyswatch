
import click
import os
import re

from .core import BOARDS_PARAM_TYPE, obtain_leases
from ..utilities import handle_rich_click_subcommand_exceptions
from ...action import FileResult
from ...board import Processor, ServiceModule
from ...console.input import get_command_parameters
from ...console.utilities import check_parameters_for_fsm, disengage_devices_from_fsm, engage_devices_in_fsm
from ...console.utilities import ensure_devices_in_fsm_initial_state, reset_devices_in_fsm, run_command
from ...console.utilities import run_fsm_transition


__DEVICE_TYPE_MAP = {
    'all': {Processor, ServiceModule},
    'proc': {Processor},
    'service-module': {ServiceModule}
}


__DEFAULT_DEVICE_TYPE = 'all'


def __device_type_callback(_ctx, _param, value):
    return __DEVICE_TYPE_MAP[value]


# check-parameters BOARD_REGEX/_HOSTNAME DEVICE_REGEX FSM_ID [--device-type=(proc|service-module)] [--verbose]
@click.command('check-parameters')
@click.argument('boards', metavar='board_id_regex_or_hostname', type=BOARDS_PARAM_TYPE)
@click.argument('device_regex', type=str)
@click.argument('fsm_id', type=str)
@click.option('--device-type', default=__DEFAULT_DEVICE_TYPE, type=click.Choice(__DEVICE_TYPE_MAP.keys()),
              callback=__device_type_callback, help='Filter based on device type ("proc" or "service-module")')
@click.option('--verbose', is_flag=True, help='Print values of all parameters')
@click.pass_obj
@handle_rich_click_subcommand_exceptions
def check_parameters(obj, boards, device_regex, fsm_id, device_type, verbose):
    device_pattern = re.compile(device_regex)
    devices = []
    for board in boards:
        for x in board.devices:
            if device_pattern.match(x.id):
                if type(x) in device_type:
                    devices.append(x)

    check_parameters_for_fsm(obj.console, devices, fsm_id, obj.settings_pool, verbose=verbose)
    obj.console.print('Parameter values validated - no errors found.')


# engage-fsm BOARD_REGEX/_HOSTNAME DEVICE_REGEX FSM_ID [--force] [--device-type=(proc|service-module)]
@click.command('engage-fsm')
@click.argument('boards', metavar='board_id_regex_or_hostname', type=BOARDS_PARAM_TYPE)
@click.argument('device_regex', type=str)
@click.argument('fsm_id', type=str)
@click.option('--force', is_flag=True, help='Disengage from other FSM first if required')
@click.option('--device-type', default=__DEFAULT_DEVICE_TYPE, type=click.Choice(__DEVICE_TYPE_MAP.keys()),
              callback=__device_type_callback, help='Filter based on device type ("proc" or "service-module")')
@click.pass_obj
@handle_rich_click_subcommand_exceptions
def engage_fsm(obj, boards, device_regex, fsm_id, force, device_type):
    device_pattern = re.compile(device_regex)
    devices = []
    for board in boards:
        for x in board.devices:
            if device_pattern.match(x.id):
                if type(x) in device_type:
                    devices.append(x)

    devices_without_requested_fsm = [d.path for d in devices if not d.has_fsm(fsm_id)]

    if len(devices_without_requested_fsm) > 0:
        raise click.ClickException((f'No FSM named "{fsm_id}" is registered in ' +
                                    ', '.join(devices_without_requested_fsm)))

    leases = obtain_leases(boards, 2)
    if force:
        ensure_devices_in_fsm_initial_state(obj.console, devices, fsm_id, leases)
    else:
        devices_already_engaged = [d.path for d in devices if d.currentFSM is not None]
        if len(devices_already_engaged) > 0:
            raise click.ClickException((f'{len(devices_already_engaged)} devices already engaged in an FSM: ' +
                                        ', '.join(devices_already_engaged)))

        engage_devices_in_fsm(obj.console, devices, fsm_id, leases)


# disengage-fsm BOARD_REGEX/_HOSTNAME DEVICE_REGEX [--device-type=(proc|service-module)]
@click.command('disengage-fsm')
@click.argument('boards', metavar='board_id_regex_or_hostname', type=BOARDS_PARAM_TYPE)
@click.argument('device_regex', type=str)
@click.option('--device-type', default=__DEFAULT_DEVICE_TYPE, type=click.Choice(__DEVICE_TYPE_MAP.keys()),
              callback=__device_type_callback, help='Filter based on device type ("proc" or "service-module")')
@click.pass_obj
@handle_rich_click_subcommand_exceptions
def disengage_fsm(obj, boards, device_regex, device_type):
    device_pattern = re.compile(device_regex)
    devices = []
    for board in boards:
        for x in board.devices:
            if device_pattern.match(x.id):
                if type(x) in device_type:
                    devices.append(x)

    disengage_devices_from_fsm(obj.console, devices, obtain_leases(boards, 2))


# reset-fsm BOARD_REGEX/_HOSTNAME DEVICE_REGEX [--device-type=(proc|service-module)]
@click.command('reset-fsm')
@click.argument('boards', metavar='board_id_regex_or_hostname', type=BOARDS_PARAM_TYPE)
@click.argument('device_regex', type=str)
@click.option('--device-type', default=__DEFAULT_DEVICE_TYPE, type=click.Choice(__DEVICE_TYPE_MAP.keys()),
              callback=__device_type_callback, help='Filter based on device type ("proc" or "service-module")')
@click.pass_obj
@handle_rich_click_subcommand_exceptions
def reset_fsm(obj, boards, device_regex, device_type):
    device_pattern = re.compile(device_regex)
    devices = []
    for board in boards:
        for x in board.devices:
            if device_pattern.match(x.id):
                if type(x) in device_type:
                    devices.append(x)

    reset_devices_in_fsm(obj.console, devices, obtain_leases(boards, 2))


# run-command BOARD_REGEX/_HOSTNAME DEVICE_REGEX COMMAND_ID [--strict] [--skip-metric-update]
#             [--device-type=(proc|service-module)]
@click.command('run-command')
@click.argument('boards', metavar='board_id_regex_or_hostname', type=BOARDS_PARAM_TYPE)
@click.argument('device_regex', type=str)
@click.argument('command_id', type=str)
@click.option('--interactive', is_flag=True,
              help='Enter parameters manually, rather than taking them from the config file')
@click.option('--strict', is_flag=True, help='Return non-zero exit code if warning issued')
@click.option('--skip-metric-update', is_flag=True, help='Do not update monitoring data at end')
@click.option('--device-type', default=__DEFAULT_DEVICE_TYPE, type=click.Choice(__DEVICE_TYPE_MAP.keys()),
              callback=__device_type_callback, help='Filter based on device type ("proc" or "service-module")')
@click.option('--save-files-to', 'file_save_path', default='/tmp', help='Path to which file results are saved')
@click.pass_obj
@handle_rich_click_subcommand_exceptions
def _run_command(obj, boards, device_regex, command_id, interactive, strict, skip_metric_update, device_type,
                 file_save_path):
    device_pattern = re.compile(device_regex)
    devices = []
    for board in boards:
        for x in board.devices:
            if device_pattern.match(x.id):
                if type(x) in device_type:
                    devices.append(x)

    param_sets = []
    if interactive or obj.settings_pool is None:
        for device in devices:
            param_sets.append(get_command_parameters(device.command(command_id), msg_suffix=f' for {device.path}'))
    else:
        obj.console.print('Extracting parameters from config file')
        for device in devices:
            param_sets.append({})
            for parameter in device.command(command_id).parameters:
                value = obj.settings_pool.get_parameter('no_namespace', command_id, parameter.id, device.contexts)
                if value is None:
                    raise click.ClickException(f'Could not find parameter {parameter.id} for {device.path}'
                                               ' in config file')
                param_sets[-1][parameter.id] = value

    snapshots = run_command(obj.console, devices, command_id, param_sets, obtain_leases(boards, 20), strict=strict)

    os.makedirs(file_save_path, exist_ok=True)
    for device, cmd_snapshot in zip(devices, snapshots):
        if type(cmd_snapshot.result) is FileResult:
            file_path = os.path.join(file_save_path, f'{device.id}-{cmd_snapshot.result.name}')
            obj.console.print(f'Saving file result from {device.path} {command_id} to {file_path}')
            cmd_snapshot.result.save(file_path)

    if not skip_metric_update:
        for device in (devices if obj.system is None else obj.system.connected_processors):
            obj.console.print(f'Updating monitoring data for {device.path}')
            device.refresh()


# run-transition BOARD_REGEX/_HOSTNAME DEVICE_REGEX TRANSITION_ID [--skip-metric-update] [--strict]
#                [--device-type=(proc|service-module)] [--var]
@click.command('run-transition')
@click.argument('boards', metavar='board_id_regex_or_hostname', type=BOARDS_PARAM_TYPE)
@click.argument('device_regex', type=str)
@click.argument('transition_id', type=str)
@click.option('--strict', is_flag=True, help='Return non-zero exit code if warning issued')
@click.option('--skip-metric-update', is_flag=True, help='Do not update monitoring data at end')
@click.option('--device-type', default=__DEFAULT_DEVICE_TYPE, type=click.Choice(__DEVICE_TYPE_MAP.keys()),
              callback=__device_type_callback, help='Filter based on device type ("proc" or "service-module")')
@click.pass_obj
@handle_rich_click_subcommand_exceptions
def _run_transition(obj, boards, device_regex, transition_id, skip_metric_update, strict, device_type):
    device_pattern = re.compile(device_regex)
    devices = []
    for board in boards:
        for x in board.devices:
            if device_pattern.match(x.id):
                if type(x) in device_type:
                    devices.append(x)

    device_states = {}
    for d in devices:
        if (d.currentFSM, d.currentState) not in device_states:
            device_states[(d.currentFSM, d.currentState)] = [d.path]
        else:
            device_states[(d.currentFSM, d.currentState)].append(d.path)

    if len(device_states) > 1:
        message = 'Devices are in multiple FSMs/states: '
        for (fsm_id, state_id), devices in device_states:
            message += ', '.join(devices)
            if fsm_id is None:
                message += ' are not in any FSM'
            else:
                message += f' are in FSM {fsm_id}, state {state_id}'
            message += ';'
        raise click.ClickException(message[0:-1])

    fsm_id, state_id = list(device_states.keys())[0]

    run_fsm_transition(obj.console, devices, fsm_id, state_id, transition_id,
                       obj.settings_pool, obtain_leases(boards, 20),
                       refresh=not skip_metric_update, strict=strict)


COMMANDS = [check_parameters, engage_fsm, disengage_fsm, reset_fsm, _run_command, _run_transition]
