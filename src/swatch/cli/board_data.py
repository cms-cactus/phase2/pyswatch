
import click
import re

from .utilities import decorate_rich_click_root_command, parse_index_list_string
from ..utilities.board_data import compare, read, write, FileFormat, BoardData, ChannelData
from ..utilities.misc import format_index_list
from click import echo

CONTEXT_SETTINGS = {'help_option_names': ['-h', '--help']}


def print_mismatches(match_results, expected_data, observed_data, binary=False):
    n_frames_compared = 0
    n_header_fields_differ = 0
    n_data_words_differ = 0
    for c in match_results.bad_channels:
        n_frames_compared += match_results.at(c).n_frames_compared
        n_header_fields_differ += match_results.at(c).n_header_fields_differ
        n_data_words_differ += (match_results.at(c).n_fields_differ - match_results.at(c).n_header_fields_differ)

    echo(f'Mismatches found in {n_frames_compared} frames on channels '
         f'{format_index_list(match_results.bad_channels)}:\n'
         f'  {n_header_fields_differ} metadata mismatches '
         f'(strobe, valid, start of orbit, start of packet, end of packet)\n'
         f'  {n_data_words_differ} data word mismatches\n')

    for c in sorted(match_results.bad_channels):
        ch_result = match_results.at(c)
        for j, (diff, exp_frame, obs_frame) in enumerate(zip(ch_result.diff,
                                                             expected_data.at(c)._data,
                                                             observed_data.at(c)._data[ch_result.latency:])):
            if (diff.strobe or diff.valid or diff.start_of_orbit or
               diff.start_of_packet or diff.end_of_packet or diff.payload):
                print(f'Ch{c:3}, expected[{j:4}] != observed[{ch_result.latency + j:4}]:   '
                      f'{int(exp_frame.strobe)} {int(exp_frame.valid)} {int(exp_frame.start_of_orbit)} '
                      f'{int(exp_frame.start_of_packet)} {int(exp_frame.end_of_packet)}   '
                      f'{format_data(exp_frame.payload, binary=binary)}   vs   '
                      f'{int(obs_frame.strobe)} {int(obs_frame.valid)} {int(obs_frame.start_of_orbit)} '
                      f'{int(obs_frame.start_of_packet)} {int(obs_frame.end_of_packet)}   '
                      f'{format_data(obs_frame.payload, binary=binary)}')
        print()


# Returns 64-bit data word, with space separating segments
def format_data(x, binary=True):
    x = int(x)
    if binary:
        return f'{x>>32:039_b}'.replace('_', ' ') + '   ' + f'{x&0xFFFFFFFF:039_b}'.replace('_', ' ')
    else:
        return f'0x{x:019_x}'.replace('_', ' ')


#############################################################################
# SECTION 3: CLI

def parse_channels(_ctx, _param, value):
    return parse_index_list_string(value)


def _main_compare(expected, observed, channels, latencies, max_frames, verbose, quiet, binary):
    """Compares a pair of board data files

    EXPECTED    Path for expected board data (i.e. file from emulator)
    OBSERVED    Path for captured board data (i.e. file from sim/HW)
    """

    if (verbose and quiet):
        raise click.ClickException("'--verbose' and '--quiet' are mutually exclusive")

    if (max_frames is not None) and (max_frames < 0):
        raise click.ClickException("'--max-frames' cannot have a negative value")

    for x in latencies:
        if x < 0:
            raise RuntimeError('Latency values must be non-negative integers')

    expected_data = read(expected)
    observed_data = read(observed)

    echo('Board data files ...')
    echo(f'    Reference: {expected:{max(len(expected),len(observed))}}    '
         f'({len(expected_data.channels)} channels: {format_index_list(expected_data.channels)})')
    echo(f'    Observed:  {observed:{max(len(expected),len(observed))}}    '
         f'({len(observed_data.channels)} channels: {format_index_list(observed_data.channels)})')

    if channels is not None:
        for i in set(expected_data.channels) - set(channels):
            expected_data.remove(i)
        for i in set(observed_data.channels) - set(channels):
            observed_data.remove(i)

    # Check for missing channels in reference or observed files
    extra_channels = list(set(expected_data.channels) - set(observed_data.channels))
    if len(extra_channels) > 0:
        raise click.ClickException(f'Channels {format_index_list(extra_channels)} are missing from the observed data')
    extra_channels = list(set(observed_data.channels) - set(expected_data.channels))
    if len(extra_channels) > 0:
        raise click.ClickException(f'Channels {format_index_list(extra_channels)} are missing from the reference data')

    # Compare observed data with reference
    echo()
#    echo(f'Comparing data in channels {format_index_list(observed_data.channels)}')
    match_result = compare(expected_data, observed_data, latencies=latencies, max_frames=max_frames)

    if bool(match_result):
        print(f'Perfect match found (channels {format_index_list(expected_data.channels)})')
        return match_result
    else:
        print_mismatches(match_result, expected_data, observed_data, binary=binary)
        bad_channels = [c for c in match_result.channels if not bool(match_result.at(c))]
        raise click.ClickException(f'Could not find a perfect match on channels {format_index_list(bad_channels)}!')


@click.command()
@click.argument('expected', type=click.Path('r'))  # , help='Path to expected board data (i.e. from emulator)')
@click.argument('observed', type=click.Path('r'))  # , help='Path to captured board data (i.e. from sim/HW)')
@click.option('-c', '--channels', default=None, callback=parse_channels,
              help='Subset of channels to compare, e.g. "0,5,8-11"')
@click.option('--latency', callback=parse_channels,
              help='Number of clock cycles at start of observed data that are ignored - give a range')
@click.option('--max-frames', type=click.INT, default=None, help='Maximum number of clock cycles to compare')
@click.option('-v', '--verbose', is_flag=True, help='Increase verbosity')
@click.option('-q', '--quiet', is_flag=True, help='Decrease verbosity')
@click.option('--binary', is_flag=True, help='Print out data as binary (rather than hex)')
def main_compare(expected, observed, channels, latency, max_frames, verbose, quiet, binary):
    """Compares a pair of board data files

    EXPECTED    Path for expected board data (i.e. file from emulator)
    OBSERVED    Path for captured board data (i.e. file from sim/HW)
    """
    _main_compare(expected, observed, channels, latency, max_frames, verbose, quiet, binary)


def _main_merge(inputs, fmt, output):

    merge_data = {}
    for file in inputs:
        board_data = read(file)
        for chan, data_columns in board_data._data.items():
            if chan in merge_data:
                raise RuntimeError(f"Duplicate channel: {chan}")

            merge_data[chan] = data_columns

    write(BoardData('merge', merge_data), output, fmt)


_FILE_FORMAT_MAP = {
    'APx': FileFormat.APx,
    'EMPv1': FileFormat.EMPv1,
    'EMPv2': FileFormat.EMPv2,
    'X2O': FileFormat.X2O
}

_FILE_FORMAT_TYPE = click.Choice(_FILE_FORMAT_MAP.keys(), case_sensitive=False)


def _file_format_callback(_ctx, _param, value):
    return _FILE_FORMAT_MAP[value]


@click.command()
@click.argument('inputs', nargs=-1, type=click.Path('r'))
@click.argument('file_format', type=_FILE_FORMAT_TYPE, callback=_file_format_callback)
@click.argument('output', type=click.Path())
@decorate_rich_click_root_command
# pylint: disable-next=unused-argument
def main_merge(console, inputs, file_format, output):
    _main_merge(inputs, file_format, output)


_PACKET_STRUCTURE_OPTION_REGEX = r'(\d+)\/(\d+)(?:@(-?\d+))?'


def _parse_packet_structure_option(_ctx, _param, value):
    if value is None:
        return None

    match = re.fullmatch(_PACKET_STRUCTURE_OPTION_REGEX, value)
    if not match:
        raise click.BadParameter('Expected "LENGTH/PERIODICITY" or "LENGTH/PERIODICITY@START", '
                                 'e.g. "10/54" for 10-frame-long TMUX6 packets')

    return {
        'length': int(match[1]),
        'periodicity': int(match[2]),
        'start': 0 if match[3] is None else int(match[3])
    }


@click.command()
@click.argument('input_file', type=click.Path('r'))
@click.argument('file_format', type=_FILE_FORMAT_TYPE, callback=_file_format_callback)
@click.argument('output', type=click.Path())
@click.option('--strip-packet-structure', is_flag=True)
@click.option('--add-packet-structure', 'packet_structure', default=None, callback=_parse_packet_structure_option)
@decorate_rich_click_root_command
# pylint: disable-next=unused-argument
def main_convert(console, input_file, file_format, output, strip_packet_structure, packet_structure):

    board_data = read(input_file)

    if strip_packet_structure:
        board_data.strip_packet_structure()
    if packet_structure is not None:
        board_data.add_packet_structure(**packet_structure)

    write(board_data, output, file_format)


@click.command()
@click.argument('channels', callback=parse_channels)
@click.argument('depth', type=click.IntRange(min=1))
@click.argument('file_path', type=click.Path())
@click.argument('file_format', type=_FILE_FORMAT_TYPE, callback=_file_format_callback)
@decorate_rich_click_root_command
# pylint: disable-next=unused-argument
def main_generate(console, channels, depth, file_path, file_format):
    console.print(f'Channels: {channels}')
    console.print(f'Depth:    {depth}')
    console.print(f'Format:   {file_format}')
    console.print(f'Path:     {file_path}')

    board_data = BoardData('generated', {c: ChannelData(depth) for c in channels})

    write(board_data, file_path, file_format)
