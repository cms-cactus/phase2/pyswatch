
import typing

from dataclasses import dataclass
from datetime import datetime, timezone
from enum import Enum, unique
from typing import Optional, Any


@dataclass(frozen=True)  # TODO From Python 3.10: kw_only=True
class ErrorLocation:
    '''Represents location from which an exception has been thrown'''
    function: str
    file: str
    line: int


@dataclass(frozen=True)  # TODO From Python 3.10: kw_only=True
class ErrorInfo:
    '''Represents an exception reported by HERD'''
    exception_type: str
    message: str
    location: Optional[ErrorLocation]
    cause: Optional[Any]  #: The nested/inner exception (if present)


def parse_error_info(data):
    if data is None:
        return None

    loc_data = data['location']
    if loc_data:
        location = ErrorLocation(function=loc_data['function'], file=loc_data['file'], line=loc_data['line'])
    else:
        location = None

    return ErrorInfo(
        exception_type=data['type'],
        message=data['message'],
        location=location,
        cause=data['nestedError']
    )


@unique
class Status(Enum):
    GOOD = 'Good'
    WARNING = 'Warning'
    ERROR = 'Error'
    NO_LIMIT = 'NoLimit'
    UNKNOWN = 'Unknown'

    def __rich__(self) -> str:
        return _STATUS_RICH_MARKUP[self]


_STATUS_RICH_MARKUP = {
    Status.GOOD: '[green]Good[/green]',
    Status.WARNING: '[orange_red1]Warning[/orange_red1]',
    Status.ERROR: '[red]Error[/red]',
    Status.NO_LIMIT: 'Neutral',
    Status.UNKNOWN: 'Unknown'
}


@unique
class Setting(Enum):
    ENABLED = 'Enabled'
    NON_CRITICAL = 'Non-critical'
    DISABLED = 'Disabled'


@unique
class MetricType(Enum):
    BOOL = 'bool'
    UINT16 = 'uint16'
    UINT32 = 'uint32'
    UINT64 = 'uint64'
    INT32 = 'int32'
    INT64 = 'int64'
    FLOAT = 'float'
    DOUBLE = 'double'
    STRING = 'string'


@unique
class FloatingPointNotation(Enum):
    FIXEDPOINT = 'fixedpoint'
    SCIENTIFIC = 'scientific'


@unique
class Base(Enum):
    BINARY = 'binary'
    OCTAL = 'octal'
    HEXIMAL = 'hex'
    DECIMAL = 'decimal'


@dataclass(frozen=True)  # TODO From Python 3.10: kw_only=True
class IntegralFormat:
    '''Represents recommended format (base, width, fill char) for an integral-type metric/property'''
    base: Base
    width: [int] = None
    fill_char: typing.Optional[str] = None


@dataclass(frozen=True)  # TODO From Python 3.10: kw_only=True
class FloatFormat:
    '''Represents recommended format (base, width, fill char) for an integral-type metric/property'''
    notation: FloatingPointNotation
    precision: typing.Optional[int] = None


@dataclass(frozen=True)  # TODO From Python 3.10: kw_only=True
class Metric:
    '''Represents a item of monitoring data, inside a monitorable object'''
    id: str
    path: str
    alias: str
    unit: typing.Optional[str]
    format: typing.Union[FloatFormat, IntegralFormat]
    value_type: MetricType
    value: 'typing.Any'
    update_time: datetime
    setting: Setting
    status: Status
    error_condition: str
    warning_condition: str


_SETTING_DECODER = {
    'enabled': Setting.ENABLED,
    'non-critical': Setting.NON_CRITICAL,
    'disabled': Setting.DISABLED
}

_STATUS_DECODER = {
    'good': Status.GOOD,
    'warning': Status.WARNING,
    'error': Status.ERROR,
    'no limit': Status.NO_LIMIT,
    'unknown': Status.UNKNOWN
}

_METRIC_TYPE_DECODER = {
    'bool': MetricType.BOOL,
    'uint16': MetricType.UINT16,
    'uint32': MetricType.UINT32,
    'uint64': MetricType.UINT64,
    'int32': MetricType.INT32,
    'int64': MetricType.INT64,
    'float': MetricType.FLOAT,
    'double': MetricType.DOUBLE,
    'string': MetricType.STRING
}

_FLOATINGPOINTNOTATION_DECODER = {
    'fix': FloatingPointNotation.FIXEDPOINT,
    'sci': FloatingPointNotation.SCIENTIFIC
}

_BASE_DECODER = {
    'bin': Base.BINARY,
    'oct': Base.OCTAL,
    'hex': Base.HEXIMAL,
    'dec': Base.DECIMAL
}


@dataclass(eq=False)  # TODO From Python 3.10: kw_only=Tru
class MonitorableObject(object):
    def __init__(self, *, obj_id: str, parent, raw_info: dict, raw_mon_data: dict, custom_monitorables=None):
        if custom_monitorables is None:
            custom_monitorables = {}

        self._id = obj_id
        self._alias = raw_mon_data['alias'] if len(raw_mon_data['alias']) > 0 else obj_id
        self._parent = parent
        self._setting = None
        self._status = None
        self._metric_update_duration = None
        self._monitorables = {}
        self._metrics = {}

        if type(self) is MonitorableObject:
            self._update(raw_info, raw_mon_data, custom_monitorables=custom_monitorables)

    # NOTE: All attribute are read-only
    @property
    def id(self):
        return self._id

    @property
    def alias(self):
        return self._alias

    @property
    def setting(self):
        return self._setting

    @property
    def status(self):
        return self._status

    @property
    def path(self):
        if self._parent is None:
            return self.id
        else:
            return self._parent.path + "." + self.id

    def monitorable(self, child_id: str):
        if child_id not in self._monitorables:
            raise RuntimeError(f'MonitorableObject "{self._id}" does not contain any monitorable child with '
                               f'ID "{child_id}"')
        return self._monitorables[child_id]

    @property
    def monitorables(self):
        return self._monitorables.values()

    def metric(self, metric_id: str):
        if metric_id not in self._metrics:
            raise RuntimeError(f'MonitorableObject "{self._id}" does not contain any metric with ID "{metric_id}"')
        return self._metrics[metric_id]

    @property
    def metrics(self):
        return self._metrics.values()

    @property
    def metric_update_duration(self):
        '''Time taken to update child metrics (excluding metrics of any descendants)'''
        return self._metric_update_duration

    @property
    def total_metric_update_duration(self):
        '''Time taken to update child metrics, and metrics of all non-disabled descendants'''
        total = self.metric_update_duration
        for child in self.monitorables:
            if child.setting != Setting.DISABLED:
                total += child.total_metric_update_duration
        return total

    def _update(self, _raw_info, raw_mon_data, *, custom_monitorables: dict):
        '''Update monitoring data and status flags'''
        self._setting = _SETTING_DECODER[raw_mon_data['setting']]

        self._status = _STATUS_DECODER[raw_mon_data['status']]

        self._metric_update_duration = raw_mon_data['updateDuration']

        for k, v in raw_mon_data['monitorables'].items():
            if (k not in self._monitorables):
                creator, sub_info = custom_monitorables.get(f'{self.path}.{k}', (MonitorableObject, {}))
                self._monitorables[k] = creator(obj_id=k,
                                                parent=self,
                                                raw_info=sub_info,
                                                raw_mon_data=v,
                                                custom_monitorables=custom_monitorables)
            else:
                child_obj = self._monitorables[k]
                sub_info = custom_monitorables[child_obj.path][1] if child_obj.path in custom_monitorables else {}
                child_obj._update(sub_info, v, custom_monitorables=custom_monitorables)

        for k in self._monitorables:
            if k not in raw_mon_data['monitorables']:
                del self._monitorables[k]

        for k in list(self._metrics.keys()):
            del self._metrics[k]
        assert (len(self._metrics) == 0)
        for k, v in raw_mon_data['metrics'].items():
            t = _METRIC_TYPE_DECODER[v['type']]
            f = None
            if t in [MetricType.FLOAT, MetricType.DOUBLE]:
                notation = _FLOATINGPOINTNOTATION_DECODER[v['format'][0]]
                if len(v['format']) == 1:
                    f = FloatFormat(notation=notation)
                else:
                    f = FloatFormat(notation, v['format'][1])
            elif t in [MetricType.UINT16, MetricType.UINT32, MetricType.UINT64, MetricType.INT32, MetricType.INT64]:
                base = _BASE_DECODER[v['format'][0]]
                if len(v['format']) == 1:
                    f = IntegralFormat(base=base)
                else:
                    f = IntegralFormat(base=base, width=v['format'][1], fill_char=v['format'][2])
            self._metrics[k] = Metric(id=k,
                                      path=self.path + '.' + k,
                                      alias=v['alias'] if len(v['alias']) > 0 else k,
                                      unit=v['unit'] if 'unit' in v else None,
                                      format=f,
                                      value_type=t,
                                      value=None if v['value'] is None else v['value'],
                                      update_time=datetime.fromtimestamp(v['updateTime'], timezone.utc),
                                      setting=_SETTING_DECODER[v['setting']],
                                      status=_STATUS_DECODER[v['status']],
                                      error_condition=v['errorCondition'],
                                      warning_condition=v['warningCondition'])


@unique
class PropertyType(Enum):
    BOOL = 'bool'
    UINT16 = 'uint16'
    UINT32 = 'uint32'
    UINT64 = 'uint64'
    INT32 = 'int32'
    INT64 = 'int64'
    FLOAT = 'float'
    DOUBLE = 'double'
    STRING = 'string'
    TABLE = 'table'


_PROPERTY_TYPE_DECODER = {
    'bool': PropertyType.BOOL,
    'uint16': PropertyType.UINT16,
    'uint32': PropertyType.UINT32,
    'uint64': PropertyType.UINT64,
    'int32': PropertyType.INT32,
    'int64': PropertyType.INT64,
    'float': PropertyType.FLOAT,
    'double': PropertyType.DOUBLE,
    'string': PropertyType.STRING,
    'table': PropertyType.TABLE
}


@dataclass(frozen=True)  # TODO From Python 3.10: kw_only=True
class Property:
    '''Represents a property, inside a monitorable object'''
    name: str
    path: str
    group: typing.Optional[str]
    description: typing.Optional[str]
    type: PropertyType
    unit: typing.Optional[str]
    format: typing.Union[FloatFormat, IntegralFormat]
    value_type: MetricType
    value: 'typing.Any'
    update_time: datetime


@dataclass(eq=False)  # TODO From Python 3.10: kw_only=True
class PropertiedMonitorableObject(MonitorableObject):

    def __init__(self, *, obj_id: str, parent, raw_info: dict, raw_mon_data: dict, custom_monitorables=None):
        if custom_monitorables is None:
            custom_monitorables = {}

        super().__init__(obj_id=obj_id,
                         parent=parent,
                         raw_info=raw_info,
                         raw_mon_data=raw_mon_data,
                         custom_monitorables=custom_monitorables)

        self._properties = {}

        if type(self) is PropertiedMonitorableObject:
            self._update(raw_info, raw_mon_data, custom_monitorables=custom_monitorables)

    @property
    def is_present(self):
        return self._is_present

    def prop(self, prop_id: str):
        if prop_id not in self._properties:
            raise RuntimeError(f'Object "{self._id}" does not contain any property with ID "{prop_id}"')
        return self._properties[prop_id]

    @property
    def properties(self):
        return self._properties.values()

    def _update(self, raw_info, raw_mon_data, *, custom_monitorables: dict):
        '''Update property data and 'is_present' flag'''
        # Default value here since "isPresent" isn't encoded for optical module channels
        self._is_present = raw_info.get('isPresent', True)

        for k in list(self._properties.keys()):
            del self._properties[k]
        for k, v in raw_info['properties'].items():
            t = _PROPERTY_TYPE_DECODER[v['type']]
            f = None
            if t in [PropertyType.FLOAT, PropertyType.DOUBLE]:
                notation = _FLOATINGPOINTNOTATION_DECODER[v['format'][0]]
                if len(v['format']) == 1:
                    f = FloatFormat(notation=notation)
                else:
                    f = FloatFormat(notation, v['format'][1])
            elif t in [PropertyType.UINT16, PropertyType.UINT32, PropertyType.UINT64,
                       PropertyType.INT32, PropertyType.INT64]:
                base = _BASE_DECODER[v['format'][0]]
                if len(v['format']) == 1:
                    f = IntegralFormat(base=base)
                else:
                    f = IntegralFormat(base=base, width=v['format'][1], fill_char=v['format'][2])
            self._properties[k] = Property(name=k,
                                           path=self.path + '.' + k,
                                           description=v['description'],
                                           group=v['group'] if len(v['group']) > 0 else None,
                                           type=t,
                                           unit=v['unit'] if 'unit' in v else None,
                                           format=f,
                                           value_type=t,
                                           value=None if v['value'] is None else v['value'],
                                           update_time=datetime.fromtimestamp(v['updateTime'], timezone.utc))

        super()._update(raw_info, raw_mon_data, custom_monitorables=custom_monitorables)
