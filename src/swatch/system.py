
import itertools
import logging

from .board import Board, Link, Processor, OpticalModule
from .system_peripherals import DAQBoard, FrontendModule
from .utilities.tracing import time_function

_LOGGER = logging.getLogger('shep')


class Crate(object):

    def __init__(self, crate_id, building, rack, height, description, csp_id):
        self.__id = crate_id
        self.__building = building
        self.__rack = rack
        self.__height = height
        self.__description = description
        self.__csp_id = csp_id

        self.__boards_by_logical_slot = {}
        self.__boards_by_id = {}

    @property
    def board_by_logical_slot(self, slot_number):
        if slot_number not in self.__boards_by_logical_slot:
            raise RuntimeError(f'No board in slot {slot_number} of crate "{self.id}"')
        return self.__boards_by_logical_slot

    def is_logical_slot_occupied(self, i):
        return (i in self.__boards_by_logical_slot)

    def add_board(self, board):
        if board.id in self.__boards_by_id:
            raise RuntimeError(f'Crate "{self.id}" already contains board with ID "{board.id}"')
        if board.logical_slot in self.__boards_by_logical_slot:
            raise RuntimeError(f'Crate "{self.id}" already contains board in logical slot {board.logical_slot}')
        self.__boards_by_id[board.id] = board
        self.__boards_by_logical_slot[board.logical_slot] = board

    def board(self, board_id):
        if board_id not in self.__boards_by_id:
            raise RuntimeError(f'Crate "{self.id}" does not contain board "{board_id}"')
        return self.__boards_by_id[board_id]

    @property
    def id(self):
        return self.__id

    @property
    def building(self):
        return self.__building

    @property
    def rack(self):
        return self.__rack

    @property
    def height(self):
        return self.__height

    @property
    def description(self):
        return self.__description

    @property
    def csp_id(self):
        return self.__csp_id


class ExternalOutputPort(object):
    def __init__(self, port_id, parent_path, index):
        self.__id = port_id
        self.__parent_path = parent_path
        self.__index = index

    @property
    def id(self):
        return self.__id

    @property
    def alias(self):
        return self.__id

    @property
    def path(self):
        return self.__parent_path + '.' + self.id

    @property
    def index(self):
        return self.__index

    @property
    def csp_index(self):
        return self.index

    @property
    def is_masked(self):
        return False


class ExternalInputPort(object):
    def __init__(self, port_id, parent_path, index):
        self.__id = port_id
        self.__parent_path = parent_path
        self.__index = index

    @property
    def id(self):
        return self.__id

    @property
    def alias(self):
        return self.__id

    @property
    def path(self):
        return self.__parent_path + '.' + self.id

    @property
    def index(self):
        return self.__index

    @property
    def is_masked(self):
        return False


class ExternalProcessor(object):
    def __init__(self, obj_id, parent_path, inputs, outputs):
        self.__id = obj_id
        self.__parent_path = parent_path
        self._is_included = True

        self.__inputs = {}
        for x in inputs:
            self.__inputs[x] = ExternalInputPort(f'Rx{x:02d}', self.path, x)
        self.__outputs = {}
        for x in outputs:
            self.__outputs[x] = ExternalOutputPort(f'Tx{x:02d}', self.path, x)

    @property
    def id(self):
        return self.__id

    @property
    def alias(self):
        return self.__id

    @property
    def path(self):
        return self.__parent_path + '.' + self.id

    @property
    def is_included(self):
        '''Whether this processor should be considered active in system-level actions'''
        return self._is_included

    def exclude(self):
        '''Declare that this processor should be considered active from system-level actions'''
        self._is_included = False

    def include(self):
        '''Declare that this processor should be considered active in system-level actions'''
        self._is_included = True

    @property
    def inputs(self):
        return self.__inputs.values()

    def input(self, i):
        if i not in self.__inputs:
            raise RuntimeError(f'Processor "{self.path}" does not contain an input port with index {i}')
        return self.__inputs[i]

    @property
    def outputs(self):
        return self.__outputs.values()

    def output(self, i):
        if i not in self.__outputs:
            raise RuntimeError(f'Processor "{self.path}" does not contain an output port with index {i}')
        return self.__outputs[i]


class ExternalBoard(object):
    def __init__(self, stub):
        self.__stub = stub
        self._processors = {}
        proc_ids = set(list(stub.inputs.keys()) + list(stub.outputs.keys()))
        for x in proc_ids:
            self._processors[x] = ExternalProcessor(x, self.id, stub.inputs.get(x, []), stub.outputs.get(x, []))

    @property
    def id(self):
        return self.__stub.id

    @property
    def alias(self):
        return self.id

    @property
    def path(self):
        return self.id

    @property
    def crate_id(self):
        return self.__stub.crate

    @property
    def logical_slot(self):
        return self.__stub.slot

    @property
    def processors(self):
        return self._processors.values()

    def processor(self, proc_id):
        if proc_id not in self._processors:
            raise RuntimeError(f'Board "{self.id}" does not contain any processor with ID "{proc_id}"')
        return self._processors[proc_id]


class System(object):

    @time_function('System construction')
    def __init__(self, system_stub):
        _LOGGER.info('Constructing System object')

        self._crates = {}
        for x in system_stub.crates:
            self._crates[x.id] = Crate(x.id, x.building, x.rack, x.height, x.description, x.csp_id)

        self._boards = {}
        for x in system_stub.boards:
            if x.crate not in self._crates:
                raise RuntimeError(f"Crate '{x.crate}' referenced by board '{x.id}'")
            self._boards[x.id] = Board(x)
            self._crates[x.crate].add_board(self._boards[x.id])

        self._external_boards = {}
        for x in system_stub.external_boards:
            if x.id in self._boards:
                raise RuntimeError(f'Cannot add external board "{x.id}": System already contains standard board with '
                                   'same ID')
            self._external_boards[x.id] = ExternalBoard(x)

        # Other boards which the system doesn't know about
        self._daq_boards = {}
        for x in system_stub.daq_boards:
            if x.id in self._boards or x.id in self._external_boards:
                raise RuntimeError(f'Cannot add out-of-system board "{x.id}": System already contains standard board '
                                   'with same ID')
            self._daq_boards[x.id] = DAQBoard(x)

        self._frontends = {}
        for x in system_stub.frontends:
            self._frontends[x.id] = FrontendModule(x)

        self._optical_links = {}
        connected_input_ports = {}
        connected_output_ports = {}
        for board in self._boards.values():
            for proc in board.processors:
                connected_input_ports[proc] = set()
                connected_output_ports[proc] = set()

        # Create link objects
        links_by_src_port = {}
        links_by_dst_port = {}
        for x in system_stub.links:
            if (x.src_board in self._boards) and (x.dst_board in self._boards):
                link = Link(x.id,
                            self.board(x.src_board),
                            x.src_device,
                            x.src_channel,
                            self.board(x.dst_board),
                            x.dst_device,
                            x.dst_channel,
                            is_optical=True)
                connected_input_ports[link.destination.processor].add(link.destination.port)
                connected_output_ports[link.source.processor].add(link.source.port)
                self._optical_links[x.id] = link
            elif (x.src_board in self._boards) and (x.dst_board in (*self._external_boards,
                                                                    *self._daq_boards,
                                                                    *self._frontends)):
                if x.dst_board in self._external_boards:
                    dst_brd = self.external_board(x.dst_board)
                elif x.dst_board in self._daq_boards:
                    dst_brd = self.daq_board(x.dst_board)
                else:
                    dst_brd = self.frontend(x.dst_board)
                link = Link(x.id,
                            self.board(x.src_board),
                            x.src_device,
                            x.src_channel,
                            dst_brd,
                            x.dst_device,
                            x.dst_channel,
                            is_optical=True)
                connected_output_ports[link.source.processor].add(link.source.port)
                self._optical_links[x.id] = link
            elif (x.src_board in (*self._external_boards,
                                  *self._daq_boards,
                                  *self._frontends)) and (x.dst_board in self._boards):
                if x.src_board in self._external_boards:
                    src_brd = self.external_board(x.src_board)
                elif x.src_board in self._daq_boards:
                    src_brd = self.daq_board(x.src_board)
                else:
                    src_brd = self.frontend(x.src_board)
                link = Link(x.id,
                            src_brd,
                            x.src_device,
                            x.src_channel,
                            self.board(x.dst_board),
                            x.dst_device,
                            x.dst_channel,
                            is_optical=True)
                connected_input_ports[link.destination.processor].add(link.destination.port)
                self._optical_links[x.id] = link
            elif ((x.src_board in (*self._external_boards, *self._daq_boards, *self._frontends)) and
                  (x.dst_board in (*self._external_boards, *self._daq_boards, *self._frontends))):
                raise RuntimeError(f'Link "{x.id}": Both source and destination boards ({x.src_board}, {x.dst_board}) '
                                   'are externally-controlled. At least one end of declared links must be controlled '
                                   'by HERD.')
            else:
                raise RuntimeError(f'Link "{x.id}": Source and/or destination boards ({x.src_board}, {x.dst_board}) '
                                   'do not exist')

            if link.source.port.path not in links_by_src_port:
                links_by_src_port[link.source.port.path] = [link]
            else:
                links_by_src_port[link.source.port.path].append(link)
            if link.destination.port.path not in links_by_dst_port:
                links_by_dst_port[link.destination.port.path] = [link]
            else:
                links_by_dst_port[link.destination.port.path].append(link)

        # Add ports from board-internal links to the 'connected port' lists
        for board in self._boards.values():
            for link in board.interfpga_links:
                connected_input_ports[link.rx.processor].add(link.rx.port)
                connected_output_ports[link.tx.processor].add(link.tx.port)

        # Check that no ports are duplicated in link mapping
        for port_path, link_list in links_by_src_port.items():
            if len(link_list) > 1:
                raise RuntimeError(f'{len(link_list)} links ({", ".join([x.id for x in link_list])})'
                                   f' have the same TX port ({port_path})')
        for port_path, link_list in links_by_dst_port.items():
            if len(link_list) > 1:
                raise RuntimeError(f'{len(link_list)} links ({", ".join([x.id for x in link_list])})'
                                   f' have the same RX port ({port_path})')

        # Store maps of unconnected ports for later
        self._unconnected_input_ports = {}
        self._unconnected_output_ports = {}
        for board in self._boards.values():
            for proc in board.processors:
                self._unconnected_input_ports[proc] = []
                self._unconnected_output_ports[proc] = []

                for port in proc.inputs:
                    if port not in connected_input_ports[proc]:
                        self._unconnected_input_ports[proc].append(port)
                for port in proc.outputs:
                    if port not in connected_output_ports[proc]:
                        self._unconnected_output_ports[proc].append(port)

        for k in self._unconnected_input_ports:
            self._unconnected_input_ports[k].sort(key=lambda x: x.index)
            self._unconnected_output_ports[k].sort(key=lambda x: x.index)

        # Exclude boards specified in stub (in time, this may be factored off into different class which would also
        #   handle automatic exclusion based on included FEDs using rules, similar to LinkModeManager)
        for object_id in system_stub.exclusions:
            if '.' in object_id:
                board_id, proc_id = object_id.split('.')
                board = self.board(board_id) if board_id in self._boards else self.external_board(board_id)
                board.processor(proc_id).exclude()
            else:
                board = self.board(object_id) if object_id in self._boards else self.external_board(object_id)
                for proc in board.processors:
                    proc.exclude()

        _LOGGER.info('Finished System construction')

    @property
    def boards(self):
        return self._boards.values()

    def board(self, board_id):
        if board_id not in self._boards:
            raise RuntimeError(f'System does not contain board "{board_id}"')
        return self._boards[board_id]

    @property
    def external_boards(self):
        return self._external_boards.values()

    def external_board(self, board_id):
        if board_id not in self._external_boards:
            raise RuntimeError(f'System does not contain externally-controlled board "{board_id}"')
        return self._external_boards[board_id]

    @property
    def daq_boards(self):
        return self._daq_boards.values()

    def daq_board(self, board_id):
        if board_id not in self._daq_boards:
            raise RuntimeError(f'System does not contain DAQ board "{board_id}"')
        return self._daq_boards[board_id]

    @property
    def frontends(self):
        return self._frontends.values()

    def frontend(self, fe_id):
        if fe_id not in self._frontends:
            raise RuntimeError(f'System does not contain frontend module "{fe_id}"')
        return self._frontends[fe_id]

    @property
    def crates(self):
        return self._crates.values()

    def crate(self, crate_id):
        if crate_id not in self._crates:
            raise RuntimeError(f'System does not contain crate "{crate_id}"')
        return self._crates[crate_id]

    @property
    def links(self):
        return itertools.chain(self.optical_links, *(x.interfpga_links for x in self.boards))

    @property
    def optical_links(self):
        return self._optical_links.values()

    @property
    def optical_link(self, link_id):
        if link_id not in self._optical_links:
            raise RuntimeError(f'System does not contain link "{link_id}"')
        return self._optical_links[link_id]

    @property
    def connected_boards(self):
        boards = set()

        # Step 1: Add any processors connected by optical links
        for link in self.optical_links:
            if link.source.processor.is_included and link.destination.processor.is_included:
                if type(link.source.processor) is Processor:
                    boards.add(link.source.board)

                if type(link.destination.processor) is Processor:
                    boards.add(link.destination.board)

        return sorted(boards, key=lambda x: x.path)

    @property
    def connected_external_boards(self):
        boards = set()

        # Step 1: Add any processors connected by optical links
        for link in self.optical_links:
            if link.source.processor.is_included and link.destination.processor.is_included:
                if type(link.source.processor) is not Processor:
                    boards.add(link.source.board)

                if type(link.destination.processor) is not Processor:
                    boards.add(link.destination.board)

        return sorted(boards, key=lambda x: x.path)

    @property
    def connected_processors(self):
        procs = set()
        boards = set()

        # Step 1: Add any processors connected by optical links
        for link in self.optical_links:
            if type(link.source.processor) is Processor:
                procs.add(link.source.processor)
                boards.add(link.source.board)

            if type(link.destination.processor) is Processor:
                procs.add(link.destination.processor)
                boards.add(link.destination.board)

        # Step 2: Also add any other processors connected to this one by board-internal links
        for board in self.boards:
            for proc in board.processors:
                for tx_port in proc.outputs:
                    if tx_port.connected_port is not None:
                        procs.add(board.processor(tx_port.connected_port[0]))

                for rx_port in proc.inputs:
                    if rx_port.connected_port is not None:
                        procs.add(board.processor(rx_port.connected_port[0]))

        procs = [proc for proc in procs if proc.is_included]
        return sorted(procs, key=lambda x: x.path)

    @property
    def connected_processors_in_tcds_configuration_order(self):
        result = [[]]
        done = set()
        remaining_procs = set()

        for proc in self.connected_processors:
            if proc.tcds2_source is None:
                result[0].append(proc)
                done.add(proc.path)
            else:
                remaining_procs.add(proc)

        while len(remaining_procs) > 0:
            result.append([])

            for proc in remaining_procs:
                path_of_source = proc.path[0:len(proc.path) - len(proc.id) - 1] + '.' + proc.tcds2_source
                if path_of_source in done:
                    result[-1].append(proc)

            if len(result[-1]) == 0:
                raise RuntimeError('Could not determine processor TCDS2 configuration order (there may be loops in the'
                                   ' graph). Remaining processors: ' + ', '.join([p.path for p in remaining_procs]))

            for x in result[-1]:
                remaining_procs.remove(x)
                done.add(x.path)

        return result

    @property
    def connected_processors_in_backend_link_order(self):
        dependencies = {x: set() for x in self.connected_processors}
        for link in self.optical_links:
            if (type(link.destination.processor) is Processor) and (type(link.source.processor) is Processor):
                dependencies[link.destination.processor].add(link.source.processor)

        for board in self.connected_boards:
            for link in board.interfpga_links:
                dependencies[link.rx.processor].add(link.tx.processor)

        result = []
        done = set()
        while len(dependencies) > 0:
            result.append(set())

            for k, v in dependencies.items():
                unconfigured_dependencies = v - done
                if len(unconfigured_dependencies) == 0:
                    result[-1].add(k)

            if len(result[-1]) == 0:
                raise RuntimeError('Could not determine processor configuration order (there may be loops in the'
                                   ' graph). Remaining processors: ' + ', '.join([p.path for p in dependencies]))

            for x in result[-1]:
                dependencies.pop(x)
                done.add(x)

        return result

    @property
    def excluded_processors(self):
        return [proc for board in self.boards for proc in board.processors if not proc.is_included]

    @property
    def connected_optics(self):
        result = set()
        for link in self.optical_links:
            if type(link.source.optical_module) is OpticalModule:
                result.add(link.source.optical_module)
            if type(link.destination.optical_module) is OpticalModule:
                result.add(link.destination.optical_module)
        return sorted(result, key=lambda x: (x.path.split('.')[0], x.id))  # Sort by board, ID

    def mask_links(self, link_manager, *, leases):
        tx_ports_to_mask, masked_tx_links, unmasked_tx_links = link_manager.get_masked_outputs(self)
        rx_ports_to_mask, masked_rx_links, unmasked_rx_links = link_manager.get_masked_inputs(self)

        for x in self.connected_processors:
            for p in self._unconnected_output_ports[x]:
                tx_ports_to_mask[x].append(p.index)
            for p in self._unconnected_input_ports[x]:
                rx_ports_to_mask[x].append(p.index)

        for proc in self.connected_processors:
            proc.mask_input_ports(port_indices=rx_ports_to_mask[proc], lease=leases[proc])
            proc.mask_output_ports(port_indices=tx_ports_to_mask[proc], lease=leases[proc])

        masked_links = sorted(set(masked_tx_links + masked_rx_links), key=lambda x: x.id)
        unmasked_links = sorted(set(unmasked_tx_links + unmasked_rx_links), key=lambda x: x.id)
        return masked_links, unmasked_links
