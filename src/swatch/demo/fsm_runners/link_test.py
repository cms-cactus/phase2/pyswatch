
from os.path import join, dirname

from ...cli.demonstrators import main_link_test
from ..utilities import CLIDemo, run_cli_demos


_DUMMY_CONFIG = join(dirname(dirname(dirname(__file__))), 'config', 'link-test', 'dummy.yml')

_FIRMWARE_PATH = 'http://emp-fwk.web.cern.ch/emp-fwk/fw/bitfiles/v0.8.1/serenity_vu9p_so2.tgz'
_INPUT_DATA_FILE = join(dirname(dirname(dirname(__file__))), 'config', 'input_data.txt')


def get_link_test_demo_list():
    return [
        CLIDemo(description='Link test FSM runner: Help menu',
                cmd_obj=main_link_test,
                cmd_name='run-link-test',
                args=['--help'],
                expected_exit_code=0),
        CLIDemo(description='Link test FSM runner: Dry run',
                cmd_obj=main_link_test,
                cmd_name='run-link-test',
                args=[_DUMMY_CONFIG,
                      f'--var=firmware_package={_FIRMWARE_PATH}',
                      f'--var=input_data_file={_INPUT_DATA_FILE}',
                      '--dry-run'],
                expected_exit_code=0),
        CLIDemo(description='Link test FSM runner: Normal (default options)',
                cmd_obj=main_link_test,
                cmd_name='run-link-test',
                args=[_DUMMY_CONFIG,
                      f'--var=firmware_package={_FIRMWARE_PATH}',
                      f'--var=input_data_file={_INPUT_DATA_FILE}',
                      '--no-pause-for-externals'],
                expected_exit_code=0),
        CLIDemo(description='Link test FSM runner: Normal, show input power',
                cmd_obj=main_link_test,
                cmd_name='run-link-test',
                args=[_DUMMY_CONFIG,
                      f'--var=firmware_package={_FIRMWARE_PATH}',
                      f'--var=input_data_file={_INPUT_DATA_FILE}',
                      '--show-metrics=.*(sourceLink|sourceBoard)',
                      '--no-pause-for-externals'],
                expected_exit_code=0),
        CLIDemo(description='Link test FSM runner: Normal, repeated configuration',
                cmd_obj=main_link_test,
                cmd_name='run-link-test',
                args=[_DUMMY_CONFIG,
                      f'--var=firmware_package={_FIRMWARE_PATH}',
                      f'--var=input_data_file={_INPUT_DATA_FILE}',
                      '--repeat=2',
                      '--repeat-io-config=5',
                      '--config-interval=15',
                      '--check-interval=5',
                      '--no-pause-for-externals'],
                expected_exit_code=0)
        ]


if __name__ == "__main__":

    run_cli_demos(get_link_test_demo_list())
