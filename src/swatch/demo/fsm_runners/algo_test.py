import os
import shutil
import sys

from os.path import join, dirname

from ...cli.demonstrators import main_single_board_test
from ..utilities import CLIDemo, run_cli_demos


_FIRMWARE_PACKAGE = 'http://emp-fwk.web.cern.ch/emp-fwk/fw/bitfiles/v0.8.1/serenity_vu9p_so2.tgz'
_INPUT_DATA_FILE = join(dirname(dirname(dirname(__file__))), 'config', 'input_data.txt')
_INPUT_DATA_FILE_IDX = join(dirname(dirname(dirname(__file__))), 'config', 'input_data{file_index}.txt')
_OUTPUT_DATA_FILE_IDX = join(dirname(dirname(dirname(__file__))), 'config', 'output_data{file_index}.txt')
_INPUT_DATA_FILE_PADIDX = join(dirname(dirname(dirname(__file__))), 'config', 'input_data{file_index:04}.txt')
_OUTPUT_DATA_FILE_PADIDX = join(dirname(dirname(dirname(__file__))), 'config', 'output_data{file_index:05}.txt')


def get_algo_test_demo_list(herd_hostname):
    return [
        CLIDemo(description='Algo test FSM runner: Help menu',
                cmd_obj=main_single_board_test,
                cmd_name='run-algo-test',
                args=['--help'],
                expected_exit_code=0),
        CLIDemo(description='Algo test FSM runner: Dry run',
                cmd_obj=main_single_board_test,
                cmd_name='run-algo-test',
                args=[herd_hostname,
                      'x0',
                      'default:dummy',
                      _FIRMWARE_PACKAGE,
                      _INPUT_DATA_FILE,
                      'output.txt',
                      '--dry-run'],
                expected_exit_code=0),
        CLIDemo(description='Algo test FSM runner: Normal',
                cmd_obj=main_single_board_test,
                cmd_name='run-algo-test',
                args=[herd_hostname,
                      'x0',
                      'default:dummy',
                      _FIRMWARE_PACKAGE,
                      _INPUT_DATA_FILE,
                      'output.txt'],
                expected_exit_code=0),
        CLIDemo(description='Algo test FSM runner: Error (bad hostname)',
                cmd_obj=main_single_board_test,
                cmd_name='run-algo-test',
                args=['incorrect_hostname',
                      'x0',
                      'default:dummy',
                      _FIRMWARE_PACKAGE,
                      _INPUT_DATA_FILE,
                      'output.txt'],
                expected_exit_code=1),
        CLIDemo(description='Algo test FSM runner: Error (wrong port)',
                cmd_obj=main_single_board_test,
                cmd_name='run-algo-test',
                args=['localhost:65535',
                      'x0',
                      'default:dummy',
                      _FIRMWARE_PACKAGE,
                      _INPUT_DATA_FILE,
                      'output.txt'],
                expected_exit_code=1),
        CLIDemo(description='Algo test FSM runner: Error (wrong port) with traceback',
                cmd_obj=main_single_board_test,
                cmd_name='run-algo-test',
                args=['localhost:65535',
                      'x0',
                      'default:dummy',
                      _FIRMWARE_PACKAGE,
                      _INPUT_DATA_FILE,
                      'output.txt',
                      '--show-traceback'],
                expected_exit_code=1),
        CLIDemo(description='Algo test FSM runner: Multiple input files',
                cmd_obj=main_single_board_test,
                cmd_name='run-algo-test',
                args=[herd_hostname,
                      'x0',
                      'default:dummy',
                      _FIRMWARE_PACKAGE,
                      _INPUT_DATA_FILE_IDX,
                      _OUTPUT_DATA_FILE_IDX,
                      '--file-indices', '98-101'],
                expected_exit_code=0),
        CLIDemo(description='Algo test FSM runner: Error (file-index out of bounds) with multiple input files',
                cmd_obj=main_single_board_test,
                cmd_name='run-algo-test',
                args=[herd_hostname,
                      'x0',
                      'default:dummy',
                      _FIRMWARE_PACKAGE,
                      _INPUT_DATA_FILE_IDX,
                      _OUTPUT_DATA_FILE_IDX,
                      '--file-indices', '97-101'],
                expected_exit_code=1),
        CLIDemo(description='Algo test FSM runner: Error (filerange out of bounds) with multiple input files',
                cmd_obj=main_single_board_test,
                cmd_name='run-algo-test',
                args=[herd_hostname,
                      'x0',
                      'default:dummy',
                      _FIRMWARE_PACKAGE,
                      _INPUT_DATA_FILE_IDX,
                      _OUTPUT_DATA_FILE_IDX,
                      '--file-indices', '98-102'],
                expected_exit_code=1),
        CLIDemo(description='Algo test FSM runner: Multiple input files - padded file name',
                cmd_obj=main_single_board_test,
                cmd_name='run-algo-test',
                args=[herd_hostname,
                      'x0',
                      'default:dummy',
                      _FIRMWARE_PACKAGE,
                      _INPUT_DATA_FILE_PADIDX,
                      _OUTPUT_DATA_FILE_PADIDX,
                      '--file-indices', '98-101'],
                expected_exit_code=0)

    ]


if __name__ == "__main__":

    if len(sys.argv) != 2:
        print('ERROR: Invalid usage! Expected one argument - hostname (and optionally port) of the dummy HERD instance')
        sys.exit(1)

    # Create multiple files temporarily
    for filecount in range(98, 102):
        temp_dir = dirname(_INPUT_DATA_FILE)
        temp_filename_list = ['input_data{}.txt'.format(filecount),
                              'input_data{:04}.txt'.format(filecount)]
        for temp_filename in temp_filename_list:
            with open(join(temp_dir, temp_filename), 'w+', encoding='utf-8') as temp_file:
                with open(_INPUT_DATA_FILE, 'r', encoding='utf-8') as input_file:
                    shutil.copyfileobj(input_file, temp_file)

    run_cli_demos(get_algo_test_demo_list(sys.argv[1]))

    # Clean up the temporary input and output files
    for filecount in range(98, 102):
        os.remove(join(dirname(_INPUT_DATA_FILE_IDX), 'input_data{}.txt'.format(filecount)))
        os.remove(join(dirname(_OUTPUT_DATA_FILE_IDX), 'output_data{}.txt'.format(filecount)))
        os.remove(join(dirname(_INPUT_DATA_FILE_PADIDX), 'input_data{:04}.txt'.format(filecount)))
        os.remove(join(dirname(_OUTPUT_DATA_FILE_PADIDX), 'output_data{:05}.txt'.format(filecount)))
