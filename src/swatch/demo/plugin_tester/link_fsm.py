
import sys

from os.path import join, dirname

from ..utilities import CLIDemo, run_cli_demos
from ...test_plugin.__main__ import main


_FIRMWARE_PACKAGE = 'https://emp-fwk.web.cern.ch/emp-fwk/fw/bitfiles/v0.8.1/serenity_vu13p_so2.tgz'
_INPUT_DATA_FILE = join(dirname(dirname(dirname(__file__))), 'config', 'input_data.txt')


def get_link_test_demo_list(herd_hostname):

    config_file = join(dirname(dirname(dirname(__file__))), 'config', 'plugin-test', 'dummy.yml')
    return [
        CLIDemo(description='Plugin test suite, link FSM: Help options',
                cmd_obj=main,
                cmd_name='run-plugin-test-suite',
                args=['--help'],
                expected_exit_code=0),

        CLIDemo(description='Plugin test suite, link FSM: All processors',
                cmd_obj=main,
                cmd_name='run-plugin-test-suite',
                args=['-v', '--tb=short', '-rfE',
                      '-k', 'link_test',
                      '--board', herd_hostname,
                      '--channels', '0-135',
                      '--fsm-config', config_file,
                      '--fw-package', _FIRMWARE_PACKAGE,
                      '--input-data-file', _INPUT_DATA_FILE,
                      '--link-latency', '0,30',
                      '--tolerate-warnings',
                      '--repeat-io-config', '2',
                      '--io-port-mode', 'PRBS31',
                      '--io-port-mode', 'CSP:40/54:idle1'
                      ],
                expected_exit_code=0),

        CLIDemo(description='Plugin test suite, link FSM: Single processor',
                cmd_obj=main,
                cmd_name='run-plugin-test-suite',
                args=['-v', '--tb=short', '-rfE',
                      '-k', 'link_test',
                      '--board', herd_hostname,
                      '--channels', '0-135',
                      '--processor', 'x0',
                      '--fsm-config', config_file,
                      '--fw-package', _FIRMWARE_PACKAGE,
                      '--input-data-file', _INPUT_DATA_FILE,
                      '--link-latency', '0,30',
                      '--tolerate-warnings',
                      '--repeat-io-config', '2',
                      '--io-port-mode', 'PRBS31',
                      '--io-port-mode', 'CSP:40/54:idle1'
                      ],
                expected_exit_code=0),

        CLIDemo(description='Plugin test suite, link FSM: Bad hostname',
                cmd_obj=main,
                cmd_name='run-plugin-test-suite',
                args=['-v', '--tb=short', '-rfE',
                      '-k', 'link_test',
                      '--board', 'incorrect_hostname',
                      '--channels', '0-135',
                      '--processor', 'x0',
                      '--fsm-config', config_file,
                      '--fw-package', _FIRMWARE_PACKAGE,
                      '--input-data-file', _INPUT_DATA_FILE,
                      '--link-latency', '0,30',
                      '--tolerate-warnings',
                      '--repeat-io-config', '2',
                      '--io-port-mode', 'PRBS31',
                      '--io-port-mode', 'CSP:40/54:idle1'
                      ],
                expected_exit_code=4),
        CLIDemo(description='Plugin test suite, link FSM: Error (wrong port)',
                cmd_obj=main,
                cmd_name='run-plugin-test-suite',
                args=['-v', '--tb=short', '-rfE',
                      '-k', 'link_test',
                      '--board', 'localhost:65535',
                      '--channels', '0-135',
                      '--processor', 'x0',
                      '--fsm-config', config_file,
                      '--fw-package', _FIRMWARE_PACKAGE,
                      '--input-data-file', _INPUT_DATA_FILE,
                      '--link-latency', '0,30',
                      '--tolerate-warnings',
                      '--repeat-io-config', '2',
                      '--io-port-mode', 'PRBS31',
                      '--io-port-mode', 'CSP:40/54:idle1'
                      ],
                expected_exit_code=4)
        ]


if __name__ == "__main__":

    if len(sys.argv) != 2:
        print('ERROR: Invalid usage! Expected one argument - hostname (and optionally port) of the dummy HERD instance')
        sys.exit(1)

    run_cli_demos(get_link_test_demo_list(sys.argv[1]))
