
import sys

from os.path import join, dirname

from ..utilities import CLIDemo, run_cli_demos
from ...test_plugin.__main__ import main


_FIRMWARE_PACKAGE = 'https://emp-fwk.web.cern.ch/emp-fwk/fw/bitfiles/v0.8.1/serenity_vu13p_so2.tgz'
_INPUT_DATA_FILE = join(dirname(dirname(dirname(__file__))), 'config', 'input_data.txt')


def get_algo_test_demo_list(herd_hostname):

    config_file = join(dirname(dirname(dirname(__file__))), 'config', 'plugin-test', 'dummy.yml')
    return [
        CLIDemo(description='Plugin test suite, algo FSM: Help options',
                cmd_obj=main,
                cmd_name='run-plugin-test-suite',
                args=['--help'],
                expected_exit_code=0),

        CLIDemo(description='Plugin test suite, algo FSM: Normal',
                cmd_obj=main,
                cmd_name='run-plugin-test-suite',
                args=['-v', '--tb=short', '-rfE',
                      '-k', 'algo_test',
                      '--board', herd_hostname,
                      '--channels', '0-119',
                      '--processor', 'x0',
                      '--fsm-config', config_file,
                      '--fw-package', _FIRMWARE_PACKAGE,
                      '--input-data-file', _INPUT_DATA_FILE,
                      '--algo-latency', '0',
                      '--tolerate-warnings'
                      ],
                expected_exit_code=0),
        CLIDemo(description='Plugin test suite, algo FSM: Bad hostname',
                cmd_obj=main,
                cmd_name='run-plugin-test-suite',
                args=['-v', '--tb=short', '-rfE',
                      '-k', 'algo_test',
                      '--board', 'incorrect_hostname',
                      '--channels', '1-5',
                      '--processor', 'x0',
                      '--fsm-config', config_file,
                      '--fw-package', _FIRMWARE_PACKAGE,
                      '--input-data-file', _INPUT_DATA_FILE,
                      '--algo-latency', '0',
                      '--tolerate-warnings',
                      ],
                expected_exit_code=4),
        CLIDemo(description='Plugin test suite, algo FSM: Error (wrong port)',
                cmd_obj=main,
                cmd_name='run-plugin-test-suite',
                args=['-v', '--tb=short', '-rfE',
                      '-k', 'algo_test',
                      '--board', 'localhost:65535',
                      '--channels', '1-5',
                      '--processor', 'x0',
                      '--fsm-config', config_file,
                      '--fw-package', _FIRMWARE_PACKAGE,
                      '--input-data-file', _INPUT_DATA_FILE,
                      '--algo-latency', '0',
                      '--tolerate-warnings'
                      ],
                expected_exit_code=4)
        ]


if __name__ == "__main__":

    if len(sys.argv) != 2:
        print('ERROR: Invalid usage! Expected one argument - hostname (and optionally port) of the dummy HERD instance')
        sys.exit(1)

    run_cli_demos(get_algo_test_demo_list(sys.argv[1]))
