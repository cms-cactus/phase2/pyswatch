
import sys
import time

from dataclasses import dataclass

from ..console.utilities import create_console


@dataclass(frozen=True)  # TODO From Python 3.10: kw_only=True
class CLIDemo:
    '''Represents single command that will be run in a demo sequence'''
    description: str
    cmd_obj: str
    cmd_name: str
    args: str
    expected_exit_code: int


def run_cli_demos(demos):
    console = create_console()

    for i, demo in enumerate(demos):
        if i > 0:
            console.rule(characters='=')
        full_command = demo.cmd_name + ' ' + ' '.join(demo.args)
        console.rule(f'({i+1} of {len(demos)}) [b]{demo.description}[/]', characters='=')
        console.print(f'[rule.line]--> Running command:[/] {full_command}', style=None)

        system_exit_raised = False
        start = time.time()
        try:
            demo.cmd_obj(demo.args)
        except SystemExit as e:
            duration = time.time() - start
            console.rule(f'END of "{demo.description}". Command took {duration:.1f} seconds', characters='=')
            system_exit_raised = True
            if e.code != demo.expected_exit_code:
                console.print(f'ERROR: Exit code of last command was {e.code}, '
                              f'but expected {demo.expected_exit_code}', style='red')
                sys.exit(1)

    if system_exit_raised is False:
        console.print('ERROR: Expected SystemExit to be raised, but it was not', style='red')
        sys.exit(1)
