import pytest
import warnings

from ..cli.board_data import _main_compare
from ..cli.demonstrators import get_capture_data_files
from ..console.utilities import create_console, check_link_map, check_parameters_for_fsm, empty_line
from ..console.utilities import ensure_devices_in_fsm_initial_state, run_fsm_transition
from ..io_settings import CSPSettings
from ..monitoring import Setting, Status
from ..utilities.board_data import BoardData, ChannelData, FileFormat, read, write
from ..utilities.misc import format_index_list
from os.path import join


# FIXME: Exclude (i.e. mask) ports that aren't included in channels list
def test_mgt_loopback(settings_pools, lease, processor, input_file,
                      channels, repeat_io_config, metric_regex, io_port_mode,
                      tolerate_warnings, link_latencies, tmp_path):

    console = create_console()
    for pool in settings_pools:
        check_parameters_for_fsm(console, [processor], 'linkTest', pool, verbose=False)

    port_indices = {x.index for x in processor.inputs if x.is_present}
    port_indices = list(port_indices.intersection({x.index for x in processor.outputs if x.is_present}))
    masked_tx_ports = [x.index for x in processor.outputs if x.index not in port_indices]
    masked_rx_ports = [x.index for x in processor.inputs if x.index not in port_indices]

    # 1. Enter FSM and set I/O port settings
    ensure_devices_in_fsm_initial_state(console, [processor], 'linkTest', {processor: lease})
    processor.set_io_operating_modes(lease=lease,
                                     input_modes={i: io_port_mode for i in port_indices},
                                     output_modes={i: io_port_mode for i in port_indices},
                                     loopback=port_indices)

    processor.mask_input_ports(port_indices=masked_rx_ports, lease=lease)
    processor.mask_output_ports(port_indices=masked_tx_ports, lease=lease)

    empty_line(console)

    # 2. Run setup transition
    run_fsm_transition(console, [processor], 'linkTest', 'Halted', 'setup',
                       settings_pools[0], {processor: lease},
                       refresh=True, metric_regex=metric_regex,
                       strict=not tolerate_warnings)

    for test_data_file, settings_pool in zip(input_file, settings_pools):
        for i in range(1, repeat_io_config + 1):
            print(f'Testing with input file "{test_data_file}", iteration {i} of {repeat_io_config}')

            # 3. Run configure & capture transition
            steps = [('Synchronized', 'configureTx', False), ('TxConfigured', 'configureRx', True)]
            for state, transition, refresh in steps:
                run_fsm_transition(console, [processor], 'linkTest', state, transition,
                                   settings_pool, {processor: lease},
                                   refresh=refresh, metric_regex=metric_regex,
                                   strict=not tolerate_warnings)

            snapshots = run_fsm_transition(console, [processor], 'linkTest', 'RxConfigured', 'capture',
                                           settings_pool, {processor: lease},
                                           refresh=True, metric_regex=metric_regex,
                                           strict=not tolerate_warnings)[processor]

            check_monitoring_status(processor, tolerate_warnings)

            # 4. If CSP: Check link mapping and compare captured data with injected data
            if type(io_port_mode) is CSPSettings:
                # FIXME: Check link mapping

                rx_data_file, tx_data_file = get_capture_data_files(snapshots, rx_required=True, tx_required=True)
                print(f'Saving captured rx data to {tmp_path}/capture_rx_{i}.txt')
                rx_data_file.save(join(tmp_path, f'capture_rx_{i}.txt'))
                print(f'Saving captured tx data to {tmp_path}/capture_tx_{i}.txt')
                tx_data_file.save(join(tmp_path, f'capture_tx_{i}.txt'))

                console.print('\nComparing captured output data with injected data')
                match_result = _main_compare(test_data_file,
                                             join(tmp_path, f'capture_tx_{i}.txt'),
                                             channels,
                                             [0],
                                             None, True, False, False)
                assert bool(match_result), 'Captured output data differs from injected data'

                console.print('\nComparing captured input data with injected data')
                match_result = _main_compare(test_data_file,
                                             join(tmp_path, f'capture_rx_{i}.txt'),
                                             channels,
                                             link_latencies,
                                             None, True, False, False)
                assert bool(match_result), 'Captured input data differs from injected data'

                console.print('Output -> input latencies:')
                for lat in match_result.latencies:
                    console.print(f' * Latency {lat}: Channels {format_index_list(match_result.latencies[lat])}')

            # 5. Run 'continue' transition
            run_fsm_transition(console, [processor], 'linkTest', 'RxConfigured', 'continue',
                               settings_pool, {processor: lease},
                               refresh=True, metric_regex=metric_regex,
                               strict=not tolerate_warnings)

            empty_line(console)


# FIXME: Add TestCriteria struct or similar?
# FIXME: Exclude (i.e. mask) ports that aren't included in channels list
@pytest.mark.no_interfpga_links  # Skip if there are no inter-FPGA links
def test_interfpga_links(settings_pools, lease, board, processors, input_file,
                         channels, repeat_io_config, metric_regex, io_port_mode,
                         tolerate_warnings, link_latencies, tmp_path):

    console = create_console()
    for pool in settings_pools:
        check_parameters_for_fsm(console, processors, 'linkTest', pool, verbose=False)

    # 0. Identify links between selected processors, and corresponding ports
    proc_ids = {x.id for x in processors}
    links = [x for x in board.interfpga_links if x.tx.processor.id in proc_ids and x.rx.processor.id in proc_ids]

    unmasked_tx_ports_by_proc = {p: [] for p in processors}
    unmasked_rx_ports_by_proc = {p: [] for p in processors}
    for link in links:
        if (link.tx.port.index in channels) and (link.rx.port.index in channels):
            unmasked_tx_ports_by_proc[link.tx.processor].append(link.tx.port.index)
            unmasked_rx_ports_by_proc[link.rx.processor].append(link.rx.port.index)

    masked_tx_ports_by_proc = {}
    masked_rx_ports_by_proc = {}
    for proc in processors:
        masked_tx_ports_by_proc[proc] = [x.index for x in proc.outputs
                                         if x.index not in unmasked_tx_ports_by_proc[proc]]
        masked_rx_ports_by_proc[proc] = [x.index for x in proc.inputs
                                         if x.index not in unmasked_rx_ports_by_proc[proc]]

    # 1. Enter FSM and set I/O port settings
    for proc in processors:
        ensure_devices_in_fsm_initial_state(console, processors, 'linkTest', {p: lease for p in processors})

    for proc in processors:
        proc.set_io_operating_modes(lease=lease,
                                    input_modes={i: io_port_mode for i in unmasked_rx_ports_by_proc[proc]},
                                    output_modes={i: io_port_mode for i in unmasked_tx_ports_by_proc[proc]},
                                    loopback=[])
        proc.mask_input_ports(port_indices=masked_rx_ports_by_proc[proc], lease=lease)
        proc.mask_output_ports(port_indices=masked_tx_ports_by_proc[proc], lease=lease)

    # 2. Run setup transition
    procs_in_setup_order = board.processors_in_tcds_configuration_order
    procs_in_setup_order = [list(filter(lambda p: p in processors, x)) for x in procs_in_setup_order]
    for procs in procs_in_setup_order:
        run_fsm_transition(console, procs, 'linkTest', 'Halted', 'setup',
                           settings_pools[0], {p: lease for p in procs},
                           refresh=True, metric_regex=metric_regex,
                           strict=not tolerate_warnings)

    for test_data_file, settings_pool in zip(input_file, settings_pools):
        for i in range(1, repeat_io_config + 1):
            print(f'Testing with input file "{test_data_file}", iteration {i} of {repeat_io_config}')

            # 3. Run configure & capture transition
            steps = [('Synchronized', 'configureTx', False), ('TxConfigured', 'configureRx', True)]
            for state, transition, refresh in steps:
                run_fsm_transition(console, processors, 'linkTest', state, transition,
                                   settings_pool, {p: lease for p in processors},
                                   refresh=refresh, metric_regex=metric_regex,
                                   strict=not tolerate_warnings)

            snapshots = run_fsm_transition(console, processors, 'linkTest', 'RxConfigured', 'capture',
                                           settings_pool, {p: lease for p in processors},
                                           refresh=True,
                                           metric_regex=metric_regex,
                                           strict=not tolerate_warnings)
            for proc in processors:
                check_monitoring_status(proc, tolerate_warnings)

            # 4. If CSP: Check link mapping and compare captured data with injected data
            if type(io_port_mode) is CSPSettings:
                check_link_map(console, links, crate_map={})

                injected_data = read(test_data_file)
                expected_rx_data = {proc.id: BoardData(f'{proc.id}_rx', {}) for proc in processors}
                for link in links:
                    if link.rx.port.index in unmasked_rx_ports_by_proc[link.rx.processor]:
                        chan_data = ChannelData(injected_data.at(link.tx.port.index)._data, copy=False)
                        expected_rx_data[link.rx.processor.id].add(link.rx.port.index, chan_data)

                for proc in processors:
                    rx_data_file, tx_data_file = get_capture_data_files(snapshots[proc],
                                                                        rx_required=True,
                                                                        tx_required=True)
                    print(f'Saving captured rx data for processor {proc.id} to {tmp_path}/capture_rx_{proc.id}.txt')
                    rx_data_file.save(join(tmp_path, f'capture_rx_{proc.id}.txt'))
                    print(f'Saving captured tx data for processor {proc.id} to {tmp_path}/capture_tx_{proc.id}.txt')
                    tx_data_file.save(join(tmp_path, f'capture_tx_{proc.id}.txt'))
                    print(f'Saving expected rx data for processor {proc.id} to {tmp_path}/ref_rx_{proc.id}.txt')
                    write(expected_rx_data[proc.id], join(tmp_path, f'ref_rx_{proc.id}.txt'), FileFormat.EMPv2)

                for proc in processors:
                    print(f'\nComparing output data captured from processor {proc.id} with injected data')
                    match_result = _main_compare(test_data_file,
                                                 join(tmp_path, f'capture_tx_{proc.id}.txt'),
                                                 unmasked_tx_ports_by_proc[proc],
                                                 [0],
                                                 None, True, False, False)
                    assert bool(match_result), f'Processor {proc.id}: Captured output data differs from injected data'

                for proc in processors:
                    print(f'\nComparing input data captured from processor {proc.id} with injected data '
                          f'(valid latencies: {format_index_list(link_latencies)})')
                    match_result = _main_compare(join(tmp_path, f'ref_rx_{proc.id}.txt'),
                                                 join(tmp_path, f'capture_rx_{proc.id}.txt'),
                                                 unmasked_rx_ports_by_proc[proc],
                                                 link_latencies,
                                                 None, True, False, False)
                    assert bool(match_result), f'Processor {proc.id}: Captured input data does not match injected data'

                    console.print('Output -> input latencies:')
                    for x in match_result.latencies:
                        console.print(f' * Latency {x}: Rx channels {format_index_list(match_result.latencies[x])}')

            # 5. Run 'continue' transition
            run_fsm_transition(console, processors, 'linkTest', 'RxConfigured', 'continue',
                               settings_pool, {p: lease for p in processors},
                               refresh=True,
                               metric_regex=metric_regex,
                               strict=not tolerate_warnings)

            empty_line(console)


def check_monitoring_status(proc, tolerate_monitoring_warnings):
    '''
   Monitoring object is algo
   Monitoring object is inputPorts
   Monitoring object is outputPorts
   Monitoring object is readout
   Monitoring object is ttc
    '''
    for mon_obj in proc.monitorables:
        if mon_obj.id in {'ttc', 'algo', 'inputPorts', 'outputPorts'}:
            assert mon_obj.setting == Setting.ENABLED, (f"{mon_obj.id} component should be enabled, but it is " +
                                                        f"{mon_obj.setting.value} instead")
        else:
            assert mon_obj.setting == Setting.DISABLED, (f"{mon_obj.id} component's monitoring should be disabled, " +
                                                         f"is {mon_obj.setting.value} instead")
            continue

        # Check monitoring status of components
        allowed_status = {Status.GOOD, Status.WARNING} if tolerate_monitoring_warnings else {Status.GOOD}
        if mon_obj.id == 'algo':
            allowed_status.add(Status.NO_LIMIT)
        assert mon_obj.status in allowed_status, (f'Status of {mon_obj.id} component is {mon_obj.status}' +
                                                  ', but should be ' + ' or '.join([str(x) for x in allowed_status]))

        if tolerate_monitoring_warnings and mon_obj.status == Status.WARNING:
            warnings.warn(UserWarning(f'Status of {mon_obj.id} component is {mon_obj.status}'))
