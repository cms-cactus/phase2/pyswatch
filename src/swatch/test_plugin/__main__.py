# This file allows one to run "python -m swatch.test_plugin <options defined in conftest>".
# Created as a workaround for "pytest --pyargs swatch.test_plugin <options defined in conftest>"
# ... until https://github.com/pytest-dev/pytest/issues/1596 is resolved

import os
import sys

HERE = os.path.dirname(__file__)


# Factorised the following lines out into function so that can run tests from demo module as well
def main(args):
    import pytest

    errcode = pytest.main([HERE] + args)
    sys.exit(errcode)


def cli():
    main(sys.argv[1:])


if __name__ == "__main__":
    cli()
