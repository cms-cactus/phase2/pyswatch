
import pytest
import re

from ..board import Board, Lease
from ..cli.utilities import parse_index_list_string, parse_ioport_mode_arg
from ..config import parse_parameter_file
from ..io_settings import CSPSettings, IdleMethod
from ..parameters import File
from ..stubs import BoardStub
from ..utilities.board_data import generate_board_data_file
from argparse import ArgumentTypeError
from os.path import join, dirname
from urllib.parse import urlparse


_LEASE_DURATION = 120


def parse_ioport_mode(value):
    try:
        return parse_ioport_mode_arg(value)
    except ValueError:
        raise pytest.UsageError(f'I/O port mode argument has invalid value, "{value}"')


def get_ioport_mode_testname(mode):
    if type(mode) is CSPSettings:
        idle_str = 'idle1' if mode.idle_method == IdleMethod.Method1 else 'idle2'
        return f'CSP({mode.packet_length}/{mode.packet_periodicity}:{idle_str})'
    else:
        return str(mode)


def parse_board_host_port(hostname):
    port = 3000
    if ':' in hostname:
        port = int(hostname.split(':')[1])
        hostname = hostname.split(':')[0]

    stub = BoardStub('board', hostname=hostname, port=port)

    # FIXME: In future should verfy that HERD is running using utility function from elsewhere (e.g. Board class)
    try:
        _ = Board(stub)
    except Exception:
        # Note: for unknown reasons, argparse doesn't print the exception message we throw argparse.ArgumentTypeError
        raise ArgumentTypeError(f'Could not connect to HERD application at {stub.hostname}:{stub.port}') from None

    return stub


def _parse_index_list_string(value):
    return parse_index_list_string(value, exception_type=pytest.UsageError)


def pytest_addoption(parser):
    parser.addoption('--fsm-config', action='store', help='Configuration file', required=True)
    parser.addoption('--channels', dest='channels', type=str, help="List of input/output channels")
    parser.addoption('--metric-regex', type=str, default='^$',
                     help='Regex for metrics to displays after each transition')

    parser.addoption('--tolerate-warnings', action='store_true', help="Allow warnings issued (in actions or metrics)")

    parser.addoption('--board', type=parse_board_host_port, required=True, help='Board hostname')
    parser.addoption('--processor', dest='processor', default='.*')
    parser.addoption('--fw-package', help='bit file name', required=True)
    parser.addoption('--input-data-file', nargs='+', required=True)
    parser.addoption('--io-port-mode', action='append', type=parse_ioport_mode, default=[],
                     help="I/O operating modes for link tests")
    parser.addoption('--algo-latency', dest='algo_latency', type=int, default='0',
                     help="Number of clock cycles at start of observed data that are ignored - give a range")
    parser.addoption('--link-latency', dest='link_latencies', type=_parse_index_list_string, default='0',
                     help="Number of clock cycles at start of observed data that are ignored - give a range")
    parser.addoption('--repeat-io-config',  type=int, default=1,
                     help='Number of times that I/O configuration transitions are run per iteration of full FSM')


def pytest_generate_tests(metafunc):

    if "channels_str" in metafunc.fixturenames:
        option_value = metafunc.config.option.channels
        metafunc.parametrize("channels_str", [option_value])
    if "metric_regex" in metafunc.fixturenames:
        option_value = metafunc.config.option.metric_regex
        metafunc.parametrize("metric_regex", [option_value])
    if "fw_package" in metafunc.fixturenames:
        option_value = metafunc.config.option.fw_package
        metafunc.parametrize("fw_package", [option_value])
    if "input_data_file" in metafunc.fixturenames:
        option_value = metafunc.config.option.input_data_file
        metafunc.parametrize("input_data_file", [option_value])
    if "algo_latency" in metafunc.fixturenames:
        option_value = metafunc.config.option.algo_latency
        metafunc.parametrize("algo_latency", [option_value])
    if "link_latencies" in metafunc.fixturenames:
        option_value = metafunc.config.option.link_latencies
        metafunc.parametrize("link_latencies", [option_value])
    if "tolerate_warnings" in metafunc.fixturenames:
        option_value = metafunc.config.option.tolerate_warnings
        metafunc.parametrize("tolerate_warnings", [option_value])
    if "repeat_io_config" in metafunc.fixturenames:
        option_value = metafunc.config.option.repeat_io_config
        metafunc.parametrize("repeat_io_config", [option_value])

    # --io-port-mode: Parametrize io_port_mode
    if 'io_port_mode' in metafunc.fixturenames:
        metafunc.parametrize('io_port_mode',
                             metafunc.config.getoption('io_port_mode'),
                             ids=get_ioport_mode_testname)

    # --processor: Parameterise processor_id
    if 'processor_id' in metafunc.fixturenames:
        board = Board(metafunc.config.getoption('board'))
        proc_id_pattern = re.compile(metafunc.config.getoption('processor'))
        proc_ids = [x.id for x in board.processors if proc_id_pattern.match(x.id)]
        metafunc.parametrize('processor_id', proc_ids)


def pytest_configure(config):
    config.addinivalue_line("markers", "no_interfpga_links: Mark test to be skipped when there are no inter-FPGA links")


def pytest_collection_modifyitems(config, items):
    board = Board(config.getoption('board'))
    proc_id_pattern = re.compile(config.getoption('processor'))
    interfpga_links_present = False
    for link in board.interfpga_links:
        if (proc_id_pattern.match(link.tx.processor.id) and (proc_id_pattern.match(link.rx.processor.id))):
            interfpga_links_present = True
            break

    if not interfpga_links_present:
        skip_no_interfpga_links = pytest.mark.skip(reason='Disabled when there are no inter-FPGA links')
        for item in items:
            if 'no_interfpga_links' in item.keywords:
                item.add_marker(skip_no_interfpga_links)


@pytest.fixture
def input_file(request):
    input_data_file = request.config.getoption("input_data_file")
    for iloop in range(len(input_data_file)):
        url = urlparse(input_data_file[iloop])
        if url.scheme.__eq__("generate"):
            gen_file_name = join(dirname(dirname(dirname(__file__))), f'autoGenerated_algoTest_ifile{iloop}.txt')
            print(f"Path of the generated file is {gen_file_name}")
            generate_board_data_file("v2", input_data_file[iloop], f"{gen_file_name}", 128, 1024)
            input_data_file[iloop] = gen_file_name
    return input_data_file


@pytest.fixture
def channels(channels_str):
    return parse_index_list_string(channels_str, exception_type=RuntimeError)


@pytest.fixture(name='board')
def board_fixture(request):
    stub = request.config.getoption('board')
    print(f'Connecting to board at {stub.hostname}:{stub.port}')
    return Board(stub)


@pytest.fixture(name='lease')
def lease_fixture(board, request):
    lease = Lease(board, _LEASE_DURATION)

    def retire_lease():
        lease.retire()

    request.addfinalizer(retire_lease)

    return lease


@pytest.fixture
def processors(board, request):
    proc_id_pattern = re.compile(request.config.getoption('processor'))
    return [x for x in board.processors if proc_id_pattern.match(x.id)]


@pytest.fixture
def processor(board, processor_id):
    proc = board.processor(processor_id)
    return proc


@pytest.fixture(name='replacements')
def replacements_fixture(channels_str, request):
    fw_package = request.config.getoption("fw_package")
    replacements = {'firmware_package': File(fw_package, content_type='', content_format='')}
    if channels is not None:  # which it should be since by default it takes all the channels
        replacements['input_channels'] = channels_str
        replacements['output_channels'] = channels_str

    return replacements


@pytest.fixture
def settings_pools(replacements, request):
    pools = []
    input_data_files = request.config.getoption("input_data_file")
    fsm_config = request.config.getoption("fsm_config")

    print(f'Parsing config file {fsm_config}')
    print('Variables:')
    max_key_length = max([len(k) for k in replacements])
    for k in sorted(replacements.keys()):
        print(f'   {k:{max_key_length}} = ' + repr(replacements[k]))
    print('')

    for file_path in input_data_files:
        replacements['input_data_file'] = File(file_path, content_type='', content_format='')
        pools.append(parse_parameter_file(fsm_config, replacements=replacements, use_all_replacements=True))

    return pools
