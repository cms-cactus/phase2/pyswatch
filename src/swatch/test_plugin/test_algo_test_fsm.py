import warnings

from ..cli.board_data import _main_compare
from ..cli.demonstrators import get_capture_data_files
from ..console.utilities import check_parameters_for_fsm, create_console, empty_line
from ..console.utilities import ensure_devices_in_fsm_initial_state, run_fsm_transition
from ..monitoring import Setting, Status
from os.path import join


def test_algo_test_fsm(settings_pools, lease, processor, input_file,
                       channels, metric_regex, tolerate_warnings, algo_latency, tmp_path):

    console = create_console()
    for pool in settings_pools:
        check_parameters_for_fsm(console, [processor], 'payloadTest', pool, verbose=False)

    # 1. Enter FSM
    ensure_devices_in_fsm_initial_state(console, [processor], 'payloadTest', {processor: lease})
    empty_line(console)

    # 2. Run setup transition
    run_fsm_transition(console, [processor], 'payloadTest', 'Halted', 'setup',
                       settings_pools[0], {processor: lease},
                       refresh=True, metric_regex=metric_regex,
                       strict=not tolerate_warnings)

    for i, (test_data_file, settings_pool) in enumerate(zip(input_file, settings_pools)):
        print(f'\nTesting with input file "{test_data_file}"')

        # 3. Run playback transition
        snapshots = run_fsm_transition(console, [processor], 'payloadTest', 'Ready', 'playback',
                                       settings_pool, {processor: lease},
                                       refresh=True, metric_regex=metric_regex,
                                       strict=not tolerate_warnings)[processor]

        check_monitoring_status(processor, tolerate_warnings)

        # 4. Compare captured data with injected data
        rx_data_file, tx_data_file = get_capture_data_files(snapshots, rx_required=True, tx_required=True)
        print(f'Saving captured rx data to {tmp_path}/capture_rx_{i}.txt')
        rx_data_file.save(join(tmp_path, f'capture_rx_{i}.txt'))
        print(f'Saving captured tx data to {tmp_path}/capture_tx_{i}.txt')
        tx_data_file.save(join(tmp_path, f'capture_tx_{i}.txt'))

        console.print('\nComparing captured input data with injected data')
        match_result = _main_compare(test_data_file,
                                     join(tmp_path, f'capture_rx_{i}.txt'),
                                     channels,
                                     [0],
                                     None, True, False, False)
        assert bool(match_result), 'Captured input data differs from injected data'

        console.print('\nComparing captured output data with injected data')
        match_result = _main_compare(test_data_file,
                                     join(tmp_path, f'capture_tx_{i}.txt'),
                                     channels,
                                     [algo_latency],
                                     None, True, False, False)
        assert bool(match_result), 'Captured output data differs from injected data'


def check_monitoring_status(proc, tolerate_monitoring_warnings):
    '''
    Checks status of all child monitorable objects algo
    '''
    for mon_obj in proc.monitorables:
        if mon_obj.id in {'ttc', 'algo'}:
            assert mon_obj.setting == Setting.ENABLED, (f"{mon_obj.id} component should be enabled, but it is " +
                                                        f"{mon_obj.setting.value} instead")
        else:
            assert mon_obj.setting == Setting.DISABLED, (f"{mon_obj.id} component's monitoring should be disabled, " +
                                                         f"is {mon_obj.setting.value} instead")

    # Check monitoring status of components
    allowed_status = {Status.GOOD, Status.WARNING} if tolerate_monitoring_warnings else {Status.GOOD}
    assert proc.monitorable('ttc').status in allowed_status, (f'TTC status is {proc.monitorable("ttc").status}' +
           ', but should be ' + ' or '.join(allowed_status))
    allowed_status.add(Status.NO_LIMIT)
    assert proc.monitorable('algo').status in allowed_status, (f'Algo status is {proc.monitorable("algo").status}' +
           ', but should be ' + ' or '.join(allowed_status))

    if tolerate_monitoring_warnings and proc.monitorable('ttc').status == Status.WARNING:
        warnings.warn(UserWarning(f'TTC status is {proc.monitorable("ttc").status}'))
    if tolerate_monitoring_warnings and proc.monitorable('algo').status == Status.WARNING:
        warnings.warn(UserWarning(f'Algo status is {proc.monitorable("algo").status}'))
