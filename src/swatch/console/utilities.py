
import re
import rich
import time

from datetime import datetime, timezone
from os import environ
from rich.console import Console, Group
from rich.live import Live
from rich.padding import Padding
from rich.progress import BarColumn, Progress, TaskProgressColumn, TextColumn
from rich.table import Table
from rich.text import Text

from swatch.action import Command, CommandSnapshot, Transition, TransitionSnapshot, run
from swatch.board import Board
from swatch.monitoring import FloatingPointNotation, MetricType, PropertyType, Setting, Status
from swatch.system_peripherals import DAQBoard, FrontendModule

from typing import Any, Mapping, List

from ..utilities.misc import format_index_list

_CONSOLE_THEME = rich.theme.Theme({
    'repr.number': rich.style.Style(),
    'repr.ellipsis': rich.style.Style(),
    'repr.path': rich.style.Style(),
    'repr.filename': rich.style.Style(),
    'repr.str': rich.style.Style(),
    'repr.call': rich.style.Style(),
    "repr.ipv4": rich.style.Style(),
    "repr.ipv6": rich.style.Style(),
    "repr.eui48": rich.style.Style(),
    "repr.eui64": rich.style.Style(),
})


class DurationFmt:
    def __init__(self, x):
        if isinstance(x, CommandSnapshot) or isinstance(x, TransitionSnapshot):
            self._value = x.duration
        else:
            self._value = x

    def __format__(self, format_spec):
        if self._value >= 1e-1:
            suffix = ' seconds' if format_spec == 'l' else 's'
            value = self._value
        elif self._value >= 1e-4:
            suffix = ' milliseconds' if format_spec == 'l' else 'ms'
            value = self._value * 1e3
        elif self._value >= 1e-7:
            suffix = ' microseconds' if format_spec == 'l' else 'us'
            value = self._value * 1e6
        elif self._value >= 1e-10:
            suffix = ' nanoseconds' if format_spec == 'l' else 'ns'
            value = self._value * 1e9
        elif self._value >= 1e-13:
            suffix = ' picoseconds' if format_spec == 'l' else 'ps'
            value = self._value * 1e12
        else:
            return '<1ps'

        return (f'{value:.2g}' if abs(value) < 10 else f'{value:.0f}') + suffix


def create_console(record=False):
    kwargs = {
        'record': record
    }
    if ('GITLAB_CI' in environ) and (environ['GITLAB_CI'] == 'true'):
        kwargs['force_terminal'] = True
        kwargs['force_interactive'] = False
        kwargs['width'] = 100
    return Console(theme=_CONSOLE_THEME, **kwargs)


def empty_line(console):
    # Print invisible character (Zero Width No-Break Space), to make sure that empty line is visible in
    # GitLab website for CI job (otherwise, if just print empty string, GitLab CI website won't display empty line)
    console.print('\uFEFF')


def create_metric_data_table(devices,
                             *,
                             obj_status_filter=lambda x, y: (x is not Setting.DISABLED) and (y is not Status.GOOD),
                             metric_regex='^$'):
    # Recursive function that appends metrics whose paths match the specified regex, or
    def append_metrics(table, board_id, mon_obj_ancestry, passes_status_filter=True):
        first_row_in_section = True

        mon_obj = mon_obj_ancestry[-1]
        for i, metric in enumerate(mon_obj.metrics):
            if metric.setting is Setting.DISABLED:
                continue

            if (not passes_status_filter) and (re.fullmatch(metric_regex, metric.path, re.IGNORECASE) is None):
                continue

            if first_row_in_section:
                obj_path_description = ''
                for i, x in enumerate(mon_obj_ancestry):
                    if i > 0:
                        obj_path_description += ' \u00BB '
                    if x.status is Status.GOOD:
                        obj_path_description += '[green]' + x.alias + '[/]'
                    elif x.status is Status.WARNING:
                        obj_path_description += '[dark_orange]' + x.alias + '[/]'
                    elif x.status is Status.ERROR:
                        obj_path_description += '[bright_red]' + x.alias + '[/]'
                    else:
                        obj_path_description += x.alias
                cells = [board_id, obj_path_description]
            else:
                cells = [None, None]

            if metric.status is Status.GOOD:
                metric_style = 'green'
            elif metric.status is Status.WARNING:
                metric_style = 'dark_orange'
            elif metric.status is Status.ERROR:
                metric_style = 'bright_red'
            else:
                metric_style = ''

            cells.append(Text(metric.alias))
            value_suffix = '' if metric.unit is None else metric.unit
            if metric.status is Status.UNKNOWN:
                cells.append(None)
            elif metric.value_type in [MetricType.FLOAT, MetricType.DOUBLE]:
                fmt = 'f' if metric.format.notation is FloatingPointNotation.FIXEDPOINT else 'e'
                if metric.format.precision is not None:
                    fmt = f'.{metric.format.precision}{fmt}'
                cells.append(Text(f'{metric.value:{fmt}}{value_suffix}', style=metric_style))
            else:
                cells.append(Text(f'{metric.value}{value_suffix}', style=metric_style))

            if metric.status is Status.GOOD:
                cells.append(f'[{metric_style}]GOOD[/]')
            elif metric.status is Status.WARNING:
                cells.append(f'[reverse {metric_style}]WARNING[/] [dim]({metric.warning_condition})[/]')
            elif metric.status is Status.ERROR:
                cells.append(f'[reverse {metric_style}]ERROR[/] [dim]({metric.error_condition})[/]')
            elif metric.status is Status.UNKNOWN:
                cells.append('[reverse dark_orange]UNKNOWN[/]')
            else:
                cells.append(None)

            update_time = metric.update_time.astimezone(timezone.utc)
            cells.append(f'{update_time:%H:%M:%S}.{update_time.microsecond // 1000:03d}')
            table.add_row(*cells)
            first_row_in_section = False

        if table.row_count > 0:
            table.rows[-1].end_section = True

        for x in mon_obj.monitorables:
            # Never include metrics from objects that have been disabled
            #    (since data will be stale, and typically meaningless)
            if x.setting is not Setting.DISABLED:
                append_metrics(table,
                               board_id,
                               mon_obj_ancestry + [x],
                               passes_status_filter and obj_status_filter(x.setting, x.status))

    table = Table()
    table.add_column('Board')
    table.add_column("Component")
    table.add_column("Metric")
    table.add_column("Value", justify='right')
    table.add_column("State")
    table.add_column("Timestamp (UTC)", style="dim", justify="center")

    for device in sorted(devices, key=lambda x: x.path):
        board_id = device.path.split('.')[0]
        append_metrics(table, board_id, [device], passes_status_filter=obj_status_filter(device.setting, device.status))

    return table if table.row_count > 0 else None


def create_property_table(devices,
                          *,
                          obj_status_filter=lambda x, y: (x is not Setting.DISABLED),
                          relative_path_regex='^$'):

    # Recursive function that appends paths whose paths match the specified regex
    def append_properties(table, board_id, mon_obj_ancestry, passes_status_filter=True):
        first_row_in_section = True

        mon_obj = mon_obj_ancestry[-1]
        for i, prop in enumerate(mon_obj.properties):
            if (not passes_status_filter) and (re.fullmatch(relative_path_regex, prop.path, re.IGNORECASE) is None):
                continue
            if prop.type is PropertyType.TABLE:
                continue

            if first_row_in_section:
                obj_path_description = ''
                for i, x in enumerate(mon_obj_ancestry):
                    if i > 0:
                        obj_path_description += ' \u00BB '
                    if x.status is Status.GOOD:
                        obj_path_description += '[green]' + x.alias + '[/]'
                    elif x.status is Status.WARNING:
                        obj_path_description += '[dark_orange]' + x.alias + '[/]'
                    elif x.status is Status.ERROR:
                        obj_path_description += '[bright_red]' + x.alias + '[/]'
                    else:
                        obj_path_description += x.alias
                cells = [board_id, obj_path_description]
            else:
                cells = [None, None]

            if prop.group is None:
                cells.append(Text(prop.name))
            else:
                cells.append(Text(f'{prop.group} \u00BB {prop.name}'))

            value_suffix = '' if prop.unit is None else prop.unit
            if prop.value is None:
                cells.append(None)
            elif prop.value_type in [PropertyType.FLOAT, PropertyType.DOUBLE]:
                fmt = 'f' if prop.format.notation is FloatingPointNotation.FIXEDPOINT else 'e'
                if prop.format.precision is not None:
                    fmt = f'.{prop.format.precision}{fmt}'
                cells.append(Text(f'{prop.value:{fmt}}{value_suffix}'))
            else:
                cells.append(Text(f'{prop.value}{value_suffix}'))

            update_time = prop.update_time.astimezone(timezone.utc)
            cells.append(f'{update_time:%H:%M:%S}.{update_time.microsecond // 1000:03d}')
            table.add_row(*cells)
            first_row_in_section = False

        if table.row_count > 0:
            table.rows[-1].end_section = True

        for x in mon_obj.monitorables:
            # Never include properties from objects that have been disabled
            #    (since data will be stale, and typically meaningless)
            if hasattr(x, 'properties') and (x.setting is not Setting.DISABLED):
                append_properties(table,
                                  board_id,
                                  mon_obj_ancestry + [x],
                                  passes_status_filter and obj_status_filter(x.setting, x.status))

    table = Table()
    table.add_column('Board')
    table.add_column("Component")
    table.add_column("Property")
    table.add_column("Value", justify='right')
    table.add_column("Timestamp (UTC)", style="dim", justify="center")

    for device in sorted(devices, key=lambda x: x.path):
        board_id = device.path.split('.')[0]
        append_properties(table, board_id, [device],
                          passes_status_filter=obj_status_filter(device.setting, device.status))

    return table if table.row_count > 0 else None


# TODO: Add progress as %, in case of failure
def create_command_snapshot_table(s):
    table = rich.table.Table(rich.table.Column('', max_width=120), box=rich.box.SIMPLE, show_header=False)

    for j, (_, msg) in enumerate(s.messages):
        table.add_row('\u00BB ' + msg, end_section=(j + 1 == len(s.messages)), style='grey42')

    if s.state == Command.State.DONE:
        if s.result is not None and len(str(s.result)) > 0:
            table.add_row(f'Result: {s.result}', end_section=True, style='grey42')
        table.add_row(f':white_check_mark: Command completed successfully! [grey42]({s.duration:.1f}s)[/grey42]')
    elif s.state == Command.State.WARNING:
        table.add_row('Command completed, but [bold orange]warning issued![/bold orange]! ' +
                      f'[grey42]({s.duration:.1f}s)[/grey42]')
    elif s.state == Command.State.ERROR:
        msg_prefix = '[bold]ERROR[/bold] occurred' if s.error_info is None else 'An exception was thrown'
        table.add_row(f'[red]{msg_prefix} in {s.id} command[/red] [grey42](after' +
                      f' {s.duration:.1f}s, progress {100*s.progress:.0f}%)[/grey42]')

        if s.error_info is not None:
            table.add_row(rf'[red][bold]\[{s.error_info.exception_type}][/bold] [i]{s.error_info.message}[/i][/red]')
    else:
        table.add_row(f'[grey42]Command in progress. ({s.duration:.1f}s, {100*s.progress:.0f}%)[/grey42]')

    return table


def create_transition_snapshot_table(snapshots, statuses, refresh_durations):

    table = rich.table.Table(rich.table.Column(),
                             rich.table.Column(),
                             box=rich.box.ROUNDED,
                             show_header=False)
    for s, status, refresh_duration in sorted(zip(snapshots, statuses, refresh_durations), key=lambda x: x[0].path):
        child_table = rich.table.Table(rich.table.Column('', max_width=120),
                                       box=rich.box.SIMPLE,
                                       pad_edge=False,
                                       show_edge=False,
                                       show_header=False)
        for i, c in enumerate(s.commands, start=1):
            cmd_color = ''
            if c.state == Transition.State.ERROR:
                cmd_color = 'red'
            if c.state == Transition.State.WARNING:
                cmd_color = 'orange_red1'
            child_table.add_row(f'[{i}] [bold]{c.id}[/bold] [grey42]({DurationFmt(c):l})[/grey42]', style=cmd_color)
            if len(c.parameters) > 0:
                parameter_list = ', '.join([k + '=' + repr(v) for k, v in c.parameters.items()])
                child_table.add_row(rich.padding.Padding(f'Parameters: {parameter_list}', (0, 0, 0, 4)),
                                    # Ensure 1-line gap before next command if no messages
                                    end_section=(len(c.messages) == 0),
                                    style='bright_blue italic')
            else:
                child_table.add_row('    No parameters', style='bright_blue italic')
            for j, (_, msg) in enumerate(c.messages):
                # Alternate arrows: \u203A \u276f
                child_table.add_row(rich.padding.Padding('\u00BB ' + msg, (0, 0, 0, 4)),
                                    end_section=(j + 1 == len(c.messages)),
                                    style='bright_blue')  # Alt background-agnostic color: cyan, deep_sky_blue1?

        if s.state == Transition.State.RUNNING:
            child_table.add_row(f'[bright_blue]Transition in progress. ({DurationFmt(s)},'
                                f'{100*s.progress:.0f}%)[/bright_blue]')
        elif len(s.commands) == 0:
            child_table.add_row(':yawning_face:  No commands to run.')
        elif s.state == Transition.State.DONE:
            child_table.add_row(':white_check_mark:  Transition completed successfully!'
                                f' [grey42]({DurationFmt(s)})[/grey42]')
        elif s.state == Transition.State.WARNING:
            # :grey_exclamation:
            child_table.add_row(':exclamation:  Transition completed, but [bold orange_red1]warning issued!' +
                                f'[/bold orange_red1] [grey42]({DurationFmt(s)})[/grey42]')
        elif s.state == Transition.State.ERROR:
            # :cross_mark:
            msg_prefix = 'An error occurred' if s.commands[-1].error_info is None else 'An exception was thrown'
            child_table.add_row(f':x:  [red]{msg_prefix} in {s.commands[-1].id} command.[/red] ' +
                                f'[grey42]({DurationFmt(s)}, ' +
                                f'progress {100*s.progress:.0f}%)[/grey42]')
            if s.commands[-1].error_info is not None:
                child_table.add_row(rf'    [red][bold]\[{s.commands[-1].error_info.exception_type}][/bold]'
                                    f' [i]{s.commands[-1].error_info.message}[/i][/red]\n')

        if refresh_duration is not None:
            child_table.add_row(f'    Monitoring status: [bold]{status.__rich__()}[/bold] '
                                f'[grey42]({DurationFmt(refresh_duration)})[/grey42]')

        time_str = f'{s.start_time.astimezone(timezone.utc):%H:%M:%S} UTC'
        table.add_row(f'[bold]{s.actionable_path}[/bold]\n[grey42]\\[{time_str}][/grey42]',  # noqa: W605
                      child_table,
                      end_section=True)
    return table


def run_command(console, devices, command_id, param_sets, leases, *, strict=False):
    def poll_callback(s):
        live.update(create_command_snapshot_table(s))

    devices_without_requested_cmd = [d.path for d in devices if not d.has_command(command_id)]
    if len(devices_without_requested_cmd) > 0:
        raise RuntimeError((f'No command named "{command_id}" is registered in ' +
                            ', '.join(devices_without_requested_cmd)))

    snapshots = []
    for i, device in enumerate(devices):
        console.print(f'Running {command_id} command on {device.path} ...', style='bold')
        with Live('', refresh_per_second=4, console=console, vertical_overflow='visible') as live:
            s = device.command(command_id).run(param_sets[i], leases[device], poll_callback=poll_callback)
        snapshots.append(s)
        empty_line(console)

    errors = []
    warnings = []

    for device, snapshot in zip(devices, snapshots):
        if snapshot.state is Command.State.ERROR:
            errors.append(device)

        if snapshot.state is Command.State.WARNING:
            warnings.append(device)

    if len(errors) > 0:
        msg = f'Error occurred when running command {command_id} on '
        msg += ', '.join([device.path for device in errors])
        raise RuntimeError(msg)
    if strict and len(warnings) > 0:
        msg = f'Warning occurred when running command {command_id} on '
        msg += ', '.join([device.path for device in warnings])
        raise RuntimeError(msg)

    return snapshots


def engage_devices_in_fsm(console, devices, fsm_id, leases):
    console.print(f':grey_exclamation: Engaging {fsm_id} FSM on {", ".join([d.path for d in devices])}',
                  style='bright_blue')
    for d in devices:
        d.fsm(fsm_id).engage(leases[d])


def ensure_devices_in_fsm_initial_state(console, devices, fsm_id, leases):

    devices_to_disengage_map = {}
    devices_to_engage = []
    devices_to_reset = []
    for device in devices:
        if device.currentFSM != fsm_id:
            if device.currentFSM is not None:
                if device.currentFSM not in devices_to_disengage_map:
                    devices_to_disengage_map[device.currentFSM] = []
                devices_to_disengage_map[device.currentFSM].append(device)
            devices_to_engage.append(device)
        else:
            devices_to_reset.append(device)

    devices_to_engage.sort(key=lambda x: x.path)
    devices_to_reset.sort(key=lambda x: x.path)

    for current_fsm, devices_to_disengage in devices_to_disengage_map.items():
        devices_to_disengage.sort(key=lambda x: x.path)
        console.print(f':grey_exclamation: Disengaging {", ".join([d.path for d in devices_to_disengage])}'
                      f' from current FSM ({current_fsm})', style='bright_blue')
        for device in devices_to_disengage:
            device.fsm(current_fsm).disengage(leases[device])

    if len(devices_to_engage) > 0:
        console.print(f':grey_exclamation: Engaging {fsm_id} FSM on '
                      f'{", ".join([d.path for d in devices_to_engage])}', style='bright_blue')
        for device in devices_to_engage:
            device.fsm(fsm_id).engage(leases[device])

    if len(devices_to_reset) > 0:
        console.print(f':grey_exclamation: Resetting {fsm_id} FSM on '
                      f'{", ".join([d.path for d in devices_to_reset])}', style='bright_blue')
        for device in devices_to_reset:
            device.fsm(fsm_id).reset(leases[device])


def disengage_devices_from_fsm(console, devices, leases):
    current_fsms = {}
    for d in devices:
        if d.currentFSM not in current_fsms:
            current_fsms[d.currentFSM] = [d.path]
        else:
            current_fsms[d.currentFSM].append(d.path)

    if None in current_fsms:
        raise RuntimeError(f'Devices {", ".join(current_fsms[None])} are not engaged in any FSM')

    for k, v in current_fsms.items():
        console.print(f':grey_exclamation: Disengaging from FSM {k} on {", ".join([x for x in v])}')

    for d in devices:
        d.fsm(d.currentFSM).disengage(leases[d])


def reset_devices_in_fsm(console, devices, leases):
    current_fsms = {}
    for d in devices:
        if d.currentFSM not in current_fsms:
            current_fsms[d.currentFSM] = [d.path]
        else:
            current_fsms[d.currentFSM].append(d.path)

    if None in current_fsms:
        raise RuntimeError(f'Devices {", ".join(current_fsms[None])} are not engaged in any FSM')

    for k, v in current_fsms.items():
        console.print(f':grey_exclamation: Resetting FSM {k} on {", ".join([x for x in v])}')

    for d in devices:
        d.fsm(d.currentFSM).reset(leases[d])


def run_fsm_transition(console, devices, fsm_id, state_id, transition_id, params, leases, *,
                       refresh=False, strict=False, metric_regex='^$', verbose=True):
    # 1. Check that each device is engaged in correct FSM, and in desired state
    devices_in_wrong_state = []
    for device in devices:
        if device.currentFSM is None:
            console.print(f'  {device.path} is not engaged in any FSM', style='red')
            devices_in_wrong_state.append(device)
        elif device.currentFSM != fsm_id:
            console.print(f'  {device.path} is engaged in the wrong FSM ({device.currentFSM} rather than'
                          f' {fsm_id})', style='red')
            devices_in_wrong_state.append(device)
        elif device.currentState != state_id:
            console.print(f'  {device.path} is engaged in the wrong state of {device.currentFSM} FSM'
                          f' ({device.currentState} rather than {state_id})', style='red')
            devices_in_wrong_state.append(device)

    if len(devices_in_wrong_state) > 0:
        raise RuntimeError(f'{len(devices_in_wrong_state)} devices are in the wrong FSM/state')

    # 2. Run the transitions
    transitions = [d.fsm(fsm_id).state(state_id).transition(transition_id) for d in devices]
    progress = Progress(
        TextColumn('  {task.description}'),
        BarColumn(),
        TaskProgressColumn(),
        TextColumn('  {task.fields[status_summary]}'),
        console=console
    )
    tasks = {}
    for t in sorted(transitions, key=lambda x: x.actionable.path):
        tasks[t.actionable.path] = progress.add_task(t.actionable.path,
                                                     total=float(len(t.steps) + 1),
                                                     status_summary='...')

    start_time = time.time()
    start_time_str = f'{datetime.fromtimestamp(start_time).astimezone(timezone.utc):%H:%M:%S} UTC'
    end_time = None

    def create_status_renderable():
        title = Text('', style='bold')
        title.append(f'[{start_time_str}]', style='grey42')
        title.append(f' Running {transition_id} transition ({state_id} \u279E {transitions[0].end_state})')
        title.append(f' on {", ".join(sorted(d.path for d in devices))} ')
        if end_time is None:
            title.append(f'({DurationFmt(time.time() - start_time)})', style='grey42')
        else:
            title.append(f'(took {DurationFmt(end_time - start_time)})', style='grey42')
        return Group(title, Text(), progress, Text())

    with Live(create_status_renderable(), refresh_per_second=4) as live:
        def poll_callback(snapshots, refresh_durations):
            for device, snapshot, refresh_duration in zip(devices, snapshots, refresh_durations):
                if snapshot is None:
                    status_summary = 'Sending request'
                elif snapshot.state is Transition.State.SCHEDULED:
                    status_summary = 'Transition scheduled for execution'
                elif snapshot.state is Transition.State.RUNNING:
                    status_summary = (f'Running {snapshot.commands[-1].id} command ({len(snapshot.commands)}'
                                      f' of {snapshot.total_num_commands} commands)')
                elif snapshot.is_finished():

                    if snapshot.state is Transition.State.DONE:
                        status_summary = ':white_check_mark: Transition successful!'
                    elif snapshot.state is Transition.State.WARNING:
                        status_summary = 'Transition completed, but [orange_red1]warning issued![/orange_red1]'
                    elif snapshot.state is Transition.State.ERROR:
                        status_summary = f'[red]Error occurred in {snapshot.commands[-1].id} command![/red]'

                    if refresh_duration is None:
                        status_summary += ' Refreshing monitoring data.'
                    elif device.status != Status.GOOD:
                        status_summary += f' Monitoring: [bold]{device.status.__rich__()}[/bold]'

                if snapshot is not None:
                    status_summary += ' [grey42]('
                    status_summary += f'{DurationFmt(snapshot.duration)}'
                    if refresh_duration is not None:
                        status_summary += f' + {DurationFmt(refresh_duration)}'
                    status_summary += ')[/grey42]'

                completed = 0.0 if snapshot is None else snapshot.total_num_commands * snapshot.progress
                if refresh_duration is not None:
                    completed += 1.0
                progress.update(tasks[device.path], completed=completed, status_summary=status_summary)

            live.update(create_status_renderable())

        snapshots, refresh_durations = run(transitions, params, leases, poll_callback=poll_callback, refresh=refresh)
        end_time = time.time()
        poll_callback(snapshots, refresh_durations)

    # Print table listing all commands, their parameters and messages if in verbose mode, or if error/warning occurs
    if verbose or any(s.state != Transition.State.DONE for s in snapshots):
        console.print('Detailed information:')
        table = create_transition_snapshot_table(snapshots, [d.status for d in devices], refresh_durations)
        console.print(Padding(table, (1, 0, 1, 2)))

    results = {device: s for device, s in zip(devices, snapshots)}
    cmd_error_map = {}
    cmd_warning_map = {}

    for device, transition_snapshot in results.items():
        if transition_snapshot.state is Transition.State.ERROR:
            if transition_snapshot.commands[-1].id not in cmd_error_map:
                cmd_error_map[transition_snapshot.commands[-1].id] = [device.path]
            else:
                cmd_error_map[transition_snapshot.commands[-1].id].append(device.path)

        if transition_snapshot.state is Transition.State.WARNING:
            if transition_snapshot.commands[-1].id not in cmd_warning_map:
                cmd_warning_map[transition_snapshot.commands[-1].id] = [device.path]
            else:
                cmd_warning_map[transition_snapshot.commands[-1].id].append(device.path)

    if len(cmd_error_map) > 0:
        msg = 'Error occurred '
        msg += '; '.join([f'in command {cmd} on {", ".join(dev_paths)}' for cmd, dev_paths in cmd_error_map.items()])
        raise RuntimeError(msg)
    if strict and len(cmd_warning_map) > 0:
        msg = 'Warning occurred '
        msg += '; '.join([f'in command {cmd} on {", ".join(dev_paths)}' for cmd, dev_paths in cmd_warning_map.items()])
        raise RuntimeError(msg)

    # Show monitoring data table, for objects in errors/warning, and metrics matching regex
    status_summary = create_metric_data_table(devices, metric_regex=metric_regex)
    if status_summary is not None:
        console.print(Padding.indent(status_summary, 2))
        empty_line(console)

    allowed_status_values = [Status.NO_LIMIT, Status.GOOD]
    if not strict:
        allowed_status_values += [Status.WARNING, Status.UNKNOWN]
    require_monitoring_status(devices, allowed_status_values)

    return results


def require_monitoring_status(mon_objs, allowed_status_values):
    '''Raises exectpion if monitoring status isn't in supplied list'''

    # Raise exception based on monitoring status
    devices_by_status = {d.status: [] for d in mon_objs}
    for mon_obj in mon_objs:
        devices_by_status[mon_obj.status].append(mon_obj)

    if (Status.ERROR not in allowed_status_values) and (Status.ERROR in devices_by_status):
        # FIXME: Print summary of objects/metrics causing error
        device_paths = [x.path for x in devices_by_status[Status.ERROR]]
        raise RuntimeError(f'{len(device_paths)} objects(s) in error: ' + ', '.join(device_paths))

    if (Status.WARNING not in allowed_status_values) and (Status.WARNING in devices_by_status):
        # FIXME: Print summary of objects/metrics causing warning
        device_paths = [x.path for x in devices_by_status[Status.WARNING]]
        raise RuntimeError(f'{len(device_paths)} objects(s) in warning: ' + ', '.join(device_paths))

    if (Status.UNKNOWN not in allowed_status_values) and (Status.UNKNOWN in devices_by_status):
        # FIXME: Print summary of objects/metrics causing status
        device_paths = [x.path for x in devices_by_status[Status.UNKNOWN]]
        raise RuntimeError(f'Monitoring data missing in {len(device_paths)} objects(s): ' + ', '.join(device_paths))

    if (Status.NO_LIMIT not in allowed_status_values) and (Status.NO_LIMIT in devices_by_status):
        device_paths = [x.path for x in devices_by_status[Status.NO_LIMIT]]
        raise RuntimeError(f'{len(device_paths)} objects(s) have neutral status: ' + ', '.join(device_paths))

    if (Status.GOOD not in allowed_status_values) and (Status.GOOD in devices_by_status):
        device_paths = [x.path for x in devices_by_status[Status.GOOD]]
        raise RuntimeError(f'{len(device_paths)} objects(s) have good status: ' + ', '.join(device_paths))


def check_parameters_for_fsm(console, devices, fsm_id, settings_pool, *, verbose):
    transitions = []
    for device in devices:
        for state in device.fsm(fsm_id).states:
            for transition in state.transitions:
                transitions.append(transition)

    # TODO: Sort into appropriate order - e.g. state then transition then board/device

    param_summary_tables = {}
    missing_params_map = {}
    type_mismatches_map = {}
    rule_violations_map = {}

    for t in transitions:
        # Extract parameters and run checks
        param_sets = t.extract_parameters(settings_pool, strict=False)
        missing_params = t.check_for_missing_parameters(param_sets)
        type_mismatches, rule_violations = t.check_for_invalid_parameters(param_sets)
        if len(missing_params) > 0:
            missing_params_map[t] = missing_params
        if len(type_mismatches) > 0:
            type_mismatches_map[t] = type_mismatches
        if len(rule_violations) > 0:
            rule_violations_map[t] = rule_violations

        # Populate 'parameter set' table with data
        transition_label = f'transition "{t.id}" ({t.start_state} -> {t.end_state})'
        if transition_label not in param_summary_tables:
            table = Table()
            table.add_column('Board & device')
            table.add_column('Command')
            table.add_column('Parameter')
            table.add_column('Value')
            table.add_column('Value OK?', justify='center')
            param_summary_tables[transition_label] = table

        for i, (step, param_set) in enumerate(zip(t.steps, param_sets)):
            if len(param_set) == 0:
                param_summary_tables[transition_label].add_row(t.actionable.path if i == 0 else '',
                                                               step.command.id,
                                                               '[italic]None[/]',
                                                               '',
                                                               '',
                                                               end_section=(i == len(param_sets) - 1))

            for j, param_id in enumerate(sorted(param_set)):
                value_ok = (sum(x.parameter == param_id and
                                x.nspace == step.namespace and
                                x.command == step.command.id
                                for x in type_mismatches) +
                            sum(param_id == y.parameter and
                                x.nspace == step.namespace and
                                x.command == step.command.id
                                for x in rule_violations for y in x.violations)) == 0
                param_summary_tables[transition_label].add_row(t.actionable.path if i == 0 and j == 0 else '',
                                                               f'{step.command.id}' if j == 0 else '',
                                                               param_id,
                                                               str(param_set[param_id]),
                                                               ':white_check_mark:' if value_ok else ':x:',
                                                               end_section=((i == len(param_sets) - 1) and
                                                                            (j == len(param_set) - 1)))

            if (i != len(param_sets) - 1) and len(param_set) > 1:
                param_summary_tables[transition_label].add_row()

    if verbose or (len(missing_params_map) + len(type_mismatches_map) + len(rule_violations_map)) > 0:
        for transition_label, table in param_summary_tables.items():
            if table.row_count > 0:
                console.print('Parameters for ' + transition_label)
                console.print(Padding(table, (1, 4)))

    # Report on missing parameters
    if len(missing_params_map) > 0:
        table = Table()
        table.add_column('Board & device')
        table.add_column('Transition')
        table.add_column('Namespace')
        table.add_column('Command')
        table.add_column('Parameter')
        table.add_column('Description')
        for transition in missing_params_map:
            for i, missing_param in enumerate(missing_params_map[transition]):
                param_description = transition.actionable.command(missing_param.command).parameter(
                                        missing_param.parameter).description
                table.add_row(transition.actionable.path if i == 0 else '',
                              f'{transition.id} ({transition.start_state} -> {transition.end_state})' if i == 0
                              else '',
                              missing_param.nspace,
                              missing_param.command,
                              missing_param.parameter,
                              param_description,
                              end_section=(i == len(missing_params_map[transition]) - 1))

        console.print('Missing parameters:')
        console.print(Padding(table, (1, 4)))

    # Report on parameter type mismatches
    if len(type_mismatches_map) > 0:
        table = Table()
        table.add_column('Board & device')
        table.add_column('Transition')
        table.add_column('Namespace')
        table.add_column('Command')
        table.add_column('Parameter')
        table.add_column('Type expected')
        table.add_column('Type supplied')
        for transition in type_mismatches_map:
            for i, type_mismatch in enumerate(type_mismatches_map[transition]):
                table.add_row(transition.actionable.path if i == 0 else '',
                              f'{transition.id} ({transition.start_state} -> {transition.end_state})' if i == 0
                              else '',
                              type_mismatch.nspace,
                              type_mismatch.command,
                              type_mismatch.parameter,
                              type_mismatch.expected_type,
                              type_mismatch.actual_type,
                              end_section=(i == len(type_mismatches_map[transition]) - 1))

        console.print('Incorrect types:')
        console.print(Padding(table, (1, 4)))

    # Report on parameter rule violations
    if len(rule_violations_map) > 0:
        table = Table()
        table.add_column('Board & device')
        table.add_column('Transition')
        table.add_column('Namespace')
        table.add_column('Command')
        table.add_column('Parameter')
        table.add_column('Value')
        table.add_column('Requirement')
        for transition in rule_violations_map:
            i = 0
            for i, rule_violation in enumerate(rule_violations_map[transition]):
                for j, x in enumerate(rule_violation.violations):
                    table.add_row(transition.actionable.path if i + j == 0 else '',
                                  f'{transition.id} ({transition.start_state} -> {transition.end_state})' if i + j == 0
                                  else '',
                                  rule_violation.nspace if j == 0 else '',
                                  rule_violation.command if j == 0 else '',
                                  x.parameter,
                                  str(rule_violation.param_set[x.parameter]),
                                  x.rule_description,
                                  end_section=(i == len(rule_violations_map[transition]) - 1) and
                                              (j == len(rule_violation.violations) - 1))

        console.print('Invalid parameter values (e.g. violating requirements declared by on-board plugin):')
        console.print(Padding(table, (1, 4)))

    if len(missing_params_map) + len(type_mismatches_map) + len(rule_violations_map) > 0:
        num_missing_params = sum(len(x) for x in missing_params_map.values())
        num_type_mismatches = sum(len(x) for x in type_mismatches_map.values())
        num_rule_violations = sum(len(x) for x in rule_violations_map.values())
        exception_suffix = []
        if num_missing_params > 0:
            exception_suffix.append(f'{num_missing_params} missing')
        if num_type_mismatches == 1:
            exception_suffix.append(f'{num_type_mismatches} value is of incorrect type')
        elif num_type_mismatches > 1:
            exception_suffix.append(f'{num_type_mismatches} values are of incorrect type')
        if num_rule_violations == 1:
            exception_suffix.append(f'{num_rule_violations} value is invalid')
        elif num_rule_violations > 1:
            exception_suffix.append(f'{num_rule_violations} values are invalid')
        raise RuntimeError('Detected problems with supplied parameter values - ' + ', '.join(exception_suffix))


def check_link_map(console, links, crate_map):

    console.print(':spaghetti: Comparing declared link map with sender IDs embedded in the link protocol ...',
                  style='bold')
    empty_line(console)
    external_links = []
    masked_links = []
    disabled_links = []
    incorrect_links = []
    correct_links = []
    unknown_links = []

    num_links = 0
    for link in links:
        num_links += 1
        if isinstance(link.source.board, (DAQBoard, FrontendModule)):
            masked_links.append(link)
        elif isinstance(link.destination.board, (DAQBoard, FrontendModule)):
            masked_links.append(link)
        elif type(link.destination.board) is not Board:
            external_links.append(link)
        elif link.destination.port.is_masked:
            masked_links.append(link)
        elif link.destination.port.setting == Setting.DISABLED:
            disabled_links.append(link)
        elif link.destination.port.source_info is None:
            unknown_links.append(link)
        elif ((link.tx.board.crate_id in crate_map) and
              (link.rx.port.source_info.crate != crate_map[link.tx.board.crate_id].csp_id)):
            incorrect_links.append(link)
        elif (link.tx.board.logical_slot is not None) and link.rx.port.source_info.slot != link.tx.board.logical_slot:
            incorrect_links.append(link)
        elif link.destination.port.source_info.channel != link.source.port.csp_index:
            incorrect_links.append(link)
        else:
            correct_links.append(link)

    if len(correct_links) > 0:
        excluded_notes = []
        if len(external_links) > 0:
            excluded_notes.append(f'{len(external_links)} links to externally-controlled boards')
        if len(masked_links) > 0:
            excluded_notes.append(f'{len(masked_links)} masked links')
        msg_suffix = '' if len(excluded_notes) == 0 else f' ( excludes {" and ".join(excluded_notes)})'
        console.print(f'  :white_check_mark:  Source info matches declared link map for {len(correct_links)} of the '
                      f'{num_links - len(masked_links)} active links{msg_suffix}')
    if len(disabled_links) > 0:
        disabled_link_str = ', '.join([link.id for link in disabled_links])
        console.print(f'      NOTE: Receiver is not enabled on {len(disabled_links)} links ({disabled_link_str})')
        raise RuntimeError(f'Receiver is not enabled on {len(disabled_links)} links')

    empty_line(console)

    if len(incorrect_links) > 0:
        console.print('  :x:  Sender ID received on the following input ports differs from declared link map:',
                      style='red')
        table = Table()
        table.add_column('Link')
        table.add_column('Receiver (board, FPGA & channel)')
        table.add_column('Sender in configuration file')
        table.add_column('Sender info from link protocol')
        for link in incorrect_links:
            source_info = link.destination.port.source_info
            expected_tx_suffix = ''
            if (link.source.port.index != link.source.port.csp_index):
                expected_tx_suffix = f' = CSP ch{link.source.port.csp_index}'
            if (link.source.optical_module is not None) and (link.source.optical_channel is not None):
                expected_tx_suffix += (f' (optics: {link.source.optical_module.alias} ch' +
                                       f'{link.source.optical_channel.index})')

            rx_suffix = ''
            if (link.rx.optical_module is not None) and (link.rx.optical_channel is not None):
                rx_suffix = f'\n(Optics: {link.rx.optical_module.alias} ch {link.rx.optical_channel.index})'

            if link.source.board.crate_id in crate_map:
                source_crate_csp_id = crate_map[link.source.board.crate_id].csp_id
            else:
                source_crate_csp_id = 'unknown'

            table.add_row(link.id,
                          f'{link.destination.board.id} \u00BB {link.destination.processor.alias} channel '
                          f'{link.destination.port.index}{rx_suffix}',
                          f'{link.source.board.id} (crate {source_crate_csp_id}, slot '
                          f'{link.source.board.logical_slot})\n{link.source.processor.alias} ch'
                          f'{link.source.port.index}{expected_tx_suffix}',
                          f'Crate {source_info.crate}, slot {source_info.slot}, channel {source_info.channel}',
                          end_section=True)
        console.print(Padding(table, (1, 4)))
        raise RuntimeError(f'Source info received on {len(incorrect_links)} links differs from the declared link map')

    if len(unknown_links) > 0:
        console.print('  :x:  Sender ID unknown on the following input ports:', style='red')
        for link in unknown_links:
            console.print(f'       * Board {link.destination.board.id}: {link.destination.processor.alias} channel '
                          f'{link.destination.port.index} (link "{link.id}")')
        raise RuntimeError(f'Source info unknown for {len(unknown_links)} links')


def print_info(console, board):
    for d in board.devices:
        console.print(f"Device '[bold]{d.id}[/bold]':")

        console.print(f'  Type:          {d.type}')
        console.print(f'  URI:           {d.uri}', style=None)
        console.print(f'  Address table: {d.addressTable}')
        empty_line(console)

        if len(d.commands) == 0:
            console.print('  No commands defined')
        else:
            console.print('  Commands:')

            table = rich.table.Table(rich.table.Column('', max_width=30),
                                     rich.table.Column('', max_width=90),
                                     box=rich.box.SIMPLE,
                                     show_header=False)
            max_text_length = max([len(p.id) + len(p.type) for c in d.commands for p in c.parameters])
            for command in d.commands:
                table.add_row(f'  \u00BB [bold]{command.id}[/bold]', f'[italic]{command.description}[/italic]')
                if len(command.parameters) == 0:
                    table.add_row('      [italic]No parameters[italic]', '', end_section=True)
                else:
                    # max_text_length = max([len(p.id) + len(p.type) for p in command.parameters])
                    for j, p in enumerate(command.parameters):
                        table.add_row(f'      {p.id:{max_text_length - len(p.type)}}' +
                                      rf' [grey42]\[{p.type}][/grey42]',  # noqa: W605
                                      f'[grey42][italic]{p.description}[/italic][/grey42]',
                                      end_section=(j + 1 == len(command.parameters)))

            console.print(table)

        if len(d.fsms) == 0:
            console.print('  No FSMs defined')
        else:
            console.print('  FSMs:')

            for fsm in d.fsms:
                console.print(f'    \u00BB [bold]{fsm.id}[/bold]')
                list_of_states = ', '.join([s.id for s in fsm.states])
                console.print(f'      States: {list_of_states}')
                console.print('      Transitions:')
                for s in fsm.states:
                    for t in s.transitions:
                        list_of_commands = ', '.join([step.command.id for step in t.steps])
                        console.print(f'       * [bold]{t.id}[/bold] ({s.id} -> {t.end_state}): {list_of_commands}')

        empty_line(console)
        empty_line(console)


def check_connected_optics(console, system, dry_run):

    boards = system.boards
    connected_optics = system.connected_optics
    connected_optics_by_board = []

    if len(connected_optics) == 0:
        return

    for b in boards:
        optics = []
        if type(b) is Board:
            for c in connected_optics:
                if c.path.split('.')[0] == b.id:
                    optics.append(c)
            connected_optics_by_board.append(optics)

    bad_optics = []
    table = Table()
    table.add_column('Board')
    table.add_column('Connected optics')
    table.add_column('Status')

    for i, b in enumerate(boards):
        for j, module in enumerate(connected_optics_by_board[i]):
            table.add_row(b.id if j == 0 else '',
                          module.id,
                          f'[bold]{module.status.__rich__()}[/bold]',
                          end_section=(j == len(connected_optics_by_board[i]) - 1))
            if module.status is not Status.GOOD:
                bad_optics.append(module.path)

    if table.row_count > 0:
        empty_line(console)
        console.print('Status of connected optics by board:')
        console.print(Padding(table, (1, 4)))

    if not dry_run:  # Don't flag bad status if optics are not configured
        if len(bad_optics) > 0:
            console.print('Problem with optical modules: ' + ', '.join(bad_optics))
            empty_line(console)
        else:
            console.print('Connected optical modules are all good!')
            empty_line(console)


def get_system_map(system) -> Mapping[Any, Mapping[Any, List[Any]]]:  # all procs in sys
    # NB: map excludes links to or from external boards, as well as masked links
    channel_map = {}
    proc_map: Mapping[Any, int] = {}
    for proc in system.connected_processors:
        proc_map[proc] = proc.path
        channel_map[proc.path] = {}

    for link in system.links:
        if ((type(link.destination.processor).__name__ != 'ExternalProcessor') and
                (type(link.source.processor).__name__ != 'ExternalProcessor') and
                (not link.rx.port.is_masked)):
            source_proc = proc_map[link.source.processor]
            dest_proc = proc_map[link.destination.processor]
            channel_map[dest_proc][link.destination.port.index] = [source_proc, link.source.port.index]

    return channel_map


def create_link_comparison_table(console, system, match_results, bad_channels):
    table = Table()
    table.add_column('Rx processor')
    table.add_column('Matched channels')
    table.add_column('Mismatched channels')

    for proc in system.connected_processors:
        if len(match_results[proc.path].channels) == 0:
            continue
        result = match_results[proc.path]
        matched_channels = []
        mismatched_channels = []
        if bool(result):
            matched_channels = format_index_list(result.channels)
        else:
            mismatched_channels = bad_channels[proc.path]
            matched_channels = format_index_list([c for c in result.channels if c not in mismatched_channels])

        table.add_row(f'{proc.path}',
                      f':white_check_mark: {matched_channels}' if matched_channels else '-',
                      f':x: [red]{format_index_list(mismatched_channels)}[/red]' if mismatched_channels else '-',
                      end_section=(len(result.channels) - 1))

    if table.row_count > 0:
        empty_line(console)
        console.print('Summary of link test Rx expected / observed data comparison:', style='bold')
        console.print(Padding(table, (1, 4)))
