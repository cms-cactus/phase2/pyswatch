
import os
import re

from ..parameters import File
from prompt_toolkit import PromptSession
from prompt_toolkit.completion import WordCompleter, PathCompleter
from prompt_toolkit.validation import Validator


def _convert_string_to_bool(value):
    value_map = {'true': True, 't': True, '1': True,
                 'false': False, 'f': False, '0': False,
                 }

    if isinstance(value, bool):
        return value

    if not isinstance(value, str):
        raise ValueError('invalid literal for boolean. Not a string.')

    lower_value = value.lower()
    if lower_value in value_map:
        return value_map[lower_value]
    else:
        raise ValueError('invalid literal for boolean: "%s"' % value)


def _read_file(path):
    return File(path, content_type='', content_format='')


def _is_bool(text):
    return text.lower() in ['true', 'false', 't', 'f', '0', '1']


_REGEX_PATTERN_UINT = re.compile(r"^[+]?[0-9]+$")
_REGEX_PATTERN_INT = re.compile(r"^[-+]?[0-9]+$")
_REGEX_PATTERN_FLOAT = re.compile(r"^[-+]?[0-9]*\.?[0-9]+$")


def _is_uint(text):
    return _REGEX_PATTERN_UINT.match(text)


def _is_int(text):
    return _REGEX_PATTERN_INT.match(text)


def _is_float(text):
    return _REGEX_PATTERN_FLOAT.match(text)


def _is_file(text):
    return os.path.isfile(text) or text.startswith('http://') or text.startswith('https://')


# Prompt sessions for each parameter type (bool, uint, ...). Dynamically populated to avoid import
# errors in setups where there is no stdin
_PARAM_PROMPT_SESSIONS = {}


def get_param_prompt_session(param_type):
    if len(_PARAM_PROMPT_SESSIONS) == 0:
        _PARAM_PROMPT_SESSIONS['bool'] = PromptSession(completer=WordCompleter(['True', 'true', 'False', 'false']),
                                                       validator=Validator.from_callable(_is_bool,
                                                       error_message='Input is not a bool!',
                                                       move_cursor_to_end=True))
        _PARAM_PROMPT_SESSIONS['uint32'] = PromptSession(validator=Validator.from_callable(_is_uint,
                                                         error_message='Input contains non-numeric characters',
                                                         move_cursor_to_end=True))
        _PARAM_PROMPT_SESSIONS['int'] = PromptSession(validator=Validator.from_callable(_is_int,
                                                      error_message='Input contains non-numeric characters',
                                                      move_cursor_to_end=True))
        _PARAM_PROMPT_SESSIONS['float'] = PromptSession(validator=Validator.from_callable(_is_float,
                                                        error_message='Input is not a float',
                                                        move_cursor_to_end=True))
        _PARAM_PROMPT_SESSIONS['string'] = PromptSession()
        _PARAM_PROMPT_SESSIONS['vector:string'] = PromptSession()
        _PARAM_PROMPT_SESSIONS['file'] = PromptSession(completer=PathCompleter(min_input_len=1),
                                                       validator=Validator.from_callable(_is_file,
                                                       error_message='Input is not a path for a file',
                                                       move_cursor_to_end=True))

    return _PARAM_PROMPT_SESSIONS[param_type]


_PARAM_CONVERTERS = {
  "bool": _convert_string_to_bool,
  "uint32": int,
  "int": int,
  "float": float,
  "string": str,
  "vector:string": lambda x: [str(x)],
  "file": lambda x: File(x, content_type='', content_format='')
}


def get_command_parameters(command, msg_suffix=''):
    parameters = {}
    if len(command.parameters) != 0:
        print(f'Please enter parameter values{msg_suffix} ...')

        field_widths = (max([len(p.id) for p in command.parameters]),
                        max([len(p.type) + 2 for p in command.parameters]))
        for param in command.parameters:
            message = "  {0:{2[0]}} {1:>{2[1]}}: ".format(param.id, '['+param.type+']', field_widths)
            value = get_param_prompt_session(param.type).prompt(message=message)
            parameters[param.id] = _PARAM_CONVERTERS[param.type](value)
        print()
    return parameters


def get_transition_parameters(transition):
    commands = [x.command for x in transition.steps]

    parameter_sets = [{} for c in commands]
    if sum([len(c.parameters) for c in commands]) > 0:
        print("Please enter parameter values ...")

        for i, c in enumerate(commands):
            if len(c.parameters) == 0:
                print("  {}) {}: No parameters".format(i+1, c.id))
            else:
                print("  {}) {}".format(i+1, c.id))

                field_widths = (max([len(p.id) for p in c.parameters]), max([len(p.type) + 2 for p in c.parameters]))
                for param in c.parameters:
                    message = "       {0:{2[0]}} {1:>{2[1]}}: ".format(param.id, '['+param.type+']', field_widths)
                    value = get_param_prompt_session(param.type).prompt(message=message)
                    parameter_sets[i][param.id] = _PARAM_CONVERTERS[param.type](value)
        print()
    return parameter_sets
