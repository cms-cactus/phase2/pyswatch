from dataclasses import dataclass
from datetime import datetime
from enum import Enum, unique
from typing import Any


@unique
class EventType(Enum):
    FSMEngage = "fsm:engage"
    FSMReset = "fsm:reset"
    FSMDisengage = "fsm:disengage"
    FSMTransition = "fsm:transition"
    Command = "command"
    LeaseRequest = "lease:obtain"
    LeaseRenewal = "lease:renew"
    LeaseRetire = "lease:retire"


@dataclass
class FSMEngage():
    device: str
    fsm: str
    time: datetime
    event_type: Enum = EventType.FSMEngage


@dataclass
class FSMReset():
    device: str
    fsm: str
    time: datetime
    event_type: Enum = EventType.FSMReset


@dataclass
class FSMDisengage():
    device: str
    fsm: str
    time: datetime
    event_type: Enum = EventType.FSMDisengage


@dataclass
class Command():
    device: str
    command: str
    time: datetime
    snapshot: Any
    event_type: Enum = EventType.Command


@dataclass
class Transition():
    device: str
    fsm: str
    transition: str
    time: datetime
    snapshot: Any
    event_type: Enum = EventType.FSMTransition


@dataclass
class LeaseRequest():
    supervisor: str
    duration: float
    time: datetime
    event_type: Enum = EventType.LeaseRequest


@dataclass
class LeaseRenewal():
    supervisor: str
    duration: float
    time: datetime
    event_type: Enum = EventType.LeaseRenewal


@dataclass
class LeaseRetire():
    supervisor: str
    duration: float
    time: datetime
    event_type: Enum = EventType.LeaseRetire
