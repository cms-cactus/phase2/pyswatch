import re

from dataclasses import dataclass, field
from enum import Enum, unique
from typing import Any, Dict, List, Literal, Mapping, Optional, Pattern

from pydantic import conlist, field_validator, model_validator
from pydantic import BaseModel, ConfigDict, Field


_ALPHANUM_REGEX = re.compile(r'^[a-zA-Z0-9_]+$')
_ALPHANUMWITHDASH_REGEX = re.compile(r'^[a-zA-Z0-9_-]+$')
_HOSTNAME_REGEX = re.compile(r'^[a-zA-Z0-9_\.-]+$')


def _validate_key_alphanumeric(key): return _ALPHANUM_REGEX.match(key) is not None
def _validate_key_alphanumwithdash(key): return _ALPHANUMWITHDASH_REGEX.match(key) is not None


class FrozenBaseModel(BaseModel):
    model_config = ConfigDict(frozen=True, validate_default=True, extra='forbid')


@dataclass(frozen=True)
class CrateStub:
    '''Holds data required to create Crate object instance'''
    id: str
    building: str
    rack: str
    height: int
    description: str
    csp_id: int


class CrateStubModel(FrozenBaseModel):
    building: str = Field(..., pattern=_ALPHANUM_REGEX, description="The building identifier")
    rack: str = Field(..., pattern=_ALPHANUM_REGEX, description="The rack identifier")
    height: int = Field(..., ge=0, description="The height of the crate in the rack")
    csp_id: int = Field(..., ge=0, description="The CSP identifier of the crate")
    description: Optional[str] = Field('', description="The description of the crate")


@dataclass(frozen=True)
class BoardStub:
    '''Holds data required to create Board object instance'''
    id: str
    hostname: str
    crate: str = None
    slot: int = None
    port: int = 3000


class BoardStubModel(FrozenBaseModel):
    hostname: str = Field(..., pattern=_HOSTNAME_REGEX, description="The hostname of the board")
    crate: str = Field(..., pattern=_ALPHANUM_REGEX, description="The crate identifier")
    slot: int = Field(..., ge=0, le=20, description="The logical slot number")
    port: Optional[int] = Field(3000, ge=0, description="The port that Herd is running on")


@dataclass(frozen=True)
class ExternalBoardStub:
    '''Holds data required to create ExternalBoard object instance'''
    id: str
    crate: str
    slot: int
    inputs: Dict[str, List[int]]
    outputs: Dict[str, List[int]]


class LinkIdModel(FrozenBaseModel):
    _LINK_ID_REGEX: Literal[Pattern] = re.compile(
        r'([a-zA-Z0-9_]+)\[((?:[0-9]+(?:-[0-9]+)?)(?:,(?:[0-9]+(?:-[0-9]+)?))*)\]([a-zA-Z0-9_]+)?')
    link_id: str = Field(..., pattern=_LINK_ID_REGEX, description="The link identifier")


class IndexListStringModel(FrozenBaseModel):
    _INDEX_LIST_STRING_REGEX: Literal[Pattern] = re.compile(r'^([0-9]+(?:-[0-9]+)?)(?:,([0-9]+(?:-[0-9]+)?))*$')
    index_list_string: str = Field(..., pattern=_INDEX_LIST_STRING_REGEX, description="The index list string")


class ExternalBoardStubModel(FrozenBaseModel):
    crate: str = Field(..., pattern=_ALPHANUM_REGEX, description="The crate identifier")
    slot: int = Field(..., strict=True, ge=0, description="The logical slot number")
    inputs: Optional[Dict[str, str]] = Field({}, description="The input channels of the external board")
    outputs: Optional[Dict[str, str]] = Field({}, description="The output channels of the external board")

    @model_validator(mode='before')
    @classmethod
    def check_inputs_or_outputs(cls, values):
        if isinstance(values, Mapping):
            if not values.get('inputs') and not values.get('outputs'):
                raise ValueError("At least one of 'inputs' or 'outputs' must be provided")
        return values

    @field_validator('inputs', 'outputs')
    @classmethod
    def check_input_output_regex(cls, v: Dict[str, str]):
        for key, value in v.items():
            if not _validate_key_alphanumeric(key):
                raise ValueError(f"Key '{key}' is not alphanumeric")
            IndexListStringModel(index_list_string=value)
        return v


@unique
class DAQCategories(Enum):
    RO = 'RO'

    def __str__(self):
        return self.value


@dataclass(frozen=True)
class DAQBoardStub:
    ''' Holds data required to create DAQBoard object instance.
        May be merged with stub for externally-controlled boards in the future.
        Used to describe readout links currently. '''
    id: str
    category: DAQCategories
    inputs: Dict[str, List[int]]
    outputs: Dict[str, List[int]]


class DAQBoardStubModel(FrozenBaseModel):
    inputs: Optional[Dict[str, str]] = Field({}, description="The input channels of the DAQ board")
    outputs: Optional[Dict[str, str]] = Field({}, description="The output channels of the DAQ board")

    @model_validator(mode='before')
    @classmethod
    def check_inputs_or_outputs(cls, values):
        if isinstance(values, Mapping):
            if not values.get('inputs') and not values.get('outputs'):
                raise ValueError("At least one of 'inputs' or 'outputs' must be provided")
        return values

    @field_validator('inputs', 'outputs')
    @classmethod
    def check_input_output_regex(cls, v: Dict[str, str]):
        for key, value in v.items():
            if not _validate_key_alphanumeric(key):
                raise ValueError(f"Key '{key}' is not alphanumeric")
            IndexListStringModel(index_list_string=value)
        return v


@dataclass(frozen=True)
class FrontendStub:
    ''' Holds data required to create Frontend module instance.
        Currently assumes all frontend modules have multiple lpGBTs.
        Each lpGBT has one input, one output and a unique fuse ID.'''
    id: str
    serial: str
    lpgbt_fuseids: List[int]


class FrontendStubModel(FrozenBaseModel):
    serial: str = Field(..., description="The physical ID of the frontend module.")
    lpgbt_fuseids: str = Field(..., description="The unique fuse IDs of the lpGBTs in the module.")

    @field_validator('lpgbt_fuseids')
    @classmethod
    def check_fuseids_regex(cls, v: str):
        IndexListStringModel(index_list_string=v)
        return v


@dataclass(frozen=True)
class LinkStub:
    '''Holds data required to create Link object instance'''
    id: str
    src_board: str
    src_device: str
    src_channel: int
    dst_board: str
    dst_device: str
    dst_channel: int


class LinkStubModel(FrozenBaseModel):
    from_: conlist(str, min_length=1, max_length=3) = Field(..., alias='from', description="The source of the link")
    to: conlist(str, min_length=1, max_length=3) = Field(..., description="The destination of the link")

    @field_validator('from_', 'to', mode='before')
    @classmethod
    def check_pattern_format(cls, v):
        if not isinstance(v, list):
            raise ValueError("Link terminus must be a list")
        if not _validate_key_alphanumwithdash(v[0]):
            raise ValueError(f"Link terminus board ID field '{v[0]}' is not alphanumeric")
        if len(v) == 3:
            if not _validate_key_alphanumwithdash(v[0]) and not _validate_key_alphanumeric(v[1]):
                raise ValueError(f"Board and processor ID fields, '{v[0]}' and '{v[1]}', for link terminus are not"
                                 " alphanumeric")
            if not _validate_key_alphanumeric(v[1]):
                raise ValueError(f"Link terminus processor ID field '{v[1]}' is not alphanumeric")
            IndexListStringModel(index_list_string=v[2])
        if len(v) == 2:
            IndexListStringModel(index_list_string=v[1])
        return v

    @model_validator(mode='before')
    @classmethod
    def check_from_to_elements(cls, values):
        from_ = values.get('from')
        to = values.get('to')
        if isinstance(from_, list) and isinstance(to, list):
            if len(from_) != 3 and len(to) != 3:
                raise ValueError(f"In link terminus {from_[0]} and {to[0]} definitions: \
                                Either 'from' or 'to' must have 3 elements. Both source\
                                and destination cannot be frontend modules")
        return values


@dataclass(frozen=True)
class SystemStub:
    crates: List[CrateStub]
    boards: List[BoardStub]
    external_boards: List[ExternalBoardStub]
    links: List[LinkStub]
    # Excluded boards (in time, this may be factored off into different class which would also
    #   handle automatic exclusion based on included FEDs using rules, similar to LinkModeManager)
    exclusions: List[str]
    daq_boards: Optional[List[DAQBoardStub]] = field(default_factory=list)
    frontends: Optional[List[FrontendStub]] = field(default_factory=list)


class SystemStubModel(FrozenBaseModel):
    # TODO: Add regex pattern for IDs?
    boards: Dict[str, BoardStubModel] = Field(..., description="The system's boards")
    crates: Dict[str, CrateStubModel] = Field(..., description="The system's crates")
    # TODO: Add Dict key to link stub model?
    links: Dict[str, LinkStubModel] = Field(..., description="The system's boards")
    external_boards: Optional[Dict[str, ExternalBoardStubModel]] = Field(
        {}, description="The boards not controlled by Herd")
    daq_boards: Optional[Dict[str, DAQBoardStubModel]] = Field(
        {}, description="The DAQ boards reading out data from the trigger system")
    frontends: Optional[Dict[str, FrontendStubModel]] = Field(
        {}, description="The frontend modules connected to the L1 trigger system")

    # TODO: Extend this to other stub ids (boards, crates, external_boards)
    @field_validator('boards', 'daq_boards', 'frontends', mode='before')
    @classmethod
    def check_external_daq_boards_id(cls, v):
        if isinstance(v, Mapping):
            for key, _ in v.items():
                if _ALPHANUMWITHDASH_REGEX.match(key) is None:
                    raise ValueError(f"Key '{key}' is not alphanumeric including '-'")
        return v

    @model_validator(mode='after')
    @classmethod
    def check_links(cls, v):
        links = v.links
        fe_keys = v.frontends.keys()
        for link in links.values():
            for lterminus in [link.from_, link.to]:
                if len(lterminus) != 3:
                    if lterminus[0] not in fe_keys:
                        raise ValueError(f"Link terminus '{lterminus[0]}' is not a frontend module.\
                                         Needs mandatory (board, processor, port) format.")

                if lterminus[0] in fe_keys:
                    if len(lterminus) == 3:
                        raise ValueError(f"Link terminus '{lterminus[0]}' is a frontend module.\
                                         Must have only (module, channel) or (module) only format.")
        return v


class LinkTerminusModel(FrozenBaseModel):
    model_config = ConfigDict(extra="forbid")
    board: Optional[str] = Field('b0', description="The board regex")
    processor: Optional[str] = Field('pr0', description="The processor regex")
    port: Optional[int | str] = Field('po0', description="The I/O port index or port ID regex")


class LinkSettingsModel(FrozenBaseModel):
    model_config = ConfigDict(extra="forbid")
    default: Optional[Any] = Field(None)
    id: Optional[str] = Field('')
    from_: Optional[LinkTerminusModel] = Field({}, alias='from')
    to: Optional[LinkTerminusModel] = Field({})
    mode: Optional[str | List] = Field('')
    mask: Optional[bool] = Field(False)


class LinkSettingsListModel(FrozenBaseModel):
    link_settings: List[LinkSettingsModel] = Field(...)

    @field_validator('link_settings', mode='before')
    @classmethod
    def check_link_settings(cls, v):
        for i, link in enumerate(v):
            if not isinstance(link, dict):
                raise ValueError(f"Error in {v}. Link settings must be a list of dictionary")
            if 'default' in link.keys():
                if i != 0:
                    raise ValueError("Default link settings must be first in list")
                if 'id' in link.keys() or 'from' in link.keys() or 'to' in link.keys():
                    raise ValueError("Default link settings must not contain 'id', 'from' or 'to'")

            if 'mode' not in link.keys() and 'mask' not in link.keys():
                raise ValueError("Link settings must contain 'mode' or 'mask'")
        return v


class ConfigModel(FrozenBaseModel):
    system: SystemStubModel = Field(..., description="Description of the physical system"
                                                     " (i.e. lists of boards, crates and links)")
    link_settings: List[Dict] = Field(..., description="Settings for the system's links")
    contexts: Dict = Field(..., description="Parameter values for the Herd plugins' commands")
    exclude: Optional[List[str]] = Field([], description="A list of excluded boards and/or processors")
    variables: Optional[Dict[str, Any]] = Field({}, description="The variables to replace placeholders in 'contexts'")

    @field_validator('link_settings', mode='before')
    @classmethod
    def check_link_settings(cls, v):
        LinkSettingsListModel(link_settings=v)
        return v

    @field_validator('exclude', mode='before')
    @classmethod
    def check_exclude(cls, v):
        for key in v:
            exclude_pattern_regex = re.compile(r'^[a-zA-Z0-9_]+(?:\.[a-zA-Z0-9_]+)?$')
            if not exclude_pattern_regex.match(key):
                raise ValueError(f"Excluded '{key}' is not of format 'board' or 'board.processor'")
        return v
