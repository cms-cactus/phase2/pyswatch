
import functools
import json
import time

from datetime import datetime
from os import getpid
from threading import current_thread
from typing import Callable, List

# Several sections of code heavily inspired by (or directly copied from) https://github.com/JonathanHiggs/trace_events


__EXTERNAL_PROCESS_ID_MAP = {}
__EXTERNAL_PROCESS_THREAD_MAP = {}
__EXTERNAL_THREAD_ID_MAP = {}
__NEXT_ARTIFICIAL_THREAD_ID = 1
__PROCESS_STACKS = {}
__TRACE_EVENTS_DATA = []


def perf_time():
    """ returns the perf_counter in microseconds """
    return time.perf_counter_ns() * 1e-3


def register_external_process(process_name: str, thread_names: List[str]) -> None:
    if process_name in __EXTERNAL_PROCESS_THREAD_MAP:
        original_thread_names = __EXTERNAL_PROCESS_THREAD_MAP[process_name]
        if sorted(thread_names) != sorted(original_thread_names):
            raise RuntimeError(f'External process "{process_name}" has already been registered, but with different '
                               f'threads - {",".join(original_thread_names)} rather than '
                               f'{",".join(thread_names)}')

    if len(thread_names) == 0:
        raise RuntimeError(f'No threads declared for process "{process_name}"')

    __EXTERNAL_PROCESS_THREAD_MAP[process_name] = thread_names

    global __NEXT_ARTIFICIAL_THREAD_ID  # pylint: disable=global-statement
    proc_id = __NEXT_ARTIFICIAL_THREAD_ID
    __EXTERNAL_PROCESS_ID_MAP[process_name] = proc_id

    for thread_name in thread_names:
        __EXTERNAL_THREAD_ID_MAP[thread_name] = (proc_id, __NEXT_ARTIFICIAL_THREAD_ID)
        __NEXT_ARTIFICIAL_THREAD_ID += 1


def get_ids_for_external_thread(thread_name: str) -> tuple[int, int]:
    if thread_name not in __EXTERNAL_THREAD_ID_MAP:
        raise RuntimeError(f'External thread "{thread_name}" has not been registered')

    return __EXTERNAL_THREAD_ID_MAP[thread_name]


class EventTimer:
    def __init__(self, name: str):
        self._event_times = []
        self._name = name
        self._category = 'shep'

    def __enter__(self):
        self._event_times.append(perf_time())
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        last_start = self._event_times[-1]
        self._event_times[-1] = (last_start, perf_time() - last_start)

        for i, (start_time, duration) in enumerate(self._event_times, start=1):
            name = self._name if len(self._event_times) == 1 else f'{self._name} (pt{i})'
            _add_complete_event(name, self._category, start_time, duration, pid=getpid(), tid=current_thread().ident)

    def add_pause(self, start_time, end_time):
        last_start = self._event_times[-1]
        self._event_times[-1] = (last_start, start_time - last_start)
        self._event_times.append(end_time)

    def pause(self):
        return EventTimerPause(self)


class EventTimerPause:
    def __init__(self, event_timer: EventTimer):
        self._timer = event_timer

    def __enter__(self):
        self._start_time = perf_time()

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._timer.add_pause(self._start_time, perf_time())


def time_function(name: str | Callable[..., str]):

    def decorator(func):
        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            event_name = name(*args, **kwargs) if callable(name) else name
            with EventTimer(event_name):
                return func(*args, **kwargs)

        return wrapper

    return decorator


def begin_duration_event(name, *, process_name=None):
    __TRACE_EVENTS_DATA.append({
        'name': name,
        'cat': 'shep',
        'ph': 'B',
        'ts': perf_time(),
        'pid': getpid(),
        'tid': current_thread().ident
    })

    if process_name not in __PROCESS_STACKS:
        __PROCESS_STACKS[process_name] = []
    __PROCESS_STACKS[process_name].append(name)


def end_current_duration_event(process_name=None):
    __TRACE_EVENTS_DATA.append({
        'name': __PROCESS_STACKS[process_name].pop(),
        'cat': 'shep',
        'ph': 'E',
        'ts': perf_time(),
        'pid': getpid(),
        'tid': current_thread().ident
    })


def _add_complete_event(name: str, category: str, start_time: float, duration: float, *, pid: int, tid: int):
    __TRACE_EVENTS_DATA.append({
        'name': name,
        'cat': category,
        'ph': 'X',
        'ts': start_time,
        'dur': duration,
        'pid': pid,
        'tid': tid
    })


def add_external_event(name: str, start_time: datetime, duration: float, *, thread_name: str):
    proc_id, thread_id = get_ids_for_external_thread(thread_name)

    offset = perf_time() - time.time_ns() * 1e-3
    _add_complete_event(name,
                        'herd',
                        1e6 * start_time.timestamp() + offset,
                        int(1e6 * duration),
                        pid=proc_id,
                        tid=thread_id)


def save(file_path):
    data = {
        'traceEvents': __TRACE_EVENTS_DATA,
    }

    for proc_name, proc_id in __EXTERNAL_PROCESS_ID_MAP.items():
        data['traceEvents'].append({
            'name': 'process_name',
            'ph': 'M',
            'pid': proc_id,
            'tid': proc_id,
            'args': {'name': proc_name}
        })

    for thread_name, (proc_id, thread_id) in __EXTERNAL_THREAD_ID_MAP.items():
        data['traceEvents'].append({
            'name': 'thread_name',
            'ph': 'M',
            'pid': proc_id,
            'tid': thread_id,
            'args': {'name': thread_name}
        })

    with open(file_path, 'w', encoding='utf-8') as f:
        print(f'Saving trace event data to {file_path}')
        json.dump(data, f, indent=2)
