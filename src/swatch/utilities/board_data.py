
import gzip
import numpy
import random

from enum import Enum, unique
from functools import singledispatchmethod
from typing import Mapping
from urllib.parse import urlparse


#############################################################################
# SECTION 1: Board data

# class Frame:
#     def __init__(self, data=0, *, valid=False, strobe=True, start_of_orbit=False, start=False, end=False):
#         self._data = data
#         self._valid = valid
#         self._strobe = strobe
#         self._start_of_orbit = start_of_orbit
#         self._start_of_packet = start
#         self._end_of_packet = end

#     def __eq__(self, other):
#         result = self._valid == other._valid
#         if self._valid and self._strobe:  # Only include data in comparison if word is 'valid'
#             result = result and (self._data == other._data)
#         result = result and (self._strobe == other._strobe)
#         if self._valid:
#             result = result and (self._start_of_orbit == other._start_of_orbit)
#             result = result and (self._start_of_packet == other._start_of_packet)
#             result = result and (self._end_of_packet == other._end_of_packet)
#         return result


@unique
class Field(Enum):
    '''Represents metadata fields'''
    StartOfOrbit = 'StartOfOrbit'
    StartOfPacket = 'StartOfPacket'
    EndOfPacket = 'EndOfPacket'
    Valid = 'Valid'
    Strobe = 'Strobe'


def _all_fields():
    return {Field.StartOfOrbit, Field.StartOfPacket, Field.EndOfPacket, Field.Valid, Field.Strobe}


class ChannelData:
    _ARRAY_DTYPE = '?, ?, ?, ?, ?, u8'
    _RECORD_ARRAY_DTYPE = [
        ('strobe', '?'),
        ('valid', '?'),
        ('start_of_orbit', '?'),
        ('start_of_packet', '?'),
        ('end_of_packet', '?'),
        ('payload', 'u8')
    ]

    @singledispatchmethod
    def __init__(self, arg, metadata=None, copy=True):
        raise ValueError(f'ChannelData: Unsupported __init__ argument type, {type(arg)}')

    @__init__.register(numpy.recarray)
    def __init(self, data, metadata=None, copy=True):  # pylint: disable=unused-private-member
        if copy:
            self._data = numpy.copy(data)
        else:
            self._data = data

        self._metadata = set(_all_fields() if metadata is None else metadata)

        self.strobe = self._data.strobe
        self.valid = self._data.valid
        self.start_of_orbit = self._data.start_of_orbit
        self.start_of_packet = self._data.start_of_packet
        self.end_of_packet = self._data.end_of_packet
        self.payload = self._data.payload

    @__init__.register(int)
    # pylint: disable-next=unused-argument,unused-private-member
    def __init(self, depth, metadata=None, copy=True):  # noqa: F811
        self._data = numpy.zeros(depth, dtype=ChannelData._ARRAY_DTYPE)
        self._data = numpy.rec.array(self._data, dtype=ChannelData._RECORD_ARRAY_DTYPE)

        self._metadata = set(_all_fields() if metadata is None else metadata)

        self.strobe = self._data.strobe
        self.valid = self._data.valid
        self.start_of_orbit = self._data.start_of_orbit
        self.start_of_packet = self._data.start_of_packet
        self.end_of_packet = self._data.end_of_packet
        self.payload = self._data.payload

    def __len__(self):
        return self._data.__len__()

    def __getitem__(self, key):
        return self._data.__getitem__(key)

    def __iter__(self):
        return self._data.__iter__()

    def has_field(self, field):
        return field in self._metadata

    def has_fields(self, fields):
        for field in fields:
            if field not in self._metadata:
                return False
        return True

    def add_packet_structure(self, length, periodicity, *, start):
        if len(self._metadata) > 0:
            raise RuntimeError('Cannot add board data packet structure since the following fields are already defined:'
                               ', '.join(str(x) for x in self._metadata))

        if length > periodicity:
            raise RuntimeError(f'Invalid packet structure - packet length ({length}) cannot be larger than '
                               f' periodicity ({periodicity})')

        self._metadata = _all_fields()

        for i in range(len(self._data)):
            self._data[i].strobe = True
            self._data[i].start_of_orbit = (i == start)
            self._data[i].start_of_packet = (((i - start) % periodicity) == 0)
            self._data[i].valid = (((i - start) % periodicity) < length)
            self._data[i].end_of_packet = (((i - start) % periodicity) == length - 1)

    def strip_packet_structure(self):
        self._metadata = set()

    def truncate(self, depth):
        if depth < len(self._data):
            self._data = self._data[0:depth]


class BoardData:
    # TODO: Store data in 2D table.

    def __init__(self, name, data):
        self._name = name
        self._data = data
        for i, channel_data in self._data.items():
            if i < 0:
                raise RuntimeError('Channel index cannot be a negative number ({i} used)')

            if type(channel_data) is not ChannelData:
                raise ValueError('BoardData constructor: data argument must be a map[int, ChannelData] instance')

    @property
    def name(self):
        return self._name

    @property
    def channels(self):
        return sorted(self._data.keys())

    def at(self, i):
        return self._data[i]

    def add(self, i, data):
        if type(data) is not ChannelData:
            raise RuntimeError('BoardData.add: data argument must be a ChannelData instance')
        self._data[i] = data

    def remove(self, i):
        del self._data[i]

    def has_field(self, field):
        return all(channel_data.has_field(field) for channel_data in self._data.values())

    def has_fields(self, fields):
        return all(channel_data.has_fields(fields) for channel_data in self._data.values())

    def add_packet_structure(self, length, periodicity, *, start):
        for channel_data in self._data.values():
            channel_data.add_packet_structure(length=length,
                                              periodicity=periodicity,
                                              start=start)

    def strip_packet_structure(self):
        for channel_data in self._data.values():
            channel_data.strip_packet_structure()


@unique
class FileFormat(Enum):
    APx = 'APx'
    EMPv1 = 'EMPv1'
    EMPv2 = 'EMPv2'
    X2O = 'X2O'


_REQUIRED_FIELDS = {
    FileFormat.APx: [],
    FileFormat.EMPv1: [Field.Valid, Field.Strobe],
    FileFormat.EMPv2: [Field.StartOfOrbit, Field.StartOfPacket, Field.EndOfPacket, Field.Valid, Field.Strobe]
    # FileFormat.X2O
}


def generate_random():
    data = random.getrandbits(64)
    hexdata = "0x{:016x}".format(data)
    return hexdata


def generate_pattern(depth):
    hexdata = "0x{:016x}".format(depth)
    return hexdata


def generate_data(netloc, nchannels, ndepth, packet_size, gap_size):

    links = []
    data_by_row = []
    # actual data
    frame_num = 0
    for chan in range(nchannels):
        links.append(int(chan))
    for dep in range(0, ndepth):
        if ((dep % (packet_size+gap_size) == 0) and dep != 0):
            frame_num = 0
        valid = 0
        start_of_orbit = 0
        start_of_packet = 0
        end_of_packet = 0
        if (dep == 0):
            start_of_orbit = 1
        if (frame_num % packet_size == 0):
            start_of_packet = 1
        if ((frame_num+1) % packet_size == 0):
            end_of_packet = 1
        if (frame_num < packet_size):
            valid = 1
        if (valid == 0):
            start_of_packet = 0
            start_of_orbit = 0
            end_of_packet = 0
        row = []
        for chan in range(nchannels):
            hexdata = 0
            if netloc.__eq__("random"):
                hexdata = generate_random()
            if netloc.__eq__("pattern"):
                hexdata = generate_pattern(dep)
            if valid == 0:
                hexdata = "0x{:016x}".format(0)
            start_of_orbit + start_of_packet + end_of_packet + hexdata  # pylint: disable=pointless-statement
            # row.append(Frame(data=hexdata, valid=valid, strobe=True,
            #                  start_of_orbit=start_of_orbit, start=start_of_packet,
            #                  end=end_of_packet))
        data_by_row.append(row)
        frame_num = frame_num + 1

    data_map = {channel: data for channel, data in zip(links, zip(*data_by_row))}
    board_data = BoardData(name=netloc, data=data_map)
    return board_data


def generate_board_data_file(version, pattern, file_name, nchannels, depth):
    url = urlparse(pattern)
    tokens = url.netloc.split(":")
    packet_size = 128
    gap_size = packet_size
    if len(tokens) > 1:
        packet_size = int(tokens[1])
        gap_size = int(packet_size)
    if len(tokens) > 2:
        packet_size = int(tokens[1])
        gap_size = int(tokens[2])
    if (tokens[0] != ("random")) and (tokens[0] != ("pattern")):
        raise RuntimeError('For auto-generation of input data \
        file, the type has to be either generate://random or \
        generate://pattern OR even generate://random:M:N OR generate://pattern:M:N')
    board_data = generate_data(tokens[0], nchannels, depth, packet_size, gap_size)
    if version.__eq__("v1"):
        _write_emp_v1_file(file_name, board_data)
    if version.__eq__("v2"):
        _write_emp_v2_file(file_name, board_data)


def _read_apx_file(file_path):
    line_num = 1
    with open(file_path, 'r', encoding='utf-8') as f:
        # 1) Extract Metadata
        sideband = None
        meta_lines = []
        while True:
            line = f.readline()
            if line[0] == '#':
                meta_lines.append(line.rstrip())
                if line.strip() == "#BeginData":
                    break
            else:
                break
            line_num += 1

        if len(meta_lines) != 3:
            raise RuntimeError(f'Unexpected number of metadata lines. Expected 3, found {len(meta_lines)}')

        # 2) Extract file ID
        file_id = ''

        # 3) Extract column headings
        links = []
        line = meta_lines[-2].strip()
        tokens = line.split()
        if len(tokens) < 2:
            raise RuntimeError(f'Invalid format on line 2 (searching for columns heading): "{line}"')
        if tokens[0] == "#WordCnt":
            for j, x in enumerate(tokens[1:], start=1):
                if not x[5:].isdigit():
                    raise RuntimeError(f'Invalid format on line 2: Heading, "{x}", for column {j} '
                                       'does not have a corrent link number format')
                links.append(int(x[5:]))
                if links[-1] < 0:
                    raise RuntimeError(f'Invalid format on line 2: Heading for column {j} '
                                       f'is a negative number ({links[-1]})')
        else:
            raise RuntimeError('Invalid format on line 2: Expected link IDs, but start of line is '
                               'incorrect')

        # 4) Check that metadata line is present
        line = meta_lines[-1].strip()
        if line == "#BeginData":
            pass
        else:
            raise RuntimeError('Expected "#BeginData" metadata line on line 3')

        # 5) Extract data
        position_of_first_data_line = f.tell()
        num_frames = sum(1 for _ in f)
        f.seek(position_of_first_data_line)

        frame_index = 0
        for line_num, line in enumerate(f, start=line_num + 1):
            line = line.strip()
            tokens = line.split("   ")
            tokens = [token.strip() for token in tokens]
            if len(tokens) < 2:
                raise RuntimeError(f'Invalid format on line {line_num} - expected data row')
            tokens[0] = tokens[0].strip()
            expected_prefix = f'{frame_index:#06x}'
            if tokens[0].lower() != expected_prefix:
                raise RuntimeError(f'Invalid format on line {line_num} - expected prefix "{expected_prefix}", '
                                   f'but found "{tokens[0]}"')
            if (len(tokens) - 1) != len(links):
                raise RuntimeError(f'Invalid format on line {line_num} - expected "{len(links)}" data words, but found '
                                   f'"{len(tokens) - 1}"')

            for j, x in enumerate(tokens[1:]):
                frame_tokens = x.split(" ")
                if (len(frame_tokens[-1]) != 18):
                    raise RuntimeError(f'Invalid format on line {line_num} - data word, "{x}", in column {j} '
                                       '(incorrect number of data bits)')
                # Auto-detect presence of sideband info based on first frame on first link
                if sideband is None:
                    sideband = len(frame_tokens) > 1
                    fields = _all_fields() if sideband else {}
                    data_columns = [ChannelData(num_frames, metadata=fields) for _ in range(len(links))]

                if sideband:
                    if len(frame_tokens) < 2:
                        raise RuntimeError(f'Invalid format on line {line_num} - data word, "{x}", in column {j} '
                                           '(too few spaces)')
                    if len(frame_tokens) > 2:
                        raise RuntimeError(f'Invalid format on line {line_num} - data word, "{x}", in column {j} '
                                           '(too many spaces)')
                    if (len(frame_tokens[0]) != 4):
                        raise RuntimeError(f'Invalid format on line {line_num} - data word, "{x}", in column {j} '
                                           '(incorrect number of metadata bits)')

                    strobe = True
                    start_of_orbit = ((int(frame_tokens[0], 16) >> 0) & 1 == 1)
                    start_of_packet = ((int(frame_tokens[0], 16) >> 1) & 1 == 1)
                    end_of_packet = ((int(frame_tokens[0], 16) >> 2) & 1 == 1)
                    valid = ((int(frame_tokens[0], 16) >> 3) & 1 == 1)
                    data = int(frame_tokens[1], base=16)

                    data_columns[j]._data[frame_index] = (  # pylint: disable=possibly-used-before-assignment
                        strobe, valid, start_of_orbit, start_of_packet, end_of_packet, data)

                else:
                    if len(frame_tokens) > 1:
                        raise RuntimeError(f'Invalid format on line {line_num} - data word, "{x}", in column {j} '
                                           '(too many spaces for file without sideband)')

                    data = int(frame_tokens[0], base=16)
                    data_columns[j]._data[frame_index] = (False, False, False, False, False, data)

            frame_index += 1

    for x in data_columns:
        x.truncate(frame_index)
    return BoardData(file_id, {chan: data_columns[col] for col, chan in enumerate(links)})


def _read_emp_v1_file(file_path):
    line_num = 1
    links = []

    open_func = gzip.open if file_path.endswith('.gz') else open
    with open_func(file_path, 'rt') as f:
        # 1) Extract file ID
        while True:
            line = f.readline()
            if line[0] == '#':
                continue
            if line.startswith('Board '):
                file_id = line[6:].strip()
                break
            else:
                raise RuntimeError(f'Unexpected contents found in line {line_num} '
                                   f'when searching for board ID: "{line}"')
            line_num += 1

        # 2) Extract column headings
        links = []
        while True:
            line_num += 1
            line = f.readline().strip()
            tokens = line.split(":")
            if len(tokens) != 2:
                raise RuntimeError(f'Invalid format on line {line_num} (searching for column headings): "{line}"')

            if tokens[0].strip() == "Quad/Chan":
                continue
            if tokens[0].strip() == "Link":
                headings = tokens[1].split()
                for j, x in enumerate(headings, start=1):
                    if not x.isdigit():
                        raise RuntimeError(f'Invalid format on line {line_num}: Heading, "{x}", for column {j} '
                                           'is not a number')
                    links.append(int(x))
                    if links[-1] < 0:
                        raise RuntimeError(f'Invalid format on line {line_num}: Heading for column {j} '
                                           f'is a negative number ({links[-1]})')
                break
            else:
                raise RuntimeError(f'Invalid format on line {line_num}: Expected link IDs, but start of line is '
                                   'incorrect.')

        # 3) Extract data
        position_of_first_data_line = f.tell()
        num_frames = sum(1 for _ in f)
        f.seek(position_of_first_data_line)

        data_columns = [ChannelData(num_frames, metadata=[Field.Valid, Field.Strobe]) for _ in range(len(links))]
        frame_index = 0
        for line_num, line in enumerate(f, start=line_num + 1):
            tokens = line.split(":")
            if len(tokens) != 2:
                raise RuntimeError(f'Invalid format on line {line_num} - expected one semicolon, '
                                   f'found {len(tokens) - 1}')

            tokens[0] = tokens[0].strip()
            expected_prefix = f'Frame {frame_index:04}'
            if tokens[0] != expected_prefix:
                raise RuntimeError(f'Invalid format on line {line_num} - expected prefix "{expected_prefix}", '
                                   f'but found "{tokens[0]}"')

            tokens = tokens[1].split()
            if len(tokens) != len(links):
                raise RuntimeError(f'Invalid format on line {line_num} - expected "{len(links)}" data words, '
                                   f'but found "{len(tokens)}"')

            for j, x in enumerate(tokens):
                if x.startswith(('Us', 'Xs', '0s', '1s')):
                    if len(x) != 20:
                        raise RuntimeError(f'Invalid format on line {line_num} - data word, "{x}", in column {j} '
                                           '(incorrect length)')
                    try:
                        data_columns[j]._data[frame_index] = (
                            x[0] == '1', x[2] == '1', False, False, False, int(x[4:], base=16))
                    except ValueError:
                        raise RuntimeError(f'Invalid format on line {line_num} - data word, "{x}", in column {j}')
                elif x.startswith(('Uv', 'Xv', '0v', '1v')):
                    if len(x) != 18:
                        raise RuntimeError(f'Invalid format on line {line_num} - data word, "{x}", in column {j} '
                                           '(incorrect length)')
                    try:
                        data_columns[j]._data[frame_index] = (
                            True, x[0] == '1', False, False, False, int(x[2:], base=16))
                    except ValueError:
                        raise RuntimeError(f'Invalid format on line {line_num} - data word, "{x}", in column {j}')
                else:
                    if len(x) != 16:
                        raise RuntimeError(f'Invalid format on line {line_num} - data word, "{x}", in column {j} '
                                           '(incorrect length)')
                    try:
                        data_columns[j]._data[frame_index] = (True, True, False, False, False, int(x[2:], base=16))
                    except ValueError:
                        raise RuntimeError(f'Invalid format on line {line_num} - data word, "{x}", in column {j}')

            frame_index += 1

    for x in data_columns:
        x.truncate(frame_index)
    return BoardData(file_id, {chan: data_columns[col] for col, chan in enumerate(links)})


def _read_emp_v2_file(file_path):
    line_num = 1
    links = []

    open_func = gzip.open if file_path.endswith('.gz') else open
    with open_func(file_path, mode='rt') as f:
        # 1) Extract file ID
        while True:
            line = f.readline()
            if line[0] == '#':
                continue
            if line.startswith('ID: '):
                file_id = line[4:].strip()
                break
            else:
                raise RuntimeError(f'Unexpected contents found in line {line_num} '
                                   f'when searching for board ID: "{line}"')
            line_num += 1

        # 2) Check that metadata line is is present
        line_num += 1
        line = f.readline()
        if not line.startswith("Metadata: (strobe,) start of orbit, start of packet, end of packet, valid"):
            raise RuntimeError(f'Expected metadata encoding spec ("Metadata: ...") not found on line {line_num}')

        line_num += 1
        line = f.readline()
        line = line.rstrip()
        if line != "":
            raise RuntimeError(f'Expected empty line on line {line_num} (with column headings on line after)')

        # 3) Extract column headings
        links = []
        while True:
            line_num += 1
            line = f.readline().strip()
            tokens = line.split()
            if len(tokens) <= 1:
                raise RuntimeError(f'Invalid format on line {line_num} (searching for column headings): "{line}"')

            if tokens[0] == "Link":
                for j, x in enumerate(tokens[1:], start=1):
                    if not x.isdigit():
                        raise RuntimeError(f'Invalid format on line {line_num}: Heading, "{x}", for column {j} '
                                           'is not a number')
                    links.append(int(x))
                    if links[-1] < 0:
                        raise RuntimeError(f'Invalid format on line {line_num}: Heading for column {j} '
                                           f'is a negative number ({links[-1]})')
                break
            else:
                raise RuntimeError(f'Invalid format on line {line_num}: Expected link IDs, but start of line is '
                                   'incorrect.')

        # 4) Extract data
        position_of_first_data_line = f.tell()
        num_frames = sum(1 for _ in f)
        f.seek(position_of_first_data_line)

        data_columns = [ChannelData(num_frames) for _ in range(len(links))]
        frame_index = 0
        for line_num, line in enumerate(f, start=line_num + 1):
            line = line.strip()  # Remove newline chars
            tokens = line.split("  ")
            if len(tokens) <= 2:
                raise RuntimeError(f'Invalid format on line {line_num} - expected data row')

            tokens[0] = tokens[0].strip()
            expected_prefix = f'Frame {frame_index:04}'
            if tokens[0] != expected_prefix:
                raise RuntimeError(f'Invalid format on line {line_num} - expected prefix "{expected_prefix}", '
                                   f'but found "{tokens[0]}"')

            if (len(tokens) - 2) != len(links):
                raise RuntimeError(f'Invalid format on line {line_num} - expected "{len(links)}" data words, but found '
                                   f'"{len(tokens) - 2}"')

            for j, token in enumerate(tokens[2:], start=0):
                token = token.strip()

                if (len(token) != 21) and (len(token) != 22):
                    raise RuntimeError(f'Invalid format on line {line_num} - data word, "{token}", in column {j} '
                                       '(incorrect length)')
                if token[-17] != ' ':
                    raise RuntimeError(f'Invalid format on line {line_num} - data word, "{token}", in column {j} '
                                       '(char 4 should be a space)')

                strobe = True if len(token) == 21 else (token[0] == '1')
                start_of_orbit = (token[-21] == '1')
                start_of_packet = (token[-20] == '1')
                end_of_packet = (token[-19] == '1')
                valid = (token[-18] == '1')
                data = int(token[-16:], base=16)

                data_columns[j]._data[frame_index] = (
                    strobe, valid, start_of_orbit, start_of_packet, end_of_packet, data)

            frame_index += 1

    for x in data_columns:
        x.truncate(frame_index)
    return BoardData(file_id, {chan: data_columns[col] for col, chan in enumerate(links)})


def _read_x2o_file(file_path):
    raise RuntimeError("X2O file parsing not yet implemented.")


def _write_apx_file(board_data, file_path):
    # Sideband OFF if not all fields defined
    sideband = board_data.has_fields(_all_fields())

    with open(file_path, 'w', encoding='utf-8') as f:
        # 1) Headers
        f.write('#\n')
        f.write('#WordCnt  ')
        channels = sorted(board_data.channels)
        n_spaces = 16 if sideband else 11
        f.write('    '.join([n_spaces * ' ' + f'LINK_{x:02n}' for x in channels]) + '\n')
        f.write('#BeginData\n')

        # 2) Data
        for i in range(len(board_data.at(channels[0]))):
            line = f'0x{i:04X}'
            filler = '   '
            for c in channels:
                frame = board_data.at(c)[i]
                if sideband:
                    sideband_value = int(frame.start_of_orbit)
                    sideband_value = sideband_value | (int(frame.start_of_packet) << 1)
                    sideband_value = sideband_value | (int(frame.end_of_packet) << 2)
                    sideband_value = sideband_value | (int(frame.valid) << 3)
                    line += filler + f'0x{sideband_value:02X} 0x{frame.payload:016X}'
                else:
                    line += filler + f'0x{frame.payload:016X}'

                filler = '    '

            f.write(line + '\n')


def _write_emp_v1_file(board_data, file_path):
    strobed = {}
    for i in board_data.channels:
        strobed[i] = False
        for x in board_data.at(i):
            if not x.strobe:
                strobed[i] = True
                break

    with open(file_path, 'w', encoding='utf-8') as f:
        # 1) Headers
        f.write(f'Board {board_data.name}\n')

        channels = sorted(board_data.channels)
        f.write(' Quad/Chan :')
        for c in channels:
            f.write((8 + int(strobed[c])) * ' ' + f'q{c // 4:02n}c{c % 4}' + (6 + int(strobed[c])) * ' ')
        f.write('\n      Link :')
        for c in channels:
            f.write((9 + int(strobed[c])) * ' ' + f'{c:03n}' + (7 + int(strobed[c])) * ' ')
        f.write('\n')

        # 2) Data
        for i in range(len(board_data.at(channels[0]))):
            line = f'Frame {i:04n} :'
            for c in channels:
                frame = board_data.at(c)[i]
                line += ' '
                if strobed[c]:
                    line += f'{int(frame.strobe)}s'
                line += f'{int(frame.valid)}v{frame.payload:016x}'

            f.write(line + '\n')


def _write_emp_v2_file(board_data, file_path):
    strobed = [False for i in range(max(board_data.channels) + 1)]
    for i in board_data.channels:
        for x in board_data.at(i):
            if not x.strobe:
                strobed[i] = True
                break

    with open(file_path, 'w', encoding='utf-8') as f:
        # 1) Headers
        f.write(f'ID: {board_data.name}\n')
        f.write('Metadata: (strobe,) start of orbit, start of packet, end of packet, valid\n')
        f.write('\n')

        channels = sorted(board_data.channels)
        f.write('      Link  ')
        for c in channels:
            f.write((12 + int(strobed[c])) * ' ' + f'{c:03n}' + 8 * ' ')
        f.write('\n')

        # 2) Data
        for i in range(len(board_data.at(channels[0]))):
            line = f'Frame {i:04n}  '
            for c in channels:
                strobe, valid, start_of_orbit, start_of_packet, end_of_packet, payload = board_data.at(c)._data[i]

                line += '  '
                if strobed[c]:
                    line += '1' if strobe else '0'
                line += '1' if start_of_orbit else '0'
                line += '1' if start_of_packet else '0'
                line += '1' if end_of_packet else '0'
                line += '1 ' if valid else '0 '
                line += f'{payload:016x}'

            f.write(line + '\n')


def _write_x2o_file(board_data, file_path):
    raise RuntimeError("X20 file writing not yet implemented.")


_FILE_PARSERS = {
    FileFormat.APx: _read_apx_file,
    FileFormat.EMPv1: _read_emp_v1_file,
    FileFormat.EMPv2: _read_emp_v2_file,
    FileFormat.X2O: _read_x2o_file,
}


def read(file_path, file_format=None):
    # Automatically detect input file type if not specified
    if file_format is None:
        open_func = gzip.open if file_path.endswith('.gz') else open
        with open_func(file_path, mode='rt') as f:
            for line_num, line in enumerate(f, start=1):
                if line.startswith("#WordCnt"):
                    file_format = FileFormat.APx
                    break
                if line.strip().startswith("Metadata: (strobe,) start of orbit, start of packet, end of packet, valid"):
                    file_format = FileFormat.EMPv2
                    break
                if line.strip().startswith("Link :"):
                    file_format = FileFormat.EMPv1
                    break
                if line_num > 5:
                    raise RuntimeError(f'Could not automatically detect the format of buffer I/O file "{file_path}"')

    if file_format is None:
        raise RuntimeError(f'Could not auto-detect format of board data file {file_path}')
    if file_format not in _FILE_PARSERS:
        raise RuntimeError(f"Invalid format, '{file_format}'. Known values are: " +
                           ", ".join([str(x) for x in _FILE_PARSERS.keys()]))

    return _FILE_PARSERS[file_format](file_path)


_FILE_WRITERS = {
    FileFormat.APx: _write_apx_file,
    FileFormat.EMPv1: _write_emp_v1_file,
    FileFormat.EMPv2: _write_emp_v2_file,
    FileFormat.X2O: _write_x2o_file,
}


def write(board_data, file_path, file_format):
    missing_fields = []
    for x in _REQUIRED_FIELDS[file_format]:
        if not board_data.has_field(x):
            missing_fields.append(x)
    if len(missing_fields):
        raise RuntimeError(f'Board data object is missing the following mandatory fields for the {file_format.name} '
                           f'file format: {", ".join(str(x.name) for x in missing_fields)}')

    _FILE_WRITERS[file_format](board_data, file_path)


#############################################################################
# SECTION 2: Comparing board data

class ChannelDataMatchResult:

    _DIFF_DTYPE = [
        ('strobe', '?'),
        ('valid', '?'),
        ('start_of_orbit', '?'),
        ('start_of_packet', '?'),
        ('end_of_packet', '?'),
        ('payload', '?')
    ]

    def __init__(self, ref_data, obs_data, latency, *, n_frames=None):
        self.latency = latency

        n_frames_obs = len(obs_data) - latency
        n_frames_ref = len(ref_data)
        self.n_frames_compared = min(n_frames_obs, n_frames_ref)

        if n_frames is not None:
            if self.n_frames_compared < n_frames:
                raise RuntimeError(f'Could not compare {n_frames} frames of channel data '
                                   f'(len(ref)={len(ref_data)}, len(obs)={len(obs_data)}, '
                                   f'latency={latency})')
            else:
                self.n_frames_compared = n_frames

        x = ref_data[:self.n_frames_compared]
        y = obs_data[latency:self.n_frames_compared + latency]

        self.diff = numpy.rec.array(numpy.column_stack([
            x.strobe != y.strobe,
            x.valid != y.valid,
            x.start_of_orbit != y.start_of_orbit,
            x.start_of_packet != y.start_of_packet,
            x.end_of_packet != y.end_of_packet,
            x.payload != y.payload
        ]).ravel().view(ChannelDataMatchResult._DIFF_DTYPE))

        # If strobe or valid should be low, ignore differences in data word
        # data_mask = numpy.logical_or(numpy.logical_and(x.strobe, x.valid),
        #                              numpy.logical_and(y.strobe, y.valid))
        data_mask = numpy.logical_and(x.strobe, x.valid)
        self.diff.payload = numpy.logical_and(data_mask, self.diff.payload)
        self.data_mask = data_mask

        # Calculate statistics
        self.n_header_fields_differ = int(numpy.add.reduce(self.diff.strobe) +
                                          numpy.add.reduce(self.diff.valid) +
                                          numpy.add.reduce(self.diff.start_of_orbit) +
                                          numpy.add.reduce(self.diff.start_of_packet) +
                                          numpy.add.reduce(self.diff.end_of_packet))
        self.n_data_words_differ = int(numpy.add.reduce(self.diff.payload))
        self.n_fields_differ = self.n_header_fields_differ + self.n_data_words_differ

        self.n_header_fields_compared = 5 * self.n_frames_compared
        self.n_data_words_compared = int(numpy.add.reduce(data_mask))
        self.n_fields_compared = self.n_header_fields_compared + self.n_data_words_compared
        self.n_bits_compared = self.n_header_fields_compared + 64 * self.n_data_words_compared

        self.n_header_fields_equal = self.n_header_fields_compared - self.n_header_fields_differ
        self.n_data_words_equal = self.n_data_words_compared - self.n_data_words_differ
        self.n_fields_equal = self.n_fields_compared - self.n_fields_differ

    def __bool__(self):
        return self.n_fields_equal == self.n_fields_compared


class BoardDataMatchResult:
    def __init__(self, channel_results: Mapping[int, ChannelDataMatchResult]):
        self._channel_results = channel_results

    def __bool__(self):
        return all(self._channel_results.values())

    @property
    def channels(self):
        return list(self._channel_results)

    def at(self, i):
        return self._channel_results[i]

    @property
    def good_channels(self):
        return [c for c, channel_result in self._channel_results.items() if bool(channel_result)]

    @property
    def bad_channels(self):
        return [c for c, channel_result in self._channel_results.items() if not bool(channel_result)]

    @property
    def latencies(self):
        '''Returns map of latency to channel list'''
        result = {}
        for c, channel_result in self._channel_results.items():
            if channel_result.latency not in result:
                result[channel_result.latency] = [c]
            else:
                result[channel_result.latency].append(c)

        return result


# Returns map(channel index -> list of indices of mismatching frames)
def compare(ref_data, obs_data, *, latencies=None, max_frames=None, require_equal_channels=True):
    # TODO: By default, adjust latency in order to get best match, using valid/start/stop signals wherever possible
    #       (as exact latency sometimes not known) - but provide option to specify known latency (or latency range).

    # Masked / unconnected channels are not included in the expected data file
    # The total number of channels may not be equal in the above cases
    if (require_equal_channels):
        if sorted(ref_data.channels) != sorted(obs_data.channels):
            raise RuntimeError('Channel IDs in expected and observed data do not match '
                               f'({ref_data.channels} vs {obs_data.channels})')

    if latencies is None:
        latencies = range(min(len(ref_data.at(ref_data.channels[0])), len(obs_data.at(obs_data.channels[0]))) - 1)

    channel_results = {}
    for c in ref_data.channels:
        ref_chdata = ref_data.at(c)
        obs_chdata = obs_data.at(c)

        results_by_latency = []
        for latency in latencies:

            n_frames_obs = len(obs_chdata) - latency
            n_frames_ref = len(ref_chdata)
            if max_frames is not None:
                n_frames_obs = min(n_frames_obs, max_frames)
                n_frames_ref = min(n_frames_ref, max_frames)

            if (latency + n_frames_obs) < n_frames_ref:
                continue

            results_by_latency.append(
                ChannelDataMatchResult(ref_chdata, obs_chdata, latency, n_frames=min(n_frames_obs, n_frames_ref))
            )

        if len(results_by_latency) == 0:
            raise RuntimeError(f'Channel {c}: Fewer words in observed file than in reference '
                               f'for all each specified latency ({str(x) for x in latencies})')

        def match_results_score(x):
            return (  # Prioritise perfect matches of metadata (since drive input FSMs)
                # with at least some data words compared
                (x.n_header_fields_differ > 0) or (x.n_data_words_compared == 0),
                x.n_data_words_compared == 0,
                # Overall metric: % of fields that match in total
                x.n_fields_differ / x.n_fields_compared,
                - x.n_header_fields_equal,
                x.latency)
        results_by_latency.sort(key=match_results_score)

        channel_results[c] = results_by_latency[0]

    return BoardDataMatchResult(channel_results)
