
def format_index_list(channels):
    ranges = []

    for i in sorted(channels):
        if (len(ranges) == 0) or (ranges[-1][1] != (i - 1)):
            ranges.append((i, i))
        else:
            ranges[-1] = (ranges[-1][0], i)

    ranges = [str(i) if i == j else str(i) + '-' + str(j) for i, j in ranges]
    return ','.join(ranges)
