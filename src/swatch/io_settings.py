
from dataclasses import dataclass
from enum import Enum, unique


@unique
class PRBSMode(Enum):
    PRBS7 = 'PRBS7'
    PRBS9 = 'PRBS9'
    PRBS15 = 'PRBS15'
    PRBS23 = 'PRBS23'
    PRBS31 = 'PRBS31'

    def __str__(self):
        return self.name


@unique
class IdleMethod(Enum):
    Method1 = 'IdleMethod1'
    Method2 = 'IdleMethod2'

    def __str__(self):
        return self.value


@dataclass(frozen=True)  # TODO From Python 3.10: kw_only=True
class CSPSettings:
    packet_length: int
    packet_periodicity: int
    idle_method: IdleMethod

    def __repr__(self):
        return f'CSP({self.packet_length}, {self.packet_periodicity}, {self.idle_method})'
