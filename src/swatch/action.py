
import asyncio
import base64
import json
import logging
import os
import time

from dataclasses import dataclass
from datetime import datetime, timezone
from enum import Enum, unique
from typing import Dict, List

from .config import SettingsPool
from .monitoring import PropertiedMonitorableObject, parse_error_info
from .parameters import Constraint, File, Parameter, create_rule, serialize_parameter_set, serialize_parameter_set_list
from .utilities.tracing import add_external_event, EventTimer


_LOGGER = logging.getLogger('shep')
_RUNNER = asyncio.Runner()


class FileResult:
    # TODO: Merge with the above
    def __init__(self, name, contents, *, content_type, content_format):
        self._name = name
        self._type = content_type
        self._format = content_format
        self._contents = contents

    @property
    def name(self):
        return self._name

    @property
    def type(self):
        return self._type

    @property
    def format(self):
        return self._format

    def __str__(self):
        return f'FileResult({self.name}, type="{self.type}", format="{self.format}")'

    def save(self, path):
        if len(os.path.dirname(path)) > 0:
            os.makedirs(os.path.dirname(path), exist_ok=True)
        with open(path, 'w', encoding='utf-8') as f:
            f.write(self._contents)


def _serialize_result(x):
    if (type(x) is dict) and (sorted([k for k in x]) == ['contents', 'format', 'name', 'type']):
        decoded_contents = ''
        if x['contents'] is None:
            decoded_contents = None
        else:
            decoded_contents = base64.b64decode(x['contents']).decode('utf-8')
        return FileResult(x['name'],
                          decoded_contents,
                          content_format=x['format'],
                          content_type=x['type']
                          )
    else:
        return x


# TODO: keyword-only?
# @dataclass(frozen=true)
class CommandSnapshot:
    def __init__(self, path, state, parameters, start_time, duration, progress, messages, result, error_info):
        self.path = path
        self.state = state
        self.parameters = parameters
        self.start_time = start_time
        self.duration = duration
        self.progress = progress
        self.messages = messages
        self.result = result
        self.error_info = error_info

    @property
    def id(self):
        return self.path[self.path.rfind('.')+1:]


class Command:

    @unique
    class State(Enum):
        INITIAL = 'Initial'
        SCHEDULED = 'Scheduled'
        RUNNING = 'Running'
        ERROR = 'Error'
        WARNING = 'Warning'
        DONE = 'Done'

    @dataclass  # TODO From Python 3.10: kw_only=True
    class ParamTypeMismatch:
        '''Represents a parameter that has the incorrect type'''
        parameter: str
        expected_type: str
        actual_type: str

    @dataclass  # TODO From Python 3.10: kw_only=True
    class ParamRuleViolation:
        '''Represents a parameter that has the incorrect type'''
        parameter: str
        rule_description: str

    def __init__(self, cmd_id, device, **kwargs):
        self.id = cmd_id
        self.__actionable = device
        self.alias = kwargs['alias']
        self.description = kwargs['description']
        self.parameters = kwargs['parameters']
        self.constraints = kwargs['constraints']
        self.__contexts = kwargs['contexts']

    @staticmethod
    def parse_snapshot(snapshot, parameters):

        return CommandSnapshot(path=snapshot['path'],
                               state=Command.State(snapshot['state']),
                               parameters=parameters,
                               start_time=datetime.fromtimestamp(snapshot['startTime'], timezone.utc),
                               duration=snapshot['duration'],
                               progress=snapshot['progress'],
                               messages=snapshot['messages'],
                               result=_serialize_result(snapshot['result']),
                               error_info=parse_error_info(snapshot['errorDetails']))

    @property
    def actionable(self):
        return self.__actionable

    def parameter(self, param_id):
        c = next((x for x in self.parameters if x.id == param_id), None)
        if c is None:
            raise RuntimeError(f"Command '{self.id}' from '{self.__actionable.path}' does not have any parameter named "
                               f"'{param_id}'")
        return c

    def run(self, parameters, lease, *, poll_callback=None):
        with EventTimer(f'{self.actionable.path}: {self.id}'):
            _LOGGER.info('%s: Running %s (command)', self.actionable.path, self.id)

            if isinstance(parameters, SettingsPool):
                pool = parameters
                parameters = {}
                for param in self.parameters:
                    parameters[param.id] = pool.get_parameter(None, self.id, param.id, self.__contexts)

            # TODO: Verify that correct parameters specified (and that they're of correct type)

            req_data = {'lease': lease._token, 'parameters': serialize_parameter_set(parameters)}
            r = self.__actionable._sync_client.put(f'/commands/{self.__actionable.id}/{self.id}', json=req_data)
            if r.status_code != 200:
                raise RuntimeError(f"Control app reported an error when running command '{self.id}' " +
                                   f"(device '{self.__actionable.path}'): {r.text}")

            snapshot = Command.parse_snapshot(json.loads(r.text), parameters)
            while snapshot.state not in (Command.State.DONE, Command.State.WARNING, Command.State.ERROR):
                time.sleep(0.2)
                r = self.__actionable._sync_client.get(f'/commands/{self.__actionable.id}/{self.id}')
                if r.status_code != 200:
                    raise RuntimeError("Control app reported an error when requesting status of command " +
                                       f"'{self.id}' (device '{self.__actionable.path}'): {r.text}")

                snapshot = Command.parse_snapshot(json.loads(r.text), parameters)
                if poll_callback is not None:
                    poll_callback(snapshot)

        _LOGGER.info('%s: Completed %s with status %s', self.actionable.path, self.id, snapshot.state.name)
        add_external_event(f'Command: {self.id}', snapshot.start_time, snapshot.duration,
                           thread_name=self.actionable.path)
        return snapshot


class State:
    def __init__(self, actionable_path, fsm_id, state_id, transitions):
        self._actionable_path = actionable_path
        self._fsm_id = fsm_id
        self.id = state_id
        self.transitions = transitions

    def transition(self, transition_id):
        t = next((x for x in self.transitions if x.id == transition_id), None)
        if t is None:
            raise RuntimeError(f"'{self.id}' state in '{self._fsm_id}' FSM of device '{self._actionable_path}' "
                               f"does not have any transition named '{transition_id}'")
        return t


class TransitionSnapshot:
    def __init__(self, *, path, actionable_path, state, start_time, duration, progress, num_completed_commands,
                 total_num_commands, commands):
        self.path = path
        self.actionable_path = actionable_path
        self.state = state
        self.start_time = start_time
        self.duration = duration
        self.progress = progress
        self.num_completed_commands = num_completed_commands
        self.total_num_commands = total_num_commands
        self.commands = commands

    @property
    def id(self):
        return self.path[self.path.rfind('.')+1:]

    def is_finished(self):
        return self.state in (Transition.State.DONE, Transition.State.WARNING, Transition.State.ERROR)


class Transition:

    State = Command.State

    class Step:
        def __init__(self, namespace, command):
            self.namespace = namespace
            self.command = command

    def __init__(self, transition_id, alias, device, fsm_id, start_state, end_state, commands, contexts):
        self.id = transition_id
        self.alias = alias
        self.__actionable = device
        self.__fsm_id = fsm_id
        self.__start_state = start_state
        self.__end_state = end_state
        self.steps = commands
        self.__contexts = contexts

    @staticmethod
    def parse_snapshot(snapshot, actionable_path, parameters):

        actionable_id_length = snapshot['path'].find('.')
        commands = []
        for s, p in zip(snapshot['commands'], parameters):
            commands.append(CommandSnapshot(path=actionable_path + s['path'][actionable_id_length:],
                                            state=Command.State(s['state']),
                                            parameters=p,
                                            start_time=datetime.fromtimestamp(s['startTime'], timezone.utc),
                                            duration=s['duration'],
                                            progress=s['progress'],
                                            messages=s['messages'],
                                            result=_serialize_result(s['result']),
                                            error_info=parse_error_info(s['errorDetails'])))

        return TransitionSnapshot(path=actionable_path + snapshot['path'][actionable_id_length:],
                                  actionable_path=actionable_path,
                                  state=Transition.State(snapshot['state']),
                                  start_time=datetime.fromtimestamp(snapshot['startTime'], timezone.utc),
                                  duration=snapshot['duration'],
                                  progress=snapshot['progress'],
                                  num_completed_commands=snapshot['numCommandsDone'],
                                  total_num_commands=snapshot['numCommandsTotal'],
                                  commands=commands
                                  )

    @property
    def actionable(self):
        return self.__actionable

    @property
    def fsm(self):
        return self.actionable.fsm(self.__fsm_id)

    @property
    def start_state(self):
        return self.__start_state

    @property
    def end_state(self):
        return self.__end_state

    @dataclass  # TODO From Python 3.10: kw_only=True
    class MissingParam:
        '''Represents a parameter that is missing from the SettingsPool'''
        nspace: str
        command: str
        parameter: str

    @dataclass  # TODO From Python 3.10: kw_only=True
    class ParamTypeMismatch:
        '''Represents a parameter that has the incorrect type'''
        nspace: str
        command: str
        parameter: str
        expected_type: str
        actual_type: str

    @dataclass  # TODO From Python 3.10: kw_only=True
    class ParamRuleViolation:
        '''Represents a parameter that has the incorrect type'''
        nspace: str
        command: str
        param_set: Dict
        violations: List[Command.ParamRuleViolation]

    def extract_parameters(self, settings_pool, *, strict=True):
        param_sets = []

        for s in self.steps:
            param_set = {}
            for param in s.command.parameters:
                value = settings_pool.get_parameter(s.namespace, s.command.id, param.id, self.__contexts)
                if strict and (value is None):
                    raise RuntimeError(f"Could not find parameter '{param.id}' for command '{s.command.id}'"
                                       f" (namespace '{s.namespace}') in following contexts: "
                                       f"{', '.join(self.__contexts)}")
                elif value is not None:
                    # TODO: Check type, and check value vs rule, if strict = True
                    param_set[param.id] = value

            param_sets.append(param_set)
        return param_sets

    def check_for_missing_parameters(self, param_sets):
        missing_params = []

        for param_set, step in zip(param_sets, self.steps):
            for param in step.command.parameters:
                if param.id not in param_set:
                    missing_params.append(Transition.MissingParam(step.namespace, step.command.id, param.id))

        return missing_params

    def check_for_invalid_parameters(self, param_sets):
        type_mismatches = []
        rule_violations = []

        for param_set, step in zip(param_sets, self.steps):
            violations_in_this_step = []
            for param in step.command.parameters:
                if param.id in param_set:
                    value = param_set[param.id]
                    if not param.check_type(value):
                        type_mismatches.append(Transition.ParamTypeMismatch(step.namespace,
                                                                            step.command.id,
                                                                            param.id,
                                                                            param.type,
                                                                            type(value).__name__))
                    elif (type(value) is File) and not value.exists():
                        violations_in_this_step.append(Command.ParamRuleViolation(param.id, 'File does not exist'))
                    elif not param.check_value(value):
                        rule_description = param.rule.description if param.rule is not None else ''
                        violations_in_this_step.append(Command.ParamRuleViolation(param.id, rule_description))

            if len(violations_in_this_step) > 0:
                rule_violations.append(Transition.ParamRuleViolation(step.namespace,
                                                                     step.command.id,
                                                                     param_set,
                                                                     violations_in_this_step))

        return type_mismatches, rule_violations

    async def async_run(self, parameters, lease, *, poll_callback=None):
        with EventTimer(f'{self.actionable.path}: {self.id}') as timer:
            _LOGGER.info('%s: Running %s (transition from %s to %s)',
                         self.actionable.path, self.id, self.start_state, self.end_state)

            if isinstance(parameters, SettingsPool):
                parameters = self.extract_parameters(parameters, strict=True)

            with timer.pause():
                r = await self.__actionable._client.put(f'/fsms/{self.__actionable.id}/{self.__fsm_id}/'
                                                        f'{self.__start_state}/{self.id}',
                                                        json={
                                                            'lease': lease._token,
                                                            'parameters': serialize_parameter_set_list(parameters)})
                if r.status_code != 200:
                    raise RuntimeError(f"Control app reported an error when running transition '{self.id}' " +
                                       f"(device '{self.__actionable.path}'): {r.text}")

                snapshot = Transition.parse_snapshot(json.loads(r.text), self.actionable.path, parameters)
                while not snapshot.is_finished():
                    await asyncio.sleep(0.01)
                    r = await self.__actionable._client.get(f'/fsms/{self.__actionable.id}/{self.__fsm_id}/'
                                                            f'{self.__start_state}/{self.id}')
                    if r.status_code != 200:
                        raise RuntimeError("Control app reported an error when checking status of transition "
                                           f"'{self.id}' (device '{self.__actionable.path}', FSM '{self.__fsm_id}', "
                                           f"state '{self.__start_state}'): {r.text}")

                    snapshot = Transition.parse_snapshot(json.loads(r.text), self.actionable.path, parameters)
                    if poll_callback is not None:
                        poll_callback(snapshot)

            _LOGGER.info('%s: Completed %s with status %s', self.actionable.path, self.id, snapshot.state.name)
            if snapshot.state is Transition.State.ERROR:
                self.actionable.currentState = self.fsm.error_state
            else:
                self.actionable.currentState = self.end_state

        add_external_event(f'Transition: {self.id}', snapshot.start_time, snapshot.duration,
                           thread_name=self.actionable.path)
        for cmd_snapshot in snapshot.commands:
            add_external_event(cmd_snapshot.id, cmd_snapshot.start_time, cmd_snapshot.duration,
                               thread_name=self.actionable.path)
        return snapshot

    def run(self, parameters, lease, *, poll_callback=None):
        return _RUNNER.run(self.async_run(parameters, lease, poll_callback=poll_callback))


async def _async_run(transitions, parameters, leases, *, poll_callback, refresh):
    _LOGGER.info('Running %s transitions in parallel', len(transitions))
    with EventTimer('System transition'):
        if isinstance(parameters, SettingsPool):
            parameters = [parameters for _ in range(len(transitions))]

        snapshots = [None for _ in range(len(transitions))]
        refresh_durations = [None for _ in range(len(transitions))]
        coro_objs = []
        for i, (t, p) in enumerate(zip(transitions, parameters)):
            # 'create' function is required to bind the value of i from the current loop iteration
            def create_callback(i):
                def transition_callback(snapshot):
                    snapshots[i] = snapshot
                    poll_callback(snapshots, refresh_durations)
                return transition_callback
            coro_objs.append(t.async_run(p, leases[t.actionable], poll_callback=create_callback(i)))

        snapshots = await asyncio.gather(*coro_objs)

    if refresh:
        _LOGGER.info('All %s transitions have now finished. Now refreshing monitoring data', len(transitions))

        with EventTimer('System mon update'):
            coro_objs = []
            for t in transitions:
                coro_objs.append(t.actionable.async_refresh())
            poll_callback(snapshots, refresh_durations)
            refresh_durations = await asyncio.gather(*coro_objs)
            _LOGGER.info('Finished refreshing monitoring data on all %s actionables', len(transitions))
    else:
        _LOGGER.info('All %s transitions have now finished', len(transitions))

    return snapshots, refresh_durations


def run(transitions, parameters, leases, *, poll_callback=None, refresh=False):
    return _RUNNER.run(_async_run(transitions, parameters, leases, poll_callback=poll_callback, refresh=refresh))


class FSM:

    def __init__(self, fsm_id, device, alias, initial_state, error_state, states):
        self.id = fsm_id
        self.__actionable = device
        self.alias = alias
        self.initial_state = initial_state
        self.error_state = error_state
        self.states = states

    @property
    def actionable(self):
        return self.__actionable

    def state(self, state_id):
        return next((x for x in self.states if x.id == state_id), None)

    def engage(self, lease):
        _LOGGER.info('%s: Engaging FSM %s', self.actionable.path, self.id)
        r = self.__actionable._sync_client.put(f'/fsms/{self.__actionable.id}/{self.id}/engage',
                                               json={'lease': lease._token})
        if r.status_code != 200:
            raise RuntimeError(f"Control app reported an error when engaging FSM '{self.id}' " +
                               f"(device '{self.__actionable.path}'): {r.text}")
        self.actionable.currentFSM = self.id
        self.actionable.currentState = self.initial_state

    def reset(self, lease):
        _LOGGER.info('%s: Resetting FSM %s', self.actionable.path, self.id)
        r = self.__actionable._sync_client.put(f'/fsms/{self.__actionable.id}/{self.id}/reset',
                                               json={'lease': lease._token})
        if r.status_code != 200:
            raise RuntimeError(f"Control app reported an error when resetting FSM '{self.id}' " +
                               f"(device '{self.__actionable.path}'): {r.text}")
        self.actionable.currentState = self.initial_state

    def disengage(self, lease):
        _LOGGER.info('%s: Disengaging FSM %s', self.actionable.path, self.id)
        r = self.__actionable._sync_client.put(f'/fsms/{self.__actionable.id}/{self.id}/disengage',
                                               json={'lease': lease._token})
        if r.status_code != 200:
            raise RuntimeError(f"Control app reported an error when disengaging FSM '{self.id}' " +
                               f"(device '{self.__actionable.path}'): {r.text}")
        self.actionable.currentFSM = None
        self.actionable.currentState = None


class ActionableObject(PropertiedMonitorableObject):

    def __init__(self, obj_id, parent, *, raw_info, raw_action_info, contexts, raw_mon_data,
                 custom_monitorables):
        super().__init__(obj_id=obj_id,
                         parent=parent,
                         raw_info=raw_info,
                         raw_mon_data=raw_mon_data,
                         custom_monitorables=custom_monitorables)
        self.role = raw_info['role']
        self.type = raw_info['type']
        self.uri = raw_info['uri']
        self.addressTable = raw_info['addressTable']
        self.currentFSM = raw_info['fsm']
        self.currentState = raw_info['state']
        self.runningActions = raw_info['runningActions']
        self.contexts = contexts
        self._is_included = True

        # Commands
        self.commands = []
        for command_id, command_spec in raw_action_info[0][self.id].items():
            params = []
            for param_id, param_spec in command_spec['parameters'].items():
                params.append(Parameter(param_id,
                                        param_spec['type'],
                                        param_spec['description'],
                                        create_rule(param_spec['rule'])))

            constraints = []
            for constraint_name, (param_ids, description) in command_spec['constraints'].items():
                constraints.append(Constraint(constraint_name, description, param_ids))

            self.commands.append(Command(command_id,
                                         self,
                                         alias=command_spec['alias'],
                                         description=command_spec['description'],
                                         parameters=params,
                                         constraints=constraints,
                                         contexts=self.contexts))

        # FSMs
        self.fsms = []
        for fsm_id, fsm_spec in raw_action_info[1][obj_id].items():
            states = []
            for s, transition_map in fsm_spec['states'].items():
                transitions = []
                for transition_id, transition_spec in transition_map.items():
                    commands = [Transition.Step(transition_id if len(ns) == 0 else ns, self.command(cmd_id))
                                for cmd_id, ns in transition_spec['commands']]
                    transitions.append(Transition(transition_id,
                                                  transition_spec['alias'],
                                                  self,
                                                  fsm_id,
                                                  s,
                                                  transition_spec['endState'],
                                                  commands,
                                                  self.contexts))
                states.append(State(self.path, fsm_id, s, transitions))
            self.fsms.append(FSM(fsm_id,
                                 self,
                                 fsm_spec['alias'],
                                 fsm_spec['initialState'],
                                 fsm_spec['errorState'],
                                 states))

    def command(self, cmd_id):
        c = next((x for x in self.commands if x.id == cmd_id), None)
        if c is None:
            raise RuntimeError(f"Device '{self.path}' does not have any command named '{cmd_id}'")
        return c

    def fsm(self, fsm_id):
        f = next((x for x in self.fsms if x.id == fsm_id), None)
        if f is None:
            raise RuntimeError(f"Device '{self.path}' does not have any FSM named '{fsm_id}'")
        return f

    def has_command(self, cmd_id):
        for cmd in self.commands:
            if cmd.id == cmd_id:
                return True
        return False

    def has_fsm(self, fsm_id):
        for fsm in self.fsms:
            if fsm.id == fsm_id:
                return True
        return False

    @property
    def is_included(self):
        '''Whether this actionable should be included in system-level actions'''
        return self._is_included

    def exclude(self):
        '''Declare that this actionable should be excluded from system-level actions'''
        self._is_included = False

    def include(self):
        '''Declare that this actionable should be included in system-level actions'''
        self._is_included = True
