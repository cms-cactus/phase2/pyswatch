"""
    This module contains classes used to describe the auxillary componenents
    outside the core Herd-controlled system. This includes the classes needed for
    links to the DAQ system and the readout system.
"""


class PeripheralOutputPort(object):
    def __init__(self, port_id, parent_path, index):
        self.__id = port_id
        self.__parent_path = parent_path
        self.__index = index

    @property
    def id(self):
        return self.__id

    @property
    def alias(self):
        return self.__id

    @property
    def path(self):
        return self.__parent_path + '.' + self.id

    @property
    def index(self):
        return self.__index

    @property
    def is_masked(self):
        return False


class PeripheralInputPort(object):
    def __init__(self, port_id, parent_path, index):
        self.__id = port_id
        self.__parent_path = parent_path
        self.__index = index

    @property
    def id(self):
        return self.__id

    @property
    def alias(self):
        return self.__id

    @property
    def path(self):
        return self.__parent_path + '.' + self.id

    @property
    def index(self):
        return self.__index

    @property
    def is_masked(self):
        return False


class PeripheralProcessor(object):
    def __init__(self, obj_id, parent_path, inputs, outputs):
        self.__id = obj_id
        self.__parent_path = parent_path
        self._is_included = True

        self.__inputs = {}
        for x in inputs:
            self.__inputs[x] = PeripheralInputPort(f'Rx{x:02d}', self.path, x)
        self.__outputs = {}
        for x in outputs:
            self.__outputs[x] = PeripheralOutputPort(f'Tx{x:02d}', self.path, x)

    @property
    def id(self):
        return self.__id

    @property
    def alias(self):
        return self.__id

    @property
    def path(self):
        return self.__parent_path + '.' + self.id

    @property
    def is_included(self):
        return self._is_included

    def exclude(self):
        self._is_included = False

    def include(self):
        self._is_included = True

    @property
    def inputs(self):
        return self.__inputs.values()

    def input(self, i):
        if i not in self.__inputs:
            raise RuntimeError(f'Processor "{self.path}" does not contain an input port with index {i}')
        return self.__inputs[i]

    @property
    def outputs(self):
        return self.__outputs.values()

    def output(self, i):
        if i not in self.__outputs:
            raise RuntimeError(f'Processor "{self.path}" does not contain an output port with index {i}')
        return self.__outputs[i]


class PeripheralBaseModule(object):
    def __init__(self, stub):
        self.__stub = stub
        self._processors = {}

    @property
    def id(self):
        return self.__stub.id

    @property
    def alias(self):
        return self.id

    @property
    def path(self):
        return self.id

    @property
    def processors(self):
        return self._processors.values()

    def processor(self, proc_id):
        return self._processors[proc_id]


class DAQBoard(PeripheralBaseModule):
    def __init__(self, stub):
        self.__stub = stub
        super().__init__(stub)
        proc_ids = set(list(stub.inputs.keys()) + list(stub.outputs.keys()))
        for x in proc_ids:
            self._processors[x] = PeripheralProcessor(x, self.path, stub.inputs.get(x, []), stub.outputs.get(x, []))

    @property
    def category(self):
        return self.__stub.category

    def processor(self, proc_id):
        if proc_id not in self._processors:
            raise RuntimeError(f'DAQBoard "{self.id}" does not contain any processor with ID "{proc_id}"')
        return self._processors[proc_id]


class LpGBT(PeripheralProcessor):
    def __init__(self, obj_id, parent_path, fuseid):
        self._fuseid = fuseid
        self._obj_id = obj_id
        super().__init__(obj_id, parent_path, [0], [0])

    @property
    def fuseid(self):
        return self._fuseid


class FrontendModule(PeripheralBaseModule):
    def __init__(self, stub):
        self.__stub = stub
        super().__init__(stub)
        for i, x in enumerate(stub.lpgbt_fuseids):
            self._processors[f'lpGBT{i:02d}'] = LpGBT(f'lpGBT{i:02d}', self.path, x)

    @property
    def serial(self):
        return self.__stub.serial

    @property
    def lpgbts(self):
        return self._processors.values()

    def processor(self, proc_id):
        if proc_id not in self._processors:
            raise RuntimeError(f'FrontendModule "{self.id}" does not contain any processor with ID "{proc_id}"')
        return self._processors[proc_id]

    def lpgbt(self, proc_id):
        return self.processor(proc_id)
