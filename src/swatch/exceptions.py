from collections.abc import Sequence
from pydantic import ValidationError


class InvalidArgumentError(Exception):
    """Raised when an invalid argument is passed to a function"""


class InvalidConfigFormatError(Exception):
    """Raised when format of config data does not match specification"""


class DuplicateEntryError(Exception):
    """Raised when an entry is added to a collection that already exists"""

# Custom error message for invalid inputs to the system config file


class ConfigValidationError(Exception):

    def __init__(self, errors: ValidationError):
        self.custom_message = self._build_custom_message(errors)

    def _generate_msg(self, message):
        if message == 'Field required':
            return "Missing required field in config file"
        elif message.startswith("Value error, "):
            return message.replace("Value error, ", "Value Error.\n")
        else:
            return message

    def _loc_string(self, loc):
        return ".".join(loc) if isinstance(loc, Sequence) else loc

    def _build_custom_message(self, errors: ValidationError):
        messages = []
        for error in errors.errors():
            loc = error['loc']
            field = self._loc_string(loc) if loc else "unknown field"
            message = error['msg']
            custom_msg = self._generate_msg(message)
            messages.append(f"{custom_msg}: '{field}'")
        return "\n".join(messages)

    def __str__(self):
        return self.custom_message


class SystemLinkError(Exception):
    """Raised when a system link is invalid"""
