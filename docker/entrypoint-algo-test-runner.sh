#!/bin/bash

# Change directory to ${WORKDIR} if defined
if [ -n "${WORKDIR}" ]; then
  if [ ! -e "${WORKDIR}" ]; then
    echo "ERROR: Specified working directory, \"${WORKDIR}\", does not exist"
    exit 1
  elif [ ! -d "${WORKDIR}" ]; then
    echo "ERROR: Specified working directory, \"${WORKDIR}\", is not a directory"
    exit 1
  fi

  echo "Changing working directory to: ${WORKDIR}"
  cd ${WORKDIR}
fi

run-algo-test "$@"

