
FROM centos:7 as base

LABEL author "Tom Williams <tom.williams@cern.ch>"

ARG TARGETPLATFORM


# 1) Install RPMs
RUN \
  if [ "${TARGETPLATFORM}" == "linux/arm/v7" ]; then \
    echo "armhfp" > /etc/yum/vars/basearch; \
    echo "armv7hl" > /etc/yum/vars/arch; \
    echo "armv7hl-redhat-linux-gpu" > /etc/rpm/platform; \
    echo -e "[epel]\nname=Epel rebuild for armhfp\nbaseurl=https://armv7.dev.centos.org/repodir/epel-pass-1/\nenabled=1\ngpgcheck=0" > /etc/yum.repos.d/epel.repo; \
  else \
    yum -y install epel-release; \
  fi && \
  yum -y install python36 python3-pip python36-numpy && \
  yum clean all


# 2) Copy over script and install Python packages
COPY . /pyswatch
RUN \
  yum -y install gcc python36-devel && \
  python3.6 -m pip install -r /pyswatch/requirements.txt && \
  python3.6 -m pip install /pyswatch

# Set locale for click (see https://click.palletsprojects.com/en/7.x/python3/)
ENV LC_ALL=en_US.utf8
ENV LANG=en_US.utf8


# Image A: board-manager console
FROM base as board-manager

ENTRYPOINT ["board-manager"]
CMD ["console"]


# Image B: Single-board algo test runner
FROM base as algo-test-runner

ENTRYPOINT ["run-algo-test"]
CMD []


# Image C: Multi-board link test runner
FROM base as link-test-runner

ENTRYPOINT ["run-link-test"]
CMD []


# Image D: Multi-board slice test runner
FROM base as slice-test-runner

ENTRYPOINT ["run-slice-test"]
CMD []

# Image E: Plugin test suite
FROM base as plugin-test-suite

ENTRYPOINT ["run-plugin-test-suite"]
CMD []
