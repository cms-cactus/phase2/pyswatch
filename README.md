# PySWATCH

PySWATCH is a Python library designed for controlling electronics in the phase-2 upgrade of CMS,
based on the concepts from the SWATCH C++ library developed for phase-1, and the HERD network
API. It has been designed to hide all details of the network communication with boards, so that
scripts and applications for controlling and monitoring multiple boards can be developed without
any knowledge of the underlying network protocols and encodings (i.e. by only knowing the boards'
hostnames or IP addresses).

PySWATCH includes a few command-line tools for controlling and monitoring electronics:
 * A command-line console that allows one to run specific commands/transitions
   (useful in particular when developing plugins)

The library is packaged in docker images (one for each of the main tools listed above), available
in the GitLab container registry. To ensure indentical behaviour whereever the software is run,
whenever possible users should run the tools using those container images, by following the
instructions at these links:

 * Console: https://cms-l1t-phase2.docs.cern.ch/online-sw/user-guide/board-manager.html
 * FSM runner scripts: https://cms-l1t-phase2.docs.cern.ch/online-sw/user-guide/fsm-runner-scripts/index.html

In contrast with the above user guide, this README is mainly aimed at developers of this 
repository.

*Note: This library is under development, and so the API may change significantly at any point.*


## Implementation

This library requires version 3.6 of Python or higher, along with the following packages from PyPI:
 * `dataclasses`
 * `prompt_toolkit`
 * `requests`
 * `rich`

The versions of these dependencies that the library is tested against are listed in
`requirements.txt`, and the dependencies can be installed by running `python -m pip install -r requirements.txt`


# Usage

Setup:
```bash
python3 -m pip install -r requirements.txt
python3 -m pip install -e .
```

## Board manager CLI

The `board-manager` is a command-line tool that allows you to remotely control and monitor one
or HERD-based boards. Its most frequently used subcommands are described in the [user guide](https://cms-l1t-phase2.docs.cern.ch/online-sw/user-guide/board-manager.html);
if you're using a locally cloned copy of this repository, simply replace ``docker run .. CONTAINER_URL``
with ``board-manager``  


## Single-board tests

To run single-board demonstrator tests (i.e. loading input data onto buffers, playing that data through the algorithm firmware, and downloading the capture output signals from output buffers):
```bash
run-algo-test BOARD_HOSTNAME DEVICE FW_BITFILE_PACKAGE INPUT_FILE OUTPUT_FILE
```

For example, for the `x1` FPGA on a board with IP address `192.168.100.54`:
```bash
run-algo-test 192.168.100.54 x1 my_bitfile.tgz inputs.txt outputs.txt
```
